# LICENSE

This project **be.seeld.seeld2-server** is licensed under a **GNU General Public License 3 (GPL-3.0)** (see https://opensource.org/licenses/GPL-3.0 for details).

## Dependencies to other licenses

* Spring Boot 2.2.5: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* Spring Framework 5.2.4: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* io.jsonwebtoken.jjwt 0.9.1: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* org.bitbucket.simon_massey.thinbus-srp6a-js 1.6.2: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* org.bouncycastle.bcpg-jdk15on & org.bouncycastle.bcproc-jdk15on: 
  * Apache Software License, Version 1.1 - http://www.apache.org/licenses/LICENSE-1.1
  * Bouncy Castle Licence - http://www.bouncycastle.org/licence.html
* name.neuhalfen.projects.crypto.bouncycastle.openpgp.bounct-gpg: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* Apache Ignite 2.10.0: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* com.h2database.h2 1.4.197: MPL 2.0 or EPL 1.0 - http://h2database.com/html/license.html
* Apache Commons Lang 3.9: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* Lombok 1.18.12: MIT License - https://projectlombok.org/LICENSE
* uk.com.robust-it.cloning 1.9.12: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* EasyMock 4.2: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
* AssertJ 2.2.7: Apache License, Version 2.0 - https://www.apache.org/licenses/LICENSE-2.0
