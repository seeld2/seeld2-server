# seeld2-server

This module implements the whole server back end of Seeld.

# Release notes

## 2.20.0

* Update the version to a "production ready" version

## 0.19.0

* Implement new API end-points to serve the client's new approach in syncing conversations and messages

## 0.18.0

* Fix log4j vulnerability (log4shell) by upgrading it, even though the backend was not supposedly affected 

## 0.16.0

* Remove code for management of Message Life: this is now created and handled in seeld2-shredder

### 0.15.1

* Attempt fixing "split-brain" issue by adapting various timeout parameters

## 0.15.0

* Adjust vbox and production application properties
* Various minor cleanups

### 0.12.2

* Upgrade Ignite to 2.10.0

## 0.12.0

* Adapt Ignite configuration to be able to work with a cluster of Ignite nodes
* Enable SSL communication between Ignite nodes using JKS stores

### 0.11.2

* Update Ignite to 2.9.1
* Implement APIs for conversation and message deletion

### 0.11.1

* Improve defense against pseudo harvesting by adding wait time before returning if a pseudo exists or not

## 0.11.0

* Adapt messages, events and profiles to work with the new API which uses the new security field
* Add access field to messages and events' tables
* Fix bad PongResponse
* Significant internal package refactoring

## 0.10.0

* Add profile deletion menu option
* Improve security on some endpoints by using AES-encrypted username at WepApi query 
* Implement database exporter to facilitate tests

### 0.9.1

* Handle multiple devices (max 3) for real-time notifications
* Add API endpoint to provide server time in milliseconds since epoch

## 0.9.0

* Add pagination on REST API for conversations and messages
* Remove obsolete REST endpoints
* Restore authentication on most endpoints to avoid harvesting of encrypted messages

## 0.8.0

* Add case-insensitivity to usernames, at login and registration
* Add REST API for AndroidNotifications work
* Separate REST API's allowed origins (CORS) from WebSocket's

### 0.7.3

* Update allowed origins to include http://localhost for mobile app's WebView >= 4

### 0.7.2

* Adapt application-local.properties allowedOrigins
* Add WebSocketHandshakeTracer for server debugging of origins
* Add WebCallTracer interceptor to debug origins of calls

### 0.7.1

* Filter allowed origins for WebSockets

## 0.7.0

* Add WebSocket support for "new message" notifications
* Change REST API /api/registrations/{username} to /api/registrations/{username}/availability
* Update Spring Boot to 2.2.5.RELEASE 
* Remove useless dependency to tomcat server and jdbc
* Remove usage of cookies
* Remove CSRF filter and related headers, since cookies usage has been completely removed

## 0.5.0

* Fix bug in MessageLife that would prevent creating all the expected entries at each message exchanged
* Modify logs path to /var/log/seeld/ for conformity

## 0.4.0 (first public beta)

* Implement registration limitation to prevent server overload 
* Add messages' API to fetch a conversation's messages' IDs and to fetch a batch of conversation messages by a list of IDs
* Create "message life" records for exchanged messages

### 0.3.2

* Modify API to fetch a conversation's messages after a given time 

## 0.3.0

* Adopt secure remote password (SRP) for authentication, so that the user password is never sent over the web! 
* Device storage key, exchange key and user box address are now securely generated on the server side and sent back to client at login, encrypted using SRP's session key
* SRP's M2, JWT and user profile payload are sent at login, along with the user's profile's credentials (which are encrypted using SRP's session key)
* Sensitive data between client and server is now AES-encrypted using the exchange key
* Credentials for resuming a session are now encrypted with the device storage key and stored in session storage for browsers (for security reasons, they are persisted only as far as the tab is open)
* Change the remotely stored (and PGP-encrypted) contacts' structure in the user profile's encrypted payload, to prepare for multiple key types in the future
* Likewise, change the way keys are stored in the encrypted user profile's payload so that multiple types of keys can eventually be stored
* Switch completely to ECC keys for all PGP encrypting work (for increased speed and security)
* Contact requests to other persons, as well as all user events, are fully PGP-encrypted and encrypted with the exchange key before being sent to the server
* The user events' "return payload" is encrypted with the PGP sender's key, so that the system cannot access its data
* The user events' "return box address" is AES-encrypted by the server, for added security in case of DB leaks 

### 0.1.5

* Add application configuration to destroy the PGP key rings' cache at startup

### 0.1.4

* Add API to fetch the user's exchange key
* Disable PGP key regeneration for the moment being...
* Various refactorings

### 0.1.2

* Upgrade to Ignite 2.7
* Remove READ, READ_ONCE and VALID_UNTIL notions from Message: it's over-engineering, and it's not actually needed
* Disable CORS mapping on "/login", which is now "/api/login"
* Implement API to enable conversations' synchronization with clients
* Reinforce security by encryption of certain DB fields that could provide sensitive metadata in case of intrusion
* Various bug fixes


## 0.1.0

First production release

