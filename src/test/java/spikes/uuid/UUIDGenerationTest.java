package spikes.uuid;

import org.junit.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

public class UUIDGenerationTest {

    @Test
    public void generateAndDisplayUUIDs() {
        UUID[] uuids = new UUID[10];
        for (int i = 0; i < 10; i++) {
            uuids[i] = UUID.randomUUID();
        }

        assertThat(uuids).containsOnlyOnce(uuids);

        Arrays.stream(uuids).forEach(System.out::println);
    }
}
