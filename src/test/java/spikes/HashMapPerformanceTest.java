package spikes;

import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.UUID;

public class HashMapPerformanceTest {

    @Test
    public void addKeys() {

        int keysAmount = 8_000_000;
        int memConsumptionSampleSize = 100;

        Map<UUID, String> map = new HashMap<>();

        System.out.println("Adding entries");

        long memAtStart = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

        List<Long> executionTimes = new ArrayList<>(keysAmount / memConsumptionSampleSize);

        long timeAtStart = System.currentTimeMillis();
        for (int i = 0; i < keysAmount; i++) {
            map.put(UUID.randomUUID(), RandomString.make(8));

            if (i % memConsumptionSampleSize == 0) {
                long timeAtEnd = System.currentTimeMillis();
                executionTimes.add(timeAtEnd - timeAtStart);
                timeAtStart = timeAtEnd;
            }
        }
        long memAtEnd = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

        long memConsumptionInMb = (memAtEnd - memAtStart) / (1024L * 1024L);
        System.out.println("Memory consumption: " + memConsumptionInMb);

        OptionalDouble average = executionTimes.stream().mapToDouble(value -> value).average();
        System.out.println("Average speed in millis: " + average.orElse(0.0d));

        System.out.println("Accessing entries");
    }
}
