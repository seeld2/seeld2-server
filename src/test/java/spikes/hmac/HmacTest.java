package spikes.hmac;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Arrays;
import java.util.Base64;

import static org.junit.Assert.assertEquals;

public class HmacTest {

    private static final String SECRET_KEY = "ddZQSrmcajDIMYCjueZcfAnxgWvTNjIcasqcdSZZjMLUPNmOfDZuHPpuQwGjULUd";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    public void hashAMessage() {
        try {
            byte[] encodedSecretKey = SECRET_KEY.getBytes(StandardCharsets.UTF_8);
            System.out.println("Encoded secret key: " + Arrays.toString(encodedSecretKey));
            SecretKeySpec secretKeySpec = new SecretKeySpec(encodedSecretKey, "HmacSHA512");

            Mac hmac = Mac.getInstance("HmacSHA512");
            hmac.init(secretKeySpec);

            byte[] encodedData = "This is a test".getBytes(StandardCharsets.UTF_8);
            System.out.println("Encoded data: " + Arrays.toString(encodedData));

            byte[] encodedHashedData = hmac.doFinal(encodedData);
            System.out.println("Encoded hashed data: " + Arrays.toString(encodedHashedData));

            String base64Hash = Base64.getUrlEncoder().encodeToString(encodedHashedData);
            assertEquals("28pSdRrwRtrmjnuF9ELs5YuC1SchR15Kd4og-_usJwhSx-PmronKHRqQ3ka2G-oI28yZSQ7d-2PE-BpZgHCeAg==", base64Hash);

            int[] unsignedEncodedHashedData = new int[encodedHashedData.length];
            for (int i = 0; i < encodedHashedData.length; i++) {
                unsignedEncodedHashedData[i] = Byte.toUnsignedInt(encodedHashedData[i]);
            }
            System.out.println(Arrays.toString(unsignedEncodedHashedData));

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }
}
