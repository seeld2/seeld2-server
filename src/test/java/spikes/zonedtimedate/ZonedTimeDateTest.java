package spikes.zonedtimedate;

import org.assertj.core.data.Offset;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ZonedTimeDateTest {

    @Test
    public void listAllTheAvailableZoneIds() {
        String allAvailableZoneIds = ZoneId.getAvailableZoneIds().stream().sorted().collect(Collectors.joining(", "));
        System.out.println(allAvailableZoneIds);
    }

    @Test
    public void listEuropeZoneIds() {
        String europeZoneIds = ZoneId.getAvailableZoneIds().stream()
                .filter(zoneId -> zoneId.toLowerCase().startsWith("europe"))
                .sorted()
                .collect(Collectors.joining(", "));
        System.out.println(europeZoneIds);
    }

    @Test
    public void playWithTimeZones() {

        ZoneId zoneId = ZoneId.systemDefault();
        System.out.println(zoneId + ", " + zoneId.getRules().getOffset(Instant.now()));
        System.out.println(LocalDateTime.now(zoneId));
        System.out.println(ZonedDateTime.now(zoneId));

        ZoneId utcZoneId = ZoneId.of("UTC");
        System.out.println(utcZoneId + ", " + utcZoneId.getRules().getOffset(Instant.now()));
        System.out.println(LocalDateTime.now(utcZoneId));
        System.out.println(ZonedDateTime.now(utcZoneId));

        System.out.println(ZonedDateTime.now(zoneId).getHour() - ZonedDateTime.now(utcZoneId).getHour());

        System.out.println(ZonedDateTime.now().withZoneSameInstant(utcZoneId));

        System.out.println(Instant.now().toEpochMilli());
        System.out.println(ZonedDateTime.now(zoneId).toInstant().toEpochMilli());
        System.out.println(ZonedDateTime.now(utcZoneId).toInstant().toEpochMilli());
    }

    @Test
    public void equivalentWays() {

        long one = ZonedDateTime.now(Clock.systemUTC()).toInstant().toEpochMilli();
        long two = ZonedDateTime.now(ZoneId.of("UTC")).toInstant().toEpochMilli();

        assertThat(one).isCloseTo(two, Offset.offset(10000L));
    }
}
