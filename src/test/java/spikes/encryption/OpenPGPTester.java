package spikes.encryption;

import be.seeld.common.encryption.openpgp.OpenPGP;
import be.seeld.common.encryption.openpgp.PGPUserId;

public class OpenPGPTester {

    private final int keySize;
    private final String userIdEmailSuffix;

    public OpenPGPTester(int keySize, String userIdEmailSuffix) {
        this.keySize = keySize;
        this.userIdEmailSuffix = userIdEmailSuffix;
    }

    public Recipient generate(String username, String passphrase) {

        OpenPGP.Config config = OpenPGP.Config.create(
                username,
                userIdEmailSuffix,
                passphrase,
                keySize);
        OpenPGP openPGP = OpenPGP.create(config);

        openPGP.generateNewKeys();

        return new Recipient(username, passphrase, openPGP.getArmoredPublicKeyRing(), openPGP.getArmoredSecretKeyRing());
    }

    /**
     * Encrypts the specified message addressed to the specified recipient, using the specified sender's keys (the
     * sender is usually the logged user).
     *
     * @return The encrypted message.
     */
    public String encryptMessageToReceiver(Recipient sender, String messageToEncrypt, Recipient receiver) {

        OpenPGP.Config config = OpenPGP.Config.create(
                sender.username,
                userIdEmailSuffix,
                sender.passphrase,
                keySize,
                sender.armoredPublicKey,
                sender.armoredSecretKey);
        OpenPGP openPGP = OpenPGP.create(config);

        return openPGP.encryptAndSign(messageToEncrypt, receiver.armoredPublicKey, receiver.username).getPayload();
    }

    /**
     * Encrypts the specified message addressed to the system, by using the specified test user's keys.
     *
     * @return The encrypted message.
     */
    public String encryptMessageToSystem(Recipient sender, String messageToEncrypt) {
        PGPRecipient receiver = systemPgpRecipient();
        return encrypt(sender, messageToEncrypt, receiver.userId.getName(), receiver.armoredPublicKeyRing);
    }

    private String encrypt(Recipient sender, String messageToEncrypt,
                           String receiverUserIdName, String receiverArmoredPublicKey) {

        OpenPGP.Config config = OpenPGP.Config.create(
                sender.username,
                userIdEmailSuffix,
                sender.passphrase,
                keySize,
                sender.armoredPublicKey,
                sender.armoredSecretKey);

        OpenPGP userOpenPGP = OpenPGP.create(config);

        return userOpenPGP.encryptAndSign(messageToEncrypt, receiverArmoredPublicKey, receiverUserIdName).getPayload();
    }

    /**
     * Decrypts the specified encrypted message from the specified sender, using the specified recipient's keys (the
     * recipient is usually the logged user).
     *
     * @return The unencrypted message.
     */
    public String decryptMessageFromSender(Recipient receiver, String encryptedMessage, Recipient sender) {
        return decrypt(receiver, encryptedMessage, sender.username, sender.armoredPublicKey);
    }

    /**
     * Decrypts the specified encrypted message, encrypted by the system, using the specified recipient's keys.
     *
     * @return The unencrypted message.
     */
    public String decryptMessageFromSystem(Recipient receiver, String encryptedMessage) {
        PGPRecipient sender = systemPgpRecipient();
        return decrypt(receiver, encryptedMessage, sender.userId.getName(), sender.armoredPublicKeyRing);
    }

    private String decrypt(Recipient receiver, String messageToDecrypt,
                           String senderUserIdName, String senderArmoredPublicKey) {

        OpenPGP.Config config = OpenPGP.Config.create(
                receiver.username,
                userIdEmailSuffix,
                receiver.passphrase,
                keySize,
                receiver.armoredPublicKey,
                receiver.armoredSecretKey);

        OpenPGP userOpenPGP = OpenPGP.create(config);

        return userOpenPGP.decryptAndVerify(messageToDecrypt, senderArmoredPublicKey, senderUserIdName);
    }

    private PGPRecipient systemPgpRecipient() {
//        OpenPGP systemOpenPGP = (OpenPGP) ReflectionTestUtils.getField(distributedOpenPGP, "openPGP");
//        OpenPGP.Config systemConfig = (OpenPGP.Config) ReflectionTestUtils.getField(systemOpenPGP, "config");
//        PGPUserId systemUserId = (PGPUserId) ReflectionTestUtils.getField(systemConfig, "userId");
//        String armoredPublicKeyRing = distributedOpenPGP.armoredPublicKey();
//        return new PGPRecipient(armoredPublicKeyRing, systemUserId);
        return new PGPRecipient(null, null);
    }

    public static class Recipient {

        private String username;
        private String passphrase;
        private String armoredPublicKey;
        private String armoredSecretKey;

        public Recipient(String username, String armoredPublicKey) {
            this.username = username;
            this.armoredPublicKey = armoredPublicKey;
        }

        public Recipient(String username, String passphrase, String armoredPublicKey, String armoredSecretKey) {
            this.username = username;
            this.passphrase = passphrase;
            this.armoredPublicKey = armoredPublicKey;
            this.armoredSecretKey = armoredSecretKey;
        }
    }

    private static class PGPRecipient {

        private final String armoredPublicKeyRing;
        private final PGPUserId userId;

        private PGPRecipient(String armoredPublicKeyRing, PGPUserId userId) {
            this.armoredPublicKeyRing = armoredPublicKeyRing;
            this.userId = userId;
        }
    }
}
