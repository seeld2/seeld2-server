package spikes.passwordencoder;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;

/**
 * These are the results with 100 samples per strength value. It took 13 minutes to run the whole test...<br/>
 * Encoding password strength 4 => 1.22ms<br/>
 * Encoding password strength 5 => 2.13ms<br/>
 * Encoding password strength 6 => 3.91ms<br/>
 * Encoding password strength 7 => 7.71ms<br/>
 * Encoding password strength 8 => 15.35ms<br/>
 * Encoding password strength 9 => 30.5ms<br/>
 * Encoding password strength 10 => 60.87ms<br/>
 * Encoding password strength 11 => 121.65ms<br/>
 * Encoding password strength 12 => 243.87ms<br/>
 * Encoding password strength 13 => 486.65ms<br/>
 * Encoding password strength 14 => 973.04ms<br/>
 * Encoding password strength 15 => 1957.38ms<br/>
 * Encoding password strength 16 => 3905.62ms<br/>
 *<br/>
 * THEREFORE, A STRENGTH OF <strong>12</strong> LOOKS LIKE A GOOD COMPROMISE...<br/>
 *
 */
@RunWith(Parameterized.class)
public class PasswordEncoderTest {

    private static final int NUMBER_OF_SAMPLES = 10;

    @Parameterized.Parameters
    public static Collection<Object> data() {
        return Arrays.asList(new Object[] {4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
    }

    @Parameterized.Parameter
    public int strength;

    @Test
    public void encodePassword() throws NoSuchAlgorithmException {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(strength, SecureRandom.getInstanceStrong());

        int[] times = new int[NUMBER_OF_SAMPLES];
        for (int i = 0; i < times.length; i++) {
            long start = System.currentTimeMillis();
            encoder.encode(RandomStringUtils.random(30));
            long end = System.currentTimeMillis();
            times[i] = (int) (end - start);
        }

        double avg = Arrays.stream(times).average().orElse(0.0d);

        System.out.println("Encoding password strength " + strength + " => " + avg + "ms");
    }
}
