package test.spring;

import java.security.Principal;

public class PrincipalMock implements Principal {

    private final String name;

    private PrincipalMock(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static PrincipalMock of(String name) {
        return new PrincipalMock(name);
    }
}
