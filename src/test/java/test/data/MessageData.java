package test.data;

import be.seeld.conversations.messages.Message;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

import static be.seeld.conversations.messages.Message.COL_HMAC_HASH;
import static be.seeld.conversations.messages.Message.COL_BOX_ADDRESS;
import static be.seeld.conversations.messages.Message.COL_CONVERSATION_ID;
import static be.seeld.conversations.messages.Message.COL_MESSAGE_ID;
import static be.seeld.conversations.messages.Message.COL_NOTIFIED;
import static be.seeld.conversations.messages.Message.COL_PAYLOAD;
import static be.seeld.conversations.messages.Message.COL_TIMELINE;
import static be.seeld.conversations.messages.Message.TABLE;
import static be.seeld.conversations.messages.Message.columnsAsCsv;

public class MessageData {

    private final JdbcTemplate jdbcTemplate;

    public MessageData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Message find(UUID boxAddress, UUID messageId) {
        return DataAccessUtils.singleResult(jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_BOX_ADDRESS + " = ? and " + COL_MESSAGE_ID + " = ?",
                new Object[]{boxAddress.toString(), messageId.toString()},
                messageRowMapper()));
    }

    public List<Message> findAll() {
        return jdbcTemplate.query("select * from " + TABLE, messageRowMapper());
    }

    public List<Message> findAllWithBoxAddresses(UUID boxAddress) {
        return jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_BOX_ADDRESS + " = ?",
                new Object[]{boxAddress.toString()},
                messageRowMapper());
    }

    public List<Message> findAllWithConversationIds(UUID boxAddress) {
        return jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_CONVERSATION_ID + " = ?",
                new Object[]{boxAddress.toString()},
                messageRowMapper());
    }

    public Message findConversation(UUID conversationId) {
        return DataAccessUtils.singleResult(jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_CONVERSATION_ID + " = ?",
                new Object[]{conversationId.toString()},
                messageRowMapper()));
    }

    public Message insert(Message message) {
        jdbcTemplate.update(
                "insert into " + TABLE +
                        " (_key," + columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?, ?, ?, ?, ?)",
                message.cacheKey(),
                message.getBoxAddress(),
                message.getConversationId(),
                message.getMessageId(),
                message.isNotified(),
                message.getTimeline(),
                message.getPayload(),
                message.getHmacHash()
        );
        return message;
    }

    private RowMapper<Message> messageRowMapper() {
        return (rs, rowNum) -> Message.builder()
                .boxAddress(UUID.fromString(rs.getString(COL_BOX_ADDRESS)))
                .conversationId(UUID.fromString(rs.getString(COL_CONVERSATION_ID)))
                .messageId(UUID.fromString(rs.getString(COL_MESSAGE_ID)))
                .notified(rs.getBoolean(COL_NOTIFIED))
                .timeline(rs.getLong(COL_TIMELINE))
                .payload(rs.getString(COL_PAYLOAD))
                .hmacHash(rs.getString(COL_HMAC_HASH))
                .build();
    }
}
