package test.data;

import be.seeld.conversations.files.File;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.UUID;

import static be.seeld.conversations.files.File.*;

@SuppressWarnings("SqlDialectInspection")
public class FileData {

    private final JdbcTemplate jdbcTemplate;

    public FileData(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public boolean exists(UUID fileId) {
        return find(fileId) != null;
    }

    public File find(UUID fileId) {
        return DataAccessUtils.singleResult(jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_FILE_ID + " = ?",
                new Object[]{fileId.toString()},
                fileRowMapper()));
    }

    public File insert(File file) {
        jdbcTemplate.update("insert into " + TABLE +
                " (_key," + columnsAsCsv() + ")" +
                " values (?,?,?,?)",
                file.cacheKey(),
                file.getFileId(),
                file.getData(),
                file.getValidUntil());
        return file;
    }

    private RowMapper<File> fileRowMapper() {
        return (rs, rowNum) -> File.builder()
                .fileId(UUID.fromString(rs.getString(COL_FILE_ID)))
                .data(rs.getBytes(COL_DATA))
                .validUntil(rs.getLong(COL_VALID_UNTIL))
                .build();
    }
}
