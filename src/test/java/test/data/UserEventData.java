package test.data;

import be.seeld.userevents.UserEvent;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

import static be.seeld.userevents.UserEvent.*;

public class UserEventData {

    private final JdbcTemplate jdbcTemplate;

    public UserEventData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public boolean exists(UUID boxAddress, UUID eventId) {
        return find(boxAddress, eventId) != null;
    }

    public List<UserEvent> find(UUID boxAddress) {
        return jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_BOX_ADDRESS + " = ?",
                new Object[]{boxAddress.toString()},
                userEventRowMapper());
    }

    public UserEvent find(UUID boxAddress, UUID eventId) {
        return DataAccessUtils.singleResult(jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_BOX_ADDRESS + " = ? and " + COL_EVENT_ID + " = ?",
                new Object[]{boxAddress.toString(), eventId.toString()},
                userEventRowMapper()));
    }

    public UserEvent insert(UserEvent userEvent) {
        jdbcTemplate.update(
                "insert into " + TABLE +
                        " (_key," + columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?, ?, ?, ?, ?)",
                userEvent.cacheKey(),
                userEvent.getBoxAddress(),
                userEvent.getEventId(),
                userEvent.getPayload(),
                userEvent.getReturnBoxAddress(),
                userEvent.getReturnPayload(),
                userEvent.getValidUntil(),
                userEvent.getHmacHash());
        return userEvent;
    }

    private RowMapper<UserEvent> userEventRowMapper() {
        return (rs, rowNum) -> UserEvent.builder()
                .boxAddress(UUID.fromString(rs.getString(COL_BOX_ADDRESS)))
                .eventId(UUID.fromString(rs.getString(COL_EVENT_ID)))
                .payload(rs.getString(COL_PAYLOAD))
                .returnBoxAddress(rs.getString(COL_RETURN_BOX_ADDRESS))
                .returnPayload(rs.getString(COL_RETURN_PAYLOAD))
                .validUntil(rs.getLong(COL_VALID_UNTIL))
                .hmacHash(rs.getString(COL_HMAC_HASH))
                .build();
    }
}
