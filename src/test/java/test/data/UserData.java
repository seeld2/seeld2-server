package test.data;

import be.seeld.users.User;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import static be.seeld.users.User.COL_BOX_ADDRESS;
import static be.seeld.users.User.COL_CREDENTIALS;
import static be.seeld.users.User.COL_DEVICE_STORAGE_KEY;
import static be.seeld.users.User.COL_DISPLAY_NAME;
import static be.seeld.users.User.COL_EXCHANGE_KEY;
import static be.seeld.users.User.COL_PAYLOAD;
import static be.seeld.users.User.COL_PICTURE;
import static be.seeld.users.User.COL_SUBSCRIPTION_VALID_UNTIL;
import static be.seeld.users.User.COL_PSEUDO;
import static be.seeld.users.User.COL_SUBSCRIPTION_TYPE;
import static be.seeld.users.User.TABLE;
import static be.seeld.users.User.columnsAsCsv;

@SuppressWarnings("SqlDialectInspection")
public class UserData {

    private final JdbcTemplate jdbcTemplate;

    public UserData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void deleteAll() {
        jdbcTemplate.update("delete from " + TABLE);
    }

    public User find(String username) {
        return DataAccessUtils.singleResult(jdbcTemplate.query(
                "select * from User where " + COL_PSEUDO + " = ?",
                new Object[]{username},
                (rs, rowNum) -> User.builder()
                        .username(rs.getString(COL_PSEUDO))
                        .boxAddress(rs.getString(COL_BOX_ADDRESS))
                        .credentials(rs.getString(COL_CREDENTIALS))
                        .deviceStorageKey(rs.getString(COL_DEVICE_STORAGE_KEY))
                        .displayName(rs.getString(COL_DISPLAY_NAME))
                        .exchangeKey(rs.getString(COL_EXCHANGE_KEY))
                        .payload(rs.getString(COL_PAYLOAD))
                        .picture(rs.getString(COL_PICTURE))
                        .subscription(rs.getString(COL_SUBSCRIPTION_TYPE))
                        .subscriptionValidUntil(rs.getString(COL_SUBSCRIPTION_VALID_UNTIL))
                        .build()));
    }

    public User insert(User user) {
        jdbcTemplate.update(
                "insert into " + TABLE +
                        " (_key," + columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                user.getUsername(),
                user.getUsername(),
                user.getBoxAddress(),
                user.getCredentials(),
                user.getDeviceStorageKey(),
                user.getDisplayName(),
                user.getExchangeKey(),
                user.getPayload(),
                user.getPicture(),
                user.getSubscription(),
                user.getSubscriptionValidUntil());
        return user;
    }
}
