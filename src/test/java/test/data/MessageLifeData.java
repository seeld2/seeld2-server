package test.data;

import be.seeld.conversations.messages.life.MessageLife;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

import static be.seeld.conversations.messages.life.MessageLife.COL_BOX_ADDRESS;
import static be.seeld.conversations.messages.life.MessageLife.COL_CONVERSATION_ID;
import static be.seeld.conversations.messages.life.MessageLife.COL_MESSAGE_ID;
import static be.seeld.conversations.messages.life.MessageLife.COL_EXPIRES_ON;
import static be.seeld.conversations.messages.life.MessageLife.TABLE;

@SuppressWarnings("SqlDialectInspection")
public class MessageLifeData {

    private final JdbcTemplate jdbcTemplate;

    public MessageLifeData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<MessageLife> findAll() {
        return jdbcTemplate.query("select * from " + TABLE, messageLifeRowMapper());
    }

    public List<MessageLife> findAllByBoxAddress(UUID boxAddress) {
        return jdbcTemplate.query(
                "select * from " + TABLE + " where " + COL_BOX_ADDRESS + " = ?",
                new Object[] {boxAddress},
                messageLifeRowMapper());
    }

    public MessageLife insert(MessageLife messageLife) {
        jdbcTemplate.update("insert into " + TABLE +
                        " (_key," + MessageLife.columnsAsCsv() + ") values (?, ?, ?, ?, ?)",
                messageLife.cacheKey(),
                messageLife.getExpiresOn(),
                messageLife.getBoxAddress(),
                messageLife.getConversationId(),
                messageLife.getMessageId());
        return messageLife;
    }

    private RowMapper<MessageLife> messageLifeRowMapper() {
        return (rs, rowNum) -> MessageLife.builder()
                .expiresOn(rs.getLong(COL_EXPIRES_ON))
                .boxAddress(UUID.fromString(rs.getString(COL_BOX_ADDRESS)))
                .conversationId(UUID.fromString(rs.getString(COL_CONVERSATION_ID)))
                .messageId(UUID.fromString(rs.getString(COL_MESSAGE_ID)))
                .build();
    }
}
