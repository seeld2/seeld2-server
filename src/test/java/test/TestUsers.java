package test;

import be.seeld.users.Subscription;
import be.seeld.common.lang.Collections;
import lombok.ToString;
import lombok.Value;

import java.util.List;
import java.util.UUID;

public class TestUsers {

    public static final UUID ALICE_BOX_ADDRESS = UUID.fromString("ece54149-b23f-4976-8673-1162478b9635");
    public static final String ALICE_CREDENTIALS = "{\"kdf\":{\"type\":\"scrypt\",\"salt\":\"5410c256750558952982e28806a708a26788f834ec19804d21eed045bb822a17\"},\"srp\":{\"type\":\"srp6a\",\"salt\":\"ef2dd19880c0812c9d5842743b264b5ed0941d5b579e42328c86ea7388ef4f2f\",\"verifier\":\"50622a4cda976e77fb3f43e28d826677304a5d86017c79101e75320c5aca9e36449aa1401c1b320ec5f8bfa12aaf5cfdd0d32eb7ab97d039c73309e5bc074d26d70ffb932f8c3c84e4ac31485bcdc358867ba317683dce79c98992df382e18c38ef8ee449c12eeecd5981f9b847585e8cbd26e0a5fbc26f689955af48afdbe54fa78204d761769f7c941dcb8ec09a564c0e801201dd709aaf4af4328af948fec4a8472ecdfbf37fb8a45bf071c64796e6a833143b009d4666e7436256fb9af5b9aa87537c914ca11c64b93fd1fc7681ffaf5cde376d78e4f57be1f2112da6f8f33c7838bf2e2ed1dc94340dd450ad9246228ac7108c0331cfc3fa9a0e2fc\"},\"systemKeys\":{\"privateKeys\":\"alice system private keys\",\"publicKeys\":\"alice system public keys\",\"type\":\"ecc.x25519\"}}";
    public static final String ALICE_DEVICE_STORAGE_KEY = "JItVo4u5FKrdllFP_M5_Xzdlymgu5RYsz7oPkXJvUlk";
    public static final String ALICE_DISPLAY_NAME = "Alice";
    public static final String ALICE_EXCHANGE_KEY = "YmB5gbzrk-TS0dCSjZrF2zjjFsWHhAtu7pQ8bIH3iiY";
    public static final String ALICE_PASSPHRASE = "alice passphrase";
    public static final String ALICE_PAYLOAD = "alicePayload";
    public static final String ALICE_PICTURE = "alicePicture";
    public static final Subscription ALICE_SUBSCRIPTION = Subscription.FREE;
    public static final String ALICE_SYSTEM_PRIVATE_KEYS = "alice system private keys";
    public static final String ALICE_SYSTEM_PUBLIC_KEYS = "alice system public keys";
    public static final String ALICE_PSEUDO = "alice";

    public static final UUID BOB_BOX_ADDRESS = UUID.fromString("33bd1f50-7f89-4250-aeb5-e98445602073");
    public static final String BOB_CREDENTIALS = "{\"kdf\":{\"salt\":\"a2c2f18c33635fe67b72ad52325d50c5ffa249e6d8be363e98c36d1d6528ae21\"},\"srp\":{\"type\":\"srp6a\",\"salt\":\"b809970320d3fccc47cd53905a09297181766b21582040711365de6fceab7d77\",\"verifier\":\"114ef14bfd40383fcd7f03c246163b52dfff10452206a0b21fc07f37519fcf98fb60d7e2b121f1563c6c23cbb561c4aeeaf063f51ef9b3826dc93f79fcf81d322a30ddc4afcceeefa2fa7be089c37ec4234d12a541932739c2b7ec30879d35609ca1428edf620ed95a1634e8e04f6ddf8069ad85f6db3dcf7a44443ddae358d3eabe27f293c5cb176ebaa2c2621637a8ee52c48701731646265a89bff8ad0cb11459de84aa00a30c568e5aabbc4eef448f0ba368bf0ffec5e6dee79a9d962026ba047af3225f35913cb973a844fbda92ef953e0a67def0abb913a87fd2dc6b5d4c0aa97e95589527b25684d076a7036dda7bfb770a3c8aa8185ccb2db4f0\"},\"systemKeys\":{\"privateKeys\":\"bob system private keys\",\"publicKeys\":\"bob system public keys\",\"type\":\"ecc.x25519\"}}";
    public static final String BOB_DEVICE_STORAGE_KEY = "_xbRgwsi8xytSnrHnOG1LZ4NbdEX-sCRJ2rCve27Puc";
    public static final String BOB_DISPLAY_NAME = "Bob";
    public static final String BOB_EXCHANGE_KEY = "vLLw-stuK6e7jUFyeEhuni8yqmRtcNBvo-Aftmy49o8";
    public static final String BOB_PASSPHRASE = "bob passphrase";
    public static final String BOB_PAYLOAD = "bobPayload";
    public static final String BOB_PICTURE = "bobPicture";
    public static final Subscription BOB_SUBSCRIPTION = Subscription.FREE;
    public static final String BOB_SYSTEM_PRIVATE_KEYS = "bob system private keys";
    public static final String BOB_SYSTEM_PUBLIC_KEYS = "bob system public keys";
    public static final String BOB_PSEUDO = "bobby"; // Because, yeah, we need at least 4 characters, right?

    public static final UUID CAROL_BOX_ADDRESS = UUID.fromString("c7183e11-5e5b-4c28-93cc-870452ca61bc");
    public static final String CAROL_CREDENTIALS = "{\"kdf\":{\"salt\":\"cf6f590d7d4a261a820c66db6f8ba086657d9049d5a866f24bf6196323f68a6a\"},\"srp\":{\"type\":\"srp6a\",\"salt\":\"f8f671f5aa3ea6c5556040438287755c76a9f99f2fd6c35326379ed0dedcb33d\",\"verifier\":\"40c24a4fb291b12f490e7d0f343f8537dc93495fa073f85d5a5e555aa3aaa4320ddfc246ed73b46c2c496f7def041d50f8f51a43640c4b754b2f38ad9f4d238d51285931e7fded885a51b0036cf391b4890c2a224665acac9eff5191af5cb5a5c0595b011b97bf14746c5b166cebed093e51130d0b8dce7036950dfade4b2eaacf816ae4cd835746bf8ea9ecf38ddc42de75db69a66514a5d4762b9b05d969a725f88600dafc18cd10024c203a9ba4ab8366cfc1089177c8c7b89041b45f8b7cea901f7d43392c661bba2ac945454fe47e507c42aca0ddf122f372e957be2cd89364c19b861896ae3f7dd3c8c4309326c9529c4cf27628bb0f5e2034b5e0\"},\"systemKeys\":{\"privateKeys\":\"carol system private keys\",\"publicKeys\":\"carol system public keys\",\"type\":\"ecc.x25519\"}}";
    public static final String CAROL_DEVICE_STORAGE_KEY = "gwItxczBFe6veao0kh1p9H3L0rCB5Scha9HvI8Q0fpI";
    public static final String CAROL_DISPLAY_NAME = "Carol";
    public static final String CAROL_EXCHANGE_KEY = "1Nik_mnymp4IyVNXnEP1V-PiL8e1IF2iW8zbb3o7ndY";
    public static final String CAROL_PASSPHRASE = "carol passphrase";
    public static final String CAROL_PAYLOAD = "carolPayload";
    public static final String CAROL_PICTURE = "carolPicture";
    public static final Subscription CAROL_SUBSCRIPTION = Subscription.FREE;
    public static final String CAROL_SYSTEM_PRIVATE_KEYS = "carol system private keys";
    public static final String CAROL_SYSTEM_PUBLIC_KEYS = "carol system public keys";
    public static final String CAROL_USERNAME = "carol";

    public static final UUID CHUCK_BOX_ADDRESS = UUID.fromString("6402fce6-b029-4bf9-8143-845e8d3341dd");
    public static final String CHUCK_USERNAME = "chuck";

    protected final List<TestUser> list;

    public TestUsers() {
        list = Collections.listOf(
                new TestUser(ALICE_BOX_ADDRESS, ALICE_CREDENTIALS, ALICE_DEVICE_STORAGE_KEY, ALICE_DISPLAY_NAME,
                        ALICE_EXCHANGE_KEY, ALICE_PASSPHRASE, ALICE_PAYLOAD, ALICE_PICTURE, ALICE_SUBSCRIPTION,
                        ALICE_SYSTEM_PRIVATE_KEYS, ALICE_SYSTEM_PUBLIC_KEYS, ALICE_PSEUDO),
                new TestUser(BOB_BOX_ADDRESS, BOB_CREDENTIALS, BOB_DEVICE_STORAGE_KEY, BOB_DISPLAY_NAME,
                        BOB_EXCHANGE_KEY, BOB_PASSPHRASE, BOB_PAYLOAD, BOB_PICTURE, BOB_SUBSCRIPTION,
                        BOB_SYSTEM_PRIVATE_KEYS, BOB_SYSTEM_PUBLIC_KEYS, BOB_PSEUDO),
                new TestUser(CAROL_BOX_ADDRESS, CAROL_CREDENTIALS, CAROL_DEVICE_STORAGE_KEY, CAROL_DISPLAY_NAME,
                        CAROL_EXCHANGE_KEY, CAROL_PASSPHRASE, CAROL_PAYLOAD, CAROL_PICTURE, CAROL_SUBSCRIPTION,
                        CAROL_SYSTEM_PRIVATE_KEYS, CAROL_SYSTEM_PUBLIC_KEYS, CAROL_USERNAME)
        );
    }

    public TestUser get(String username) {
        return list.stream().filter(user -> username.equals(user.username)).findFirst().orElse(null);
    }

    public List<TestUser> getAll() {
        return list;
    }

    @Value
    @ToString
    public static class TestUser {
        UUID boxAddress;
        String credentials;
        String deviceStorageKey;
        String displayName;
        String exchangeKey;
        String passphrase;
        String payload;
        String picture;
        Subscription subscription;
        String systemPrivateKeys;
        String systemPublicKeys;
        String username;

        public be.seeld.users.User toUser() {
            return be.seeld.users.User.builder()
                    .boxAddress(boxAddress.toString())
                    .credentials(credentials)
                    .deviceStorageKey(deviceStorageKey)
                    .displayName(displayName)
                    .exchangeKey(exchangeKey)
                    .payload(payload)
                    .picture(picture)
                    .subscription(subscription.name())
                    .username(username)
                    .build();
        }
    }

//    private static final String ALICE_PASSPHRASE = "alicepwd";
    private static final String ALICE_ARMORED_PUBLIC_KEY = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "mI0EWxUhfwMEAL/Pb+T0SeYwL2dCgj2yjlINYVUQso1qPGttxqy2O8boSQLlhOiO\n" +
            "q150dQzIb10GMaczCXcL00d0OBhhZgODcgtj1jJf4JAh77vG9tqFwvFJsP9Gqnrc\n" +
            "aHkT8kk+U3gnyKBfRrTskprQVTp7jlJazII3cVBbBMNXaNowZ9Ench25ABEBAAG0\n" +
            "FmFsaWNlIDxhbGljZUBzZWVsZC5ldT6IqAQTAwIAEgUCWxUhfwIbAwILCQIVCgIe\n" +
            "AQAKCRDEhN0b5awn12M1A/sFZu3R+KS6XJXOInJSbwL393FJzeGcArGNmRHYmuFC\n" +
            "gp2SeeHzDoMixVIP61z18Ul0NgRX7yAwzFlWJcO0n7gkpsAJ6yBPhhO80cc59HOz\n" +
            "cJimanvQ+3b048erGDKI6jAcB8jVSuKzkfTU2JzDU8s4+MaqKCoSXu4iSn7Jk2Ru\n" +
            "sLiNBFsVIX8BBACpijFsTrY/z3gB8/iEgMyC+PygaQvFOc4MjbaXDEUYk7rUTFyE\n" +
            "t0+edv0//dq+NJJgVxnjmC6k5BJRiAbU4qeyD3Ku2SXoy7gEXKMJHnQwqPnVe84L\n" +
            "2Vp8qpH7yD3WEJ2Qh51yk5RfOuPsjeOwGN7aNiM5kmpMjlP5hs3UF4e4GwARAQAB\n" +
            "iJ8EGAMCAAkFAlsVIX8CGwwACgkQxITdG+WsJ9d8egP7BTcKMHOOfgLV4DmYymBo\n" +
            "8LjSj9cDY/UDSpNyyyU0UOB8KqSK+P+d26gMGRLBXaD9cOQl+ireGBdXo1Ppn+e9\n" +
            "mN7F6LNiROrEWObVA27zHou00IIvAutOSn4s6FjXjb5kLfyTtsDpOF9DxV1GOVLt\n" +
            "ggcQzxA7GC1lHuMHPT3wG60=\n" +
            "=egyX\n" +
            "-----END PGP PUBLIC KEY BLOCK-----\n";
    private static final String ALICE_ARMORED_SECRET_KEY = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "lQIGBFsVIX8DBAC/z2/k9EnmMC9nQoI9so5SDWFVELKNajxrbcastjvG6EkC5YTo\n" +
            "jqtedHUMyG9dBjGnMwl3C9NHdDgYYWYDg3ILY9YyX+CQIe+7xvbahcLxSbD/Rqp6\n" +
            "3Gh5E/JJPlN4J8igX0a07JKa0FU6e45SWsyCN3FQWwTDV2jaMGfRJ3IduQARAQAB\n" +
            "/gkDCETWBuuzNIBRwOTSROZIP8+UmcA+68uD4V+tm3hgBYwGsAKJdWKQ370NbSGc\n" +
            "XjqLsYyEV85hK+j8gfBrKbt0alX2YoBxVRNlnoDhO2T2VBf2F7ezZ7mJcu/CFlVy\n" +
            "2O8Aneo4iyuqxpptHUptL1dMR4AziLqB11ER5ns3zPJDkoMFJMZ8Oy+wghkMHZJ8\n" +
            "VlOpPLRLJXenrpIy6kzRciyrTy59nwDv5MLdS2QkoHQKrtEqphzhS19dsyCuyLQs\n" +
            "lTz/5Zce4l256BeX2vcKP41hp5IDS5XEzwn2MdzPRPei/7Z/XLqVHrfR/l0wTdEk\n" +
            "X+7ibB7kyn8Pjn/Piqjht5b53p+6CouDVWpBIQZP4bVsyuuKE0VRazaKGe4KTA3p\n" +
            "YWESdOqpOc6rzhdoRtJ/4g60NGGzC2CpFDpBHa/Am6+YhMX369f1KgqqFN7s1XUp\n" +
            "Xe5LYIL3YQ5bvtzxHZlgnBewRYEeZAAF2ANiWvIH8PwCcztQKSn35nS0FmFsaWNl\n" +
            "IDxhbGljZUBzZWVsZC5ldT6IqAQTAwIAEgUCWxUhfwIbAwILCQIVCgIeAQAKCRDE\n" +
            "hN0b5awn12M1A/sFZu3R+KS6XJXOInJSbwL393FJzeGcArGNmRHYmuFCgp2SeeHz\n" +
            "DoMixVIP61z18Ul0NgRX7yAwzFlWJcO0n7gkpsAJ6yBPhhO80cc59HOzcJimanvQ\n" +
            "+3b048erGDKI6jAcB8jVSuKzkfTU2JzDU8s4+MaqKCoSXu4iSn7Jk2RusJ0CBgRb\n" +
            "FSF/AQQAqYoxbE62P894AfP4hIDMgvj8oGkLxTnODI22lwxFGJO61ExchLdPnnb9\n" +
            "P/3avjSSYFcZ45gupOQSUYgG1OKnsg9yrtkl6Mu4BFyjCR50MKj51XvOC9lafKqR\n" +
            "+8g91hCdkIedcpOUXzrj7I3jsBje2jYjOZJqTI5T+YbN1BeHuBsAEQEAAf4JAwhE\n" +
            "1gbrszSAUcCVkSJpOgK4vI8RdMvhUPZD9NLf14maAwEe7kFMqvu/UH/6M2Mz2kei\n" +
            "OWvBxZbDsLsZtWmD1aapnBjWx/FSITl/1rWii6rV/fGC7iH3vQVUItf/RrJw8sBs\n" +
            "pTS6YucvVJfTaSM1QaCuRVpCHGBhoxIQjOt3C9WbfuS1xCikWmN8FKD638A2qTPE\n" +
            "I+B+o7NOW9T0h3Jr441nLXYZcZlMWUcrqr6ImTTiKGaZ+VOWHKDHJY9dDdrgXD1k\n" +
            "F5fzHT5oaksaIYPbBPHreon9TbaTGhPDlmBlf2cLdBTM3hvb32tmzUxEZs4VCuPT\n" +
            "HYzrru445VbjweTm2C5mMatP/EDnmkISrpMG0I4y5XD0T6fiq7JukTHj5feYnzI9\n" +
            "1kchmahydeLlObq4NmX3lJcjFkL8TJ3vhd6QL1gKsGUXlhM6tDIHuJxGZfYUVqxr\n" +
            "mEI7+vXFeKmVzRIAuuZ/jHSWCVeggqpPlfN5C4nmMhLWgG/miJ8EGAMCAAkFAlsV\n" +
            "IX8CGwwACgkQxITdG+WsJ9d8egP7BTcKMHOOfgLV4DmYymBo8LjSj9cDY/UDSpNy\n" +
            "yyU0UOB8KqSK+P+d26gMGRLBXaD9cOQl+ireGBdXo1Ppn+e9mN7F6LNiROrEWObV\n" +
            "A27zHou00IIvAutOSn4s6FjXjb5kLfyTtsDpOF9DxV1GOVLtggcQzxA7GC1lHuMH\n" +
            "PT3wG60=\n" +
            "=jpJD\n" +
            "-----END PGP PRIVATE KEY BLOCK-----\n";
    private static final UUID ALICE_USER_BOX_ADDRESS = UUID.fromString("a0ecc609-25ee-4d83-85b5-e0521a24b276");

//    private static final String BOB_PASSPHRASE = "bobpwd";
    private static final String BOB_ARMORED_PUBLIC_KEY = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "mI0EWxUhMQMEALVaCSpLY75o5E/PeilQUoKDhb9qfVZZ2E3TZS5eQ0Mw22hj9XWy\n" +
            "ICtf6yeUjicIHOlpTalcthpzFN95cp7vz9rFZzgtckqkNHU3n+ZrC29Yt4pwgIRF\n" +
            "gy9oDdNYDjRYkl1oHDC4h0jO+WRnPiaN9QkufV5AneNW69JFMGX/eAWJABEBAAG0\n" +
            "EmJvYiA8Ym9iQHNlZWxkLmV1PoioBBMDAgASBQJbFSExAhsDAgsJAhUKAh4BAAoJ\n" +
            "EJcVA/nWPP/PFGID/RxdbCHLgEF/RNtqCxbGCon9iKvBsqL1dcxGuTHH0IUGspWw\n" +
            "6brbywyBFQt9lKEPdFXmHXplI9qrZtyPf4TwMyMeWIEusuuVnpDty1/nhcuLyPy3\n" +
            "oOivGKvviBdMGzJhfDUeWYnBtP0oI8slC3CXjhAmiD/2YfUaCS++a5idGdxuuI0E\n" +
            "WxUhMQEEAOHzOnC/WNBgDG0tLx1T6uGqzgDhuxx1554McgXrsjd2uifv3DEiqfu7\n" +
            "S9OJFkC/PblhTnW5Q8XiL4rU1ou2rV0J8Dy0zEJoEEVSHzaRUl4GaneGd7ejJG4L\n" +
            "SOE3WXnEWeusb3i3lXdMrv5Fdo5W3NR4kAWnsGKUcPf7KPURhyy5ABEBAAGInwQY\n" +
            "AwIACQUCWxUhMQIbDAAKCRCXFQP51jz/z5ObBACyp4YTXlGhrCQ9GCTVO/W+IJea\n" +
            "wy4u668afIl4TV2/Tg0gvDdZdtIXdJcXmM5IkoGpvDXuATlgk4XoktkizydborCL\n" +
            "NI1UgJEIhnST1S5znhBRm2iYx/hJMxvAXWNTffkzSx+V0/ZJCf/qynacYkTgcFIw\n" +
            "O07PXAp4zFA0zRMWAA==\n" +
            "=38G8\n" +
            "-----END PGP PUBLIC KEY BLOCK-----\n";
    private static final String BOB_ARMORED_SECRET_KEY = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "lQIGBFsVITEDBAC1WgkqS2O+aORPz3opUFKCg4W/an1WWdhN02UuXkNDMNtoY/V1\n" +
            "siArX+snlI4nCBzpaU2pXLYacxTfeXKe78/axWc4LXJKpDR1N5/mawtvWLeKcICE\n" +
            "RYMvaA3TWA40WJJdaBwwuIdIzvlkZz4mjfUJLn1eQJ3jVuvSRTBl/3gFiQARAQAB\n" +
            "/gkDCBcYxzZFVRG1wG6hPoAt1WYj/Fnjy90HFQu7YPxNqeFqhh4QX6aToKPDS8/m\n" +
            "jPHcDlpJh7Mk+AYvrTIQ/ShYjP2/yjMyMds6R21ydwqbzXWt09JLsY/juALcCvRG\n" +
            "HfGjrsWnCtOFJ03tUA4fPQP6Ht/7HSORMPPl5PV2V80tH5Vcaw8QnwAICoEzdHtU\n" +
            "QE5ZZ4AtswDesw3QEzn9T8/tUgww6gDNDIEIi35ENJSnG0MtA6jwnjy1vAi1zjXo\n" +
            "kYv/9dTUaEErb+eE+l5Vu0SQ1bA86A1Ooz65BQsZJYo/aZOv4BeA8UlRZFld28/x\n" +
            "vp8jurIyABIUcU5T4AX4xYZ0tAyyzQpX5RdgfEyOR6rdtrEqOjc2orpDnrgAbF2i\n" +
            "OYXZ9BWq2rHBmXA1KbsDXuKnnYY23X1H0RhHmJ5MpFEauh+R2b8Ay87WRgEEbEe4\n" +
            "KouVMzKsSKPSs4J6imwarzEAlV/W7LbVs4GNEn7mmM+xi2gr8Je1CNS0EmJvYiA8\n" +
            "Ym9iQHNlZWxkLmV1PoioBBMDAgASBQJbFSExAhsDAgsJAhUKAh4BAAoJEJcVA/nW\n" +
            "PP/PFGID/RxdbCHLgEF/RNtqCxbGCon9iKvBsqL1dcxGuTHH0IUGspWw6brbywyB\n" +
            "FQt9lKEPdFXmHXplI9qrZtyPf4TwMyMeWIEusuuVnpDty1/nhcuLyPy3oOivGKvv\n" +
            "iBdMGzJhfDUeWYnBtP0oI8slC3CXjhAmiD/2YfUaCS++a5idGdxunQIGBFsVITEB\n" +
            "BADh8zpwv1jQYAxtLS8dU+rhqs4A4bscdeeeDHIF67I3dron79wxIqn7u0vTiRZA\n" +
            "vz25YU51uUPF4i+K1NaLtq1dCfA8tMxCaBBFUh82kVJeBmp3hne3oyRuC0jhN1l5\n" +
            "xFnrrG94t5V3TK7+RXaOVtzUeJAFp7BilHD3+yj1EYcsuQARAQAB/gkDCBcYxzZF\n" +
            "VRG1wB6rFrr2wFcJXTbPN2d5nXokBYu6yb2g8LDCZ/PXXn4cZaVyz5Y169rgO0gn\n" +
            "kzktSM+MojLzVrIbz0Y1Eplu5glH4FOBPovwleuiyVyVtc8J0MhNk972y9F+vUZx\n" +
            "xKYKz8qUmm01Ba9AOz7OODiT7CpUVWsyIAcAxRTfjaYi/sXAy+noR9SSbRoK+lpG\n" +
            "y5zRXeDMzZnkhpQG6y+BxxELdpKGde1FQWxZ6UriZlfX/LD6+hCE+iCOWaOX6vbm\n" +
            "Ex8C03Dx7JVlDCQZmt+fYrbnWItziGbZFYv1PsaZjJA+giQqq9c72tI7s073tPir\n" +
            "ENdF+czhBsoBQOgrjnCEmZt4gBCjzq2lfgHO4/zNe0fAoC8ZNbUiglpdOT2cV0AO\n" +
            "xqrOUgHj00xupIjKI9MuHjvyPJegWhjF1jlmmd59Ngmx7s8OxrXNmG1+RtwHVApw\n" +
            "wWOlGm8u7qYWibiXMVpegDVq1JzdvXo+Ce/5oWC36LOInwQYAwIACQUCWxUhMQIb\n" +
            "DAAKCRCXFQP51jz/z5ObBACyp4YTXlGhrCQ9GCTVO/W+IJeawy4u668afIl4TV2/\n" +
            "Tg0gvDdZdtIXdJcXmM5IkoGpvDXuATlgk4XoktkizydborCLNI1UgJEIhnST1S5z\n" +
            "nhBRm2iYx/hJMxvAXWNTffkzSx+V0/ZJCf/qynacYkTgcFIwO07PXAp4zFA0zRMW\n" +
            "AA==\n" +
            "=vW8/\n" +
            "-----END PGP PRIVATE KEY BLOCK-----\n";
    private static final UUID BOB_USER_BOX_ADDRESS = UUID.fromString("7cd17b03-cbac-46ce-80cc-8e3a42e6f1b8");
}
