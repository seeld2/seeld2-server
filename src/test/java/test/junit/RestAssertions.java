package test.junit;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.*;

public class RestAssertions {

    public static <T> T assertThatCreateResponseIs(ResponseEntity response, Class<T> type) {
        return assertThatResponseIs(response, HttpStatus.CREATED, type);
    }

    public static <T> T assertThatOkResponseIs(ResponseEntity response, Class<T> type) {
        return assertThatResponseIs(response, HttpStatus.OK, type);
    }

    public static void assertThatResponseIs(ResponseEntity response, HttpStatus status) {
        assertThat(response).as("response").isNotNull();
        assertThat(response.getStatusCode()).as("response's HTTP status code").isNotNull().isEqualTo(status);
    }

    /**
     * @param response The ResponseEntity returned by the REST API.
     * @param status The expected status for the ResponseEntity.
     * @param type The expected type of the response's body.
     * @return The response's body casted to its correct type.
     */
    public static <T> T assertThatResponseIs(ResponseEntity response, HttpStatus status, Class<T> type) {

        assertThatResponseIs(response, status);

        assertThat(response.hasBody()).as("response has body").isTrue();
        assertThat(response.getBody()).as("response body type").isInstanceOf(type);

        return type.cast(response.getBody());
    }
}
