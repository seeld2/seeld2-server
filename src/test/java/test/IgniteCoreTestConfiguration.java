package test;

import be.seeld.IgniteCoreConfiguration;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.ClientConnectorConfiguration;
import org.apache.ignite.configuration.DataPageEvictionMode;
import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.logger.slf4j.Slf4jLogger;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static be.seeld.login.LoginConfiguration.SRP6A_REGION;
import static be.seeld.notifications.NotificationsConfiguration.NOTIFICATIONS_REGION;

@Configuration
@Profile("integration")
public class IgniteCoreTestConfiguration {

    public static final int CLIENT_CONNECTOR_PORT = 11800;

    private static final Logger LOGGER = LoggerFactory.getLogger(IgniteCoreTestConfiguration.class);

    private static final String IGNITE_INSTANCE_1 = "Test-Ignite-1";
//    private static final String IGNITE_INSTANCE_2 = "Test-Ignite-2";

    private static final int TCP_COMMUNICATION_TEST_PORT = 57100;
    private static final String TCP_DISCOVERY_MULTICAST_IP_FINDER_TEST_MCAST_GROUP = "238.1.2.5";
    private static final int TCP_DISCOVERY_TEST_PORT = 57500;
    private static final int TCP_DISCOVERY_TEST_PORT_RANGE = 5;

    @Bean(name = "ignite")
    public Ignite ignite() {
        Ignite[] instances = {
                Ignition.start(igniteConfiguration(IGNITE_INSTANCE_1))//,
                // TODO This is incorrect. In reality the below starts a second CLUSTER, which won't talk to the first one
//                Ignition.start(igniteConfiguration(IGNITE_INSTANCE_2))
        };
        ArrayUtils.shuffle(instances);

        LOGGER.info("Returning ignite instance named " + instances[0].name());
        return instances[0];
    }

    private IgniteConfiguration igniteConfiguration(String igniteInstanceName) {

        return new IgniteConfiguration()
                .setIgniteInstanceName(igniteInstanceName)
                .setGridLogger(new Slf4jLogger())

                .setClientMode(false)
                .setClientConnectorConfiguration(new ClientConnectorConfiguration().setPort(CLIENT_CONNECTOR_PORT))

                .setBinaryConfiguration(IgniteCoreConfiguration.binaryConfiguration())

                .setDataStorageConfiguration(dataStorageConfiguration())

                .setCommunicationSpi(communicationSpi())
                .setDiscoverySpi(discoverySpi());
    }

    private DataStorageConfiguration dataStorageConfiguration() {
        DataStorageConfiguration dataStorageConfiguration = new DataStorageConfiguration();
        dataStorageConfiguration.getDefaultDataRegionConfiguration().setPersistenceEnabled(false);

        DataRegionConfiguration notificationsDataRegionConfiguration = new DataRegionConfiguration()
                .setName(NOTIFICATIONS_REGION)
                .setPersistenceEnabled(false)
                .setPageEvictionMode(DataPageEvictionMode.RANDOM_2_LRU)
                .setInitialSize(10L * 1024 * 1024) // 10Mb
                .setMaxSize(20L * 1024 * 1024); // 20Mb
        DataRegionConfiguration srp6DataRegionConfiguration = new DataRegionConfiguration()
                .setName(SRP6A_REGION)
                .setPersistenceEnabled(false);

        dataStorageConfiguration.setDataRegionConfigurations(
                notificationsDataRegionConfiguration,
                srp6DataRegionConfiguration);

        return dataStorageConfiguration;
    }

    private TcpCommunicationSpi communicationSpi() {
        return new TcpCommunicationSpi()
                .setLocalPort(TCP_COMMUNICATION_TEST_PORT);
    }

    private TcpDiscoverySpi discoverySpi() {
        return new TcpDiscoverySpi()
                .setLocalPort(TCP_DISCOVERY_TEST_PORT)
                .setLocalPortRange(TCP_DISCOVERY_TEST_PORT_RANGE)
                .setIpFinder(new TcpDiscoveryMulticastIpFinder()
                        .setMulticastGroup(TCP_DISCOVERY_MULTICAST_IP_FINDER_TEST_MCAST_GROUP));
    }
}
