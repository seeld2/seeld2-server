package test.rest;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

public class WebApiTestClient {

    private final String jwtPrefix;
    private final TestRestTemplate testRestTemplate;

    public WebApiTestClient(TestRestTemplate testRestTemplate, String jwtPrefix) {
        this.jwtPrefix = jwtPrefix;
        this.testRestTemplate = testRestTemplate;
    }

    public ResponseEntity<?> delete(String restPath, String token) {
        HttpHeaders headers = prepareHeadersForAuthorization(token);
        return testRestTemplate.exchange(restPath, DELETE, new HttpEntity<>(headers), (Class<?>) null);
    }

    public ResponseEntity<?> delete(String restPath, String token, Object body) {
        HttpHeaders headers = prepareHeadersForAuthorization(token);
        return testRestTemplate.exchange(restPath, DELETE, new HttpEntity<>(body, headers), (Class<?>) null);
    }

    public <T> ResponseEntity<T> get(String restPath, Class<T> responseType) {
        return testRestTemplate.getForEntity(restPath, responseType);
    }

    public <T> ResponseEntity<T> get(String restPath, String token, Class<T> responseType) {
        HttpHeaders httpHeaders = prepareHeadersForAuthorization(token);
        return testRestTemplate.exchange(restPath, GET, new HttpEntity<>(httpHeaders), responseType);
    }

    public <T> ResponseEntity<T> get(String restPath, String token, Class<T> responseType, int apiVersion) {
        HttpHeaders httpHeaders = prepareHeadersForAuthorization(token);
        httpHeaders.set("Api-Ver", Integer.toString(apiVersion));
        return testRestTemplate.exchange(restPath, GET, new HttpEntity<>(httpHeaders), responseType);
    }

    public ResponseEntity<?> post(String restPath, Object body) {
        return post(restPath, null, body, null);
    }

    public <T> ResponseEntity<T> post(String restPath, Object body, Class<T> responseType) {
        return post(restPath, null, body, responseType);
    }

    public ResponseEntity<?> post(String restPath, String token, Object body) {
        return post(restPath, token, body, null);
    }

    public <T> ResponseEntity<T> post(String restPath, String token, Object body, Class<T> responseType) {
        HttpHeaders headers = prepareHeadersForAuthorization(token);
        return testRestTemplate.exchange(restPath, POST, new HttpEntity<>(body, headers), responseType);
    }

    public ResponseEntity<?> put(String restPath, Object body) {
        return put(restPath, null, body, null);
    }

    public <T> ResponseEntity<T> put(String restPath, Object body, Class<T> responseType) {
        return put(restPath, null, body, responseType);
    }

    public ResponseEntity<?> put(String restPath, String token, Object body) {
        return put(restPath, token, body, null);
    }

    public ResponseEntity<?> put(String restPath, String token, Object body, int apiVersion) {
        return put(restPath, token, body, null, apiVersion);
    }

    public <T> ResponseEntity<T> put(String restPath, String token, Object body, Class<T> responseType) {
        HttpHeaders headers = prepareHeadersForAuthorization(token);
        return testRestTemplate.exchange(restPath, PUT, new HttpEntity<>(body, headers), responseType);
    }

    public <T> ResponseEntity<T> put(String restPath, String token, Object body, Class<T> responseType, int apiVersion) {
        HttpHeaders headers = prepareHeadersForAuthorization(token);
        headers.set("Api-Ver", Integer.toString(apiVersion));
        return testRestTemplate.exchange(restPath, PUT, new HttpEntity<>(body, headers), responseType);
    }

    private HttpHeaders prepareHeadersForAuthorization(String token) {
        HttpHeaders headers = new HttpHeaders();

        if (token != null) {
            headers.set(HttpHeaders.AUTHORIZATION, jwtPrefix + " " + token);
        }

        return headers;
    }
}
