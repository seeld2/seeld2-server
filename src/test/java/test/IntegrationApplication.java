package test;

import be.seeld.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import javax.annotation.PostConstruct;

/**
 * Prepares the Integration tests' context.<br/>
 * <br/>
 * We do exclude the scan of Application.class, so that we can specify alternative configurations depending on which
 * application context is used. <br/>
 */
@ComponentScan(
        basePackages = {"be.seeld", "test"},
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Application.class)})
@Configuration
@EnableAutoConfiguration()
public class IntegrationApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationApplication.class);

    @Value("${config.name}")
    private String configName;

    @Bean
    public String configName() {
        return configName;
    }

    /**
     * We provide a {@link SimpleClientHttpRequestFactory} instead of using the default one.<br/>
     * This is to avoid using io.netty for the {@link org.springframework.boot.test.web.client.TestRestTemplate}.
     * Indeed, the Cassandra Driver and Cassandra Unit require a lower version of netty which is incompatible with
     * TestRestTemplate. To avoid juggling with version we leave the netty version needed by Cassandra and replace the
     * netty-based http-request factory with {@link SimpleClientHttpRequestFactory} which does not need it.
     */
    @Bean
    public RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder().detectRequestFactory(false).requestFactory(SimpleClientHttpRequestFactory.class);
    }

    @PostConstruct
    public void postConstruct() {
        LOGGER.info("Using Spring Boot application configuration: " + configName);
    }
}
