package test;

import com.rits.cloning.Cloner;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.junit.runner.RunWith;

import javax.validation.Validation;
import javax.validation.Validator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test classes extending this one will have access to these goodies:<br/>
 * <ul>
 *     <li>EasyMock support</li>
 *     <li>A "Cloner" standard instance accessible through a protected <em>cloner</em> field</li>
 *     <li>An instance of SecureRandom</li>
 *     <li>A Validator instance, with its special "assert" utility method</li>
 * </ul>
 */
@RunWith(EasyMockRunner.class)
public abstract class Unit extends EasyMockSupport {

    private static final TestUsers TEST_USERS = new TestUsers();

    protected final Cloner cloner = Cloner.standard();
    protected final SecureRandom secureRandom;

    protected TestUsers.TestUser alice = TEST_USERS.get(TestUsers.ALICE_PSEUDO);
    protected TestUsers.TestUser bob = TEST_USERS.get(TestUsers.BOB_PSEUDO);

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public Unit() {
        try {
            secureRandom = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    protected <T> void assertThatValidationViolationAmountIs(int amount, T object) {
        assertThat(validator.validate(object)).as("check constraint violations' amount").size().isEqualTo(amount);
    }
}
