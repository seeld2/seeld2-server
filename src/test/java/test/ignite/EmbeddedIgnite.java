package test.ignite;

import org.apache.ignite.Ignite;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

public class EmbeddedIgnite {

    private static EmbeddedIgnite embeddedIgnite;

    private final int clientConnectorPort;
    private final Ignite ignite;

    private Connection connection;

    private EmbeddedIgnite(Ignite ignite, int clientConnectorPort) {
        this.clientConnectorPort = clientConnectorPort;
        this.ignite = ignite;
    }

    public void clearAllTables() {
        ignite.cacheNames().forEach(cacheName -> ignite.cache(cacheName).clear());
    }

    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    public DataSource getDataSource() throws ClassNotFoundException, SQLException {

        if (connection == null) {
            ReentrantLock lock = new ReentrantLock();
            lock.lock();
            try {
                if (connection == null) {
                    Class.forName("org.apache.ignite.IgniteJdbcThinDriver");
                    connection = DriverManager.getConnection("jdbc:ignite:thin://127.0.0.1:" + clientConnectorPort);
                    connection.setAutoCommit(true);
                }
            } finally {
                lock.unlock();
            }
        }

        return new SingleConnectionDataSource(connection, true);
    }

    public static EmbeddedIgnite getInstance(Ignite ignite, int clientConnectorPort) {
        if (embeddedIgnite == null) {
            ReentrantLock lock = new ReentrantLock();
            lock.lock();
            try {
                if (embeddedIgnite== null) {
                    embeddedIgnite = new EmbeddedIgnite(ignite, clientConnectorPort);
                }
            } finally {
                lock.unlock();
            }
        }
        return embeddedIgnite;
    }
}
