package test.cassandraunit;

/**
 * A utility class used to manage a cassandra-unit's embedded server.<br/>
 * Very useful in case of, for example, acceptance tests!
 */
// TODO Extract the embedded Cassandra testing approach to a separate project, as a tutorial GitHub
public class EmbeddedCassandra {

//    private static EmbeddedCassandra embeddedCassandra;
//
//    private final String keyspace;
//    private final String nodes;
//    private final int port;
//
//    private Cluster cluster;
//    private Session session;
//
//    private EmbeddedCassandra(String keyspace, String nodes, int port) {
//        this.keyspace = keyspace;
//        this.nodes = nodes;
//        this.port = port;
//    }

    /**
     * Clears all the tables in the keyspace, without actually dropping the tables.
     */
//    public void clearAllTables() {
//        Collection<TableMetadata> tables = cluster.getMetadata().getKeyspace(keyspace).getTables();
//        tables.forEach(table -> session.execute(QueryBuilder.truncate(table)));
//    }

//    public Session getSession() {
//        return session;
//    }

    /**
     * Loads additional CQL data sets, specified as a list of files in the classpath.
     */
//    public void loadScripts(String... cqlScriptPaths) {
//        CQLDataLoader dataLoader = new CQLDataLoader(session);
//        Arrays.stream(cqlScriptPaths).forEach(cqlScriptPath -> {
//            ClassPathCQLDataSet dataSet = new ClassPathCQLDataSet(cqlScriptPath);
//            dataLoader.load(dataSet);
//        });
//    }

    /**
     * Starts this instance of an embedded cassandra using the specified timeout.
     *
     * @param timeout The timeout (in ms) before considering that the embedded cassandra server failed to start.
     */
//    public void start(long timeout) {
//        try {
//            EmbeddedCassandraServerHelper.startEmbeddedCassandra(timeout);
//
//            cluster = new Cluster.Builder().addContactPoints(nodes).withPort(port).build();
//            session = cluster.connect();
//
//        } catch (Exception e) {
//            throw new RuntimeException("Unexpected error while trying to start() EmbeddedCassandra.");
//        }
//    }

    /**
     * Either creates or returns an existing {@link EmbeddedCassandra} instance. If the instance already exists, that
     * instance's parameters (keyspace, nodes, port) are used: no new instance will be generated!
     */
//    public static EmbeddedCassandra getInstance(String keyspace, String nodes, int port) {
//        if (embeddedCassandra == null) {
//            ReentrantLock lock = new ReentrantLock();
//            lock.lock();
//            try {
//                if (embeddedCassandra == null) {
//                    embeddedCassandra = new EmbeddedCassandra(keyspace, nodes, port);
//                }
//            } finally {
//                lock.unlock();
//            }
//        }
//        return embeddedCassandra;
//    }
}
