package test;

import be.seeld.SecurityConfiguration;
import be.seeld.login.LoginConfiguration;
import be.seeld.users.User;
import be.seeld.registration.cli.CliAuthData;
import be.seeld.parameters.ParameterData;
import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Encryptables;
import org.apache.ignite.Ignite;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import test.data.FileData;
import test.data.MessageData;
import test.data.MessageLifeData;
import test.data.UserData;
import test.data.UserEventData;
import test.ignite.EmbeddedIgnite;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootTest(
        classes = IntegrationApplication.class,
        properties = {"spring.profiles.active=integration"},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@RunWith(SpringRunner.class)
public abstract class Integration {

    private static final Logger LOGGER = LoggerFactory.getLogger(Integration.class);

    private static EmbeddedIgnite embeddedIgnite;

    @Autowired
    protected SecurityConfiguration.AppSecurityProperties appSecurityProperties;
    @Autowired
    protected LoginConfiguration.Srp6aProperties srp6aProperties;
    @Autowired
    protected Encryptables encryptables;
    // TODO Ideally we should attempt to use WebTestClient to build our URLs, instead of hard-coding localhost. See https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/html/spring-boot-features.html#boot-features-testing-spring-boot-applications-testing-with-running-server
    @LocalServerPort
    protected int testServerPort;

    protected TestUsers testUsers;

    protected CliAuthData cliAuthData;
    protected FileData fileData;
    protected MessageData messageData;
    protected MessageLifeData messageLifeData;
    protected ParameterData parameterData;
    protected UserData userData;
    protected UserEventData userEventData;

    @Autowired
    private Ignite ignite;

    public Integration() {
    }

    @PostConstruct
    public void postConstruct() throws SQLException, ClassNotFoundException {

        embeddedIgnite = EmbeddedIgnite.getInstance(ignite, IgniteCoreTestConfiguration.CLIENT_CONNECTOR_PORT);

        initIgniteDataTesters();

        testUsers = new TestUsers();
    }

    @PreDestroy
    public void preDestroy() throws SQLException {
        embeddedIgnite.closeConnection();
    }

    private void initIgniteDataTesters() throws SQLException, ClassNotFoundException {
        DataSource dataSource = embeddedIgnite.getDataSource();

        cliAuthData = new CliAuthData(dataSource);
        fileData = new FileData(dataSource);
        messageData = new MessageData(dataSource);
        messageLifeData = new MessageLifeData(dataSource);
        parameterData = new ParameterData(dataSource);
        userData = new UserData(dataSource);
        userEventData = new UserEventData(dataSource);
    }

    @Before
    public void setUp() {
        initIgnitePersistenceStore();
    }

    private void initIgnitePersistenceStore() {
            LOGGER.info("Initializing Ignite persistence store");

            embeddedIgnite.clearAllTables();

            LOGGER.info("Inserting default users for integration tests");
            testUsers.list.forEach(listedUser -> {

                User user = User.builder()
                        .username(listedUser.getUsername())
                        .boxAddress(listedUser.getBoxAddress().toString())
                        .credentials(listedUser.getCredentials())
                        .deviceStorageKey(listedUser.getDeviceStorageKey())
                        .displayName(listedUser.getDisplayName())
                        .exchangeKey(listedUser.getExchangeKey())
                        .payload(listedUser.getPayload())
                        .picture(listedUser.getPicture())
                        .subscription(listedUser.getSubscription().name())
                        .build();

                Encryptable<User> encryptable = encryptables.ofUnencrypted(user);

                User encrypted = encryptable.getEncrypted();

                userData.insert(encrypted);
            });
    }

    protected void clearAllTables() {
        embeddedIgnite.clearAllTables();
    }
}
