package be.seeld;

import be.seeld.login.LoginConfiguration;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.Integration;

import static org.assertj.core.api.Assertions.assertThat;

public class SecurityConfigurationITest extends Integration {

    @Autowired
    private SecurityConfiguration.AppSecurityProperties appSecurityProperties;
    @Autowired
    private LoginConfiguration.Srp6aProperties srp6aProperties;

    @Test
    public void loadAllSecurityProperties() {
        assertThat(appSecurityProperties)
                .isNotNull()
                .hasNoNullFieldsOrProperties();
        assertThat(appSecurityProperties.getAes()).hasNoNullFieldsOrProperties();
        assertThat(appSecurityProperties.getCors()).hasNoNullFieldsOrProperties();
        assertThat(appSecurityProperties.getJwt())
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("secret", "ThisIsADummySecretForDevelopmentAndIntegration");
        assertThat(appSecurityProperties.getPasswordEncoder()).hasNoNullFieldsOrProperties();
        assertThat(srp6aProperties).hasNoNullFieldsOrProperties();
    }
}
