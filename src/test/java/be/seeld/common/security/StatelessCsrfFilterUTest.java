package be.seeld.common.security;

import be.seeld.common.lang.Collections;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.savedrequest.Enumerator;
import test.Unit;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Vector;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;

public class StatelessCsrfFilterUTest extends Unit {

    private static final String COOKIE_NAME = "CSRF-COOKIE";
    private static final String HEADER_NAME = "CSRF-HEADER";

    private static final String SAFE_METHOD = "GET";
    private static final String UNSAFE_METHOD = "POST";

    private static final String CSRF_TOKEN = "CSRF token";
    private static final String DIFFERENT_CSRF_TOKEN = "A different CSRF token";

    private static final String EXCLUDED_PATH_PATTERN = "/api/exclude/*/url";
    private static final String SERVLET_PATH = "/api/normal/url";

    private StatelessCsrfFilter statelessCsrfFilter;
    @Mock
    private AccessDeniedHandler accessDeniedHandler;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() {
        statelessCsrfFilter = new StatelessCsrfFilter(
                COOKIE_NAME,
                HEADER_NAME,
                new String[]{EXCLUDED_PATH_PATTERN},
                accessDeniedHandler);
    }

    @Test
    public void passToTheNextFilterInChainWhenNoCsrfTokenIsRequired() throws ServletException, IOException {

        expect(request.getServletPath()).andReturn(SERVLET_PATH);
        expect(request.getMethod()).andReturn(SAFE_METHOD);
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    @Test
    public void passToTheNextFilterChainWhenRequiredCsrfTokensAreMatchingInCookieAndHeader() throws ServletException, IOException {

        expect(request.getServletPath()).andReturn(SERVLET_PATH);
        expect(request.getMethod()).andReturn(UNSAFE_METHOD);
        expect(request.getHeader(HEADER_NAME)).andReturn(CSRF_TOKEN);
        expect(request.getCookies()).andReturn(new Cookie[]{new Cookie(COOKIE_NAME, CSRF_TOKEN)});
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    @Test
    public void denyAccessWhenCsrfTokenIsMissingInHeader() throws IOException, ServletException {

        expect(request.getServletPath()).andReturn(SERVLET_PATH);
        expect(request.getMethod()).andReturn(UNSAFE_METHOD);
        expect(request.getHeader(HEADER_NAME)).andReturn(null);
        expect(request.getCookies()).andReturn(new Cookie[]{new Cookie(COOKIE_NAME, CSRF_TOKEN)});
        expectErrorLogging();
        accessDeniedHandler.handle(eq(request), eq(response), anyObject(AccessDeniedException.class));

        doFilterInternal(request, response, filterChain);
    }

    @Test
    public void denyAccessWhenCsrfTokenIsMissingInCookie() throws ServletException, IOException {

        expect(request.getServletPath()).andReturn(SERVLET_PATH);
        expect(request.getMethod()).andReturn(UNSAFE_METHOD);
        expect(request.getHeader(HEADER_NAME)).andReturn(CSRF_TOKEN);
        expect(request.getCookies()).andReturn(new Cookie[]{});
        expectErrorLogging();
        accessDeniedHandler.handle(eq(request), eq(response), anyObject(AccessDeniedException.class));

        doFilterInternal(request, response, filterChain);
    }

    @Test
    public void denyAccessWhenCsrfTokenInCookieAndInHeaderDoNotMatch() throws IOException, ServletException {

        expect(request.getServletPath()).andReturn(SERVLET_PATH);
        expect(request.getMethod()).andReturn(UNSAFE_METHOD);
        expect(request.getHeader(HEADER_NAME)).andReturn(CSRF_TOKEN);
        expect(request.getCookies()).andReturn(new Cookie[]{new Cookie(COOKIE_NAME, DIFFERENT_CSRF_TOKEN)});
        expectErrorLogging();
        accessDeniedHandler.handle(eq(request), eq(response), anyObject(AccessDeniedException.class));

        doFilterInternal(request, response, filterChain);
    }

    @Test
    public void passToTheNextFilterChainWhenRequestUrlIsExcluded() throws ServletException, IOException {

        expect(request.getServletPath()).andReturn("/api/exclude/any/url");
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    private void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        replayAll();
        statelessCsrfFilter.doFilterInternal(request, response, filterChain);
        verifyAll();
    }

    private void expectErrorLogging() {
        expect(request.getRequestURL()).andReturn(new StringBuffer("requestUrl"));
        expect(request.getHeaderNames()).andReturn(new Enumerator<>(new Vector<>(Collections.listOf("headerName"))));
    }
}
