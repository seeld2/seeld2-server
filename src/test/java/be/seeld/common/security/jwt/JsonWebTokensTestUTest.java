package be.seeld.common.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import test.Unit;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.newCapture;

public class JsonWebTokensTestUTest extends Unit {

    private static final String HEADER_PREFIX = "Bearer";
    private static final long LIFETIME = 123456L;
    private static final String SECRET = "secret";
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
    private static final String SUBJECT = "subject";

    private HttpServletResponse response;

    private JsonWebTokens jsonWebTokens;

    @Before
    public void setUp() {
        this.response = mock(HttpServletResponse.class);

        jsonWebTokens = JsonWebTokens.of(JsonWebTokens.Config
                .create(SIGNATURE_ALGORITHM, SECRET, LIFETIME, HEADER_PREFIX));
    }

    @Test
    public void generateAToken() {

        String token = jsonWebTokens.generate(SUBJECT);
        System.out.println(token);

        assertThat(token).isNotNull().isNotEmpty().hasSize(171);
    }

    @Test
    public void generateTokenAsAHeaderInTheResponse() {

        Capture<String> header = newCapture();
        response.addHeader(eq(HttpHeaders.AUTHORIZATION), capture(header));

        replayAll();
        jsonWebTokens.generateToResponse(SUBJECT, response);
        verifyAll();

        assertThat(header.getValue()).contains("Bearer ");
    }

    @Test
    public void parseATokenToReturnItsClaims() {

        String token = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(SUBJECT)
                .signWith(SIGNATURE_ALGORITHM, SECRET)
                .compact();

        Jws<Claims> claims = jsonWebTokens.parse(token);
        String subject = claims.getBody().getSubject();

        assertThat(subject).isEqualTo(SUBJECT);
    }
}