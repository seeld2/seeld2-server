package be.seeld.common.security.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import test.Unit;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonWebKeyUTest extends Unit {

    private static final String ALG = "A256GCM";
    private static final boolean EXT = true;
    private static final String K = "Bb1tVukizt3GqFopFf41xPMMNERYUR43cNPVReJyWUs";
    private static final String KEY_OPS = "[\"encrypt\",\"decrypt\"]";
    private static final String[] KEY_OPS_ARRAY = {"encrypt","decrypt"};
    private static final String KTY = "oct";

    private static final String JSON = "{" +
            "\"alg\":\"" + ALG + "\"," +
            "\"ext\":" + EXT + "," +
            "\"k\":\"" + K + "\"," +
            "\"key_ops\":" + KEY_OPS + "," +
            "\"kty\":\"" + KTY + "\"" +
            "}";

    @Test
    public void instantiateFromJSON() throws IOException {

        JsonWebKey jsonWebKey = JsonWebKey.fromJSON(JSON, JsonWebKey.class);

        assertThat(jsonWebKey)
                .hasFieldOrPropertyWithValue("alg", ALG)
                .hasFieldOrPropertyWithValue("ext", EXT)
                .hasFieldOrPropertyWithValue("k", K)
                .hasFieldOrPropertyWithValue("key_ops", KEY_OPS_ARRAY)
                .hasFieldOrPropertyWithValue("kty", KTY);
    }

    @Test
    public void generateJSONStringFromAnInstance() throws JsonProcessingException {

        JsonWebKey jsonWebKey = JsonWebKey.builder().alg(ALG).ext(EXT).k(K).key_ops(KEY_OPS_ARRAY).kty(KTY).build();

        String json = jsonWebKey.asJSON();

        assertThat(json).isEqualTo(JSON);
    }
}