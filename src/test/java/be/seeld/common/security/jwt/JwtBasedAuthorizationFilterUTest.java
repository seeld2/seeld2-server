package be.seeld.common.security.jwt;

import be.seeld.users.UserFixture;
import be.seeld.common.security.jwt.JwtBasedAuthorizationFilter.Config;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.easymock.Mock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import test.Unit;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;

public class JwtBasedAuthorizationFilterUTest extends Unit {

    private static final String HEADER_PREFIX = "Bearer ";
    private static final String SECRET = "Shhh";

    private static final String NO_TOKEN = null;
    private static final long ONE_MINUTE = 60000L;
    private static final String USERNAME = UserFixture.USERNAME;

    private JwtBasedAuthorizationFilter filter;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(USERNAME, null, Collections.emptyList()));
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void rejectAuthorizationWhenNoTokenIsProvidedInHeader() throws IOException, ServletException {
        filter = newJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(NO_TOKEN);
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);

        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void rejectAuthorizationWhenTokenInHeaderDoesNotStartWithTheRightPrefix() throws IOException, ServletException {
        filter = newJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(token());
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);

        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void authorizeUserForAValidTokenInHeader() throws IOException, ServletException {
        filter = newJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(HEADER_PREFIX + token());
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertThat(authentication)
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", USERNAME);
    }

    @Test(expected = AccessDeniedException.class)
    public void rejectAuthorizationIfTheSignatureAlgorithmIsIncorrect() throws IOException, ServletException {
        filter = newJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(HEADER_PREFIX + tokenWithIncorrectSignatureAlgorithm());
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    private void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        replayAll();
        filter.doFilterInternal(request, response, filterChain);
        verifyAll();
    }

    private JwtBasedAuthorizationFilter newJwtBasedAuthorizationFilter() {
        JsonWebTokens jsonWebTokens = JsonWebTokens.of(JsonWebTokens.Config.create(
                SignatureAlgorithm.HS512, SECRET, 1000000L, HEADER_PREFIX));

        return new JwtBasedAuthorizationFilter(authenticationManager,
                jsonWebTokens,
                Config.create(HEADER_PREFIX));
    }

    private String token() {
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(USERNAME)
                .setExpiration(new Date(System.currentTimeMillis() + JwtBasedAuthorizationFilterUTest.ONE_MINUTE))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private String tokenWithIncorrectSignatureAlgorithm() {
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(USERNAME)
                .setExpiration(new Date(System.currentTimeMillis() + ONE_MINUTE))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }
}
