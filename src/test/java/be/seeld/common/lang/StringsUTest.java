package be.seeld.common.lang;

import org.junit.Test;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;

public class StringsUTest extends Unit {

    @Test
    public void generateAStringOfGivenLengthUsingAGivenTextToRepeat() {
        String generated = Strings.ofLength(14, "test");
        assertThat(generated)
                .isNotEmpty()
                .hasSize(14)
                .isEqualTo("testtesttestte");
    }

    @Test
    public void repeatATextNTimesAndJoinItWithTheSpecifiedSeparator() {
        String generated = Strings.repeatAndJoin("blah", 3, ",");
        assertThat(generated).isEqualTo("blah,blah,blah");
    }

    @Test
    public void reverseAString() {
        String reversed = Strings.reverse("abcd");
        assertThat(reversed).isEqualTo("dcba");
    }
}
