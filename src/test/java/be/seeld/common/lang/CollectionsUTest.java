package be.seeld.common.lang;

import org.junit.Test;
import test.Unit;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectionsUTest extends Unit {

    @Test
    public void shouldBuildAListOfItemsProvidedAsVarArgs() {
        List<Integer> list = Collections.listOf(1, 2, 3);
        assertThat(list).containsExactly(1, 2, 3);
    }

    @Test
    public void shouldPartitionASetOfItemsIntoAListOfSubsets() {
        Set<Integer> set = Arrays.stream(new Integer[]{1, 2, 3, 4, 5, 6, 7}).collect(Collectors.toSet());

        List<Set<Integer>> subsets = Collections.partitionOf(set, 3);

        assertThat(subsets).hasSize(3);
        assertThat(subsets.get(0)).containsExactly(1, 2, 3);
        assertThat(subsets.get(1)).containsExactly(4, 5, 6);
        assertThat(subsets.get(2)).containsExactly(7);
    }

    @Test
    public void shouldBuildASetOfItemsProvidedAsVarArgs() {
        Set<Integer> set = Collections.setOf(1, 2, 3);
        assertThat(set).containsExactly(1, 2, 3);
    }
}