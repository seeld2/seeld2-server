package be.seeld.common.lang;

import org.junit.Test;
import test.Unit;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class BytesUTest extends Unit {

    @Test
    public void concatenateToArraysOfBytes() {

        byte[] one = new byte[]{1, 2, 3};
        byte[] two = new byte[]{4, 5, 6, 7};

        Optional<byte[]> concatenatedArray = Bytes.concat(one, two);

        assertThat(concatenatedArray).isPresent();
        assertThat(concatenatedArray.get())
                .hasSize(7)
                .containsSequence(1, 2, 3, 4, 5, 6, 7);
    }
}