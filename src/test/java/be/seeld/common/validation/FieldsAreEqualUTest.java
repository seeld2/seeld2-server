package be.seeld.common.validation;

import org.junit.Test;
import test.Unit;

public class FieldsAreEqualUTest extends Unit {

	@Test
	public void invalidateBeanWhoseThreeFieldsAreNotEqual() {
		ThreeFieldsBean bean = new ThreeFieldsBean("abc", "def", "abc");

        assertThatValidationViolationAmountIs(1, bean);
	}

	@Test
	public void invalidateBeanWhoseTwoFieldsAreNotEqual() {
		TwoFieldsBean bean = new TwoFieldsBean("abc", "def");

        assertThatValidationViolationAmountIs(1, bean);
	}

	@Test
	public void validateBeanWhoseThreeFieldsAreEqual() {
		ThreeFieldsBean bean = new ThreeFieldsBean("oxo", "oxo", "oxo");

        assertThatValidationViolationAmountIs(0, bean);
	}

	@Test
	public void validateBeanWhoseTwoFieldsAreEqual() {
		TwoFieldsBean bean = new TwoFieldsBean("oxo", "oxo");

        assertThatValidationViolationAmountIs(0, bean);
	}

	@FieldsAreEqual(fieldsNames = {"field1", "field2", "field3"})
	public class ThreeFieldsBean {
		private String field1;
		private String field2;
		private String field3;
		public ThreeFieldsBean(String field1, String field2, String field3) {
			this.field1 = field1;
			this.field2 = field2;
			this.field3 = field3;
		}
	}

	@FieldsAreEqual(fieldsNames = {"field1", "field2"})
	public class TwoFieldsBean {
		private String field1;
		private String field2;
		public TwoFieldsBean(String field1, String field2) {
			this.field1 = field1;
			this.field2 = field2;
		}
	}
}
