package be.seeld.common.encryption.aes;

import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import javax.crypto.SecretKey;
import java.util.Arrays;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class AESUTest {

    private static final int IV_SIZE = 12;
    private static final String MODE = "GCM";
    private static final String PADDING = "NoPadding";
    private static final String SECRET_KEY_HEX_ENCODED = "10ac85e33764766841ea66aeb7e2c4ca83ebf12df9ab00ba3ece2e2121c250d8";
    private static final String SECRET_KEY_URL_ENCODED = "V1npla1V6_uU2ArICCw5NO2k0_dtcV85nsqjNE1XSyo";
    private static final int SECRET_KEY_SIZE = 256;
    private static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";

    private static final String DATA_ENCRYPTED_AS_BASE64_STRING = "q3YTXdCq8cYA64qnJVjOmvQXRYYM1cJQADS727EIzcrUD14BLF1MzddPqblOra2jDskGGcqpuJVF-g==";
    private static final int[] DATA_ENCRYPTED_AS_UNSIGNED_BYTES = new int[]{90, 220, 215, 36, 197, 5, 93, 216, 240, 90, 232, 203, 143, 186, 46, 203, 212, 32, 56, 197, 149, 221, 227, 104, 74, 25, 113, 214, 52, 20, 204, 21, 9, 173, 174, 227, 16, 211, 131, 101, 232, 191, 2, 102, 45, 36, 74, 45, 124, 168, 110, 10, 96, 68, 152, 67, 37, 252};
    private static final String DATA_ENCRYPTED_WITH_HEX_ENCODED_KEY = "00258a6a22d361138cd3a539f727c36fd85b77c81ee71d1540741a0c8b7ca2ee022245f2503c4f1df186258fb46b4e92bf218d9658398032c1c4";
    private static final String DATA_ENCRYPTED_WITH_URL_ENCODED_KEY = "fa352cba8c149e83ab6febe50a8a165bfb912aa36b26a378b00d15d36e2a8e1c6500e308b5cf27d5ea5599a4c3749cad60e3d3db3df8baccb927";
    private static final String DATA_UNENCRYPTED = "This is what must be encrypted";

    public static class WhenGeneratingKeys extends Unit {

        @Test
        public void generateAHex_256_SecretKey() {
            SecretKey secretKey = aesWithHexEncodedKey(256).generateSecretKey();
            String key = Hex.toHexString(secretKey.getEncoded());
            assertThat(key).hasSize(64);
            System.out.println("Hex: generateASecretKey(): " + key);
        }

        @Test
        public void generateAHex_192_SecretKey() {
            SecretKey secretKey = aesWithHexEncodedKey(192).generateSecretKey();
            String key = Hex.toHexString(secretKey.getEncoded());
            assertThat(key).hasSize(48);
            System.out.println("Hex: generateASecretKey(): " + key);
        }

        @Test
        public void generateAHex_128_SecretKey() {
            SecretKey secretKey = aesWithHexEncodedKey(128).generateSecretKey();
            String key = Hex.toHexString(secretKey.getEncoded());
            assertThat(key).hasSize(32);
            System.out.println("Hex: generateASecretKey(): " + key);
        }

        @Test
        public void generateBase64EncodedSecretKey() {
            SecretKey secretKey = aesWithBase64EncodedKey().generateSecretKey();
            String key = Base64.getUrlEncoder().encodeToString(secretKey.getEncoded());
            assertThat(key).hasSize(44);
            System.out.println("Base64: generateASecretKey(): " + key);
        }

        @Test
        public void generateBase64EncodedSecretKeyAsBase64EncodedString() {
            String key = aesWithBase64EncodedKey().generateSecretKeyAsBase64();
            assertThat(key).hasSize(43);
            System.out.println("generateASecretKeyAsBase64(): " + key);
        }
    }

    public static class WhenWorkingWithBytesAsData extends Unit {

        @Test
        public void encryptBytesUsingHexEncodedKey() {
            byte[] encrypted = aesWithHexEncodedKey().encrypt(DATA_UNENCRYPTED.getBytes(UTF_8));
            assertThat(encrypted).isNotEmpty().hasSize(58);
            System.out.println("encryptBytesUsingHexEncodedKey(): " + new String(Hex.encode(encrypted)));
        }

        @Test
        public void encryptBytesUsingUrlEncodedKey() {
            byte[] encrypted = aesWithBase64EncodedKey().encrypt(DATA_UNENCRYPTED.getBytes(UTF_8));
            assertThat(encrypted).isNotEmpty().hasSize(58);
            System.out.println("encryptBytesUsingUrlEncodedKey(): " + new String(Hex.encode(encrypted)));
        }

        @Test
        public void decryptBytesUsingHexEncodedKey() {
            byte[] unencrypted = aesWithHexEncodedKey().decrypt(Hex.decode(DATA_ENCRYPTED_WITH_HEX_ENCODED_KEY.getBytes(UTF_8)));
            assertThat(unencrypted).isNotEmpty();
            assertThat(new String(unencrypted)).isEqualTo(DATA_UNENCRYPTED);
        }

        @Test
        public void decryptBytesUsingUrlEncodedKey() {
            byte[] unencrypted = aesWithBase64EncodedKey().decrypt(Hex.decode(DATA_ENCRYPTED_WITH_URL_ENCODED_KEY.getBytes(UTF_8)));
            assertThat(unencrypted).isNotEmpty();
            assertThat(new String(unencrypted)).isEqualTo(DATA_UNENCRYPTED);
        }

        @Test
        public void decryptUnsignedBytesUsingProvidedSecretKey() {
            String unencrypted = aesNoBase64EncodedKey().decrypt(DATA_ENCRYPTED_AS_UNSIGNED_BYTES, SECRET_KEY_URL_ENCODED);
            assertThat(unencrypted).isEqualTo(DATA_UNENCRYPTED);
        }
    }

    public static class WhenWorkingWithStringsAsData extends Unit {

        @Test
        public void encryptStringUsingUrlEncodedKey() {
            String encrypted = aesWithBase64EncodedKey().encrypt(DATA_UNENCRYPTED);
            assertThat(encrypted).isNotEmpty().hasSize(116);
            System.out.println("encryptStringUsingUrlEncodedKey(): " + encrypted);
        }

        @Test
        public void encryptStringWithProvidedSecretKey() {
            String encrypted = aesNoHexEncodedKey().encrypt(DATA_UNENCRYPTED, SECRET_KEY_HEX_ENCODED);
            assertThat(encrypted).isNotEmpty().hasSize(116);
            System.out.println("encryptStringWithProvidedSecretKey(): " + encrypted);
        }

        @Test
        public void encryptStringToBase64WithProvidedSecretKey() {
            String encrypted = aesNoBase64EncodedKey().encryptToBase64(DATA_UNENCRYPTED, SECRET_KEY_URL_ENCODED);
            assertThat(encrypted).isNotEmpty().hasSize(80);
            System.out.println("encryptStringWithProvidedSecretKey(): " + encrypted);
        }

        @Test
        public void encryptStringToUnsignedBytesWithProvidedUrlEncodedSecretKey() {
            int[] encrypted = aesNoBase64EncodedKey().encryptToUnsignedBytes(DATA_UNENCRYPTED, SECRET_KEY_URL_ENCODED);
            assertThat(encrypted).isNotEmpty().hasSize(58);
            System.out.println("encryptStringToUnsignedBytesWithProvidedUrlEncodedSecretKey(): " + Arrays.toString(encrypted));
        }

        @Test
        public void decryptStringUsingUrlEncodedKey() {
            String unencrypted = aesWithBase64EncodedKey().decrypt(DATA_ENCRYPTED_WITH_URL_ENCODED_KEY);
            assertThat(unencrypted).isEqualTo(DATA_UNENCRYPTED);
        }

        @Test
        public void decryptStringUsingProvidedSecretKey() {
            String unencrypted = aesNoHexEncodedKey().decrypt(DATA_ENCRYPTED_WITH_HEX_ENCODED_KEY, SECRET_KEY_HEX_ENCODED);
            assertThat(unencrypted).isEqualTo(DATA_UNENCRYPTED);
        }

        @Test
        public void decryptBase64StringUsingProvidedSecretKey() {
            String unencrypted = aesNoBase64EncodedKey().decryptBase64(DATA_ENCRYPTED_AS_BASE64_STRING, SECRET_KEY_URL_ENCODED);
            assertThat(unencrypted).isEqualTo(DATA_UNENCRYPTED);
        }
    }

    public static class Other extends Unit {

        @Test(expected = AES.AESException.class)
        public void raiseExceptionWhenSecretKeyIsMissingAtEncryption() {
            aesNoHexEncodedKey().encrypt(DATA_UNENCRYPTED.getBytes(UTF_8));
        }

        @Test(expected = AES.AESException.class)
        public void raiseExceptionWhenSecretKeyIsMissingAtDecryption() {
            aesNoHexEncodedKey().decrypt(Hex.decode(DATA_ENCRYPTED_WITH_HEX_ENCODED_KEY.getBytes(UTF_8)));
        }
    }

    private static AES aesNoHexEncodedKey() {
        return AES.of(new AES.Config(SECURE_RANDOM_ALGORITHM, MODE, PADDING, null, SECRET_KEY_SIZE,
                AES.SecretKeyType.HEX, IV_SIZE));
    }

    private static AES aesNoBase64EncodedKey() {
        return AES.of(new AES.Config(SECURE_RANDOM_ALGORITHM, MODE, PADDING, null, SECRET_KEY_SIZE,
                AES.SecretKeyType.BASE64, IV_SIZE));
    }

    private static AES aesWithBase64EncodedKey() {
        return AES.of(new AES.Config(SECURE_RANDOM_ALGORITHM, MODE, PADDING,
                SECRET_KEY_URL_ENCODED, SECRET_KEY_SIZE, AES.SecretKeyType.BASE64, IV_SIZE));
    }

    private static AES aesWithHexEncodedKey() {
        return aesWithHexEncodedKey(SECRET_KEY_SIZE);
    }

    private static AES aesWithHexEncodedKey(int secretKeySize) {
        return AES.of(new AES.Config(SECURE_RANDOM_ALGORITHM, MODE, PADDING,
                SECRET_KEY_HEX_ENCODED, secretKeySize, AES.SecretKeyType.HEX, IV_SIZE));
    }
}