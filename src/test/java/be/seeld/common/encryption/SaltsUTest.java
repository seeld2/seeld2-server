package be.seeld.common.encryption;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import test.Unit;

import java.security.SecureRandom;

public class SaltsUTest extends Unit {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private Salts salts;

    @Before
    public void setUp() {
        salts = new Salts(SECURE_RANDOM);
    }

    @Ignore // This test freezes at times for unknown reasons
    @Test(timeout = 3000L)
    public void generateARandomSaltForTheSpecifiedAlgorithm() {
        String salt = salts.generateRandomSalt("SHA-256", 64);
        Assertions.assertThat(salt).isNotNull().hasSize(64);
    }

    @Test(expected = Salts.SaltsException.class)
    public void throwAnExceptionWhenWrongAlgorithmIsSpecified() {
        salts.generateRandomSalt("WHATEVER", 64);
    }
}