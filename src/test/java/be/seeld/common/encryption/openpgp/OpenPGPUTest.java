package be.seeld.common.encryption.openpgp;

import be.seeld.common.encryption.openpgp.OpenPGP.EncryptionResult;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;

public class OpenPGPUTest extends Unit {

    private static final int KEY_SIZE = 1024;

    private static final String USER_ID_NAME = "userId";
    private static final String PASSPHRASE = "passphrase";
    private static final String OTHER_USER_ID_NAME = "otherUserId";
    private static final String OTHER_PASSPHRASE = "otherPassphrase";

    private static final String USER_ID_SUFFIX = "@test.com";

    private static OpenPGP.Config config;
    private static OpenPGP.Config configWithNoKeyRings;
    private static OpenPGP.Config otherConfig;

    @BeforeClass
    public static void setUpClass() {
        config = OpenPGP.Config.create(USER_ID_NAME, USER_ID_SUFFIX, PASSPHRASE, KEY_SIZE, PGP_PUBLIC_KEY_RING, PGP_SECRET_KEY_RING);
        configWithNoKeyRings = OpenPGP.Config.create(USER_ID_NAME, USER_ID_SUFFIX, PASSPHRASE, KEY_SIZE, null, null);
        otherConfig = OpenPGP.Config.create(OTHER_USER_ID_NAME, USER_ID_SUFFIX, OTHER_PASSPHRASE, KEY_SIZE, OTHER_PGP_PUBLIC_KEY_RING, OTHER_PGP_SECRET_KEY_RING);
    }

    @Test
    public void createAnInstanceThatGeneratesKeyRings() {
        OpenPGP openPGP = OpenPGP.create(configWithNoKeyRings);

        assertThat(openPGP).hasNoNullFieldsOrProperties();

        String armoredPublicKeyRing = extractArmoredPublicKeyRingFrom(openPGP);
        assertThat(armoredPublicKeyRing).isNotEmpty();

        String armoredSecretKeyRing = extractArmoredSecretKeyRingFrom(openPGP);
        assertThat(armoredSecretKeyRing).isNotEmpty();

        System.out.println(armoredPublicKeyRing);
        System.out.println(armoredSecretKeyRing);
    }

    @Test
    public void createAnInstanceWithTheSpecifiedKeyRings() {
        OpenPGP openPGP = OpenPGP.create(config);

        assertThat(openPGP).hasNoNullFieldsOrProperties();

        assertThat(extractArmoredPublicKeyRingFrom(openPGP)).isEqualTo(PGP_PUBLIC_KEY_RING);
        assertThat(extractArmoredSecretKeyRingFrom(openPGP)).isEqualTo(PGP_SECRET_KEY_RING);
    }

    @Test
    public void generateNewPublicAndSecretKeyRings() {
        OpenPGP openPGP = OpenPGP.create(config);

        assertThat(openPGP).hasNoNullFieldsOrProperties();

        String armoredPublicKeyRing = extractArmoredPublicKeyRingFrom(openPGP);
        String armoredSecretKeyRing = extractArmoredSecretKeyRingFrom(openPGP);

        openPGP.generateNewKeys();

        assertThat(openPGP).hasNoNullFieldsOrProperties();

        String newArmoredPublicKeyRing = extractArmoredPublicKeyRingFrom(openPGP);
        String newArmoredSecretKeyRing = extractArmoredSecretKeyRingFrom(openPGP);

        assertThat(newArmoredPublicKeyRing).isNotEqualTo(armoredPublicKeyRing);
        assertThat(newArmoredSecretKeyRing).isNotEqualTo(armoredSecretKeyRing);
    }

    @Test
    public void encryptAMessage() {
        OpenPGP openPGP = OpenPGP.create(config);

        EncryptionResult encryptionResult = openPGP.encryptAndSign(UNENCRYPTED_MESSAGE, OTHER_PGP_PUBLIC_KEY_RING, OTHER_USER_ID_NAME);

        assertThat(encryptionResult.getPayload()).isNotNull();
        assertThat(encryptionResult.getArmoredSigningPublicKey()).isNotNull();

        System.out.println(encryptionResult.getPayload());
    }

    @Test
    public void decryptAMessageWithoutCheckingTheSignature() {
        OpenPGP openPGP = OpenPGP.create(otherConfig);

        String decryptedMessage = openPGP.decrypt(MESSAGE_ENCRYPTED_TO_OTHER);

        assertThat(decryptedMessage)
                .isNotEmpty()
                .isEqualTo(UNENCRYPTED_MESSAGE);
    }

    @Test
    public void decryptAMessageAndCheckTheSignature() {
        OpenPGP openPGP = OpenPGP.create(otherConfig);

        String decryptedMessage = openPGP.decryptAndVerify(MESSAGE_ENCRYPTED_TO_OTHER, PGP_PUBLIC_KEY_RING, USER_ID_NAME);

        assertThat(decryptedMessage)
                .isNotEmpty()
                .isEqualTo(UNENCRYPTED_MESSAGE);
    }

    private String extractArmoredPublicKeyRingFrom(OpenPGP openPGP) {
        OpenPGP.Config config = (OpenPGP.Config) ReflectionTestUtils.getField(openPGP, "config");
        return (String) ReflectionTestUtils.getField(config, "armoredPublicKeyRing");
    }

    private String extractArmoredSecretKeyRingFrom(OpenPGP openPGP) {
        OpenPGP.Config config = (OpenPGP.Config) ReflectionTestUtils.getField(openPGP, "config");
        return (String) ReflectionTestUtils.getField(config, "armoredSecretKeyRing");
    }

    private static final String PGP_PUBLIC_KEY_RING = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "mI0EWxUWvQMEANWcJkZhKoRQ5266feeXBOACTfO9kNvXnk5pmrmiiz8Tp6yWbzx8\n" +
            "RZ7osE5Af5XVc/3WZPIql6zF1Ux1HLh6QSZJzqgmk8+pgAcQ743m6HXa6oibuNrd\n" +
            "95Pl9FrL0UGOvxvBHwXOIqOtTeHgrx3RA55jop59bo3AcR4B7p9XSKYPABEBAAG0\n" +
            "GHVzZXJJZCA8dXNlcklkQHRlc3QuY29tPoioBBMDAgASBQJbFRa9AhsDAgsJAhUK\n" +
            "Ah4BAAoJEOyDeRITrLUu4QcEAJ+DvVVC9BN0JdSooBDGoCLxwMBrh8lF5KBaLfXY\n" +
            "vjvcPQWXMNBm70dD2+BPz4Oet7XdJaldMj2TlmmrJZEm0KUfiDYQWosdAfMg4jgd\n" +
            "KogdHp0vVr8Ei5K2Exlnih9GgNCc/+SgGT0v3C+OfO9uAGiyxJXQ97RGraoxIXa5\n" +
            "yRNDuI0EWxUWvQEEAKx/UpXNe5c6RdmgFpm8xBd9o5+wCYp6Z5HN3dVn+UU7SEj+\n" +
            "YyVfVys0wmAKXjN7vJ0QSgansVEA1xSkWPN2k6lOap27fsyWAg4Iwv7B7X7r6va1\n" +
            "jSga81wSVg+dkDbPJIPkE5EP3B5kN/N/rEKeYOPrHzd0OWCbT4/7l54g1JG/ABEB\n" +
            "AAGInwQYAwIACQUCWxUWvQIbDAAKCRDsg3kSE6y1LpylBADNZkzW2tZJHmX5cQOO\n" +
            "/xVbzQsdIEFyD9Uu+XyG4s7P4KOuU4F53liRIIoNMe+Uli19mjAFK00FykXRwVLz\n" +
            "DVtCbIcH0/z/HA/qy5D8DNCbvps6yhUmp6zEURHjf378uR42o0Tyr5Gy6v6oPjwq\n" +
            "kV8YdBrQOSISxKj5KdeSkdnn0w==\n" +
            "=1bGD\n" +
            "-----END PGP PUBLIC KEY BLOCK-----\n";
    private static final String PGP_SECRET_KEY_RING = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "lQIGBFsVFr0DBADVnCZGYSqEUOduun3nlwTgAk3zvZDb155OaZq5oos/E6eslm88\n" +
            "fEWe6LBOQH+V1XP91mTyKpesxdVMdRy4ekEmSc6oJpPPqYAHEO+N5uh12uqIm7ja\n" +
            "3feT5fRay9FBjr8bwR8FziKjrU3h4K8d0QOeY6KefW6NwHEeAe6fV0imDwARAQAB\n" +
            "/gkDCC0JgxxyIdmewI1rqAf/8DXJNzfQ1ZnDBBoZafslwBMjzX7TMQSQ0xYqSAaL\n" +
            "arDQh4PQcuIx+QpaM2JWmzh1EA0hpJd09H3AgcKWG/S9sQkD5jg917nsEdXjsB+J\n" +
            "jCiqBFUbYL55h74RheF78Vs5RZxiobIEZ2oXe9K8mP+vXShsD9YPBx9PB6TSmuR3\n" +
            "04KOqFN1I4PV96AcrfpnTHdRvBG3ObA4BHWfsPJ8zwmbZoGCMlIQW754g3HdsWHl\n" +
            "VB9Waj9KhBO1Q98RFCdNYP6CSkpSh9U4uRNNUB0h/v6iJ7gXBUpYctbt6p0/WeTV\n" +
            "Mq7vDXa5XYfFPzakSRSZliQ4MSdhT6OhG32LEoTXMWOxL7UaJoGXanKLIP6hxn9M\n" +
            "NJRm28LG2swVgARn/c5bz6+MBp3ur/qC2SZbl9bWSKis8hTU+gLfwNc6w+XFb+Fd\n" +
            "sceHmQBgGQKzw+PMYl4uecl135FDN9Kn+aucE1lkgMLNWRLtC+544CC0GHVzZXJJ\n" +
            "ZCA8dXNlcklkQHRlc3QuY29tPoioBBMDAgASBQJbFRa9AhsDAgsJAhUKAh4BAAoJ\n" +
            "EOyDeRITrLUu4QcEAJ+DvVVC9BN0JdSooBDGoCLxwMBrh8lF5KBaLfXYvjvcPQWX\n" +
            "MNBm70dD2+BPz4Oet7XdJaldMj2TlmmrJZEm0KUfiDYQWosdAfMg4jgdKogdHp0v\n" +
            "Vr8Ei5K2Exlnih9GgNCc/+SgGT0v3C+OfO9uAGiyxJXQ97RGraoxIXa5yRNDnQIG\n" +
            "BFsVFr0BBACsf1KVzXuXOkXZoBaZvMQXfaOfsAmKemeRzd3VZ/lFO0hI/mMlX1cr\n" +
            "NMJgCl4ze7ydEEoGp7FRANcUpFjzdpOpTmqdu37MlgIOCML+we1+6+r2tY0oGvNc\n" +
            "ElYPnZA2zySD5BORD9weZDfzf6xCnmDj6x83dDlgm0+P+5eeINSRvwARAQAB/gkD\n" +
            "CC0JgxxyIdmewB7bDS38/Q1f8986e+A4zJjJnhA4j9n93qmnTU3IFnVOcZKdYvoy\n" +
            "AyGi85b/rTDiIHbGMDgIFZD80M48TVvd77tdI27Wc36joj+RoisdJmn97UWAOEFo\n" +
            "7gmxb7dHONxEvVX8xm+AqEh+27qbOJ4xNIEptLQslo7jPwDb5dnpRCqyUBFLeS/L\n" +
            "0sSeHull27XsH21ZZ6WJaT2qndfocENYErJmX9GJILYxZPzajZ5IrFZvQjwvUdKd\n" +
            "1FhoHcVn9V3LsRRIdYkOa88XjIqGjudWnEcy/u/WwHK4WIQ8i1byOyO4/D1O+mdM\n" +
            "mkr177Nhq0SzplczFEx9dUY7E38s3UlBbxQ8eo6lo8x9fJEubrp148JLj9FkUqmt\n" +
            "XX5jw1tQv9A5jJQ6gWocDwI9X3kjxFGpcsD1B8GUAS1DPaPe+cv49lwSenjRZ7hN\n" +
            "R7m9nxoAauiJHXolq2s+ir+Ti6hxBvU9lvIWEbRBfKOf+iuhYMSInwQYAwIACQUC\n" +
            "WxUWvQIbDAAKCRDsg3kSE6y1LpylBADNZkzW2tZJHmX5cQOO/xVbzQsdIEFyD9Uu\n" +
            "+XyG4s7P4KOuU4F53liRIIoNMe+Uli19mjAFK00FykXRwVLzDVtCbIcH0/z/HA/q\n" +
            "y5D8DNCbvps6yhUmp6zEURHjf378uR42o0Tyr5Gy6v6oPjwqkV8YdBrQOSISxKj5\n" +
            "KdeSkdnn0w==\n" +
            "=tnLc\n" +
            "-----END PGP PRIVATE KEY BLOCK-----\n";

    private static final String OTHER_PGP_PUBLIC_KEY_RING = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "mI0EWxUXOwMEAKuzUqadiv0WQ9Oge0IFp6iE9rM5sek7NeT+yWmLPFSTA8QKZEFI\n" +
            "UPtbTURRcO4sJBwW32UfN5VlXMJT8QB/PdHxKgkJhwAEjkZAwQ1qNHP+gogIL/mT\n" +
            "prIrfS9K1OXwRx1DVDHgGf0xbb9+loDxG5iIzKIFUicGToD3fbvz8YFBABEBAAG0\n" +
            "Im90aGVyVXNlcklkIDxvdGhlclVzZXJJZEB0ZXN0LmNvbT6IqAQTAwIAEgUCWxUX\n" +
            "OwIbAwILCQIVCgIeAQAKCRAm2Vbs9gqtZe+2BACfRX1GvcuIsMtJJo/w5/aUDzzx\n" +
            "4c8OvivjXcfltnzHb0ZxLQyqs91aq/des7sHm365wrM9hX2vLXj8kL18wf7sfGsX\n" +
            "oh5OSRQFMWlQmRMn1rnhhYef5aevl9ZrNbNOVOfcTrs0tnL8YcpHVKCAg1uzkg24\n" +
            "Xe7ZyGwxkmhZJvWN/7iNBFsVFzsBBADWcweDGBAdmymv32d3J8KFzE7/7NDAe/PZ\n" +
            "aQbaBblzqD5bTMkYPwpxd8KPkAxe0AqCWo7oMMiY/JI7Rlik7VQ73J4FiMRwUAku\n" +
            "gjjF/Prn1i/MjAdvdOf+ktL7I0tuojjNsbCPoDYb48eJb978TDGmS8nK4VaqM4Gz\n" +
            "Jl3mIv/ENQARAQABiJ8EGAMCAAkFAlsVFzsCGwwACgkQJtlW7PYKrWU+ZAP/Vu3N\n" +
            "Ckkg5T4AEiCDVLYREjSXLhjNVd8ah/A64u5ox+Zl1hbVI+y9k1U02sZfTQPTYqW7\n" +
            "L7RZZu/MD4p1oo8iKPtTEAvJ6cqMvyhl0LLwMIG2DOSUwQlRBnSfdQYRZFg/TnKi\n" +
            "j+YQGpPUDP0WMNxdTXsxLdgheoJHbcCtP5rqrTY=\n" +
            "=EiNL\n" +
            "-----END PGP PUBLIC KEY BLOCK-----\n";
    private static final String OTHER_PGP_SECRET_KEY_RING = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "lQIFBFsVFzsDBACrs1KmnYr9FkPToHtCBaeohPazObHpOzXk/slpizxUkwPECmRB\n" +
            "SFD7W01EUXDuLCQcFt9lHzeVZVzCU/EAfz3R8SoJCYcABI5GQMENajRz/oKICC/5\n" +
            "k6ayK30vStTl8EcdQ1Qx4Bn9MW2/fpaA8RuYiMyiBVInBk6A93278/GBQQARAQAB\n" +
            "/gkDCMJyJCP9oE3IwDoEzbmFTo2iAcJ9UoTawTGl3TPUCQb1uV+0dF0HVMPhx9+7\n" +
            "qjw5RnQN/OkVGutHGlOjpRg2+pMFz+9qI47V4p9VLJQKfgGkP+OuEUy1fbuAM/GG\n" +
            "P2bvneJuOqouTfKeWACBrcTBT6SO4E9BbLcHFQ87cTR9xIRiRmM65yrS1n8IeYqm\n" +
            "wKzmNGlKVXca2g5vQFFnW55l548XCT1BT+sAbXJC2hIY2PuBsZ9vK7tTpZSNvA0X\n" +
            "cXbzLKsXIk/s/RO2zkhKT3GF4qf2Vl1xqcncSRVOXrUogvsn8ZVMGZggTDSYbMLa\n" +
            "sSteTSaMr2RTLpCjGKE9RBjuyn+Ue/U5mK0XeCXu2fEtPUCyLNvecJxxaQqLmEVk\n" +
            "yQvQO/knOReOsYZQrDWsHt+Tk2CExQJUwz23lpScOYWWXcTmfbMu9By8g4rL+nba\n" +
            "s/RnCETcsrlZaYhCWt06et7jW0VsGBpvgVbMDbu09Afz5n0as2unDLQib3RoZXJV\n" +
            "c2VySWQgPG90aGVyVXNlcklkQHRlc3QuY29tPoioBBMDAgASBQJbFRc7AhsDAgsJ\n" +
            "AhUKAh4BAAoJECbZVuz2Cq1l77YEAJ9FfUa9y4iwy0kmj/Dn9pQPPPHhzw6+K+Nd\n" +
            "x+W2fMdvRnEtDKqz3Vqr916zuwebfrnCsz2Ffa8tePyQvXzB/ux8axeiHk5JFAUx\n" +
            "aVCZEyfWueGFh5/lp6+X1ms1s05U59xOuzS2cvxhykdUoICDW7OSDbhd7tnIbDGS\n" +
            "aFkm9Y3/nQIFBFsVFzsBBADWcweDGBAdmymv32d3J8KFzE7/7NDAe/PZaQbaBblz\n" +
            "qD5bTMkYPwpxd8KPkAxe0AqCWo7oMMiY/JI7Rlik7VQ73J4FiMRwUAkugjjF/Prn\n" +
            "1i/MjAdvdOf+ktL7I0tuojjNsbCPoDYb48eJb978TDGmS8nK4VaqM4GzJl3mIv/E\n" +
            "NQARAQAB/gkDCMJyJCP9oE3IwIx8xQrMRhRSL/s3dzM1ZjnfbHLSqePO6OwHarU/\n" +
            "8lEsW4QzRVPFlUn1p4Xnl5+/E97p7//Od3EfGvAltqMdO22EmaqH7wJ5FSFHCr+J\n" +
            "qhCWitrkQrEVJY8ubOufUfSfmHMSnWPTmxw9pruXQmdBSAiMvK0qJrxm76Tf0QwO\n" +
            "oZiUmit4MPYGe86YtNbCHvdnz35/P6Dq76DuOerDmil7BMBw+XB+zNrLcZx+TVUk\n" +
            "WMqCXzCLBEdPrgAnYw0Wih5ljMeUV0DZd5zRQopJ7u2jYTT5eWA4Ah0ShwgD588l\n" +
            "9myn/a8NTuGOPUWVpPHna98UZlAkNwbM7Kh4HXwByliR+s+geWhM5SMMHTwZEkZr\n" +
            "aNWcKQ4y9uYuXIvphF0oXZTUx9OfGmha7TqEinJ8jq33HgJN+K4B/Khm3Ui/o4ga\n" +
            "NcDA3ZjNvc+oPNrQsNp2J6gAdLcfsckyOQ8uJk9XIzUbAG0yrbx5BO9KZpBsroif\n" +
            "BBgDAgAJBQJbFRc7AhsMAAoJECbZVuz2Cq1lPmQD/1btzQpJIOU+ABIgg1S2ERI0\n" +
            "ly4YzVXfGofwOuLuaMfmZdYW1SPsvZNVNNrGX00D02Kluy+0WWbvzA+KdaKPIij7\n" +
            "UxALyenKjL8oZdCy8DCBtgzklMEJUQZ0n3UGEWRYP05yoo/mEBqT1Az9FjDcXU17\n" +
            "MS3YIXqCR23ArT+a6q02\n" +
            "=1EhO\n" +
            "-----END PGP PRIVATE KEY BLOCK-----\n";

    private static final String UNENCRYPTED_MESSAGE = "Please encrypt me, I'm a very important secret message!";
    private static final String MESSAGE_ENCRYPTED_TO_OTHER = "-----BEGIN PGP MESSAGE-----\n" +
            "Version: BCPG v1.59\n" +
            "\n" +
            "hIwDTlhSdqqXUgwBA/9AIcP5Dw1wpW+OJWWV+xVLYhsatUYwIL/JEJD2k5JA+dF7\n" +
            "aY2LOCNo6U3qMI/3zhRix4P3EDLLbiXsAHskIdvzORAgVzXEtMeJA6LKXYz2snf5\n" +
            "bXVHGjX6rxjlZo2GffPjNmpiDWLgg92UQ5UOalQlriqFVoJ+I0Z40Tgv6VdPkNLA\n" +
            "cwE8qwZ+Y9at0t4y8sTsvCAaDfS0Z8pktCL6Nahp6WaWGJbyq5G4yslFXmMjfop8\n" +
            "I2b0NWNHfSqCx1s0qqMLarKHcv7WSVF1DKyIIAHhCXoi4bocDwUmmUrHdILQ1kur\n" +
            "sF9J85lpk3U7eyfBRR2kFx+nBKm38XOU+wRstlE0kkBrxub/yGY+KTQgYxXip+Ns\n" +
            "YIiWWSSJDKaZi3jFkiqvOVZtpQYYwF6KwZ4sb+USYO1CTZ/szsPlZyS34f2HrsSi\n" +
            "l36hzVzXQRJcEGDOO9fh0qdkb0hCimVz1fJsDGZ2eqjyE7rkj56GPosAsVixR1Kp\n" +
            "+bxdAflJSuPTnDN89zbjE/MQfMsapUyBA7+mTuST2XVzDu0XAGbh6G+8KubpcS3s\n" +
            "eA26Wd9gKRHnuC0SMYZFRC9/CRM=\n" +
            "=dOco\n" +
            "-----END PGP MESSAGE-----\n";
}
