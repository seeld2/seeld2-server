package be.seeld.common.encryption.openpgp;

import org.junit.Test;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;

public class PGPUserIdUTest extends Unit {

    @Test
    public void buildAnInstanceWhenProvidingAnEmailSuffixThatStartsWithAmpersat() {
        PGPUserId pgpUserId = PGPUserId.of("name", "@test.com");
        assertThat(pgpUserId).hasFieldOrPropertyWithValue("email", "name@test.com");
    }

    @Test
    public void buildAnInstanceWhenProvidingAnEmailSuffixThatDoesNotStartWithAmpersat() {
        PGPUserId pgpUserId = PGPUserId.of("name", "test.com");
        assertThat(pgpUserId).hasFieldOrPropertyWithValue("email", "name@test.com");
    }
}