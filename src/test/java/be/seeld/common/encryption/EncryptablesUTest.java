package be.seeld.common.encryption;

import be.seeld.common.encryption.aes.AES;
import be.seeld.common.lang.Maps;
import org.bouncycastle.util.encoders.Hex;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.Test;
import test.Unit;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.mapOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.aryEq;
import static org.easymock.EasyMock.expect;

public class EncryptablesUTest extends Unit {

    private static final byte[] ENCRYPTABLE_BYTES = "encryptable".getBytes();
    private static final String ENCRYPTABLE_STRING = "encryptable";
    private static final byte[] ENCRYPTED_BYTES = "encrypted!".getBytes();
    private static final byte[] ONE_BYTES = "one".getBytes();
    private static final byte[] ONE_BYTES_ENCRYPTED = "one encrypted!".getBytes();
    private static final byte[] ONE_KEY_BYTES = "one key".getBytes();
    private static final byte[] ONE_KEY_BYTES_ENCRYPTED = "one key encrypted!".getBytes();
    private static final String ONE_KEY_STRING = "one key";
    private static final String ONE_STRING = "one";
    private static final String STRING_NOT_TO_BE_ENCRYPTED = "non encryptable";
    private static final byte[] THREE_BYTES = "three".getBytes();
    private static final byte[] THREE_BYTES_ENCRYPTED = "three encrypted!".getBytes();
    private static final byte[] THREE_KEY_BYTES = "three key".getBytes();
    private static final byte[] THREE_KEY_BYTES_ENCRYPTED = "three key encrypted!".getBytes();
    private static final String THREE_KEY_STRING = "three key";
    private static final String THREE_STRING = "three";
    private static final byte[] TWO_BYTES = "two".getBytes();
    private static final byte[] TWO_BYTES_ENCRYPTED = "two encrypted!".getBytes();
    private static final byte[] TWO_KEY_BYTES = "two key".getBytes();
    private static final byte[] TWO_KEY_BYTES_ENCRYPTED = "two key encrypted!".getBytes();
    private static final String TWO_KEY_STRING = "two key";
    private static final String TWO_STRING = "two";

    @Mock(type = MockType.DEFAULT)
    private AES aes;

    @TestSubject
    private final Encryptables encryptables = new Encryptables(aes);

    @Test
    public void buildAnEncryptableInstanceFromAnUnencryptedSimpleBean() {
        Bean bean = simpleBean();

        expect(aes.encrypt(aryEq(ENCRYPTABLE_BYTES))).andReturn(ENCRYPTED_BYTES);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofUnencrypted(bean);
        verifyAll();

        assertThat(encryptable.getUnencrypted()).isEqualToComparingFieldByField(bean);
        assertThat(encryptable.getEncrypted()).isNotNull();
        assertThat(encryptable.getEncrypted().getNonEncryptableField()).isEqualTo(STRING_NOT_TO_BE_ENCRYPTED);
        assertThat(encryptable.getEncrypted().getEncryptableField()).isNotEqualTo(ENCRYPTABLE_STRING);
    }

    @Test
    public void buildAnEncryptableInstanceFromAnEncryptedSimpleBean() {
        Bean bean = simpleBeanEncrypted();

        expect(aes.decrypt(aryEq(ENCRYPTED_BYTES))).andReturn(ENCRYPTABLE_BYTES);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofEncrypted(bean);
        verifyAll();

        assertThat(encryptable.getEncrypted()).isNotNull().isEqualToComparingFieldByField(bean);
        assertThat(encryptable.getUnencrypted()).isNotNull();
        assertThat(encryptable.getUnencrypted().getNonEncryptableField()).isEqualTo(STRING_NOT_TO_BE_ENCRYPTED);
        assertThat(encryptable.getUnencrypted().getEncryptableField()).isEqualTo(ENCRYPTABLE_STRING);
    }

    @Test(expected = Encryptables.EncryptablesException.class)
    public void throwAnExceptionIfAttemptingToBuildAnEncryptableInstanceFromANullUnencryptableBean() {
        encryptables.ofUnencrypted(null);
    }

    @Test(expected = Encryptables.EncryptablesException.class)
    public void throwAnExceptionIfAttemptingToBuildAnEncryptableInstanceFromANullEncryptableBean() {
        encryptables.ofEncrypted(null);
    }

    @Test
    public void buildAnEncryptableInstanceFromAnUnencryptedCollectionBean() {
        Bean bean = collectionBean();

        expect(aes.encrypt(aryEq(ONE_BYTES))).andReturn(ONE_BYTES_ENCRYPTED).times(2);
        expect(aes.encrypt(aryEq(TWO_BYTES))).andReturn(TWO_BYTES_ENCRYPTED).times(2);
        expect(aes.encrypt(aryEq(THREE_BYTES))).andReturn(THREE_BYTES_ENCRYPTED).times(2);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofUnencrypted(bean);
        verifyAll();

        assertThat(encryptable.getUnencrypted()).usingRecursiveComparison().isEqualTo(bean);
        assertThat(encryptable.getEncrypted().getEncryptableList()).contains(
                Hex.toHexString(ONE_BYTES_ENCRYPTED),
                Hex.toHexString(TWO_BYTES_ENCRYPTED),
                Hex.toHexString(THREE_BYTES_ENCRYPTED)
        );
        assertThat(encryptable.getEncrypted().getEncryptableSet()).contains(
                Hex.toHexString(ONE_BYTES_ENCRYPTED),
                Hex.toHexString(TWO_BYTES_ENCRYPTED),
                Hex.toHexString(THREE_BYTES_ENCRYPTED)
        );
    }

    @Test
    public void buildAnEncryptableInstanceFromAnEncryptedCollectionBean() {
        Bean bean = collectionBeanEncrypted();

        expect(aes.decrypt(aryEq(ONE_BYTES_ENCRYPTED))).andReturn(ONE_BYTES).times(2);
        expect(aes.decrypt(aryEq(TWO_BYTES_ENCRYPTED))).andReturn(TWO_BYTES).times(2);
        expect(aes.decrypt(aryEq(THREE_BYTES_ENCRYPTED))).andReturn(THREE_BYTES).times(2);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofEncrypted(collectionBeanEncrypted());
        verifyAll();

        assertThat(encryptable.getEncrypted()).usingRecursiveComparison().isEqualTo(bean);
        assertThat(encryptable.getUnencrypted().getEncryptableList()).contains(ONE_STRING, TWO_STRING, THREE_STRING);
        assertThat(encryptable.getUnencrypted().getEncryptableSet()).contains(ONE_STRING, TWO_STRING, THREE_STRING);
    }

    @Test
    public void buildAnEncryptableInstanceFromAnUnencryptedMapBean() {
        Bean bean = mapBean();

        expect(aes.encrypt(aryEq(ONE_KEY_BYTES))).andReturn(ONE_KEY_BYTES_ENCRYPTED);
        expect(aes.encrypt(aryEq(ONE_BYTES))).andReturn(ONE_BYTES_ENCRYPTED);
        expect(aes.encrypt(aryEq(TWO_KEY_BYTES))).andReturn(TWO_KEY_BYTES_ENCRYPTED);
        expect(aes.encrypt(aryEq(TWO_BYTES))).andReturn(TWO_BYTES_ENCRYPTED);
        expect(aes.encrypt(aryEq(THREE_KEY_BYTES))).andReturn(THREE_KEY_BYTES_ENCRYPTED);
        expect(aes.encrypt(aryEq(THREE_BYTES))).andReturn(THREE_BYTES_ENCRYPTED);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofUnencrypted(bean);
        verifyAll();

        assertThat(encryptable.getUnencrypted()).usingRecursiveComparison().isEqualTo(bean);
        assertThat(encryptable.getEncrypted().getEncryptableMap().get(Hex.toHexString(ONE_KEY_BYTES_ENCRYPTED)))
                .isEqualTo(Hex.toHexString(ONE_BYTES_ENCRYPTED));
        assertThat(encryptable.getEncrypted().getEncryptableMap().get(Hex.toHexString(TWO_KEY_BYTES_ENCRYPTED)))
                .isEqualTo(Hex.toHexString(TWO_BYTES_ENCRYPTED));
        assertThat(encryptable.getEncrypted().getEncryptableMap().get(Hex.toHexString(THREE_KEY_BYTES_ENCRYPTED)))
                .isEqualTo(Hex.toHexString(THREE_BYTES_ENCRYPTED));
    }

    @Test
    public void buildAnEncryptableInstanceFromAnEncryptedMapBean() {
        Bean bean = mapBeanEncrypted();

        expect(aes.decrypt(aryEq(ONE_KEY_BYTES_ENCRYPTED))).andReturn(ONE_KEY_BYTES);
        expect(aes.decrypt(aryEq(ONE_BYTES_ENCRYPTED))).andReturn(ONE_BYTES);
        expect(aes.decrypt(aryEq(TWO_KEY_BYTES_ENCRYPTED))).andReturn(TWO_KEY_BYTES);
        expect(aes.decrypt(aryEq(TWO_BYTES_ENCRYPTED))).andReturn(TWO_BYTES);
        expect(aes.decrypt(aryEq(THREE_KEY_BYTES_ENCRYPTED))).andReturn(THREE_KEY_BYTES);
        expect(aes.decrypt(aryEq(THREE_BYTES_ENCRYPTED))).andReturn(THREE_BYTES);

        replayAll();
        Encryptable<Bean> encryptable = encryptables.ofEncrypted(bean);
        verifyAll();

        assertThat(encryptable.getEncrypted()).usingRecursiveComparison().isEqualTo(bean);
        assertThat(encryptable.getUnencrypted().getEncryptableMap().get(ONE_KEY_STRING)).isEqualTo(ONE_STRING);
        assertThat(encryptable.getUnencrypted().getEncryptableMap().get(TWO_KEY_STRING)).isEqualTo(TWO_STRING);
        assertThat(encryptable.getUnencrypted().getEncryptableMap().get(THREE_KEY_STRING)).isEqualTo(THREE_STRING);
    }

    private Bean collectionBean() {
        Bean bean = new Bean();
        bean.setEncryptableList(listOf(ONE_STRING, TWO_STRING, THREE_STRING));
        bean.setEncryptableSet(setOf(ONE_STRING, TWO_STRING, THREE_STRING));
        return bean;
    }

    private Bean collectionBeanEncrypted() {
        Bean bean = new Bean();
        bean.setEncryptableList(listOf(
                Hex.toHexString(ONE_BYTES_ENCRYPTED),
                Hex.toHexString(TWO_BYTES_ENCRYPTED),
                Hex.toHexString(THREE_BYTES_ENCRYPTED)
        ));
        bean.setEncryptableSet(setOf(
                Hex.toHexString(ONE_BYTES_ENCRYPTED),
                Hex.toHexString(TWO_BYTES_ENCRYPTED),
                Hex.toHexString(THREE_BYTES_ENCRYPTED)
        ));
        return bean;
    }

    private Bean mapBean() {
        Bean bean = new Bean();
        Map<String, String> map = mapOf(
                Maps.Entry.of(ONE_KEY_STRING, ONE_STRING),
                Maps.Entry.of(TWO_KEY_STRING, TWO_STRING),
                Maps.Entry.of(THREE_KEY_STRING, THREE_STRING)
        );
        bean.setEncryptableMap(map);
        return bean;
    }

    private Bean mapBeanEncrypted() {
        Bean bean = new Bean();
        Map<String, String> map = mapOf(
                Maps.Entry.of(Hex.toHexString(ONE_KEY_BYTES_ENCRYPTED), Hex.toHexString(ONE_BYTES_ENCRYPTED)),
                Maps.Entry.of(Hex.toHexString(TWO_KEY_BYTES_ENCRYPTED), Hex.toHexString(TWO_BYTES_ENCRYPTED)),
                Maps.Entry.of(Hex.toHexString(THREE_KEY_BYTES_ENCRYPTED), Hex.toHexString(THREE_BYTES_ENCRYPTED))
        );
        bean.setEncryptableMap(map);
        return bean;
    }

    private Bean simpleBean() {
        Bean bean = new Bean();
        bean.setEncryptableField(ENCRYPTABLE_STRING);
        bean.setNonEncryptableField(STRING_NOT_TO_BE_ENCRYPTED);
        return bean;
    }

    private Bean simpleBeanEncrypted() {
        Bean bean = new Bean();
        bean.setEncryptableField(Hex.toHexString(ENCRYPTED_BYTES));
        bean.setNonEncryptableField(STRING_NOT_TO_BE_ENCRYPTED);
        return bean;
    }

    static class Bean {
        @Encrypted
        private String encryptableField;
        @Encrypted
        private List<String> encryptableList;
        @Encrypted
        private Set<String> encryptableSet;
        @Encrypted
        private Map<String, String> encryptableMap;

        private String nonEncryptableField;

        String getEncryptableField() {
            return encryptableField;
        }

        void setEncryptableField(String encryptableField) {
            this.encryptableField = encryptableField;
        }

        List<String> getEncryptableList() {
            return encryptableList;
        }

        void setEncryptableList(List<String> encryptableList) {
            this.encryptableList = encryptableList;
        }

        Set<String> getEncryptableSet() {
            return encryptableSet;
        }

        void setEncryptableSet(Set<String> encryptableSet) {
            this.encryptableSet = encryptableSet;
        }

        Map<String, String> getEncryptableMap() {
            return encryptableMap;
        }

        void setEncryptableMap(Map<String, String> encryptableMap) {
            this.encryptableMap = encryptableMap;
        }

        String getNonEncryptableField() {
            return nonEncryptableField;
        }

        void setNonEncryptableField(String nonEncryptableField) {
            this.nonEncryptableField = nonEncryptableField;
        }
    }
}
