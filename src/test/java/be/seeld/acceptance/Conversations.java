package be.seeld.acceptance;

import be.seeld.conversations.ConversationsWebApi;
import be.seeld.conversations.ConversationsWebApi.ConversationDeleteRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationIdsResponse;
import be.seeld.conversations.ConversationsWebApi.ConversationsRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationsResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.rest.WebApiTestClient;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static be.seeld.conversations.ConversationsWebApi.ConversationIdsRequest;
import static test.junit.RestAssertions.assertThatCreateResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class Conversations {

    private final WebApiTestClient webApiTestClient;

    public Conversations(WebApiTestClient webApiTestClient) {
        this.webApiTestClient = webApiTestClient;
    }

    public void deleteConversation(UUID conversationId, Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/" + conversationId + "/state/deleted",
                new ConversationDeleteRequest(hmacHashesByBoxAddress));
        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    public void deleteMessage(UUID boxAddress, UUID conversationId, UUID messageId, String hmacHash) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/" + conversationId + "/messages/" + messageId + "/state/deleted",
                new ConversationsWebApi.MessageDeleteRequest(boxAddress, hmacHash));
        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    public ConversationIdsResponse fetchConversationIds(Map<UUID, String> hmacHashesByBoxAddress) {
        ConversationIdsRequest conversationIdsRequest = new ConversationIdsRequest(hmacHashesByBoxAddress, 0, 100);
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/ids",
                conversationIdsRequest,
                ConversationIdsResponse.class);
        return assertThatCreateResponseIs(response, ConversationIdsResponse.class);
    }

    public ConversationsResponse fetchConversations(Map<UUID, String> hmacHashesByBoxAddress) {
        ConversationsRequest conversationsRequest = ConversationsRequest.with(hmacHashesByBoxAddress, 0, 25);
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations",
                conversationsRequest,
                ConversationsResponse.class);
        return assertThatCreateResponseIs(response, ConversationsResponse.class);
    }
}
