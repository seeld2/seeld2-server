package be.seeld.acceptance;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.SecurityConfiguration.AppSecurityProperties.AesProperties;
import be.seeld.common.encryption.aes.AES;
import be.seeld.login.ChallengeResponse;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import be.seeld.login.LoginProfile;
import be.seeld.login.LoginRequest;
import be.seeld.login.LoginResponse;
import be.seeld.users.User;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSessionSHA256;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6ClientCredentials;
import com.nimbusds.srp6.SRP6Exception;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import test.junit.RestAssertions;
import test.rest.WebApiTestClient;

import java.io.IOException;

import static test.TestUsers.ALICE_PASSPHRASE;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_PASSPHRASE;
import static test.TestUsers.BOB_PSEUDO;
import static test.junit.RestAssertions.assertThatCreateResponseIs;

public class Given {

    private final AES aes;
    private Results results;
    private final Srp6aProperties srp6aProperties;
    private final WebApiTestClient webApiTestClient;

    public Given(AppSecurityProperties appSecurityProperties,
                 Srp6aProperties srp6aProperties,
                 WebApiTestClient webApiRestClient) {
        this.srp6aProperties = srp6aProperties;
        this.webApiTestClient = webApiRestClient;

        AesProperties aesProperties = appSecurityProperties.getAes();
        this.aes = AES.of(new AES.Config(
                aesProperties.getSecureRandomAlgorithm(),
                aesProperties.getMode(),
                aesProperties.getPadding(),
                null,
                aesProperties.getKeySize(),
                AES.SecretKeyType.HEX,
                aesProperties.getIvSize()
        ));
    }

    public Given(AppSecurityProperties appSecurityProperties,
                 Results results,
                 Srp6aProperties srp6aProperties,
                 WebApiTestClient webApiTestClient) {
        this(appSecurityProperties, srp6aProperties, webApiTestClient);
        this.results = results;
    }

    // This indicates that we're dealing with Alice, but she doesn't need to send valid credentials when querying the API
    public void alice() {
        results.delete("aliceLoginArtifacts");
    }

    public LoginArtifacts aliceAsTheLoggedUser() throws SRP6Exception, IOException { // TODO Remove return value once all acceptance tests have been refactored
        LoginArtifacts loginArtifacts = login(ALICE_PSEUDO, ALICE_PASSPHRASE);
        if (results != null) { // TODO Remove condition once all acceptance tests have been refactored
            results.set("aliceLoginArtifacts", loginArtifacts);
        }
        return loginArtifacts;
    }

    // This indicates that we're dealing with Bob, but he doesn't need to send valid credentials when querying the API
    public void bob() {
        results.delete("bobLoginArtifacts");
    }

    public LoginArtifacts bobAsTheLoggedUser() throws SRP6Exception, IOException { // TODO Remove return value once all acceptance tests have been refactored
        LoginArtifacts loginArtifacts = login(BOB_PSEUDO, BOB_PASSPHRASE);
        if (results != null) { // TODO Remove condition once all acceptance tests have been refactored
            results.set("bobLoginArtifacts", loginArtifacts);
        }
        return loginArtifacts;
    }

    private LoginArtifacts login(String username, String passphrase) throws SRP6Exception, IOException {
        ResponseEntity<?> response = webApiTestClient.get("/api/login/challenge/" + username, ChallengeResponse.class);
        ChallengeResponse challengeResponse = RestAssertions.assertThatOkResponseIs(response, ChallengeResponse.class);

        SRP6JavaClientSession clientSession = new SRP6JavaClientSessionSHA256(srp6aProperties.getN(), srp6aProperties.getG());
        clientSession.step1(username, passphrase);
        SRP6ClientCredentials clientCredentials = clientSession.step2(challengeResponse.getSalt(), challengeResponse.getB());

        LoginRequest loginRequest = new LoginRequest(
                username,
                BigIntegerUtils.toHex(clientCredentials.A),
                BigIntegerUtils.toHex(clientCredentials.M1));
        response = webApiTestClient.post("/api/login", loginRequest, LoginResponse.class);
        LoginResponse loginResponse = assertThatCreateResponseIs(response, LoginResponse.class);

        clientSession.step3(loginResponse.getM2());
        String sessionKey = clientSession.getSessionKey(true);

        String profileJson = aes.decrypt(loginResponse.getProfile(), sessionKey);
        LoginProfile loginProfile = LoginProfile.fromJSON(profileJson, LoginProfile.class);

        User.Credentials credentials = User.Credentials.fromJSON(loginProfile.getCredentials(), User.Credentials.class);

        HttpHeaders responseHeaders = response.getHeaders();
        String jwt = responseHeaders.getFirst(HttpHeaders.AUTHORIZATION).substring(6);

        return new LoginArtifacts(credentials, jwt);
    }

    public static class LoginArtifacts {
        public User.Credentials credentials;
        public String jwt;

        LoginArtifacts(User.Credentials credentials, String jwt) {
            this.credentials = credentials;
            this.jwt = jwt;
        }
    }
}
