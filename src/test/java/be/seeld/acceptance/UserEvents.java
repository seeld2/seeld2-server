package be.seeld.acceptance;

import be.seeld.acceptance.Given.LoginArtifacts;
import be.seeld.common.encryption.aes.AES;
import be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import be.seeld.userevents.EventsProcess.NewUserEvent;
import be.seeld.userevents.EventsWebApi.CreateEventRequest;
import be.seeld.userevents.EventsWebApi.DeleteEventRequest;
import be.seeld.userevents.EventsWebApi.FetchEventsRequest;
import be.seeld.userevents.EventsWebApi.UserEventsResponse;
import be.seeld.userevents.UserEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.data.UserEventData;
import test.rest.WebApiTestClient;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class UserEvents {

    private final AES aesBase64;
    private final UserEventData userEventData;
    private final WebApiTestClient webApiTestClient;

    public UserEvents(AES aesBase64, UserEventData userEventData, WebApiTestClient webApiTestClient) {
        this.aesBase64 = aesBase64;
        this.userEventData = userEventData;
        this.webApiTestClient = webApiTestClient;
    }

    public UserEventsResponse fetchEvents(Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<UserEventsResponse> response = webApiTestClient
                .put("/api/events", new FetchEventsRequest(hmacHashesByBoxAddress), UserEventsResponse.class);
        return assertThatOkResponseIs(response, UserEventsResponse.class);
    }

    public void sendContactRequest(String hmacHash,
                                   String senderExchangeKey,
                                   LoginArtifacts senderLoginArtifacts,
                                   String contactRequestPayload,
                                   String contactRequestReturnPayload,
                                   DetailsResponse receiverDetailsResponse) throws JsonProcessingException {

        NewUserEvent newUserEvent = new NewUserEvent(receiverDetailsResponse.getBoxAddress(), hmacHash);
        CreateEventRequest createEventRequest = new CreateEventRequest(
                aesBase64.encryptToUnsignedBytes(newUserEvent.asJSON(), senderExchangeKey),
                contactRequestPayload,
                contactRequestReturnPayload);
        ResponseEntity<?> response = webApiTestClient.post("/api/events/", senderLoginArtifacts.jwt, createEventRequest);
        assertThatResponseIs(response, HttpStatus.CREATED);
    }

    public List<UserEvent> userEventsDataWithBoxAddress(UUID boxAddress) {
        return userEventData.find(boxAddress);
    }

    public void acceptContactRequest(String hmacHash,
                                     String accepterExchangeKey,
                                     LoginArtifacts accepterLoginArtifacts,
                                     String contactRequestResponseAcceptedPayload,
                                     String contactRequestResponseAcceptedReturnPayload,
                                     DetailsResponse requesterDetailsResponse) throws JsonProcessingException {

        NewUserEvent acceptanceUserEventForRequester = new NewUserEvent(requesterDetailsResponse.getBoxAddress(), hmacHash);
        CreateEventRequest createEventRequest = new CreateEventRequest(
                aesBase64.encryptToUnsignedBytes(acceptanceUserEventForRequester.asJSON(), accepterExchangeKey),
                contactRequestResponseAcceptedPayload,
                contactRequestResponseAcceptedReturnPayload);
        ResponseEntity<?> response = webApiTestClient.post("/api/events/", accepterLoginArtifacts.jwt, createEventRequest);
        assertThatResponseIs(response, HttpStatus.CREATED);
    }

    public void deleteEvent(UUID eventId, UUID boxAddress, String hmacHash) {
        ResponseEntity<?> response = webApiTestClient
                .put("/api/events/" + eventId + "/state/deleted", new DeleteEventRequest(boxAddress, hmacHash));
        assertThatResponseIs(response, NO_CONTENT);
    }

    public void rejectContactRequest(String hmacHash,
                                     String rejecterExchangeKey,
                                     LoginArtifacts rejecterLoginArtifacts,
                                     String contactRequestResponseRejectedPayload,
                                     String contactRequestResponseRejectedReturnPayload,
                                     DetailsResponse requesterDetailsResponse) throws JsonProcessingException {

        NewUserEvent rejectionUserEventForRequester = new NewUserEvent(requesterDetailsResponse.getBoxAddress(), hmacHash);
        CreateEventRequest createEventRequest = new CreateEventRequest(
                aesBase64.encryptToUnsignedBytes(rejectionUserEventForRequester.asJSON(), rejecterExchangeKey),
                contactRequestResponseRejectedPayload,
                contactRequestResponseRejectedReturnPayload);
        ResponseEntity<?> response = webApiTestClient.post("/api/events/", rejecterLoginArtifacts.jwt, createEventRequest);
        assertThatResponseIs(response, HttpStatus.CREATED);
    }
}
