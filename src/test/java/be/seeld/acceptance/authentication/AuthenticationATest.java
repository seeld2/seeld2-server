package be.seeld.acceptance.authentication;

import be.seeld.acceptance.Acceptance;
import be.seeld.common.web.AESRequest;
import be.seeld.userevents.EventsWebApi.DeleteEventRequest;
import be.seeld.userevents.EventsWebApi.FetchEventsRequest;
import be.seeld.userevents.EventsWebApi.CreateEventRequest;
import be.seeld.userevents.EventsWebApi.UserEventResponse;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.Entry;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.ConversationsWebApi.ConversationIdsRequest;
import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsRequest;
import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesRequest;
import static be.seeld.conversations.ConversationsWebApi.ConversationsRequest;
import static be.seeld.conversations.messages.MessagesWebApi.BoxAddressesRequest;
import static be.seeld.conversations.messages.MessagesWebApi.NewMessagesRequest;
import static be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import static be.seeld.profiles.ProfilesWebApi.PayloadRequest;
import static org.assertj.core.api.Assertions.assertThat;

public class AuthenticationATest extends Acceptance {

    private static final UUID BOX_ADDRESS = UUID.fromString("096e4c9d-e651-46c9-8849-83818624b054");
    private static final int[] BOX_ADDRESS_PAYLOAD = {1, 2, 3};
    private static final UUID CONVERSATION_ID = UUID.fromString("197527a2-4e5a-4364-828b-123918282863");
    private static final UUID EVENT_ID = UUID.fromString("b42a1992-635d-40ea-87ae-c7d1540a7296");
    private static final String HMAC_HASH = "hmacHash1";
    private static final UUID MESSAGE_ID = UUID.fromString("3e9f9517-4332-4187-93dd-0b01389a125b");
    private static final String PAYLOAD = "payload";
    private static final String RETURN_PAYLOAD = "return payload";

    private static final String BAD_AUTH_TOKEN = "bad token";

    @Test
    public void shouldNotBeRequiredForConversationsQueries() {
        isAuthorizedWhenUnauthenticated(webApiTestClient.put("/api/conversations/" + CONVERSATION_ID
                + "/messages", conversationMessagesRequest()));
        isAuthorizedWhenUnauthenticated(webApiTestClient.put("/api/conversations/" + CONVERSATION_ID
                + "/messages/ids", conversationMessagesIdsRequest()));
        isAuthorizedWhenUnauthenticated(webApiTestClient.put("/api/conversations", conversationsRequest()));
        isAuthorizedWhenUnauthenticated(webApiTestClient.put("/api/conversations/ids", conversationIdsRequest()));
    }

    @Test
    public void shouldBeRequiredForEventsQueries() {
        isNotAuthorizedWhenUnauthenticated(webApiTestClient.post("/api/events",
                new CreateEventRequest(BOX_ADDRESS_PAYLOAD, PAYLOAD, RETURN_PAYLOAD)));
    }

    @Test
    public void shouldBeRequiredForMessagePosting() {
        isNotAuthorizedWhenUnauthenticated(webApiTestClient
                .post("/api/messages", BAD_AUTH_TOKEN, new AESRequest(new int[]{})));
    }

    @Test
    public void shouldNotBeRequiredForEventsQueries() {
        isAuthorizedWhenUnauthenticated(webApiTestClient
                .put("/api/events", fetchEventsRequest(), UserEventResponse.class));
        isAuthorizedWhenUnauthenticated(webApiTestClient
                .put("/api/events/" + EVENT_ID + "/state/deleted", deleteEventRequest()));
    }

    @Test
    public void shouldNotBeRequiredForMessagesQueries() {
        isAuthorizedWhenUnauthenticated(webApiTestClient.put("/api/messages/new", messagesRequest()));
        isAuthorizedWhenUnauthenticated(webApiTestClient
                .put("/api/messages/new/timeline/latest", boxAddressesRequest()));
    }

    @Test
    public void shouldBeRequiredForPayloadQueries() {
        isNotAuthorizedWhenUnauthenticated(webApiTestClient
                .put("/api/profiles/username/payload", null, new PayloadRequest("payload")));
    }

    @Test
    public void shouldNotBeRequiredForProfileDetailsQueries() {
        isAuthorizedWhenUnauthenticated(webApiTestClient.get("/api/profiles/alice", DetailsResponse.class));
    }

    private BoxAddressesRequest boxAddressesRequest() {
        return new BoxAddressesRequest(setOf(BOX_ADDRESS));
    }

    private ConversationIdsRequest conversationIdsRequest() {
        return new ConversationIdsRequest(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)), 0, 2);
    }

    private ConversationMessagesIdsRequest conversationMessagesIdsRequest() {
        return new ConversationMessagesIdsRequest(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)), 0, 2);
    }

    private ConversationMessagesRequest conversationMessagesRequest() {
        return new ConversationMessagesRequest(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)), setOf(MESSAGE_ID), null, null);
    }

    private ConversationsRequest conversationsRequest() {
        return ConversationsRequest.with(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)), 0, 25);
    }

    private DeleteEventRequest deleteEventRequest() {
        return new DeleteEventRequest(BOX_ADDRESS, HMAC_HASH);
    }

    private FetchEventsRequest fetchEventsRequest() {
        return new FetchEventsRequest(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)));
    }

    private void isAuthorizedWhenUnauthenticated(ResponseEntity<?> response) {
        assertThat(new HttpStatus[]{HttpStatus.OK, HttpStatus.CREATED, HttpStatus.NO_CONTENT})
                .contains(response.getStatusCode());
    }

    private void isNotAuthorizedWhenUnauthenticated(ResponseEntity<?> response) {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    private NewMessagesRequest messagesRequest() {
        return new NewMessagesRequest(mapOf(Entry.of(BOX_ADDRESS, HMAC_HASH)));
    }
}
