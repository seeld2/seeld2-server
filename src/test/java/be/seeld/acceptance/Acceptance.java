package be.seeld.acceptance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import test.Integration;
import test.rest.WebApiTestClient;

import javax.annotation.PostConstruct;
import java.sql.SQLException;

/**
 * Extend this class for assistance when writing acceptance tests.<br/>
 * The class exposes:
 * <ul>
 *     <li>a <strong>CSRF_TOKEN</strong> to use with the REST API</li>
 *     <li>a <em>TestRestClient</em> (<strong>restClient</strong>) to facilitate querying the REST API</li>
 * </ul>
 */
public abstract class Acceptance extends Integration {

    protected WebApiTestClient webApiTestClient;

    @Autowired
    private TestRestTemplate restTemplate;

    @PostConstruct
    public void postConstruct() throws SQLException, ClassNotFoundException {
        super.postConstruct();

        webApiTestClient = new WebApiTestClient(restTemplate, appSecurityProperties.getJwt().getPrefix());
    }
}
