package be.seeld.acceptance.contactrequests;

import be.seeld.acceptance.Given;
import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.acceptance.UserEvents;
import be.seeld.common.lang.Maps.Entry;
import be.seeld.userevents.EventsWebApi.UserEventsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

import static be.seeld.acceptance.contactrequests.ContactRequestsATest.ALICE_BOB_HMAC_HASH;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.BOB_PAYLOAD_UPDATED;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_ACCEPTED_RETURN_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_REJECTED_RETURN_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RETURN_PAYLOAD;
import static be.seeld.common.lang.Maps.mapOf;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.BOB_EXCHANGE_KEY;
import static test.TestUsers.BOB_PAYLOAD;
import static test.TestUsers.BOB_PSEUDO;

class When {

    private final Profiles profiles;
    private final Results results;
    private final UserEvents userEvents;

    When(Profiles profiles, Results results, UserEvents userEvents) {
        this.profiles = profiles;
        this.userEvents = userEvents;
        this.results = results;
    }

    void aliceFetchesHerUserEvents() {
        UserEventsResponse response = userEvents.fetchEvents(mapOf(Entry.of(ALICE_BOX_ADDRESS, ALICE_BOB_HMAC_HASH)));
        results.set("userEventsResponse", response);
    }

    void aliceSendsAContactRequestToBob() throws JsonProcessingException {
        userEvents.sendContactRequest(
                ALICE_BOB_HMAC_HASH,
                ALICE_EXCHANGE_KEY,
                results.get("aliceLoginArtifacts"),
                CONTACT_REQUEST_PAYLOAD,
                CONTACT_REQUEST_RETURN_PAYLOAD,
                profiles.fetchDetails(BOB_PSEUDO));
    }

    void bobAcceptsAliceContactRequest() throws JsonProcessingException {
        Given.LoginArtifacts bobLoginArtifacts = results.get("bobLoginArtifacts");

        userEvents.acceptContactRequest(
                ALICE_BOB_HMAC_HASH,
                BOB_EXCHANGE_KEY,
                bobLoginArtifacts,
                CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD,
                CONTACT_REQUEST_RESPONSE_ACCEPTED_RETURN_PAYLOAD,
                profiles.fetchDetails(ALICE_PSEUDO));

        profiles.updatePayload(BOB_PSEUDO, BOB_EXCHANGE_KEY, BOB_PAYLOAD_UPDATED, bobLoginArtifacts);

        userEvents.deleteEvent(results.get("contactRequestEventId"), BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH);
    }

    void bobFetchesHisUserEvents() {
        UserEventsResponse response = userEvents.fetchEvents(mapOf(Entry.of(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH)));
        results.set("userEventsResponse", response);
    }

    void bobRejectsAliceContactRequest() throws JsonProcessingException {
        Given.LoginArtifacts bobLoginArtifacts = results.get("bobLoginArtifacts");

        userEvents.rejectContactRequest(
                ALICE_BOB_HMAC_HASH,
                BOB_EXCHANGE_KEY,
                bobLoginArtifacts,
                CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD,
                CONTACT_REQUEST_RESPONSE_REJECTED_RETURN_PAYLOAD,
                profiles.fetchDetails(ALICE_PSEUDO));

        profiles.updatePayload(BOB_PSEUDO, BOB_EXCHANGE_KEY, BOB_PAYLOAD, bobLoginArtifacts);

        userEvents.deleteEvent(results.get("contactRequestEventId"), BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH);
    }
}
