package be.seeld.acceptance.contactrequests;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.acceptance.Results;
import be.seeld.login.LoginConfiguration;
import test.rest.WebApiTestClient;

public class Given extends be.seeld.acceptance.Given {

    public Given(AppSecurityProperties appSecurityProperties,
                 Results results,
                 LoginConfiguration.Srp6aProperties srp6aProperties,
                 WebApiTestClient webApiRestClient) {
        super(appSecurityProperties, results, srp6aProperties, webApiRestClient);
    }
}
