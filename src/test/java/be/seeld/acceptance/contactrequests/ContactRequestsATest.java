package be.seeld.acceptance.contactrequests;

import be.seeld.SecurityConfiguration;
import be.seeld.acceptance.Acceptance;
import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.acceptance.UserEvents;
import be.seeld.common.encryption.aes.AES;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import com.nimbusds.srp6.SRP6Exception;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;

import static test.TestUsers.ALICE_PAYLOAD;
import static test.TestUsers.BOB_PAYLOAD;

public class ContactRequestsATest extends Acceptance {

    static final String ALICE_BOB_HMAC_HASH = "aliceBobHmacHash";

    static final String ALICE_PAYLOAD_UPDATED = ALICE_PAYLOAD + ":updated!";
    static final String BOB_PAYLOAD_UPDATED = BOB_PAYLOAD + ":updated!";

    static final String CONTACT_REQUEST_PAYLOAD = "contact request payload";
    static final String CONTACT_REQUEST_RETURN_PAYLOAD = "contact request return payload";
    static final String CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD = "contact request response accepted payload";
    static final String CONTACT_REQUEST_RESPONSE_ACCEPTED_RETURN_PAYLOAD = "contact request response accepted return payload";
    static final String CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD = "contact request response accepted payload";
    static final String CONTACT_REQUEST_RESPONSE_REJECTED_RETURN_PAYLOAD = "contact request response accepted return payload";

    private final Results results = new Results();

    @Autowired
    @Qualifier("pwaAesBase64")
    private AES aesBase64;
    @Autowired
    private SecurityConfiguration.AppSecurityProperties appSecurityProperties;
    @Autowired
    private Srp6aProperties srp6aProperties;

    private Given given;
    private When when;
    private Then then;

    @Before
    public void setUp() {
        super.setUp();

        Profiles profiles = new Profiles(aesBase64, userData, webApiTestClient);
        UserEvents userEvents = new UserEvents(aesBase64, userEventData, webApiTestClient);

        given = new Given(appSecurityProperties, results, srp6aProperties, webApiTestClient);
        when = new When( profiles, results, userEvents);
        then = new Then(encryptables, profiles, results, userEvents);
    }

    @Test
    public void connectToEachOther() throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();

        when.aliceSendsAContactRequestToBob();
        then.anEventIsCreatedForBob();

        given.bobAsTheLoggedUser();

        when.bobFetchesHisUserEvents();
        then.bobReceivesAContactRequestFromAlice();

        when.bobAcceptsAliceContactRequest();
        then.anAcceptanceEventIsCreatedForAlice()
                .and.bobUserPayloadIsUpdated()
                .and.bobsReceivedContactRequestEventIsDeleted();

        given.aliceAsTheLoggedUser();

        when.aliceFetchesHerUserEvents();
        then.aliceReceivesAnAcceptedContactRequestResponseFromBob()
                .and.aliceUserPayloadIsUpdated()
                .and.aliceReceivedContactRequestResponseEventIsDeleted();
    }

    @Test
    public void rejectAConnectionRequest() throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();

        when.aliceSendsAContactRequestToBob();
        then.anEventIsCreatedForBob();

        given.bobAsTheLoggedUser();

        when.bobFetchesHisUserEvents();
        then.bobReceivesAContactRequestFromAlice();

        when.bobRejectsAliceContactRequest();
        then.aRejectionEventIsCreatedForAlice()
                .and.bobUserPayloadIsNotUpdated()
                .and.bobsReceivedContactRequestEventIsDeleted();

        given.aliceAsTheLoggedUser();

        when.aliceFetchesHerUserEvents();
        then.aliceReceivesARejectedContactRequestResponseFromBob()
                .and.aliceUserPayloadIsNotUpdated()
                .and.aliceReceivedContactRequestResponseEventIsDeleted();
    }
}
