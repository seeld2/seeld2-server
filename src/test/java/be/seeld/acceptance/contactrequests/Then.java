package be.seeld.acceptance.contactrequests;

import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.acceptance.UserEvents;
import be.seeld.common.encryption.Encryptables;
import be.seeld.userevents.EventsWebApi.UserEventResponse;
import be.seeld.userevents.EventsWebApi.UserEventsResponse;
import be.seeld.userevents.UserEvent;
import be.seeld.users.User;

import java.util.List;

import static be.seeld.acceptance.contactrequests.ContactRequestsATest.ALICE_BOB_HMAC_HASH;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.ALICE_PAYLOAD_UPDATED;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.BOB_PAYLOAD_UPDATED;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_ACCEPTED_RETURN_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RESPONSE_REJECTED_RETURN_PAYLOAD;
import static be.seeld.acceptance.contactrequests.ContactRequestsATest.CONTACT_REQUEST_RETURN_PAYLOAD;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PAYLOAD;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.BOB_PAYLOAD;
import static test.TestUsers.BOB_PSEUDO;

class Then {

    final Then and = this;

    private final Encryptables encryptables;
    private final Profiles profiles;
    private final Results results;
    private final UserEvents userEvents;

    public Then(Encryptables encryptables, Profiles profiles, Results results, UserEvents userEvents) {
        this.encryptables = encryptables;
        this.profiles = profiles;
        this.results = results;
        this.userEvents = userEvents;
    }

    Then aliceReceivesAnAcceptedContactRequestResponseFromBob() {

        UserEventsResponse userEventsResponse = results.get("userEventsResponse");
        assertThat(userEventsResponse.getEvents()).hasSize(1);

        UserEventResponse userEventResponse = userEventsResponse.getEvents().get(0);
        assertThat(userEventResponse.getEventId()).isNotNull();
        assertThat(userEventResponse).hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD);

        profiles.updatePayload(ALICE_PSEUDO, ALICE_EXCHANGE_KEY,
                ALICE_PAYLOAD_UPDATED, results.get("aliceLoginArtifacts"));

        userEvents.deleteEvent(userEventResponse.getEventId(), ALICE_BOX_ADDRESS, ALICE_BOB_HMAC_HASH);

        return this;
    }

    Then aliceReceivesARejectedContactRequestResponseFromBob() {

        UserEventsResponse userEventsResponse = results.get("userEventsResponse");
        assertThat(userEventsResponse.getEvents()).hasSize(1);

        UserEventResponse userEventResponse = userEventsResponse.getEvents().get(0);
        assertThat(userEventResponse.getEventId()).isNotNull();
        assertThat(userEventResponse).hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD);

        profiles.updatePayload(ALICE_PSEUDO, ALICE_EXCHANGE_KEY, ALICE_PAYLOAD, results.get("aliceLoginArtifacts"));

        userEvents.deleteEvent(userEventResponse.getEventId(), ALICE_BOX_ADDRESS, ALICE_BOB_HMAC_HASH);

        return this;
    }

    void aliceReceivedContactRequestResponseEventIsDeleted() {
        List<UserEvent> userEvents = this.userEvents.userEventsDataWithBoxAddress(ALICE_BOX_ADDRESS);
        assertThat(userEvents).isEmpty();
    }

    Then aliceUserPayloadIsNotUpdated() {
        User user = profiles.userDataWithPseudo(ALICE_PSEUDO);
        assertThat(user.getPayload()).isEqualTo(ALICE_PAYLOAD);
        return this;
    }

    Then aliceUserPayloadIsUpdated() {
        User user = profiles.userDataWithPseudo(ALICE_PSEUDO);
        assertThat(user.getPayload()).isEqualTo(ALICE_PAYLOAD_UPDATED);
        return this;
    }

    Then anAcceptanceEventIsCreatedForAlice() {

        List<UserEvent> userEvents = this.userEvents.userEventsDataWithBoxAddress(ALICE_BOX_ADDRESS);
        assertThat(userEvents).hasSize(1);

        UserEvent userEvent = encryptables.ofEncrypted(userEvents.get(0)).getUnencrypted();
        assertThat(userEvent)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("hmacHash", ALICE_BOB_HMAC_HASH)
                .hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_RESPONSE_ACCEPTED_PAYLOAD)
                .hasFieldOrPropertyWithValue("returnBoxAddress", BOB_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("returnPayload", CONTACT_REQUEST_RESPONSE_ACCEPTED_RETURN_PAYLOAD);

        return this;
    }

    void anEventIsCreatedForBob() {
        List<UserEvent> userEvents = this.userEvents.userEventsDataWithBoxAddress(BOB_BOX_ADDRESS);
        assertThat(userEvents).hasSize(1);

        UserEvent userEvent = encryptables.ofEncrypted(userEvents.get(0)).getUnencrypted();
        assertThat(userEvent)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("hmacHash", ALICE_BOB_HMAC_HASH)
                .hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_PAYLOAD)
                .hasFieldOrPropertyWithValue("returnBoxAddress", ALICE_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("returnPayload", CONTACT_REQUEST_RETURN_PAYLOAD);
    }

    Then aRejectionEventIsCreatedForAlice() {

        List<UserEvent> userEvents = this.userEvents.userEventsDataWithBoxAddress(ALICE_BOX_ADDRESS);
        assertThat(userEvents).hasSize(1);

        UserEvent userEvent = encryptables.ofEncrypted(userEvents.get(0)).getUnencrypted();
        assertThat(userEvent)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("hmacHash", ALICE_BOB_HMAC_HASH)
                .hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_RESPONSE_REJECTED_PAYLOAD)
                .hasFieldOrPropertyWithValue("returnBoxAddress", BOB_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("returnPayload", CONTACT_REQUEST_RESPONSE_REJECTED_RETURN_PAYLOAD);

        return this;
    }

    void bobReceivesAContactRequestFromAlice() {

        UserEventsResponse userEventsResponse = results.get("userEventsResponse");
        assertThat(userEventsResponse.getEvents()).hasSize(1);

        UserEventResponse userEventResponse = userEventsResponse.getEvents().get(0);
        assertThat(userEventResponse.getEventId()).isNotNull();
        assertThat(userEventResponse).hasFieldOrPropertyWithValue("payload", CONTACT_REQUEST_PAYLOAD);

        results.set("contactRequestEventId", userEventResponse.getEventId());
    }

    void bobsReceivedContactRequestEventIsDeleted() {
        List<UserEvent> userEvents = this.userEvents.userEventsDataWithBoxAddress(BOB_BOX_ADDRESS);
        assertThat(userEvents).isEmpty();
    }

    Then bobUserPayloadIsNotUpdated() {
        User user = profiles.userDataWithPseudo(BOB_PSEUDO);
        assertThat(user.getPayload()).isEqualTo(BOB_PAYLOAD);
        return this;
    }

    Then bobUserPayloadIsUpdated() {
        User user = profiles.userDataWithPseudo(BOB_PSEUDO);
        assertThat(user.getPayload()).isEqualTo(BOB_PAYLOAD_UPDATED);
        return this;
    }
}
