package be.seeld.acceptance;

import java.util.HashMap;
import java.util.Map;

public final class Results {

    private final Map<String, Item> values = new HashMap<>();

    public void delete(String key) {
        values.remove(key);
    }

    public <T> T get(String key) {
        Item item = values.get(key);
        return item != null ? item.get() : null;
    }

    public void set(String key, Object value) {
        if (value != null) {
            values.put(key, Item.of(value.getClass(), value));
        } else {
            values.put(key, null);
        }
    }

    private static final class Item {

        public static Item of(Class<?> type, Object value) {
            return new Item(type, value);
        }

        private final Class<?> type;
        private final Object value;

        private Item(Class<?> type, Object value) {
            this.type = type;
            this.value = value;
        }

        public <T> T get() {
            //noinspection unchecked
            return (T) type.cast(value);
        }
    }
}
