package be.seeld.acceptance.conversations;

import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.conversations.ConversationsWebApi.ConversationsResponse;
import be.seeld.conversations.messages.Message;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static be.seeld.acceptance.conversations.ConversationsATest.BOX_ADDRESS_1;
import static be.seeld.acceptance.conversations.ConversationsATest.CONVERSATION_ID_1;
import static org.assertj.core.api.Assertions.assertThat;

class Then {

    final Then and = this;

    private final Messages messages;
    private final Results results;

    Then(Messages messages, Results results) {
        this.messages = messages;
        this.results = results;
    }

    void aliceConversationShouldBeDeleted() {
        List<Message> messages = this.messages.messagesWithConversationId(CONVERSATION_ID_1);
        assertThat(messages).hasSize(0);
    }

    Then theseConversationsShouldBeReturned(UUID... conversationIds) {
        List<ConversationsResponse.Conversation> conversations = results.get("conversations");
        assertThat(conversations).hasSize(conversationIds.length);
        assertThat(conversations).extracting("conversationId").contains(conversationIds);
        return this;
    }

    void theseConversationIdsShouldBeReturned(UUID... conversationIds) {
        Set<UUID> returnedConversationIds = results.get("conversationIds");
        assertThat(returnedConversationIds).hasSize(conversationIds.length);
        assertThat(returnedConversationIds).contains(conversationIds);
    }

    void theConversationsShouldBearTheFollowingMessageIdsInThisOrder(UUID... messageIds) {
        List<ConversationsResponse.Conversation> conversations = results.get("conversations");
        List<UUID> conversationsMessagesIds = conversations.stream()
                .map(ConversationsResponse.Conversation::getMessageId)
                .collect(Collectors.toList());
        assertThat(conversationsMessagesIds).containsExactly(messageIds);
    }

    void bobMessageShouldBeDeleted() {
        List<Message> messages = this.messages.messagesWithBoxAddress(BOX_ADDRESS_1);
        assertThat(messages).isEmpty();
    }
}
