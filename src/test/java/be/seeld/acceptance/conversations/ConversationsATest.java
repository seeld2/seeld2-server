package be.seeld.acceptance.conversations;

import be.seeld.acceptance.Acceptance;
import be.seeld.acceptance.Conversations;
import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.common.encryption.aes.AES;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.UUID;

import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.BOB_BOX_ADDRESS;

public class ConversationsATest extends Acceptance {

    static final String ALICE_BOB_HMAC_HASH = "aliceBobHmacHash";

    static final UUID BOX_ADDRESS_1 = UUID.fromString("6fbd5b3c-47c0-447e-b802-a338381642b4");
    static final UUID BOX_ADDRESS_2 = UUID.fromString("e6c60430-e9ff-4a54-9fba-8742bd01cca6");
    static final UUID BOX_ADDRESS_3 = UUID.fromString("963cad80-4ec4-4a0f-87b0-4b4da51d93a5");

    static final UUID CONVERSATION_ID_1 = UUID.fromString("47a0cc49-e60c-44f4-b9db-e07d218b1acc");
    static final UUID CONVERSATION_ID_2 = UUID.fromString("f5da19fb-38d7-4878-aff1-87c6e72d07c6");
    static final UUID CONVERSATION_ID_3 = UUID.fromString("c2a12730-d7ad-4fef-a1f4-9d41e97f5dcb");

    private static final long EXPIRES_ON = 123456789L;

    static final UUID MESSAGE_ID_1 = UUID.fromString("16d9f813-0350-4ece-88e5-81c6633330d1");
    static final UUID MESSAGE_ID_2 = UUID.fromString("a54abb63-7778-48f4-a06d-a871ccdef3b4");
    static final UUID MESSAGE_ID_3 = UUID.fromString("b9c42d7b-4b13-4034-afc9-2cc5a11aa043");
    static final UUID MESSAGE_ID_4 = UUID.fromString("3a5105cb-ca42-4848-b1cb-ca6a966248a5");

    private final Results results = new Results();

    @Autowired
    @Qualifier("pwaAesBase64")
    private AES aesBase64;

    private Given given;
    private When when;
    private Then then;

    @Before
    public void setUp() {
        super.setUp();

        Conversations conversations = new Conversations(webApiTestClient);
        Messages messages = new Messages(aesBase64, messageData, messageLifeData, webApiTestClient);

        given = new Given(appSecurityProperties, messages, results, srp6aProperties, webApiTestClient);
        when = new When(conversations, results);
        then = new Then(messages, results);
    }

    @Test
    public void shouldDeleteASpecifiedConversationMessage() {

        given.aMessage(BOX_ADDRESS_1, MESSAGE_ID_1, CONVERSATION_ID_1, ALICE_BOB_HMAC_HASH, 1L)
                .and.aMessageLife(EXPIRES_ON, BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

        given.bob();
        when.bobRequestsToDeleteAMessage(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, ALICE_BOB_HMAC_HASH);
        then.bobMessageShouldBeDeleted();
    }

    @Test
    public void shouldDeleteAConversation() {

        given.alice();

        given.aMessage(ALICE_BOX_ADDRESS, CONVERSATION_ID_1, MESSAGE_ID_1)
                .and.aMessage(ALICE_BOX_ADDRESS, CONVERSATION_ID_1, ALICE_BOB_HMAC_HASH, MESSAGE_ID_2)
                .and.aMessage(ALICE_BOX_ADDRESS, CONVERSATION_ID_2, ALICE_BOB_HMAC_HASH, MESSAGE_ID_3)
                .and.aMessage(BOB_BOX_ADDRESS, CONVERSATION_ID_3, ALICE_BOB_HMAC_HASH, MESSAGE_ID_4);

        when.aliceRequestsToDeleteAConversation();
        then.aliceConversationShouldBeDeleted();
    }

    @Test
    public void fetchConversationIds_WhenConversationsHaveNoHmacHashes() {

        given.aMessage(BOX_ADDRESS_1, MESSAGE_ID_1, CONVERSATION_ID_1, 1L)
                .and.aMessage(BOX_ADDRESS_1, MESSAGE_ID_2, CONVERSATION_ID_2, 2L)
                .and.aMessage(BOX_ADDRESS_2, MESSAGE_ID_3, CONVERSATION_ID_2, 3L)
                .and.aMessage(BOX_ADDRESS_3, MESSAGE_ID_4, CONVERSATION_ID_3, 4L);

        given.bob();
        given.bobHasTheseReadFromBoxAddresses(BOX_ADDRESS_1, BOX_ADDRESS_2);
        when.bobFetchesTheConversationIdsForHisReadFromBoxAddresses_WithoutSpecifyingHmacHashes();
        then.theseConversationIdsShouldBeReturned(CONVERSATION_ID_1, CONVERSATION_ID_2);
    }

    @Test
    public void fetchConversationIds() {

        given.aMessage(BOX_ADDRESS_1, MESSAGE_ID_1, CONVERSATION_ID_1, ALICE_BOB_HMAC_HASH, 1L)
                .and.aMessage(BOX_ADDRESS_1, MESSAGE_ID_2, CONVERSATION_ID_2, ALICE_BOB_HMAC_HASH, 2L)
                .and.aMessage(BOX_ADDRESS_2, MESSAGE_ID_3, CONVERSATION_ID_2, ALICE_BOB_HMAC_HASH, 3L)
                .and.aMessage(BOX_ADDRESS_3, MESSAGE_ID_4, CONVERSATION_ID_3, ALICE_BOB_HMAC_HASH, 4L);

        given.bob();
        given.bobHasTheseReadFromBoxAddresses(BOX_ADDRESS_1, BOX_ADDRESS_2);
        when.bobFetchesTheConversationIdsForHisReadFromBoxAddresses(ALICE_BOB_HMAC_HASH);
        then.theseConversationIdsShouldBeReturned(CONVERSATION_ID_1, CONVERSATION_ID_2);
    }

    @Test
    public void fetchConversations_WhenConversationsHaveNoHmacHashes() {

        given.aMessage(BOX_ADDRESS_1, MESSAGE_ID_1, CONVERSATION_ID_1, 1L)
                .and.aMessage(BOX_ADDRESS_1, MESSAGE_ID_2, CONVERSATION_ID_2, 2L)
                .and.aMessage(BOX_ADDRESS_2, MESSAGE_ID_3, CONVERSATION_ID_3, 3L)
                .and.aMessage(BOX_ADDRESS_3, MESSAGE_ID_4, CONVERSATION_ID_3, 4L);

        given.bob();
        given.bobHasTheseReadFromBoxAddresses(BOX_ADDRESS_1, BOX_ADDRESS_2, BOX_ADDRESS_3);
        when.bobFetchesTheConversationsWithIds_WithoutSpecifyingHmacHashes();
        then.theseConversationsShouldBeReturned(CONVERSATION_ID_1, CONVERSATION_ID_2, CONVERSATION_ID_3)
                .and.theConversationsShouldBearTheFollowingMessageIdsInThisOrder(MESSAGE_ID_4, MESSAGE_ID_2, MESSAGE_ID_1);
    }

    @Test
    public void fetchConversations() {

        given.aMessage(BOX_ADDRESS_1, MESSAGE_ID_1, CONVERSATION_ID_1, ALICE_BOB_HMAC_HASH, 1L)
                .and.aMessage(BOX_ADDRESS_1, MESSAGE_ID_2, CONVERSATION_ID_2, ALICE_BOB_HMAC_HASH, 2L)
                .and.aMessage(BOX_ADDRESS_2, MESSAGE_ID_3, CONVERSATION_ID_3, ALICE_BOB_HMAC_HASH, 3L)
                .and.aMessage(BOX_ADDRESS_3, MESSAGE_ID_4, CONVERSATION_ID_3, ALICE_BOB_HMAC_HASH, 4L);

        given.bob();
        given.bobHasTheseReadFromBoxAddresses(BOX_ADDRESS_1, BOX_ADDRESS_2, BOX_ADDRESS_3);
        when.bobFetchesTheConversationsForTheseBoxAddresses(ALICE_BOB_HMAC_HASH);
        then.theseConversationsShouldBeReturned(CONVERSATION_ID_1, CONVERSATION_ID_2, CONVERSATION_ID_3)
                .and.theConversationsShouldBearTheFollowingMessageIdsInThisOrder(MESSAGE_ID_4, MESSAGE_ID_2, MESSAGE_ID_1);
    }
}
