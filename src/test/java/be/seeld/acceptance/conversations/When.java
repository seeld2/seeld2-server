package be.seeld.acceptance.conversations;

import be.seeld.acceptance.Conversations;
import be.seeld.acceptance.Results;
import be.seeld.conversations.ConversationsWebApi.ConversationIdsResponse;
import be.seeld.conversations.ConversationsWebApi.ConversationsResponse;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static be.seeld.acceptance.conversations.ConversationsATest.ALICE_BOB_HMAC_HASH;
import static be.seeld.acceptance.conversations.ConversationsATest.CONVERSATION_ID_1;
import static be.seeld.common.lang.Maps.Entry;
import static be.seeld.common.lang.Maps.mapOf;
import static test.TestUsers.ALICE_BOX_ADDRESS;

class When {

    private final Results results;
    private final Conversations conversations;

    When(Conversations conversations, Results results) {
        this.conversations = conversations;
        this.results = results;
    }

    void aliceRequestsToDeleteAConversation() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_BOX_ADDRESS, ALICE_BOB_HMAC_HASH));
        conversations.deleteConversation(CONVERSATION_ID_1, hmacHashesByBoxAddress);
    }

    void bobFetchesTheConversationIdsForHisReadFromBoxAddresses(String hmacHash) {
        Set<UUID> boxAddresses = results.get("bobReadFromBoxAddresses");
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(boxAddresses, hmacHash);
        ConversationIdsResponse conversationIdsResponse = conversations.fetchConversationIds(hmacHashesByBoxAddress);
        results.set("conversationIds", conversationIdsResponse.getConversationIds());
    }

    void bobFetchesTheConversationIdsForHisReadFromBoxAddresses_WithoutSpecifyingHmacHashes() {
        Set<UUID> boxAddresses = results.get("bobReadFromBoxAddresses");
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(boxAddresses, null);
        ConversationIdsResponse conversationIdsResponse = conversations.fetchConversationIds(hmacHashesByBoxAddress);
        results.set("conversationIds", conversationIdsResponse.getConversationIds());
    }

    void bobFetchesTheConversationsForTheseBoxAddresses(String hmacHash) {
        Set<UUID> boxAddresses = results.get("bobReadFromBoxAddresses");
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(boxAddresses, hmacHash);
        ConversationsResponse conversationsResponse = conversations.fetchConversations(hmacHashesByBoxAddress);
        results.set("conversations", conversationsResponse.getConversations());
    }

    void bobFetchesTheConversationsWithIds_WithoutSpecifyingHmacHashes() {
        Set<UUID> boxAddresses = results.get("bobReadFromBoxAddresses");
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(boxAddresses, null);
        ConversationsResponse conversationsResponse = conversations.fetchConversations(hmacHashesByBoxAddress);
        results.set("conversations", conversationsResponse.getConversations());
    }

    void bobRequestsToDeleteAMessage(UUID boxAddress, UUID conversationId, UUID messageId, String hmacHash) {
        conversations.deleteMessage(boxAddress, conversationId, messageId, hmacHash);
    }
}
