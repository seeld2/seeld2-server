package be.seeld.acceptance.conversations;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.common.encryption.Encryptables;
import be.seeld.conversations.messages.Message;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import test.rest.WebApiTestClient;

import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.conversations.messages.MessageFixture.message;

class Given extends be.seeld.acceptance.Given {

    final Given and = this;

    private final Messages messages;
    private final Results results;

    Given(AppSecurityProperties appSecurityProperties,
          Messages messages,
          Results results,
          Srp6aProperties srp6aProperties,
          WebApiTestClient webApiTestClient) {
        super(appSecurityProperties, results, srp6aProperties, webApiTestClient);

        this.messages = messages;
        this.results = results;
    }

    Given aMessage(UUID boxAddress, UUID conversationId, UUID messageId) {
        messages.messagesData(message(boxAddress, conversationId, null, messageId));
        return this;
    }

    Given aMessage(UUID boxAddress, UUID messageId, UUID conversationId, long timeline) {
        messages.messagesData(message(boxAddress, conversationId, null, messageId, timeline));
        return this;
    }

    Given aMessage(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId) {
        messages.messagesData(message(boxAddress, conversationId, hmacHash, messageId));
        return this;
    }

    Given aMessage(UUID boxAddress, UUID messageId, UUID conversationId, String hmacHash, long timeline) {
        messages.messagesData(message(boxAddress, conversationId, hmacHash, messageId, timeline));
        return this;
    }

    void aMessageLife(long expiresOn, UUID boxAddress, UUID conversationId, UUID messageId) {
        messages.messageLivesData(expiresOn, boxAddress, conversationId, messageId);
    }

    void bobHasTheseReadFromBoxAddresses(UUID... boxAddresses) {
        results.set("bobReadFromBoxAddresses", setOf(boxAddresses));
    }
}
