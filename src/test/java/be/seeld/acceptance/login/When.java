package be.seeld.acceptance.login;

import be.seeld.login.ChallengeResponse;
import be.seeld.login.LoginRequest;
import be.seeld.login.LoginResponse;
import com.nimbusds.srp6.BigIntegerUtils;
import org.springframework.http.ResponseEntity;
import test.junit.RestAssertions;
import test.rest.WebApiTestClient;

import static be.seeld.acceptance.login.LoginATest.INEXISTENT_USERNAME;
import static be.seeld.acceptance.login.LoginATest.Results;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_PSEUDO;

class When {

    protected final When and = this;

    private final WebApiTestClient restClient;
    private final Results results;

    When(WebApiTestClient restClient, Results results) {

        this.restClient = restClient;
        this.results = results;
    }

    void requestingALoginChallenge() {

        ResponseEntity<?> response = restClient.get("/api/login/challenge/" + ALICE_PSEUDO, ChallengeResponse.class);

        ChallengeResponse challengeResponse = RestAssertions.assertThatOkResponseIs(response, ChallengeResponse.class);
        assertThat(challengeResponse).isNotNull().hasNoNullFieldsOrProperties();

        results.challengeResponse = challengeResponse;
    }

    void requestingALoginChallengeForAUsernameThatDoesNotExist() {

        ResponseEntity<?> response = restClient.get("/api/login/challenge/" + INEXISTENT_USERNAME, ChallengeResponse.class);

        ChallengeResponse challengeResponse = RestAssertions.assertThatOkResponseIs(response, ChallengeResponse.class);
        assertThat(challengeResponse).isNotNull().hasNoNullFieldsOrProperties();

        results.challengeResponse = challengeResponse;
    }

    void subsequentlySubmittingLoginCredentials() {
        LoginRequest loginRequest = new LoginRequest(
                ALICE_PSEUDO,
                BigIntegerUtils.toHex(results.credentials.A),
                BigIntegerUtils.toHex(results.credentials.M1));
        results.response = restClient.post("/api/login", loginRequest, LoginResponse.class);
    }
}
