package be.seeld.acceptance.login;

import be.seeld.acceptance.Acceptance;
import be.seeld.SecurityConfiguration;
import be.seeld.login.ChallengeResponse;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSession;
import com.nimbusds.srp6.SRP6ClientCredentials;
import com.nimbusds.srp6.SRP6Exception;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class LoginATest extends Acceptance {

    static final String INEXISTENT_USERNAME = "inexistent username";
    static final String WRONG_PASSPHRASE = "wrong passphrase";

    private final Results results = new Results();

    @Autowired
    private Srp6aProperties srp6aProperties;
    @Autowired
    private SecurityConfiguration.AppSecurityProperties appSecurityProperties;

    private When when;
    private Then then;

    @Before
    public void setUp() {
        super.setUp();

        when = new When(webApiTestClient, results);
        then = new Then(appSecurityProperties, srp6aProperties, results);
    }

    @Test
    public void loginSuccessfully() throws SRP6Exception {

        when.requestingALoginChallenge();
        then.userShouldGetAResponseThatAllowsPreparingTheLoginCredentials();

        when.subsequentlySubmittingLoginCredentials();
        then.userShouldObtainAValidM2FromWhichHeCanExtractSessionKey()
                .and.userShouldObtainAProfileThatCanBeDecryptedUsingTheSessionKey()
                .and.userShouldObtainAPayload()
                .and.userShouldObtainAJwtToken();
    }

    @Test
    public void preventLoginWhenUsernameDoesNotExist() throws SRP6Exception {

        when.requestingALoginChallengeForAUsernameThatDoesNotExist();
        then.userShouldGetAResponseThatAllowsPreparingTheLoginCredentials();

        when.subsequentlySubmittingLoginCredentials();
        then.userShouldBeUnauthorized();
    }

    @Test
    public void preventLoginWhenPasswordIsInvalid() throws SRP6Exception {

        when.requestingALoginChallenge();
        then.userPreparesTheLoginCredentialsUsingAnInvalidPassphrase();

        when.subsequentlySubmittingLoginCredentials();
        then.userShouldBeUnauthorized();
    }

    static class Results {
        ChallengeResponse challengeResponse;
        SRP6JavaClientSession clientSession;
        SRP6ClientCredentials credentials;
        ResponseEntity<?> response;
        String sessionKey;
    }
}
