package be.seeld.acceptance.login;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.SecurityConfiguration.AppSecurityProperties.AesProperties;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import be.seeld.login.LoginResponse;
import be.seeld.common.encryption.aes.AES;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSessionSHA256;
import com.nimbusds.srp6.SRP6ClientSession;
import com.nimbusds.srp6.SRP6Exception;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import static be.seeld.acceptance.login.LoginATest.Results;
import static be.seeld.acceptance.login.LoginATest.WRONG_PASSPHRASE;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_PASSPHRASE;
import static test.TestUsers.ALICE_PSEUDO;
import static test.junit.RestAssertions.assertThatCreateResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

class Then {

    protected final Then and = this;

    private final AesProperties aesProperties;
    private final Results results;
    private final Srp6aProperties srp6aProperties;

    Then(AppSecurityProperties appSecurityProperties, Srp6aProperties srp6aProperties, Results results) {
        this.aesProperties = appSecurityProperties.getAes();
        this.srp6aProperties = srp6aProperties;
        this.results = results;
    }

    void userPreparesTheLoginCredentialsUsingAnInvalidPassphrase() throws SRP6Exception {

        SRP6JavaClientSession clientSession = new SRP6JavaClientSessionSHA256(srp6aProperties.getN(), srp6aProperties.getG());

        clientSession.step1(ALICE_PSEUDO, WRONG_PASSPHRASE);
        assertThat(clientSession.getState()).isEqualTo(SRP6ClientSession.State.STEP_1);

        results.credentials = clientSession.step2(results.challengeResponse.getSalt(), results.challengeResponse.getB());
        assertThat(clientSession.getState()).isEqualTo(SRP6ClientSession.State.STEP_2);

        results.clientSession = clientSession;
    }

    void userShouldBeForbidden() {
        assertThatResponseIs(results.response, HttpStatus.FORBIDDEN);
    }

    void userShouldBeUnauthorized() {
        assertThatResponseIs(results.response, HttpStatus.UNAUTHORIZED);
    }

    void userShouldGetAResponseThatAllowsPreparingTheLoginCredentials() throws SRP6Exception {

        SRP6JavaClientSession clientSession = new SRP6JavaClientSessionSHA256(srp6aProperties.getN(), srp6aProperties.getG());

        clientSession.step1(ALICE_PSEUDO, ALICE_PASSPHRASE);
        assertThat(clientSession.getState()).isEqualTo(SRP6ClientSession.State.STEP_1);

        results.credentials = clientSession.step2(results.challengeResponse.getSalt(), results.challengeResponse.getB());
        assertThat(clientSession.getState()).isEqualTo(SRP6ClientSession.State.STEP_2);

        results.clientSession = clientSession;
    }

    void userShouldObtainAJwtToken() {

        HttpHeaders responseHeaders = results.response.getHeaders();
        assertThat(responseHeaders.containsKey(HttpHeaders.AUTHORIZATION)).isTrue();
        String authHeader = responseHeaders.getFirst(HttpHeaders.AUTHORIZATION);
        assertThat(authHeader).startsWith("Bearer ");
        assertThat(authHeader.length()).isGreaterThan(7);
    }

    Then userShouldObtainAPayload() {

        LoginResponse loginResponse = assertThatCreateResponseIs(results.response, LoginResponse.class);

        assertThat(loginResponse.getPayload()).isNotNull().isNotEmpty();

        return this;
    }

    Then userShouldObtainAValidM2FromWhichHeCanExtractSessionKey() throws SRP6Exception {

        LoginResponse loginResponse = assertThatCreateResponseIs(results.response, LoginResponse.class);

        results.clientSession.step3(loginResponse.getM2());

        results.sessionKey = results.clientSession.getSessionKey(true);
        assertThat(results.sessionKey).isNotNull().isNotEmpty().hasSize(64);

        return this;
    }

    Then userShouldObtainAProfileThatCanBeDecryptedUsingTheSessionKey() {

        LoginResponse loginResponse = assertThatCreateResponseIs(results.response, LoginResponse.class);

        assertThat(loginResponse.getProfile()).isNotNull().isNotEmpty();

        AES aes = AES.of(new AES.Config(
                aesProperties.getSecureRandomAlgorithm(),
                aesProperties.getMode(),
                aesProperties.getPadding(),
                null,
                aesProperties.getKeySize(),
                AES.SecretKeyType.HEX,
                aesProperties.getIvSize()
        ));
        String profile = aes.decrypt(loginResponse.getProfile(), results.sessionKey);

        assertThat(profile).isNotNull().isNotEmpty();
        System.out.println(profile);

        return this;
    }
}
