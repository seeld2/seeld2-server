package be.seeld.acceptance.registration;

import be.seeld.registration.AvailabilityResponse;
import be.seeld.registration.RegistrationRequest;
import be.seeld.registration.RegistrationRequestFixture;
import org.springframework.http.ResponseEntity;
import test.rest.WebApiTestClient;

import static be.seeld.acceptance.registration.RegistrationsATest.UNREGISTERED_PSEUDO;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static test.junit.RestAssertions.assertThatResponseIs;

class When {

    private final WebApiTestClient restClient;
    private final RegistrationsATest.Results results;

    When(WebApiTestClient restClient, RegistrationsATest.Results results) {
        this.restClient = restClient;
        this.results = results;
    }

    void checkingForANewUsername() {

        ResponseEntity<?> response = restClient
                .get("/api/registrations/" + UNREGISTERED_PSEUDO + "/availability", AvailabilityResponse.class);

        AvailabilityResponse availabilityResponse = assertThatResponseIs(response, OK, AvailabilityResponse.class);
        results.availability = availabilityResponse.isAvailable();
    }

    void checkingForAnExistingUsername() {

        ResponseEntity<?> response = restClient.get("/api/registrations/alice/availability", AvailabilityResponse.class);

        AvailabilityResponse availabilityResponse = assertThatResponseIs(response, OK, AvailabilityResponse.class);
        results.availability = availabilityResponse.isAvailable();
    }

    void submittingInvalidRegistrationDetails() {
        RegistrationRequest registrationRequest = RegistrationRequestFixture.registrationRequest(UNREGISTERED_PSEUDO);
        registrationRequest.setUsername("");

        ResponseEntity<?> response = restClient.post("/api/registrations", registrationRequest);

        assertThatResponseIs(response, BAD_REQUEST);
    }

    void submittingRegistrationDetails() {
        RegistrationRequest registrationRequest = RegistrationRequestFixture.registrationRequest(UNREGISTERED_PSEUDO);

        ResponseEntity<?> response = restClient.post("/api/registrations", registrationRequest);

        assertThatResponseIs(response, CREATED);
    }
}
