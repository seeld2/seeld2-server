package be.seeld.acceptance.registration;

import be.seeld.users.User;
import be.seeld.common.encryption.Encryptables;
import test.data.UserData;

import static be.seeld.acceptance.registration.RegistrationsATest.UNREGISTERED_PSEUDO;
import static org.assertj.core.api.Assertions.assertThat;

class Then {

    private final Encryptables encryptables;
    private final RegistrationsATest.Results results;
    private final UserData userData;

    Then(RegistrationsATest.Results results, Encryptables encryptables, UserData userData) {
        this.encryptables = encryptables;
        this.results = results;
        this.userData = userData;
    }

    void aValidUserShouldBeCreated() {

        User user = userData.find(UNREGISTERED_PSEUDO);

        assertThat(user)
                .isNotNull()
                .hasNoNullFieldsOrPropertiesExcept("subscriptionValidUntil")
                .hasFieldOrPropertyWithValue("username", UNREGISTERED_PSEUDO);

        User unencryptedUser = encryptables.ofEncrypted(user).getUnencrypted();

        assertThat(unencryptedUser)
                .hasFieldOrProperty("boxAddress")
                .hasFieldOrProperty("exchangeKey");
    }

    void itShouldBeKnownAsAvailable() {
        assertThat(results.availability).isTrue();
    }

    void itShouldBeKnownAsNotAvailable() {
        assertThat(results.availability).isFalse();
    }

    void noUserShouldBeCreated() {

        User user = userData.find(UNREGISTERED_PSEUDO);

        assertThat(user).isNull();
    }
}
