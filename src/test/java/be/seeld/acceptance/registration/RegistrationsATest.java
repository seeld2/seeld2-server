package be.seeld.acceptance.registration;

import be.seeld.acceptance.Acceptance;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import be.seeld.registration.RegistrationRequest;
import be.seeld.users.User;
import com.bitbucket.thinbus.srp6.js.HexHashedVerifierGenerator;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSessionSHA256;
import com.nimbusds.srp6.SRP6CryptoParams;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RegistrationsATest extends Acceptance {

    static final String UNREGISTERED_PSEUDO = "unregistered";

    private static final String KDF_TYPE = "scrypt";
    private static final String SRP_TYPE = "srp6a";
    private static final String SYSTEM_KEYS_TYPE = "ecc.x25519";

    private final Results results = new Results();

    @Autowired
    private Srp6aProperties srp6aProperties;

    @Autowired
    private SecureRandom secureRandom;

    private When when;
    private Then then;

    @Before
    public void setUp() {
        super.setUp();

        when = new When(webApiTestClient, results);
        then = new Then(results, encryptables, userData);
    }

    @Test
    public void returnTheAvailabilityOfAGivenUsername() {

        when.checkingForANewUsername();
        then.itShouldBeKnownAsAvailable();

        when.checkingForAnExistingUsername();
        then.itShouldBeKnownAsNotAvailable();
    }

    @Test
    public void registerANewUser() {

        when.submittingRegistrationDetails();
        then.aValidUserShouldBeCreated();
    }

    @Test
    public void submitInvalidDetailsAtRegistration() {

        when.submittingInvalidRegistrationDetails();
        then.noUserShouldBeCreated();
    }

    /**
     * This test is used to obtain the details of our test users (alice, bob, etc.).<br/>
     * It is not meant to run normally otherwise it will attempt to recreate the users that are already injected by
     * {@link test.Integration} for testing purposes!
     */
    // @Test
    public void registerTestUsers() {

        userData.deleteAll();

        testUsers.getAll().forEach((user -> {
            // KDF
            byte[] bytes = new byte[32];
            secureRandom.nextBytes(bytes);
            String kdfSalt = Hex.toHexString(bytes);

            // SRP
            String g = srp6aProperties.getG();
            String h = srp6aProperties.getH();
            String n = srp6aProperties.getN();
            int saltBytes = srp6aProperties.getSaltBytesLength();
            SRP6JavaClientSession clientSession = new SRP6JavaClientSessionSHA256(n, g);
            String srpSalt = clientSession.generateRandomSalt(saltBytes);
            SRP6CryptoParams srp6CryptoParams = new SRP6CryptoParams(new BigInteger(n), new BigInteger(g), h);
            HexHashedVerifierGenerator verifierGenerator = new HexHashedVerifierGenerator(srp6CryptoParams);
            String srpVerifier = verifierGenerator.generateVerifier(srpSalt, user.getUsername(), user.getPassphrase());

            // REGISTER
            RegistrationRequest registrationRequest = new RegistrationRequest();
            registrationRequest.setDisplayName(user.getDisplayName());
            registrationRequest.setPayload(user.getPayload());
            registrationRequest.setPicture(user.getPicture());
            registrationRequest.setUsername(user.getUsername());
            registrationRequest.setKdf(new RegistrationRequest.Kdf(kdfSalt, KDF_TYPE));
            registrationRequest.setSrp(new RegistrationRequest.Srp(srpSalt, SRP_TYPE , srpVerifier));
            registrationRequest.setSystemKeys(new RegistrationRequest.SystemKeys(
                    user.getSystemPrivateKeys(),
                    user.getSystemPublicKeys(),
                    SYSTEM_KEYS_TYPE));
            webApiTestClient.post("/api/registrations", registrationRequest);

            User encryptedBean = userData.find(user.getUsername());
            User registeredUser = encryptables.ofEncrypted(encryptedBean).getUnencrypted();
            System.out.println(registeredUser.toString());
        }));
    }

    static class Results {
        boolean availability;
    }
}
