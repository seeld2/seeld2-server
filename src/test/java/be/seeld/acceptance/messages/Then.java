package be.seeld.acceptance.messages;

import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.MessagesInResponse;

import java.util.List;

import static be.seeld.acceptance.messages.MessagesATest.ALICE_CONVERSATION_ID;
import static be.seeld.acceptance.messages.MessagesATest.ALICE_PAYLOAD;
import static be.seeld.acceptance.messages.MessagesATest.ALICE_WRITES_TO_BOB_BOX_ADDRESS;
import static be.seeld.acceptance.messages.MessagesATest.BOB_CONVERSATION_ID;
import static be.seeld.acceptance.messages.MessagesATest.BOB_PAYLOAD;
import static be.seeld.acceptance.messages.MessagesATest.MESSAGE_ID_1;
import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_BOX_ADDRESS;

class Then {

    final Then also = this;
    final Then and = this;

    private final Messages messages;
    private final Results results;

    public Then(Messages messages, Results results) {
        this.messages = messages;
        this.results = results;
    }

    void aliceShouldBeInformedOfNoUnreadMesssages() {
        assertThat(results.<Long>get("latestNewMessageTimeline")).isNull();
    }

    void aliceShouldBeInformedOfOneUnreadMessage() {
        assertThat(results.<Long>get("latestNewMessageTimeline")).isNotNull().isGreaterThan(0);
    }

    Then aMessageIsStoredOnAliceBoxAddress() {
        List<Message> messages = this.messages.messagesWithBoxAddress(ALICE_BOX_ADDRESS);

        assertThat(messages).isNotEmpty().hasSize(1);
        results.set("message", messages.get(0));

        assertThat(messages.get(0))
                .hasFieldOrPropertyWithValue("conversationId", ALICE_CONVERSATION_ID)
                .hasFieldOrPropertyWithValue("payload", ALICE_PAYLOAD);

        return this;
    }

    Then aMessageIsStoredOn_AliceWritesToBob_BoxAddress() {

        List<Message> messages = this.messages.messagesWithBoxAddress(ALICE_WRITES_TO_BOB_BOX_ADDRESS);

        assertThat(messages).isNotEmpty().hasSize(1);
        results.set("message", messages.get(0));

        assertThat(messages.get(0))
                .hasFieldOrPropertyWithValue("conversationId", BOB_CONVERSATION_ID)
                .hasFieldOrPropertyWithValue("payload", BOB_PAYLOAD);

        return this;
    }

    void bobObtainsAListOfMessageIdsForThatConversation() {
        ConversationMessagesIdsResponse messagesIdsResponse = results.get("messagesIdsResponse");
        assertThat(messagesIdsResponse.getMessagesIds())
                .hasSize(1)
                .containsExactly(MESSAGE_ID_1);
    }

    void bobObtainsAListOfMessagesForThatConversation() {
        MessagesInResponse messagesInResponse = results.get("messages");
        assertThat(messagesInResponse.getMessages())
                .hasSize(1)
                .first().hasFieldOrPropertyWithValue("messageId", MESSAGE_ID_1);
    }

    void bobObtainsAListOfMessagesMatchingTheMessagesIds() {
        MessagesInResponse messagesInResponse = results.get("messages");
        assertThat(messagesInResponse.getMessages())
                .hasSize(1)
                .first().hasFieldOrPropertyWithValue("messageId", MESSAGE_ID_1);
    }

    void bobMessageIsReturned() {
        MessagesInResponse messagesInResponse = results.get("messages");
        assertThat(messagesInResponse).isNotNull().hasNoNullFieldsOrProperties();
        assertThat(messagesInResponse.getMessages()).isNotEmpty()
                .first()
                .hasFieldOrPropertyWithValue("conversationId", BOB_CONVERSATION_ID)
                .hasFieldOrPropertyWithValue("payload", BOB_PAYLOAD)
                .hasFieldOrPropertyWithValue("readFromBoxAddress", ALICE_WRITES_TO_BOB_BOX_ADDRESS);
    }

    void bobReceivesNoMessagesBack() {
        MessagesInResponse messagesInResponse = results.get("messages");
        assertThat(messagesInResponse).isNotNull();
        assertThat(messagesInResponse.getMessages()).isEmpty();
    }

    void noMessageIdsAreReturned() {
        ConversationMessagesIdsResponse messagesIdsResponse = results.get("messagesIdsResponse");
        assertThat(messagesIdsResponse.getMessagesIds()).isEmpty();
    }

    void noMessagesAreReturned() {
        MessagesInResponse messagesInResponse = results.get("messages");
        assertThat(messagesInResponse.getMessages()).isEmpty();
    }

    Then theMessageHasAMessageId() {
        assertThat(results.<Message>get("message").getMessageId()).isNotNull();
        return this;
    }

    Then theMessageHasAnHmacHash() {
        assertThat(results.<Message>get("message").getHmacHash()).isNotNull();
        return this;
    }

    Then theMessageIdIsReturnedAsPartOfTheResponse() {
        assertThat(results.<Message>get("messageId")).isNotNull();
        return this;
    }

    Then theMessageIsStoredAsNotified() {
        assertThat(results.<Message>get("message").isNotified()).isTrue();
        return this;
    }

    Then theMessageIsStoredAsUnnotified() {
        assertThat(results.<Message>get("message").isNotified()).isFalse();
        return this;
    }
}
