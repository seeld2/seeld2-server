package be.seeld.acceptance.messages;

import be.seeld.acceptance.Acceptance;
import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.common.encryption.aes.AES;
import com.nimbusds.srp6.SRP6Exception;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.util.UUID;

import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.BOB_BOX_ADDRESS;

public class MessagesATest extends Acceptance {

    static final UUID ALICE_CONVERSATION_ID = UUID.fromString("ff557eb9-60e0-4209-8f14-980e160fa623");
    static final String ALICE_PAYLOAD = "alice payload";
    static final UUID ALICE_WRITES_TO_BOB_BOX_ADDRESS = UUID.fromString("b201ef8a-d50e-48db-aa82-557fe32ed112");

    static final String ALICE_BOB_HMAC_HASH = "aliceBobHmacHash";

    static final UUID BOB_WRITES_TO_ALICE_BOX_ADDRESS = UUID.fromString("d613f35e-a853-46f0-9a8c-1bd565ce017d");
    static final UUID BOB_CONVERSATION_ID = UUID.fromString("0eba8d7f-5fbe-4df8-8a04-650ccdfe5232");
    static final String BOB_PAYLOAD = "bob payload";

    static final UUID CONVERSATION_ID_1 = UUID.fromString("55d02dd2-89bb-4b75-915b-efc64c461ea2");
    static final UUID CONVERSATION_ID_2 = UUID.fromString("b321659b-11bb-45b6-9508-2884060f88df");
    static final UUID MESSAGE_ID_1 = UUID.fromString("cc484446-490c-492d-bb3e-3534bc9ebab2");
    static final UUID MESSAGE_ID_2 = UUID.fromString("f6b0fc5c-b40a-4b36-9ff1-64246b871e9c");

    private final Results results = new Results();

    @Autowired
    @Qualifier("pwaAesBase64")
    private AES aesBase64;

    private Given given;
    private When when;
    private Then then;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        Messages messages = new Messages(aesBase64, messageData, messageLifeData, webApiTestClient);

        given = new Given(appSecurityProperties, messages, results, srp6aProperties, webApiTestClient);
        when = new When(messages, results);
        then = new Then(messages, results);
    }

    @Test
    public void beginAConversation() throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();
        when.alicePostsAMessageForBob();
        then.aMessageIsStoredOnAliceBoxAddress()
                .and.theMessageIsStoredAsNotified()
                .and.theMessageHasAnHmacHash()
                .and.theMessageHasAMessageId()
                .and.theMessageIdIsReturnedAsPartOfTheResponse()
                .also.aMessageIsStoredOn_AliceWritesToBob_BoxAddress()
                .and.theMessageIsStoredAsUnnotified()
                .and.theMessageHasAnHmacHash()
                .and.theMessageHasAMessageId();

        given.bob();
        when.bobFetchesHisNewMessagesFromHis_AliceWritesToBob_BoxAddress();
        then.bobMessageIsReturned();
    }

    @Test
    public void fetchAConversationStoredWithoutHmacHashWhenNoHmacHashIsProvided() {

        given.aMessageForBobStoredWithoutHmacHash(ALICE_WRITES_TO_BOB_BOX_ADDRESS,
                MESSAGE_ID_1, BOB_CONVERSATION_ID, BOB_PAYLOAD);

        given.bob();
        when.bobFetchesHisNewMessagesFromHis_AliceWritesToBob_BoxAddressWithoutProvidingHmacHashes();
        then.bobMessageIsReturned();
    }

    @Test
    public void blockFetchingAConversationStoredWithHmacHashWhenNoHmacHashIsProvided()
            throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();
        when.alicePostsAMessageForBob();
        then.aMessageIsStoredOnAliceBoxAddress()
                .and.theMessageHasAnHmacHash();

        given.bob();
        when.bobFetchesHisNewMessagesFromHis_AliceWritesToBob_BoxAddressWithoutProvidingHmacHashes();
        then.bobReceivesNoMessagesBack();
    }

    @Test
    public void checkWhetherThereAreUnreadMessages() {

        given.alice();
        given.aliceHasTheseReadFromBoxAddresses(ALICE_BOX_ADDRESS, BOB_WRITES_TO_ALICE_BOX_ADDRESS);
        given.aReadMessage(ALICE_BOX_ADDRESS, MESSAGE_ID_1, CONVERSATION_ID_1);

        when.aliceChecksForUnreadMessages();
        then.aliceShouldBeInformedOfNoUnreadMesssages();

        given.anUnreadMessage(BOB_WRITES_TO_ALICE_BOX_ADDRESS, MESSAGE_ID_2, CONVERSATION_ID_2);

        when.aliceChecksForUnreadMessages();
        then.aliceShouldBeInformedOfOneUnreadMessage();
    }

    @Test
    public void synchronizeAConversationMessagesStoredWithoutHmacHash() {

        given.bob();

        given.aMessage(BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesIdsForAConversationWithoutSpecifyingHmacHashes();
        then.bobObtainsAListOfMessageIdsForThatConversation();

        when.bobFetchesTheListOfMessagesByMessagesIdsWithoutSpecifyingHmacHashes();
        then.bobObtainsAListOfMessagesMatchingTheMessagesIds();
    }

    @Test
    public void fetchAPageOfConversationMessagesStoredWithoutHmacHash() {

        given.bob();

        given.aMessage(BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesForAConversationWithoutSpecifyingHmacHashes();
        then.bobObtainsAListOfMessagesForThatConversation();
    }

    @Test
    public void synchronizeAConversationMessagesStoredWithHmacHash() {

        given.bob();

        given.aMessage(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, ALICE_BOB_HMAC_HASH, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesIdsForAConversation();
        then.bobObtainsAListOfMessageIdsForThatConversation();

        when.bobFetchesTheListOfMessagesByMessagesIds();
        then.bobObtainsAListOfMessagesMatchingTheMessagesIds();
    }

    @Test
    public void fetchAPageOfConversationMessagesStoredWithHmacHash() {

        given.bob();

        given.aMessage(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, ALICE_BOB_HMAC_HASH, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesForAConversation();
        then.bobObtainsAListOfMessagesForThatConversation();
    }

    @Test
    public void blockSynchronizationOfAConversationMessagesWithHmacHashesWhenNoHmacHashesAreProvided() {

        given.bob();

        given.aMessage(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, ALICE_BOB_HMAC_HASH, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesIdsForAConversationWithoutSpecifyingHmacHashes();
        then.noMessageIdsAreReturned();

        when.bobFetchesTheListOfMessagesByMessagesIdsWithoutSpecifyingHmacHashes();
        then.noMessagesAreReturned();
    }

    @Test
    public void blockFetchingTheConversationMessagesWithHmacHashesWhenNoHmacHashesAreProvided() {

        given.bob();

        given.aMessage(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, ALICE_BOB_HMAC_HASH, MESSAGE_ID_1);
        when.bobFetchesTheListOfMessagesForAConversationWithoutSpecifyingHmacHashes();
        then.noMessagesAreReturned();
    }
}
