package be.seeld.acceptance.messages;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import test.rest.WebApiTestClient;

import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.conversations.messages.MessageFixture.message;

@SuppressWarnings("SameParameterValue")
public class Given extends be.seeld.acceptance.Given {

    private final Messages messages;
    private final Results results;

    Given(AppSecurityProperties appSecurityProperties,
          Messages messages,
          Results results,
          Srp6aProperties srp6aProperties,
          WebApiTestClient webApiTestClient) {
        super(appSecurityProperties, results, srp6aProperties, webApiTestClient);

        this.messages = messages;
        this.results = results;
    }

    void aliceHasTheseReadFromBoxAddresses(UUID... boxAddresses) {
        results.set("aliceReadFromBoxAddresses", setOf(boxAddresses));
    }

    void aMessage(UUID boxAddress, UUID conversationId, UUID messageId) {
        messages.messagesData(message(boxAddress, conversationId, null, messageId));
    }

    void aMessage(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId) {
        messages.messagesData(message(boxAddress, conversationId, hmacHash, messageId));
    }

    void aMessageForBobStoredWithoutHmacHash(UUID boxAddress, UUID messageId, UUID conversationId, String payload) {
        messages.messagesData(message(boxAddress, conversationId, null, messageId, false, payload, 1L));
    }

    void anUnreadMessage(UUID boxAddress, UUID messageId, UUID conversationId) {
        messages.messagesData(message(boxAddress, conversationId, messageId, false, 2L));
    }

    void aReadMessage(UUID boxAddress, UUID messageId, UUID conversationId) {
        messages.messagesData(message(boxAddress, conversationId, messageId, true, 1L));
    }
}
