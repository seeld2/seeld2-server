package be.seeld.acceptance.messages;

import be.seeld.acceptance.Messages;
import be.seeld.acceptance.Results;
import be.seeld.common.lang.Maps;
import be.seeld.conversations.ConversationsWebApi;
import be.seeld.conversations.messages.MessageOut;
import be.seeld.conversations.messages.MessagesInResponse;
import be.seeld.conversations.messages.MessagesOut;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static be.seeld.acceptance.messages.MessagesATest.ALICE_BOB_HMAC_HASH;
import static be.seeld.acceptance.messages.MessagesATest.ALICE_CONVERSATION_ID;
import static be.seeld.acceptance.messages.MessagesATest.ALICE_PAYLOAD;
import static be.seeld.acceptance.messages.MessagesATest.ALICE_WRITES_TO_BOB_BOX_ADDRESS;
import static be.seeld.acceptance.messages.MessagesATest.BOB_CONVERSATION_ID;
import static be.seeld.acceptance.messages.MessagesATest.BOB_PAYLOAD;
import static be.seeld.acceptance.messages.MessagesATest.MESSAGE_ID_1;
import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.Entry;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.messages.MessagesWebApi.TimelineResponse;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_EXCHANGE_KEY;

class When {

    private final Messages messages;
    private final Results results;

    When(Messages messages, Results results) {
        this.messages = messages;
        this.results = results;
    }

    void aliceChecksForUnreadMessages() {
        Set<UUID> readFromBoxAddresses = results.get("aliceReadFromBoxAddresses");
        TimelineResponse timelineResponse = messages
                .fetchLatestNewMessageTimeline(readFromBoxAddresses);
        results.set("latestNewMessageTimeline", timelineResponse.getTimeline());
    }

    void alicePostsAMessageForBob() throws JsonProcessingException {
        MessagesOut messagesOut = MessagesOut.builder()
                .messages(listOf(
                        MessageOut.builder()
                                .boxAddress(ALICE_BOX_ADDRESS)
                                .conversationId(ALICE_CONVERSATION_ID)
                                .hmacHash(ALICE_BOB_HMAC_HASH)
                                .payload(ALICE_PAYLOAD)
                                .build(),
                        MessageOut.builder()
                                .boxAddress(ALICE_WRITES_TO_BOB_BOX_ADDRESS)
                                .conversationId(BOB_CONVERSATION_ID)
                                .hmacHash(ALICE_BOB_HMAC_HASH)
                                .payload(BOB_PAYLOAD)
                                .build()
                ))
                .build();

        UUID messageId = messages.sendMessage(messagesOut, results.get("aliceLoginArtifacts"), ALICE_EXCHANGE_KEY);
        results.set("messageId", messageId);
    }

    void bobFetchesHisNewMessagesFromHis_AliceWritesToBob_BoxAddress() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH));
        MessagesInResponse messagesInResponse = messages.fetchNewMessages(hmacHashesByBoxAddress);
        results.set("messages", messagesInResponse);
    }

    void bobFetchesHisNewMessagesFromHis_AliceWritesToBob_BoxAddressWithoutProvidingHmacHashes() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, null));
        MessagesInResponse messagesInResponse = messages.fetchNewMessages(hmacHashesByBoxAddress);
        results.set("messages", messagesInResponse);
    }

    void bobFetchesTheListOfMessagesByMessagesIds() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH));
        MessagesInResponse messagesInResponse = messages
                .fetchMessagesWithIds(setOf(MESSAGE_ID_1), BOB_CONVERSATION_ID, hmacHashesByBoxAddress);
        results.set("messages", messagesInResponse);
    }

    void bobFetchesTheListOfMessagesForAConversation() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH));
        MessagesInResponse messagesInResponse = messages.fetchMessages(BOB_CONVERSATION_ID, hmacHashesByBoxAddress);
        results.set("messages", messagesInResponse);
    }

    void bobFetchesTheListOfMessagesByMessagesIdsWithoutSpecifyingHmacHashes() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, null));
        MessagesInResponse messagesInResponse = messages
                .fetchMessagesWithIds(setOf(MESSAGE_ID_1), BOB_CONVERSATION_ID, hmacHashesByBoxAddress);
        results.set("messages", messagesInResponse);
    }

    void bobFetchesTheListOfMessagesForAConversationWithoutSpecifyingHmacHashes() {
        MessagesInResponse messagesInResponse = messages.fetchMessages(BOB_CONVERSATION_ID, new HashMap<>());
        results.set("messages", messagesInResponse);
    }

    void bobFetchesTheListOfMessagesIdsForAConversation() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH));
        ConversationsWebApi.ConversationMessagesIdsResponse messagesIdsResponse = messages
                .fetchConversationMessagesIds(BOB_CONVERSATION_ID, hmacHashesByBoxAddress);
        results.set("messagesIdsResponse", messagesIdsResponse);
    }

    void bobFetchesTheListOfMessagesIdsForAConversationWithoutSpecifyingHmacHashes() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Entry.of(ALICE_WRITES_TO_BOB_BOX_ADDRESS, null));
        ConversationsWebApi.ConversationMessagesIdsResponse messagesIdsResponse = messages
                .fetchConversationMessagesIds(BOB_CONVERSATION_ID, hmacHashesByBoxAddress);
        results.set("messagesIdsResponse", messagesIdsResponse);
    }
}
