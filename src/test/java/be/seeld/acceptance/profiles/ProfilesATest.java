package be.seeld.acceptance.profiles;

import be.seeld.SecurityConfiguration;
import be.seeld.acceptance.Acceptance;
import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.common.encryption.aes.AES;
import be.seeld.login.LoginConfiguration;
import com.nimbusds.srp6.SRP6Exception;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;

import static test.TestUsers.ALICE_PAYLOAD;

public class ProfilesATest extends Acceptance {

    static final String ALICE_PAYLOAD_UPDATED = ALICE_PAYLOAD + ":updated!";

    private final Results results = new Results();

    @Autowired
    @Qualifier("pwaAesBase64")
    private AES aesBase64;
    @Autowired
    private SecurityConfiguration.AppSecurityProperties appSecurityProperties;
    @Autowired
    private LoginConfiguration.Srp6aProperties srp6aProperties;

    private Given given;
    private When when;
    private Then then;

    @Before
    public void setUp() {
        super.setUp();

        Profiles profiles = new Profiles(aesBase64, userData, webApiTestClient);

        given = new Given(appSecurityProperties, results, srp6aProperties, webApiTestClient);
        when = new When(profiles, results);
        then = new Then(profiles, results);
    }

    @Test
    public void deleteTheLoggedUsersProfile() throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();

        when.aliceDeletesHerProfile();
        then.aliceProfileShouldBeDeleted();
    }

    @Test
    public void fetchTheDetailsOfAnExistingUser() {

        given.alice();

        when.aliceFetchesTheDetailsOfBob();
        then.bobDetailsAreReturned();
    }

    @Test
    public void fetchThePayloadOfTheLoggedUser() throws IOException, SRP6Exception {

        given.bobAsTheLoggedUser();

        when.bobFetchesHisPayload();
        then.bobShouldReceiveAResponseThatContainsHisPayload();
    }

    @Test
    public void updateTheLoggedUsersProfilePayload() throws IOException, SRP6Exception {

        given.aliceAsTheLoggedUser();

        when.aliceUpdatesHerProfilePayload();
        then.alicePayloadShouldBeUpdated();
    }
}
