package be.seeld.acceptance.profiles;

import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.users.User;

import static be.seeld.acceptance.profiles.ProfilesATest.ALICE_PAYLOAD_UPDATED;
import static be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import static be.seeld.profiles.ProfilesWebApi.PayloadResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.BOB_PAYLOAD;
import static test.TestUsers.BOB_PSEUDO;
import static test.TestUsers.BOB_SYSTEM_PUBLIC_KEYS;

public class Then {

    private final Profiles profiles;
    private final Results results;

    Then(Profiles profiles, Results results) {
        this.profiles = profiles;
        this.results = results;
    }

    void aliceProfileShouldBeDeleted() {
        User user = this.profiles.userDataWithPseudo(ALICE_PSEUDO);
        assertThat(user).isNull();
    }

    void bobDetailsAreReturned() {
        DetailsResponse detailsResponse = results.get("details");
        assertThat(detailsResponse)
                .hasFieldOrPropertyWithValue("boxAddress", BOB_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("systemPublicKeys", BOB_SYSTEM_PUBLIC_KEYS)
                .hasFieldOrPropertyWithValue("username", BOB_PSEUDO);
    }

    void bobShouldReceiveAResponseThatContainsHisPayload() {
        PayloadResponse payloadResponse = results.get("payload");
        assertThat(payloadResponse.getPayload()).isEqualTo(BOB_PAYLOAD);
    }

    void alicePayloadShouldBeUpdated() {
        User user = profiles.userDataWithPseudo(ALICE_PSEUDO);
        assertThat(user.getPayload()).isEqualTo(ALICE_PAYLOAD_UPDATED);
    }
}
