package be.seeld.acceptance.profiles;

import be.seeld.acceptance.Results;
import test.rest.WebApiTestClient;

import static be.seeld.SecurityConfiguration.AppSecurityProperties;
import static be.seeld.login.LoginConfiguration.Srp6aProperties;

public class Given extends be.seeld.acceptance.Given {

    public Given(AppSecurityProperties appSecurityProperties,
                 Results results,
                 Srp6aProperties srp6aProperties,
                 WebApiTestClient webApiTestClient) {
        super(appSecurityProperties, results, srp6aProperties, webApiTestClient);

    }
}
