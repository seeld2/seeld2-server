package be.seeld.acceptance.profiles;

import be.seeld.acceptance.Profiles;
import be.seeld.acceptance.Results;
import be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static be.seeld.acceptance.profiles.ProfilesATest.ALICE_PAYLOAD_UPDATED;
import static be.seeld.profiles.ProfilesWebApi.PayloadResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_EXCHANGE_KEY;
import static test.TestUsers.BOB_PSEUDO;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

class When {

    private final Results results;
    private final Profiles profiles;

    When(Profiles profiles, Results results) {
        this.profiles = profiles;
        this.results = results;
    }

    void aliceDeletesHerProfile() {
        ResponseEntity<?> response = profiles.deleteProfile(
                ALICE_PSEUDO,
                ALICE_EXCHANGE_KEY,
                results.get("aliceLoginArtifacts"));
        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    void aliceFetchesTheDetailsOfBob() {
        DetailsResponse detailsResponse = profiles.fetchDetails(BOB_PSEUDO);
        results.set("details", detailsResponse);
    }

    void aliceUpdatesHerProfilePayload() {
        profiles.updatePayload(
                ALICE_PSEUDO,
                ALICE_EXCHANGE_KEY,
                ALICE_PAYLOAD_UPDATED,
                results.get("aliceLoginArtifacts"));
    }

    void bobFetchesHisPayload() {

        ResponseEntity<?> response = profiles.fetchPayload(
                BOB_PSEUDO,
                BOB_EXCHANGE_KEY,
                results.get("bobLoginArtifacts"));
        PayloadResponse payloadResponse = assertThatOkResponseIs(response, PayloadResponse.class);
        assertThat(payloadResponse).hasNoNullFieldsOrProperties();
        results.set("payload", payloadResponse);
    }
}
