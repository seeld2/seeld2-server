package be.seeld.acceptance;

import be.seeld.common.encryption.aes.AES;
import be.seeld.users.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.data.UserData;
import test.rest.WebApiTestClient;

import static be.seeld.acceptance.Given.LoginArtifacts;
import static be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import static be.seeld.profiles.ProfilesWebApi.PayloadRequest;
import static be.seeld.profiles.ProfilesWebApi.PayloadResponse;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class Profiles {

    private final AES aesBase64;
    private final UserData userData;
    private final WebApiTestClient webApiTestClient;

    public Profiles(AES aesBase64, UserData userData, WebApiTestClient webApiTestClient) {
        this.aesBase64 = aesBase64;
        this.userData = userData;
        this.webApiTestClient = webApiTestClient;
    }

    public ResponseEntity<?> deleteProfile(String pseudo, String exchangeKey, LoginArtifacts loginArtifacts) {
        String aesPseudo = aesBase64.encryptToBase64(pseudo, exchangeKey);
        return webApiTestClient.delete("/api/profiles/" + aesPseudo + "/state/deleted", loginArtifacts.jwt);
    }

    public DetailsResponse fetchDetails(String pseudo) {
        ResponseEntity<DetailsResponse> response = webApiTestClient
                .get("/api/profiles/" + pseudo, DetailsResponse.class);
        return assertThatOkResponseIs(response, DetailsResponse.class);
    }

    public ResponseEntity<?> fetchPayload(String pseudo, String exchangeKey, LoginArtifacts loginArtifacts) {
        String aesPseudo = aesBase64.encryptToBase64(pseudo, exchangeKey);
        return webApiTestClient.get("/api/profiles/" + aesPseudo + "/payload",
                loginArtifacts.jwt, PayloadResponse.class);
    }

    public void updatePayload(String pseudo,
                              String exchangeKey,
                              String payload,
                              LoginArtifacts loginArtifacts) {
        String aesPseudo = aesBase64.encryptToBase64(pseudo, exchangeKey);
        ResponseEntity<?> response = webApiTestClient.put("/api/profiles/" + aesPseudo + "/payload",
                loginArtifacts.jwt, new PayloadRequest(payload));
        assertThatResponseIs(response, HttpStatus.CREATED);
    }

    public User userDataWithPseudo(String pseudo) {
        return userData.find(pseudo);
    }
}
