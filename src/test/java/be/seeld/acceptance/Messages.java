package be.seeld.acceptance;

import be.seeld.acceptance.Given.LoginArtifacts;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.web.AESRequest;
import be.seeld.common.web.AESResponse;
import be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsResponse;
import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.MessagesInResponse;
import be.seeld.conversations.messages.MessagesOut;
import be.seeld.conversations.messages.MessagesWebApi.BoxAddressesRequest;
import be.seeld.conversations.messages.MessagesWebApi.NewMessagesRequest;
import be.seeld.conversations.messages.life.MessageLife;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.data.MessageData;
import test.data.MessageLifeData;
import test.rest.WebApiTestClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsRequest;
import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesRequest;
import static be.seeld.conversations.messages.MessagesWebApi.TimelineResponse;
import static test.junit.RestAssertions.assertThatCreateResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class Messages {

    private final AES aes;
    private final MessageData messageData;
    private final MessageLifeData messageLifeData;
    private final WebApiTestClient webApiTestClient;

    public Messages(AES aes,
                    MessageData messageData,
                    MessageLifeData messageLifeData,
                    WebApiTestClient webApiTestClient) {
        this.aes = aes;
        this.messageData = messageData;
        this.messageLifeData = messageLifeData;
        this.webApiTestClient = webApiTestClient;
    }

    public ConversationMessagesIdsResponse fetchConversationMessagesIds(UUID conversationId,
                                                                        Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/" + conversationId + "/messages/ids",
                new ConversationMessagesIdsRequest(hmacHashesByBoxAddress, 0, 100),
                ConversationMessagesIdsResponse.class);
        return assertThatCreateResponseIs(response, ConversationMessagesIdsResponse.class);
    }

    public MessagesInResponse fetchNewMessages(Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/messages/new",
                new NewMessagesRequest(hmacHashesByBoxAddress),
                MessagesInResponse.class);
        return assertThatCreateResponseIs(response, MessagesInResponse.class);
    }

    public MessagesInResponse fetchMessages(UUID conversationId, Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/" + conversationId + "/messages",
                new ConversationMessagesRequest(hmacHashesByBoxAddress, null, 0, 25),
                MessagesInResponse.class);
        return assertThatCreateResponseIs(response, MessagesInResponse.class);
    }

    public MessagesInResponse fetchMessagesWithIds(Set<UUID> messageIds,
                                                   UUID conversationId,
                                                   Map<UUID, String> hmacHashesByBoxAddress) {
        ResponseEntity<?> response = webApiTestClient.put(
                "/api/conversations/" + conversationId + "/messages",
                new ConversationMessagesRequest(hmacHashesByBoxAddress, messageIds, null, null),
                MessagesInResponse.class);
        return assertThatResponseIs(response, HttpStatus.CREATED, MessagesInResponse.class);
    }

    public void messagesData(Message message) {
        messageData.insert(message);
    }

    public void messageLivesData(long expiresOn, UUID boxAddress, UUID conversationId, UUID messageId) {
        messageLifeData.insert(MessageLife.builder().expiresOn(expiresOn).boxAddress(boxAddress)
                .conversationId(conversationId).messageId(messageId).build());
    }

    public List<Message> messagesWithBoxAddress(UUID boxAddress) {
        return messageData.findAllWithBoxAddresses(boxAddress);
    }

    public List<Message> messagesWithConversationId(UUID conversationId) {
        return messageData.findAllWithConversationIds(conversationId);
    }

    public UUID sendMessage(MessagesOut messagesOut, LoginArtifacts loginArtifacts, String exchangeKey)
            throws JsonProcessingException {

        int[] aesEncrypted = aes.encryptToUnsignedBytes(messagesOut.asJSON(), exchangeKey);
        AESRequest aesRequest = new AESRequest(aesEncrypted);
        ResponseEntity<?> response = webApiTestClient
                .post("/api/messages", loginArtifacts.jwt, aesRequest, AESResponse.class);
        AESResponse aesResponse = assertThatCreateResponseIs(response, AESResponse.class);
        return UUID.fromString(aes.decrypt(aesResponse.getPayload(), exchangeKey));
    }

    public TimelineResponse fetchLatestNewMessageTimeline(Set<UUID> boxAddresses) {
        BoxAddressesRequest boxAddressesRequest = new BoxAddressesRequest(boxAddresses);
        ResponseEntity<TimelineResponse> response = webApiTestClient.put(
                "/api/messages/new/timeline/latest",
                boxAddressesRequest,
                TimelineResponse.class);
        return assertThatCreateResponseIs(response, TimelineResponse.class);
    }
}
