package be.seeld.acceptance.notifications;

import be.seeld.SecurityConfiguration;
import be.seeld.acceptance.Acceptance;
import be.seeld.acceptance.Results;
import be.seeld.common.encryption.aes.AES;
import be.seeld.login.LoginConfiguration;
import com.nimbusds.srp6.SRP6Exception;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.simp.stomp.StompSession;

import java.io.IOException;
import java.util.UUID;

public class NotificationsATest extends Acceptance {

    static final UUID ALICE_CONVERSATION_ID = UUID.fromString("d2d45345-bbef-4368-9ead-a72a8fdf2b25");
    static final UUID BOB_CONVERSATION_ID = UUID.fromString("77080c99-fd75-4898-894d-3212d362ea56");
    static final UUID BOB_WRITES_TO_ALICE_BOX_ADDRESS = UUID.fromString("184876d9-3c8a-4272-adcc-0927ad0f2e9c");

    private final Results results = new Results();

    @Autowired
    @Qualifier("pwaAesBase64")
    private AES aes;
    @Autowired
    private SecurityConfiguration.AppSecurityProperties appSecurityProperties;
    @Autowired
    private LoginConfiguration.Srp6aProperties srp6aProperties;

    private Given given;
    private When when;
    private Then then;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        given = new Given(webApiTestClient, appSecurityProperties, srp6aProperties, results, testServerPort);
        when = new When(webApiTestClient, results, aes);
        then = new Then(results);
    }

    @After
    public void tearDown() {
        if (results.get("stompSession") != null) {
            results.<StompSession>get("stompSession").disconnect();
        }
    }

    @Test
    public void receiveNotificationsWhenANewMessageArrives() throws IOException, SRP6Exception, InterruptedException {

        given.alice();
        given.aliceRegisters_BobWritesToAlice_BoxAddressForNotifications();

        given.bobAsTheLoggedUser();

        when.bobPostsAMessageForAlice();
        then.aliceShouldReceiveANotificationOfANewMessage();
    }
}
