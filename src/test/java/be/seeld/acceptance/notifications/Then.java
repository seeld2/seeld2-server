package be.seeld.acceptance.notifications;

import be.seeld.acceptance.Results;
import be.seeld.notifications.Notification;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class Then {

    private final Results results;

    public Then(Results results) {
        this.results = results;
    }

    void aliceShouldReceiveANotificationOfANewMessage() throws InterruptedException {

        Thread.sleep(1000L); // Pause a bit to make sure the server had enough time to send the notification!

        assertThat(results.<List<Notification>>get("receivedNotifications"))
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("type", Notification.Type.NEW_MSG);
    }
}
