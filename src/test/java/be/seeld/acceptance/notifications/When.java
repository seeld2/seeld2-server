package be.seeld.acceptance.notifications;

import be.seeld.acceptance.Results;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.web.AESRequest;
import be.seeld.common.web.AESResponse;
import be.seeld.conversations.messages.MessageOut;
import be.seeld.conversations.messages.MessagesOut;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import test.rest.WebApiTestClient;

import java.util.UUID;

import static be.seeld.acceptance.Given.LoginArtifacts;
import static be.seeld.acceptance.notifications.NotificationsATest.ALICE_CONVERSATION_ID;
import static be.seeld.acceptance.notifications.NotificationsATest.BOB_CONVERSATION_ID;
import static be.seeld.acceptance.notifications.NotificationsATest.BOB_WRITES_TO_ALICE_BOX_ADDRESS;
import static be.seeld.common.lang.Collections.listOf;
import static test.TestUsers.ALICE_PAYLOAD;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.BOB_EXCHANGE_KEY;
import static test.TestUsers.BOB_PAYLOAD;
import static test.junit.RestAssertions.assertThatCreateResponseIs;

class When {

    private final AES aes;
    private final WebApiTestClient restClient;
    private final Results results;

    When(WebApiTestClient restClient, Results results, AES aes) {
        this.aes = aes;
        this.restClient = restClient;
        this.results = results;
    }

    public void bobPostsAMessageForAlice() throws JsonProcessingException {

        MessagesOut messagesOut = MessagesOut.builder()
                .messages(listOf(
                        MessageOut.builder()
                                .boxAddress(BOB_BOX_ADDRESS)
                                .conversationId(BOB_CONVERSATION_ID)
                                .payload(BOB_PAYLOAD)
                                .build(),
                        MessageOut.builder()
                                .boxAddress(BOB_WRITES_TO_ALICE_BOX_ADDRESS)
                                .conversationId(ALICE_CONVERSATION_ID)
                                .payload(ALICE_PAYLOAD)
                                .build()
                ))
                .build();

        AESRequest aesRequest = new AESRequest(aes.encryptToUnsignedBytes(messagesOut.asJSON(), BOB_EXCHANGE_KEY));
        ResponseEntity<?> response = restClient
                .post("/api/messages", results.<LoginArtifacts>get("bobLoginArtifacts").jwt, aesRequest, AESResponse.class);
        AESResponse aesResponse = assertThatCreateResponseIs(response, AESResponse.class);

        results.set("messageId", UUID.fromString(aes.decrypt(aesResponse.getPayload(), BOB_EXCHANGE_KEY)));
    }
}
