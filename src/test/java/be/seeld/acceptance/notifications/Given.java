package be.seeld.acceptance.notifications;

import be.seeld.SecurityConfiguration;
import be.seeld.acceptance.Results;
import be.seeld.login.LoginConfiguration;
import be.seeld.notifications.BoxAddressesRequest;
import be.seeld.notifications.Notification;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import test.rest.WebApiTestClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static be.seeld.acceptance.notifications.NotificationsATest.BOB_WRITES_TO_ALICE_BOX_ADDRESS;
import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;

class Given extends be.seeld.acceptance.Given {

    private final int testServerPort;
    private final Results results;

    Given(WebApiTestClient restClient,
          SecurityConfiguration.AppSecurityProperties appSecurityProperties,
          LoginConfiguration.Srp6aProperties srp6aProperties,
          Results results,
          int testServerPort) {
        super(appSecurityProperties, results, srp6aProperties, restClient);

        this.results = results;
        this.testServerPort = testServerPort;

        results.set("receivedNotifications", new ArrayList<Notification>());
    }

    void aliceRegisters_BobWritesToAlice_BoxAddressForNotifications() {

        WebSocketClient webSocketClient = new StandardWebSocketClient();
        SockJsClient sockJsClient = new SockJsClient(listOf(new WebSocketTransport(webSocketClient)));
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession session;
        try {
            session = stompClient
                    .connect("http://localhost:" + testServerPort + "/wsapi", new StompSessionHandlerAdapter() {})
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        session.send("/wsapi/notifications/register", new BoxAddressesRequest(setOf(BOB_WRITES_TO_ALICE_BOX_ADDRESS)));

        session.subscribe("/user/notification", new StompFrameHandler() {
            @Override
            public Type getPayloadType(StompHeaders headers) {
                return Notification.class;
            }
            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                results.<List<Notification>>get("receivedNotifications").add((Notification) payload);
            }
        });

        results.set("stompSession", session);
    }
}
