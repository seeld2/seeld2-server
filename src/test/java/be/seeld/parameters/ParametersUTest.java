package be.seeld.parameters;

import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;

@RunWith(Enclosed.class)
public class ParametersUTest {

    public static class WhenGettingTheMobileVersion extends Common {

        private static final String MOBILE_VERSION = "1.2.3";

        @Test
        public void shouldObtainTheStoredMobileVersion() {
            expect(parameterRepository.findOne(ParameterRepository.MOBILE_VERSION))
                    .andReturn(new Parameter(ParameterRepository.MOBILE_VERSION, MOBILE_VERSION));

            Optional<String> mobileVersion = getMobileVersion();

            assertThat(mobileVersion)
                    .isPresent()
                    .get().isEqualTo(MOBILE_VERSION);
        }

        @Test
        public void shouldReturnEmptyIfTheMobileVersionParameterIsMissing() {
            expect(parameterRepository.findOne(ParameterRepository.MOBILE_VERSION)).andReturn(null);
            Optional<String> mobileVersion = getMobileVersion();
            assertThat(mobileVersion).isNotPresent();
        }

        private Optional<String> getMobileVersion() {
            replayAll();
            Optional<String> mobileVersion = parameters.getMobileVersion();
            verifyAll();
            return mobileVersion;
        }
    }

    public static class WhenGettingTheRegistrationLimit extends Common {

        private static final String LIMIT_VALUE = "1245";

        @Test
        public void obtainTheStoredRegistrationLimit() {
            expect(parameterRepository.findOne(ParameterRepository.REGISTRATION_LIMIT))
                    .andReturn(new Parameter(ParameterRepository.REGISTRATION_LIMIT, LIMIT_VALUE));

            Optional<Long> registrationLimit = getRegistrationLimit();

            assertThat(registrationLimit)
                    .isPresent()
                    .get().isEqualTo(Long.valueOf(LIMIT_VALUE));
        }

        @Test
        public void returnEmptyIfRegistrationLimitParameterIsMissing() {
            expect(parameterRepository.findOne(ParameterRepository.REGISTRATION_LIMIT)).andReturn(null);
            Optional<Long> registrationLimit = getRegistrationLimit();
            assertThat(registrationLimit).isNotPresent();
        }

        private Optional<Long> getRegistrationLimit() {
            replayAll();
            Optional<Long> registrationLimit = parameters.getRegistrationLimit();
            verifyAll();
            return registrationLimit;
        }
    }

    private static class Common extends Unit {

        Parameters parameters;
        @Mock
        ParameterRepository parameterRepository;

        @Before
        public void setUp() {
            this.parameters = new Parameters(parameterRepository);
        }
    }
}