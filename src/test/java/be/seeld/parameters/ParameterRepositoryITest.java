package be.seeld.parameters;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class ParameterRepositoryITest extends Integration {

    private static final String KEY = "some key";
    private static final String VALUE = "some value";

    @Resource
    private ParameterRepository parameterRepository;

    @Test
    public void findTheParameterWithTheSpecifiedKey() {

        Parameter storedParameter = parameterData.insert(Parameter.builder().key(KEY).value(VALUE).build());

        Parameter parameter = parameterRepository.findOne(KEY);

        assertThat(parameter).isEqualToComparingFieldByField(storedParameter);
    }

    @Test
    public void saveAParameter() {

        Parameter savedParameter = parameterRepository.save(Parameter.builder().key(KEY).value(VALUE).build());

        assertThat(parameterData.findAll())
                .hasSize(1)
                .first().isEqualToComparingFieldByField(savedParameter);
    }
}