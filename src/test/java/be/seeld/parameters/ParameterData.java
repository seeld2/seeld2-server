package be.seeld.parameters;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

import static be.seeld.parameters.Parameter.COL_KEY;
import static be.seeld.parameters.Parameter.COL_VALUE;
import static be.seeld.parameters.Parameter.TABLE;

@SuppressWarnings("SqlDialectInspection")
public class ParameterData {

    private final JdbcTemplate jdbcTemplate;

    public ParameterData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Parameter> findAll() {
        return jdbcTemplate.query("select * from " + TABLE, parameterRowMapper());
    }

    public Parameter insert(Parameter parameter) {
        jdbcTemplate.update("insert into " + TABLE + " (_key," + Parameter.columnsAsCsv() + ") values (?, ?, ?)",
                parameter.cacheKey(),
                parameter.getKey(),
                parameter.getValue());
        return parameter;
    }

    private RowMapper<Parameter> parameterRowMapper() {
        return (rs, rowNum) -> Parameter.builder()
                .key(rs.getString(COL_KEY))
                .value(rs.getString(COL_VALUE))
                .build();
    }
}
