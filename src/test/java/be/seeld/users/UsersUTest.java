package be.seeld.users;

import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Encryptables;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import test.Unit;

import java.util.Optional;

import static be.seeld.users.UserFixture.*;
import static org.assertj.core.api.Assertions.*;
import static org.easymock.EasyMock.*;

public class UsersUTest extends Unit {

    private static final String MODIFIED_PAYLOAD = "modified payload";
    private static final String MODIFIED_USER_BOX_ADDRESS = "efa23220-06a3-4d8a-a201-4ac7058a2081";

    private final User encryptedUser = user();
    private final User modifiedEncryptedUser = user();
    private final User storedEncryptedUser = user();
    private final User storedUnencryptedUser = user();
    private final User unencryptedNewUser = user();
    private final User unencryptedUser = user();

    private Users users;
    @Mock
    private Encryptables encryptables;
    @Mock
    private UserRepository userRepository;

    @Before
    public void setUp() {
        users = new Users(encryptables, userRepository);
    }

    @Test
    public void countTheAmountOfRegisteredUsers() {

        expect(userRepository.count()).andReturn(123L);

        replayAll();
        long count = users.count();
        verifyAll();

        assertThat(count).isEqualTo(123L);
    }

    @Test
    public void shouldDeleteTheUserWithTheSpecifiedUsername() {
        userRepository.delete(USERNAME);
        replayAll();
        users.delete(USERNAME);
        verifyAll();
    }

    @Test
    public void returnTrueWhenAUserWithAGivenUsernameExists() {
        expect(userRepository.exists(USERNAME)).andReturn(true);

        boolean exists = exists(USERNAME);

        assertThat(exists).isTrue();
    }

    @Test
    public void returnFalseWhenNoUserWithAGivenUsernameExists() {
        expect(userRepository.exists(USERNAME)).andReturn(false);

        boolean exists = exists(USERNAME);

        assertThat(exists).isFalse();
    }

    @SuppressWarnings("SameParameterValue")
    private boolean exists(String username) {
        replayAll();
        boolean exists = users.exists(username);
        verifyAll();
        return exists;
    }

    @Test
    public void findAndDecryptAUserWithAGivenUsername() {
        Encryptable<User> encryptable = Encryptable.of(unencryptedUser, encryptedUser, cloner);

        expect(userRepository.findOne(USERNAME)).andReturn(encryptedUser);
        expect(encryptables.ofEncrypted(encryptedUser)).andReturn(encryptable);

        User userFound = findUnencrypted(USERNAME);

        assertThat(userFound).isEqualTo(unencryptedUser);
    }

    @Test(expected = Users.UserNotFoundException.class)
    public void raiseExceptionWhenSearchingForAnUnencryptedUserThatDoesNotExist() {

        expect(userRepository.findOne(USERNAME)).andReturn(null);

        findUnencrypted(USERNAME);
    }

    @SuppressWarnings("SameParameterValue")
    private User findUnencrypted(String username) {
        replayAll();
        User userFound = users.fetchUnencrypted(username);
        verifyAll();
        return userFound;
    }

    @Test
    public void findAUserWithAGivenUsername() {
        Encryptable<User> encryptable = Encryptable.of(unencryptedUser, encryptedUser, cloner);

        expect(userRepository.findOne(USERNAME)).andReturn(encryptedUser);
        expect(encryptables.ofEncrypted(encryptedUser)).andReturn(encryptable);

        replayAll();
        Optional<Encryptable<User>> found = users.fetch(USERNAME);
        verifyAll();

        Encryptable<User> foundEncryptable = found.orElse(null);
        assertThat(foundEncryptable).isNotNull();
        assertThat(foundEncryptable.getEncrypted()).usingRecursiveComparison().isEqualTo(encryptedUser);
        assertThat(foundEncryptable.getUnencrypted()).usingRecursiveComparison().isEqualTo(unencryptedUser);
    }

    @Test
    public void saveANewUser() {
        Encryptable<User> encryptable = Encryptable.of(unencryptedNewUser, encryptedUser, cloner);

        expect(userRepository.findOne(USERNAME)).andReturn(null);
        expect(encryptables.ofUnencrypted(unencryptedNewUser)).andReturn(encryptable);
        expect(userRepository.save(encryptedUser)).andReturn(encryptedUser);

        User savedUser = save(unencryptedNewUser);

        assertThat(savedUser)
                .isNotNull()
                .isEqualTo(encryptedUser);
    }

    @Test
    public void retainUserBoxAddressWhenUpdatingAnExistingUser() {
        User userWithModifiedUserBoxAddress = user();
        userWithModifiedUserBoxAddress.setBoxAddress(MODIFIED_USER_BOX_ADDRESS);

        expect(userRepository.findOne(USERNAME)).andReturn(storedEncryptedUser);
        expect(encryptables.ofEncrypted(storedEncryptedUser))
                .andReturn(Encryptable.of(storedUnencryptedUser, storedEncryptedUser, cloner));
        expect(encryptables.ofUnencrypted(userWithModifiedUserBoxAddress))
                .andReturn(Encryptable.of(userWithModifiedUserBoxAddress, modifiedEncryptedUser, cloner));
        expect(userRepository.save(modifiedEncryptedUser)).andReturn(userWithModifiedUserBoxAddress);

        User savedUser = save(userWithModifiedUserBoxAddress);

        assertThat(savedUser)
                .isNotNull()
                .hasFieldOrPropertyWithValue("boxAddress", BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("username", USERNAME);
    }

    private User save(User user) {
        replayAll();
        User savedUser = users.store(user);
        verifyAll();
        return savedUser;
    }

    @Test
    public void updateThePayloadOfAUser() {
        Encryptable<User> encryptable = Encryptable.of(unencryptedUser, encryptedUser, cloner);

        expect(encryptables.ofUnencrypted(unencryptedUser)).andReturn(encryptable);
        expect(userRepository.save(encryptedUser)).andReturn(encryptedUser);

        replayAll();
        User updatedUser = users.updatePayload(unencryptedUser, MODIFIED_PAYLOAD);
        verifyAll();

        assertThat(updatedUser)
                .isNotNull()
                .hasFieldOrPropertyWithValue("payload", MODIFIED_PAYLOAD);
    }
}
