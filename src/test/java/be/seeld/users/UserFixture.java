package be.seeld.users;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class UserFixture {

    public static final UUID BOX_ADDRESS = UUID.fromString("01a56d9c-b02f-4863-88e4-74f6aaf4feb0");
    public static final User.Credentials CREDENTIALS = User.Credentials.builder()
            .kdf(User.Credentials.Kdf.builder().salt("scryptSalt").build())
            .srp(User.Credentials.Srp.builder().salt("srp6aSalt").verifier("srp6aVerifier").build())
            .systemKeys(User.Credentials.SystemKeys.builder()
                    .privateKeys("privateKeys")
                    .publicKeys("publicKeys")
                    .type("type")
                    .build())
            .build();
    public static final String DEVICE_STORAGE_KEY = "deviceStorageKey";
    public static final String DISPLAY_NAME = "displayName";
    public static final String EXCHANGE_KEY = "exchangeKey";
    public static final String PAYLOAD = "payload";
    public static final String PICTURE = "picture";
    public static final Subscription SUBSCRIPTION = Subscription.FREE;
    public static final String USERNAME = "username";

    public static User encryptedUser(String username) {
        return User.builder()
                .boxAddress("encrypted")
                .credentials("encrypted")
                .deviceStorageKey("encrypted")
                .displayName("encrypted")
                .exchangeKey("encrypted")
                .payload("encrypted")
                .picture("encrypted")
                .subscription("encrypted")
                .username(username)
                .build();
    }

    public static User newUser() {
        return newUser(USERNAME);
    }

    public static User newUser(String username) {
        try {
            return User.builder()
                    .credentials(CREDENTIALS.asJSON())
                    .displayName(DISPLAY_NAME)
                    .payload(PAYLOAD)
                    .picture(PICTURE)
                    .username(username)
                    .build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static User user() {
        return user(USERNAME, BOX_ADDRESS.toString());
    }

    public static User user(String username) {
        return user(username, BOX_ADDRESS.toString());
    }

    public static User user(String username, String userBoxAddress) {
        return user(username, userBoxAddress, null);
    }

    public static User user(String username, String userBoxAddress, String userPublicKey) {
        return user(username, userBoxAddress, userPublicKey, EXCHANGE_KEY);
    }

    public static User user(String username, UUID boxAddress, String exchangeKey) {
        User user = newUser(username);
        user.boxAddress(boxAddress);
        user.setExchangeKey(exchangeKey);
        return user;
    }

    public static User user(String username, String userBoxAddress, String userPublicKey, String exchangeKey) {
        User user = newUser();
        user.setUsername(username);
        user.setBoxAddress(userBoxAddress);
        user.setExchangeKey(exchangeKey);
        return user;
    }
}
