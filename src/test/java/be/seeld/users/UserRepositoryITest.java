package be.seeld.users;

import org.junit.Test;
import test.Integration;
import test.TestUsers;

import javax.annotation.Resource;

import static be.seeld.users.UserFixture.USERNAME;
import static be.seeld.users.UserFixture.user;
import static org.assertj.core.api.Assertions.assertThat;

public class UserRepositoryITest extends Integration {

    private static final String MODIFIED_DISPLAY_NAME = "modified display name";

    @Resource
    private UserRepository userRepository;

    @Test
    public void countUsers() {

        clearAllTables();

        userData.insert(User.builder().username("a").build());
        userData.insert(User.builder().username("b").build());
        userData.insert(User.builder().username("c").build());

        long count = userRepository.count();

        assertThat(count).isEqualTo(3);
    }

    @Test
    public void shouldDeleteTheUserWithTheSpecifiedUsername() {

        User user = userData.find(TestUsers.ALICE_PSEUDO);
        assertThat(user).isNotNull();

        userRepository.delete(TestUsers.ALICE_PSEUDO);

        user = userData.find(TestUsers.ALICE_PSEUDO);
        assertThat(user).isNull();
    }

    @Test
    public void createAUserEntry() {

        User savedUser = userRepository.save(user());

        User foundUser = userData.find(USERNAME);
        assertThat(foundUser).usingRecursiveComparison().isEqualTo(savedUser);
    }

    @Test
    public void shouldEnsureThatTheSavedUserUsernameIsLowerCased() {
        userRepository.save(user("Pseudo"));

        User storedUser = userData.find("pseudo");

        assertThat(storedUser).isNotNull();
    }

    @Test
    public void findAUserEntryWithAGivenUsername() {
        User user = userData.insert(user());

        User foundUser = userRepository.findOne(USERNAME);

        assertThat(foundUser).usingRecursiveComparison().isEqualTo(user);
    }

    @Test
    public void shouldConsiderTheLowerCaseOfTheUsernameToFindAGivenUser() {
        userData.insert(user("pseudo"));

        User foundUser = userRepository.findOne("Pseudo");

        assertThat(foundUser).isNotNull();
    }

    @Test
    public void checkIfAUserWithAGivenUsernameExists() {

        assertThat(userRepository.exists(USERNAME)).isFalse();

        userData.insert(user());

        assertThat(userRepository.exists(USERNAME)).isTrue();
    }

    @Test
    public void updateAUserEntry() {
        User user = userData.insert(user());

        user.setDisplayName(MODIFIED_DISPLAY_NAME);
        userRepository.save(user);

        User foundUser = userData.find(USERNAME);
        assertThat(foundUser).isNotNull();
        assertThat(foundUser.getDisplayName()).isEqualTo(MODIFIED_DISPLAY_NAME);
    }
}
