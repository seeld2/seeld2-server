package be.seeld.users;

import org.junit.Test;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;

public class UserUTest extends Unit {

    @Test
    public void shouldAlwaysReturnTheUsernameInLowercase() {
        User user = User.builder().username("DiegO").build();
        assertThat(user).hasFieldOrPropertyWithValue("username", "diego");
    }
}