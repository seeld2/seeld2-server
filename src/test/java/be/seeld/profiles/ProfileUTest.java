package be.seeld.profiles;

import be.seeld.profiles.Profile;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import test.Unit;

import static org.assertj.core.api.Assertions.*;

public class ProfileUTest extends Unit {

    private static final String USER_BOX_ADDRESS = "userBoxAddress";
    private static final String USER_PUBLIC_KEY = "userPublicKey";
    private static final String USERNAME = "username";

    @Test
    public void generateJSONStringFromInstance() throws JsonProcessingException {

        Profile profile = new Profile(USER_BOX_ADDRESS, USER_PUBLIC_KEY, USERNAME);

        String json = profile.asJSON();

        assertThat(json).isEqualTo("{" +
                "\"boxAddress\":\"" + USER_BOX_ADDRESS + "\"," +
                "\"systemPublicKeys\":\"" + USER_PUBLIC_KEY + "\"," +
                "\"username\":\"" + USERNAME + "\"" +
                "}");
    }
}