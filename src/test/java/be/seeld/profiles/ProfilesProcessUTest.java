package be.seeld.profiles;

import be.seeld.common.encryption.aes.AES;
import be.seeld.users.User;
import be.seeld.users.Users;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PAYLOAD;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.BOB_PSEUDO;
import static test.TestUsers.BOB_SYSTEM_PUBLIC_KEYS;

@RunWith(Enclosed.class)
public class ProfilesProcessUTest {

    private static final String AES_PSEUDO = "1fe56cca8900e1c";
    private static final String USER_USERNAME = "userUsername";

    private static final String PGP_PAYLOAD = "pgp payload";

    public static class WhenDeletingAProfile extends Common {

        @Before
        public void setUp() {
            super.setUp();
        }

        @Test
        public void shouldDeleteTheSpecifiedProfile() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andReturn(ALICE_PSEUDO);
            users.delete(ALICE_PSEUDO);

            replayAll();
            profilesProcess.deleteProfile(ALICE_PSEUDO, AES_PSEUDO);
            verifyAll();
        }

        @Test(expected = ProfilesProcess.ProfilesProcessException.class)
        public void shouldRaiseAnExceptionIfTheSpecifiedAesPseudoCannotBeDecryptedByTheLoggedUser() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andReturn(BOB_PSEUDO);

            replayAll();
            profilesProcess.deleteProfile(ALICE_PSEUDO, AES_PSEUDO);
        }

        @Test(expected = ProfilesProcess.ProfilesProcessException.class)
        public void shouldRaiseAnExceptionIfTheLoggedUserIsAttemptingToDeleteAnotherProfileThanItself() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andThrow(new AES.AESException("ouch"));

            replayAll();
            profilesProcess.deleteProfile(ALICE_PSEUDO, AES_PSEUDO);
        }
    }

    public static class WhenFetchingTheLoggedUserPayload extends Common {

        @Test
        public void shouldReturnTheLoggedUserPayload() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andReturn(ALICE_PSEUDO);

            replayAll();
            String payload = profilesProcess.fetchPayload(ALICE_PSEUDO, AES_PSEUDO);
            verifyAll();

            assertThat(payload).isNotEmpty().isEqualTo(ALICE_PAYLOAD);
        }

        @Test(expected = ProfilesProcess.ProfilesProcessException.class)
        public void shouldRaiseAnExceptionIfTheLoggedUserIsAttemptingToFetchAnotherPayloadThanItsOwn() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andThrow(new AES.AESException("ouch"));

            replayAll();
            profilesProcess.fetchPayload(ALICE_PSEUDO, AES_PSEUDO);
        }
    }

    public static class WhenFetchingAProfile extends Common {

        @Before
        public void setUp() {
            super.setUp();
        }

        @Test
        public void shouldReturnTheDetailsOfTheSpecifiedProfile() {

            expect(users.fetchUnencrypted(BOB_PSEUDO)).andReturn(bob.toUser());

            Profile profile = fetchProfile(BOB_PSEUDO);

            assertThat(profile)
                    .hasFieldOrPropertyWithValue("boxAddress", BOB_BOX_ADDRESS.toString())
                    .hasFieldOrPropertyWithValue("systemPublicKeys", BOB_SYSTEM_PUBLIC_KEYS)
                    .hasFieldOrPropertyWithValue("username", BOB_PSEUDO);

        }

        @Test(expected = Users.UserNotFoundException.class)
        public void raiseAnExceptionIfTheRequestedProfileDoesNotExist() {

            expect(users.fetchUnencrypted(USER_USERNAME)).andThrow(new Users.UserNotFoundException(""));

            fetchProfile(USER_USERNAME);
        }

        private Profile fetchProfile(String pseudo) {
            replayAll();
            Profile profile = profilesProcess.fetchProfile(pseudo);
            verifyAll();
            return profile;
        }
    }

    public static class WhenUpdatingAProfile extends Common {

        @Before
        public void setUp() {
            super.setUp();
        }

        @Test
        public void updateAUserProfile() {
            User aliceAsSystemUser = alice.toUser();

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(aliceAsSystemUser);
            expect(aes.decryptBase64(AES_PSEUDO, ALICE_EXCHANGE_KEY)).andReturn(ALICE_PSEUDO);
            expect(users.updatePayload(aliceAsSystemUser, PGP_PAYLOAD)).andReturn(aliceAsSystemUser);

            replayAll();
            profilesProcess.updatePayload(ALICE_PSEUDO, AES_PSEUDO, PGP_PAYLOAD);
            verifyAll();
        }
    }

    private static abstract class Common extends Unit {

        protected ProfilesProcess profilesProcess;
        @Mock
        protected AES aes;
        @Mock
        protected Users users;

        @Before
        public void setUp() {
            profilesProcess = new ProfilesProcess(aes, users);
        }
    }
}