package be.seeld.profiles;

import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.Unit;
import test.spring.PrincipalMock;

import static be.seeld.profiles.ProfilesWebApi.DetailsResponse;
import static be.seeld.profiles.ProfilesWebApi.PayloadRequest;
import static be.seeld.profiles.ProfilesWebApi.PayloadResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.ALICE_SYSTEM_PUBLIC_KEYS;
import static test.TestUsers.BOB_PSEUDO;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class ProfilesWebApiUTest extends Unit {

    private static final String LOGGED_PSEUDO = "bobby";

    private static final String AES_PSEUDO = "1fe56cca8900e1c";
    private static final String PGP_PAYLOAD = "pgp payload";

    private ProfilesWebApi profilesWebApi;
    @Mock
    private ProfilesProcess profilesProcess;

    @Before
    public void setUp() {
        profilesWebApi = new ProfilesWebApi(profilesProcess, 0);
    }

    @Test
    public void shouldDeleteAProfile() {

        profilesProcess.deleteProfile(ALICE_PSEUDO, AES_PSEUDO);

        replayAll();
        ResponseEntity<?> response = profilesWebApi
                .deleteProfile(AES_PSEUDO, PrincipalMock.of(ALICE_PSEUDO));
        verifyAll();

        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    @Test
    public void shouldReturnAProfileDetails() {

        expect(profilesProcess.fetchProfile(ALICE_PSEUDO))
                .andReturn(new Profile(ALICE_BOX_ADDRESS.toString(), ALICE_SYSTEM_PUBLIC_KEYS, AES_PSEUDO));

        replayAll();
        ResponseEntity<?> response = profilesWebApi.fetchDetails(ALICE_PSEUDO);
        verifyAll();

        DetailsResponse detailsResponse = assertThatOkResponseIs(response, DetailsResponse.class);
        assertThat(detailsResponse)
                .hasFieldOrPropertyWithValue("boxAddress", ALICE_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("systemPublicKeys", ALICE_SYSTEM_PUBLIC_KEYS)
                .hasFieldOrPropertyWithValue("boxAddress", ALICE_BOX_ADDRESS.toString());
    }

    @Test
    public void shouldReturnAUserPayload() {

        expect(profilesProcess.fetchPayload(LOGGED_PSEUDO, AES_PSEUDO)).andReturn(PGP_PAYLOAD);

        replayAll();
        ResponseEntity<?> response = profilesWebApi.fetchOwnPayload(AES_PSEUDO, PrincipalMock.of(BOB_PSEUDO));
        verifyAll();

        PayloadResponse payloadResponse = assertThatOkResponseIs(response, PayloadResponse.class);
        assertThat(payloadResponse.getPayload())
                .isNotEmpty()
                .isEqualTo(PGP_PAYLOAD);
    }

    @Test
    public void shouldUpdateAUserPayload() {

        profilesProcess.updatePayload(ALICE_PSEUDO, AES_PSEUDO, PGP_PAYLOAD);

        replayAll();
        ResponseEntity<?> response = profilesWebApi
                .updatePayload(AES_PSEUDO, new PayloadRequest(PGP_PAYLOAD), PrincipalMock.of(ALICE_PSEUDO));
        verifyAll();

        assertThatResponseIs(response, HttpStatus.CREATED);
    }
}