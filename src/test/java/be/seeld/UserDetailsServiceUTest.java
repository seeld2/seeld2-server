package be.seeld;

import be.seeld.users.User;
import be.seeld.users.UserFixture;
import be.seeld.users.Users;
import be.seeld.common.encryption.Encryptable;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import test.Unit;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;

/**
 * @deprecated UserDetails is probably not needed anymore, since authentication is done by own API instead of Spring Security
 */
@Deprecated
public class UserDetailsServiceUTest extends Unit {

    private static final String USERNAME = UserFixture.USERNAME;

    private UserDetailsService userDetailsService;
    @Mock
    private Users users;

    private Encryptable<User> encryptable;

    @Before
    public void setUp() {
        userDetailsService = new UserDetailsService(users);

        User user = UserFixture.user();
        encryptable = Encryptable.of(user, user, cloner);
    }

    @Test
    public void returnExistingUserDetails() {

        expect(users.fetch(USERNAME)).andReturn(Optional.of(encryptable));

        replayAll();
        UserDetails userDetails = userDetailsService.loadUserByUsername(USERNAME);
        verifyAll();

        assertThat(userDetails)
                .isNotNull()
                .hasFieldOrPropertyWithValue("username", UserFixture.USERNAME);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void throwExceptionWhenUsernameNotFound() {

        expect(users.fetch(USERNAME)).andReturn(Optional.empty());

        replayAll();
        userDetailsService.loadUserByUsername(USERNAME);
        verifyAll();
    }
}
