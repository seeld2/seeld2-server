package be.seeld;

import be.seeld.users.UserFixture;
import be.seeld.users.Users;
import be.seeld.common.security.jwt.JsonWebTokens;
import be.seeld.common.security.jwt.JwtBasedAuthorizationFilter;
import be.seeld.common.security.jwt.JwtBasedAuthorizationFilter.Config;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import test.Unit;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;

public class UserVerifyingJwtBasedAuthorizationFilterUTest extends Unit {

    private static final String HEADER_PREFIX = "Bearer";
    private static final String SECRET = "Shhh";

    private static final long OVERDUE = -60000L;
    private static final String USERNAME = UserFixture.USERNAME;

    private JwtBasedAuthorizationFilter filter;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private Users users;

    @Before
    public void setUp() {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(USERNAME, null, Collections.emptyList()));
    }

    @Test
    public void generateNewTokenIfTokenInHeaderIsOverdue() throws IOException, ServletException {
        filter = newUserVerifyingJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(HEADER_PREFIX + " " + token());
        expect(users.exists(USERNAME)).andReturn(true);
        response.addHeader(eq(HttpHeaders.AUTHORIZATION), anyString());
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void preventTokenRefreshIfUserIsNotValidAnymore() throws IOException, ServletException {
        filter = newUserVerifyingJwtBasedAuthorizationFilter();

        expect(request.getHeader(HttpHeaders.AUTHORIZATION)).andReturn(HEADER_PREFIX + " " + token());
        expect(users.exists(USERNAME)).andReturn(false);
        response.addHeader(eq(HttpHeaders.AUTHORIZATION), anyString());
        filterChain.doFilter(request, response);

        doFilterInternal(request, response, filterChain);
    }

    private void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        replayAll();
        filter.doFilterInternal(request, response, filterChain);
        verifyAll();
    }

    private JwtBasedAuthorizationFilter newUserVerifyingJwtBasedAuthorizationFilter() {
        JsonWebTokens jsonWebTokens = JsonWebTokens.of(JsonWebTokens.Config.create(
                SignatureAlgorithm.HS512, SECRET, 1000000L, HEADER_PREFIX));

        return new UserVerifyingJwtBasedAuthorizationFilter(authenticationManager,
                jsonWebTokens,
                Config.create(HEADER_PREFIX),
                users);
    }

    private String token() {
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(USERNAME)
                .setExpiration(new Date(System.currentTimeMillis() + OVERDUE))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }
}