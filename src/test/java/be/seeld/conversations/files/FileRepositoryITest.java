package be.seeld.conversations.files;

import org.junit.Ignore;
import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@Ignore
public class FileRepositoryITest extends Integration {

    @Resource
    private FileRepository fileRepository;

    @Test
    public void storeAFile() {

        File savedFile = fileRepository.save(FileFixture.file());

        File foundFile = fileData.find(FileFixture.FILE_ID);

        assertThat(foundFile).isEqualToComparingFieldByFieldRecursively(savedFile);
    }

    @Test
    public void findAFile() {
        File file = fileData.insert(FileFixture.file());

        File foundFile = fileRepository.findOne(FileFixture.FILE_ID);

        assertThat(foundFile).isEqualToComparingFieldByFieldRecursively(file);
    }

    @Test
    public void deleteAFile() {
        fileData.insert(FileFixture.file());

        assertThat(fileData.exists(FileFixture.FILE_ID)).isTrue();

        fileRepository.delete(FileFixture.FILE_ID);

        assertThat(fileData.exists(FileFixture.FILE_ID)).isFalse();
    }
}