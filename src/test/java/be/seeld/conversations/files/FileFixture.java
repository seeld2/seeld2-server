package be.seeld.conversations.files;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

public class FileFixture {

    public static final byte[] DATA = {1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    public static final UUID FILE_ID = UUID.fromString("681dc246-eb5c-407d-a5a0-0cb13b8e1d47");
    public static final long VALID_UNTIL = Instant.now().plus(Duration.ofDays(1L)).toEpochMilli();

    public static File file() {
        return File.builder()
                .fileId(FILE_ID)
                .data(DATA)
                .validUntil(VALID_UNTIL)
                .build();
    }
}
