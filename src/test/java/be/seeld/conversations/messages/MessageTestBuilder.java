package be.seeld.conversations.messages;

import java.util.UUID;

public final class MessageTestBuilder {

    public static final UUID BOX_ADDRESS = UUID.fromString("c01fcc3f-78a3-4d01-b2f8-43830208a3e9");
    public static final UUID CONVERSATION_ID = UUID.fromString("d871a910-c0a5-4ba8-84f4-0e93fb1c12b9");
    public static final String HMAC_HASH = "hmacHash";
    public static final UUID MESSAGE_ID = UUID.fromString("1248cb5a-260b-4d9e-b0ce-be62dda3e1b5");
    public static final boolean NOTIFIED = true;
    public static final String PAYLOAD = "payload";
    public static final long TIMELINE = 10000000L;

    public MessageTestBuilder and = this;
    public MessageTestBuilder with = this;

    public static MessageTestBuilder aMessage() {
        return new MessageTestBuilder();
    }

    private UUID boxAddress;
    private UUID conversationId;
    private String hmacHash;
    private UUID messageId;
    private boolean notified = NOTIFIED;
    private String payload;
    private long timeline = TIMELINE;

    private MessageTestBuilder() {
    }

    public Message asIs() {
        return new Message(
                this.boxAddress,
                this.conversationId,
                this.hmacHash,
                this.messageId,
                this.notified,
                this.payload,
                this.timeline
        );
    }

    public Message noBlanks() {
        return new Message(
                this.boxAddress != null ? this.boxAddress : BOX_ADDRESS,
                this.conversationId != null? this.conversationId : CONVERSATION_ID,
                this.hmacHash != null? this.hmacHash : HMAC_HASH,
                this.messageId != null? this.messageId : MESSAGE_ID,
                this.notified,
                this.payload != null ? this.payload : PAYLOAD,
                this.timeline
        );
    }

    public MessageTestBuilder boxAddress(UUID boxAddress) {
        this.boxAddress = boxAddress;
        return this;
    }

    public MessageTestBuilder conversationId(UUID conversationId) {
        this.conversationId = conversationId;
        return this;
    }

    public MessageTestBuilder hmacHash(String hmacHash) {
        this.hmacHash = hmacHash;
        return this;
    }

    public MessageTestBuilder messageId(UUID messageId) {
        this.messageId = messageId;
        return this;
    }

    public MessageTestBuilder notified(boolean notified) {
        this.notified = notified;
        return this;
    }

    public MessageTestBuilder payload(String payload) {
        this.payload = payload;
        return this;
    }

    public MessageTestBuilder timeline(long timeline) {
        this.timeline = timeline;
        return this;
    }
}
