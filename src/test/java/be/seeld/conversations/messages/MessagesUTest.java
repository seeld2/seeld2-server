package be.seeld.conversations.messages;

import be.seeld.common.lang.Maps;
import be.seeld.conversations.ConversationsProcessUTest;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import test.Unit;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.messages.MessageTestBuilder.aMessage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.BOB_BOX_ADDRESS;

@RunWith(Enclosed.class)
public class MessagesUTest {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("f868ac2e-8e0d-4b98-9878-20140b235f46");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("f0f0f56c-7a0b-4dec-9fbc-959cb756ae74");

    private static final UUID CONVERSATION_ID_1 = UUID.fromString("1f9de60e-f85e-4554-a3f8-72bf22f9edd2");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("832cb81a-596a-4cc9-bd1e-40753c64ac7e");

    private static final String HMAC_HASH_1 = "hmacHash1";
    private static final String HMAC_HASH_2 = "hmacHash2";
    private static final String HMAC_HASH_3 = "hmacHash3";

    private static final UUID MESSAGE_ID_1 = UUID.fromString("6f8caaf5-2440-4978-a395-f1e9dff3d852");
    private static final Message MESSAGE_1 = MessageFixture.message(MESSAGE_ID_1);
    private static final UUID MESSAGE_ID_2 = UUID.fromString("6ae51f4f-55e8-4a20-add4-9e2a26f0c295");
    private static final Message MESSAGE_2 = MessageFixture.message(MESSAGE_ID_2);

    private static final Long LATEST_NEW_MESSAGE_TIMELINE = 123456789L;

    private static final List<Message> CONVERSATIONS = listOf(
            MessageFixture.message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1),
            MessageFixture.message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_2));

    private static final int PAGE = 0;
    private static final int PAGE_SIZE = 2;

    private static class Common extends Unit {

        Messages messages;
        @Mock
        MessageRepository messageRepository;

        @Before
        public void setUp() {
            messages = new Messages(messageRepository);
        }
    }

    public static class WhenDeletingAConversation extends MessagesUTest.Common {

        @Test
        public void shouldDeleteAConversationsMessages() {

            expect(messageRepository.fetchAllForConversationIdWithBoxAddresses(CONVERSATION_ID_1, setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(
                            aMessage().with.boxAddress(BOX_ADDRESS_1).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_1).asIs(),
                            aMessage().with.boxAddress(BOX_ADDRESS_2).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_2).asIs()));
            messageRepository.deleteAllForConversationId(CONVERSATION_ID_1);

            deleteConversation(
                    CONVERSATION_ID_1,
                    mapOf(Maps.Entry.of(BOX_ADDRESS_1, HMAC_HASH_1), Maps.Entry.of(BOX_ADDRESS_2, HMAC_HASH_2)));
        }

        @Test
        public void shouldDeleteConversationsWhoseMessagesHaveNoHmacHashes() {

            expect(messageRepository.fetchAllForConversationIdWithBoxAddresses(CONVERSATION_ID_1, setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(
                            aMessage().with.boxAddress(BOX_ADDRESS_1).conversationId(CONVERSATION_ID_1).asIs(),
                            aMessage().with.boxAddress(BOX_ADDRESS_2).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_2).asIs()));
            messageRepository.deleteAllForConversationId(CONVERSATION_ID_1);

            deleteConversation(
                    CONVERSATION_ID_1,
                    mapOf(Maps.Entry.of(BOX_ADDRESS_1, HMAC_HASH_1), Maps.Entry.of(BOX_ADDRESS_2, HMAC_HASH_2)));
        }

        @Test
        public void shouldPreventDeletionWhenMissingAHmacHashForOneMessageFromConversation() {

            expect(messageRepository.fetchAllForConversationIdWithBoxAddresses(CONVERSATION_ID_1, setOf(BOX_ADDRESS_1)))
                    .andReturn(listOf(
                            aMessage().with.boxAddress(BOX_ADDRESS_1).conversationId(CONVERSATION_ID_1).asIs(),
                            aMessage().with.boxAddress(BOX_ADDRESS_2).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_2).asIs()));

            deleteConversation(CONVERSATION_ID_1, mapOf(Maps.Entry.of(BOX_ADDRESS_1, HMAC_HASH_1)));
        }

        @Test
        public void shouldPreventDeletionWhenHmacHashDoesNotMatchStoredHashForAMessage() {

            expect(messageRepository.fetchAllForConversationIdWithBoxAddresses(CONVERSATION_ID_1, setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(
                            aMessage().with.boxAddress(BOX_ADDRESS_1).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_1).asIs(),
                            aMessage().with.boxAddress(BOX_ADDRESS_2).conversationId(CONVERSATION_ID_1).hmacHash(HMAC_HASH_2).asIs()));

            deleteConversation(
                    CONVERSATION_ID_1,
                    mapOf(Maps.Entry.of(BOX_ADDRESS_1, HMAC_HASH_1), Maps.Entry.of(BOX_ADDRESS_2, HMAC_HASH_3)));
        }

        private void deleteConversation(UUID conversationId, Map<UUID, String> hmacHashesByBoxAddress) {
            replayAll();
            messages.deleteConversation(conversationId, hmacHashesByBoxAddress);
            verifyAll();
        }
    }

    public static class WhenFetchingTheLastUnnotifiedMessageTimelineByBoxAddress extends Common {

        @Test
        public void shouldReturnTheLatestUnnotifiedMessageTimelineValue() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1);

            Message message = Message.builder().timeline(LATEST_NEW_MESSAGE_TIMELINE).build();

            expect(messageRepository.findLatestByBoxAddressInAndNotifiedIsFalse(boxAddresses))
                    .andReturn(Optional.of(message));

            replayAll();
            Optional<Long> timeline = messages.fetchLatestUnnotifiedTimelineByBoxAddresses(boxAddresses);
            verifyAll();

            assertThat(timeline).isPresent().get().isEqualTo(LATEST_NEW_MESSAGE_TIMELINE);
        }

        @Test
        public void shouldReturnEmptyIfThereAreNoNewMessages() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1);

            expect(messageRepository.findLatestByBoxAddressInAndNotifiedIsFalse(boxAddresses))
                    .andReturn(Optional.empty());

            replayAll();
            Optional<Long> timeline = messages.fetchLatestUnnotifiedTimelineByBoxAddresses(boxAddresses);
            verifyAll();

            assertThat(timeline).isNotPresent();
        }
    }

    public static class Other extends Common {

        private static final Set<UUID> BOX_ADDRESSES = setOf(ALICE_BOX_ADDRESS, BOB_BOX_ADDRESS);

        @Test
        public void deleteAllMessagesForASetOfBoxAddresses() {
            messageRepository.deleteAllForBoxAddresses(BOX_ADDRESSES);
            replayAll();
            messages.deleteAllWithBoxAddresses(BOX_ADDRESSES);
            verifyAll();
        }

        @Test
        public void fetchAllMessagesByAListOfMessagesIdsAndForAGivenConversationId() {

            List<Message> messages = listOf(MessageFixture.message(MESSAGE_ID_1), MessageFixture.message(MESSAGE_ID_2));

            expect(messageRepository.findByMessagesIdsInAndConversationId(CONVERSATION_ID_1, setOf(MESSAGE_ID_1, MESSAGE_ID_2)))
                    .andReturn(messages);

            replayAll();
            List<Message> fetchedMessages = this.messages
                    .fetchAllByMessagesIdsForConversationId(CONVERSATION_ID_1, setOf(MESSAGE_ID_1, MESSAGE_ID_2));
            verifyAll();

            assertThat(fetchedMessages)
                    .hasSize(2)
                    .containsAll(messages);
        }

        @Test
        public void fetchAPageOfMessagesForAConversationId() {
            List<Message> messages = listOf(
                    MessageFixture.message(BOX_ADDRESS_1, MESSAGE_ID_1),
                    MessageFixture.message(BOX_ADDRESS_2, MESSAGE_ID_2));

            expect(messageRepository
                    .findByConversationIdOrderByTimelineDesc(CONVERSATION_ID_1, PageRequest.of(PAGE, PAGE_SIZE)))
                    .andReturn(messages);

            replayAll();
            List<Message> results = this.messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE);
            verifyAll();

            assertThat(results).hasSize(2).containsAll(messages);
        }

        @Test
        public void fetchAllLatestMessagesForEachDistinctConversationFoundForAListOfBoxAddresses() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1, BOX_ADDRESS_2);
            List<Message> messages = listOf(
                    MessageFixture.message(BOX_ADDRESS_1, MESSAGE_ID_1),
                    MessageFixture.message(BOX_ADDRESS_2, MESSAGE_ID_2));

            expect(messageRepository
                    .findLatestDistinctConversationIdByBoxAddressInOrderByTimelineDesc(boxAddresses,
                            PageRequest.of(PAGE, PAGE_SIZE)))
                    .andReturn(new PageImpl<>(messages));

            replayAll();
            List<Message> results = this.messages.fetchAllLatestByBoxAddressesUnreadFirst(boxAddresses, PAGE, PAGE_SIZE);
            verifyAll();

            assertThat(results).containsAll(messages);
        }

        @Test
        public void fetchAllLatestMessagesForEachConversationMatchingTheListOfConversationIds() {
            Set<UUID> conversationIds = setOf(CONVERSATION_ID_1, CONVERSATION_ID_2);

            expect(messageRepository
                    .findLatestByConversationIdsInOrderByTimelineDesc(conversationIds, PageRequest.of(PAGE, PAGE_SIZE)))
                    .andReturn(new PageImpl<>(CONVERSATIONS));

            replayAll();
            List<Message> messages = this.messages.fetchAllLatestByConversationIdsUnreadFirst(conversationIds, PAGE, PAGE_SIZE);
            verifyAll();

            assertThat(messages).containsAll(messages);
        }

        @Test
        public void fetchAllUnreadMessagesForAListOfBoxAddresses() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1, BOX_ADDRESS_2);

            expect(messageRepository.findByBoxAddressInAndNotifiedIsFalse(boxAddresses))
                    .andReturn(listOf(MESSAGE_1, MESSAGE_2));
            expect(messageRepository.save(MESSAGE_1)).andReturn(MESSAGE_1);
            expect(messageRepository.save(MESSAGE_2)).andReturn(MESSAGE_2);

            List<Message> unreadMessages = fetchAllUnreadByBoxAddresses(boxAddresses);

            assertThat(unreadMessages)
                    .hasSize(2)
                    .containsExactly(MESSAGE_1, MESSAGE_2);
        }

        @Test
        public void markAllMessagesAsNotifiedWhenFetchingAllUnreadMessages() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1);

            expect(messageRepository.findByBoxAddressInAndNotifiedIsFalse(boxAddresses))
                    .andReturn(listOf(MESSAGE_1));
            expect(messageRepository.save(MESSAGE_1)).andReturn(MESSAGE_1);

            List<Message> unreadMessages = fetchAllUnreadByBoxAddresses(boxAddresses);

            assertThat(unreadMessages).first().hasFieldOrPropertyWithValue("notified", true);
        }

        private List<Message> fetchAllUnreadByBoxAddresses(Set<UUID> boxAddresses) {
            replayAll();
            List<Message> messages = this.messages.fetchAllUnnotifiedByBoxAddresses(boxAddresses);
            verifyAll();
            return messages;
        }

        @Test
        public void saveAMessage() {

            expect(messageRepository.save(MESSAGE_1)).andReturn(MESSAGE_1);

            replayAll();
            Message savedMessage = messages.store(MESSAGE_1);
            verifyAll();

            assertThat(savedMessage).usingRecursiveComparison().isEqualTo(MESSAGE_1);
        }
    }
}