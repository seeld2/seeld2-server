package be.seeld.conversations.messages;

import be.seeld.common.encryption.aes.AES;
import be.seeld.common.lang.Maps;
import be.seeld.common.time.Dates;
import be.seeld.notifications.NotificationsDispatcher;
import be.seeld.users.Users;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.easymock.Capture;
import org.easymock.Mock;
import org.easymock.MockType;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.messages.MessageFixture.message;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.CHUCK_BOX_ADDRESS;

@RunWith(Enclosed.class)
public class MessagesProcessUTest {

    private static final UUID ALICE_CONVERSATION_ID = UUID.fromString("a4cf0d8a-6c8f-4299-b958-1c5a8a19f659");
    private static final int[] ALICE_MESSAGE_ID_AES_PAYLOAD = {98, 76, 54};
    private static final String ALICE_PAYLOAD = "alice payload";
    private static final UUID ALICE_WRITES_TO_BOB_BOX_ADDRESS = UUID.fromString("b201ef8a-d50e-48db-aa82-557fe32ed112");
    private static final UUID BOB_CONVERSATION_ID = UUID.fromString("a4cf0d8a-6c8f-4299-b958-1c5a8a19f659");
    private static final String BOB_PAYLOAD = "bob payload";
    private static final UUID BOX_ADDRESS_1 = UUID.fromString("fe6e8650-fd7e-4117-a112-ae76b8c811de");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("6dde80ca-46a7-4170-abb3-249f969e7cbd");
    private static final UUID CONVERSATION_ID_1 = UUID.fromString("d63a8512-73ae-480c-96a7-1a69dee56172");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("52d603cf-7e7b-4cb0-b0c2-eed89cb37950");
    private static final String HMAC_HASH = "28pSdRrwRtrmjnuF9ELs5YuC1SchR15Kd4og-_usJwhSx-PmronKHRqQ3ka2G-oI28yZSQ7d-2PE-BpZgHCeAg==";
    private static final String HMAC_HASH_1 = "hmacHash1";
    private static final String HMAC_HASH_2 = "hmacHash2";
    private static final Long LATEST_NEW_MESSAGE_TIMELINE = 123456789L;
    private static final UUID MESSAGE_ID = UUID.fromString("3e9f9517-4332-4187-93dd-0b01389a125b");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("93cae808-2150-4ea3-88f6-9a6bfef17a5d");
    private static final int[] MESSAGES_AES_PAYLOAD = {12, 34, 56};
    private static final String MESSAGE_PAYLOAD = "message payload";
    private static final String MESSAGE_PAYLOAD_2 = "message payload 2";
    private static final Message SAVED_MESSAGE = message();

    private static abstract class Common extends Unit {

        protected MessagesProcess messagesProcess;
        @Mock
        protected AES aes;
        @Mock
        protected Dates dates;
        @Mock
        protected Messages messages;
        @Mock(MockType.NICE)
        protected NotificationsDispatcher notificationsDispatcher;
        @Mock
        protected Users users;

        @Before
        public void setUp() {
            messagesProcess = new MessagesProcess(
                    aes,
                    dates,
                    messages,
                    notificationsDispatcher,
                    secureRandom,
                    users);

            expect(dates.zonedDateTimeUtc())
                    .andReturn(ZonedDateTime.of(2019, 12, 28, 9, 33, 25, 0, ZoneId.of("UTC")))
                    .anyTimes();
        }
    }

    public static class WhenFetchingNewMessages extends Common {

        @Test
        public void shouldFetchNewMessages() {

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Maps.Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Maps.Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID, MESSAGE_PAYLOAD);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, HMAC_HASH_2, MESSAGE_ID_2, MESSAGE_PAYLOAD_2);

            expect(messages.fetchAllUnnotifiedByBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(message1, message2));

            List<Message> returnedMessages = fetchNew(hmacHashesByBoxAddress);

            assertThat(returnedMessages).containsExactly(message1, message2);
        }

        @Test
        public void shouldFetchNewMessagesThatDoNotHaveHmacHashes() {

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Maps.Entry.of(BOX_ADDRESS_1, null),
                    Maps.Entry.of(BOX_ADDRESS_2, null));
            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, null, MESSAGE_ID, MESSAGE_PAYLOAD);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, null, MESSAGE_ID_2, MESSAGE_PAYLOAD_2);

            expect(messages.fetchAllUnnotifiedByBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(message1, message2));

            List<Message> returnedMessages = fetchNew(hmacHashesByBoxAddress);

            assertThat(returnedMessages).containsExactly(message1, message2);
        }

        @Test
        public void shouldPreventFetchingNewMessagesHavingHmacHashesNotMatchingTheProvidedHashes() {

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Maps.Entry.of(BOX_ADDRESS_1, null),
                    Maps.Entry.of(BOX_ADDRESS_2, HMAC_HASH));
            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID, MESSAGE_PAYLOAD);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, HMAC_HASH_2, MESSAGE_ID_2, MESSAGE_PAYLOAD_2);

            expect(messages.fetchAllUnnotifiedByBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                    .andReturn(listOf(message1, message2));

            List<Message> returnedMessages = fetchNew(hmacHashesByBoxAddress);

            assertThat(returnedMessages).isEmpty();
        }

        private List<Message> fetchNew(Map<UUID, String> hmacHashesByBoxAddress) {
            replayAll();
            List<Message> returnedMessages = messagesProcess.fetchNew(hmacHashesByBoxAddress);
            verifyAll();
            return returnedMessages;
        }
    }

    public static class WhenStoringMessages extends Common {

        @Test
        public void shouldStoreTheMessages() throws JsonProcessingException {

            MessagesOut messages = new MessagesOut(listOf(
                    new MessageOut(ALICE_BOX_ADDRESS, ALICE_CONVERSATION_ID, HMAC_HASH, ALICE_PAYLOAD),
                    new MessageOut(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, HMAC_HASH, BOB_PAYLOAD)
            ));

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decrypt(MESSAGES_AES_PAYLOAD, ALICE_EXCHANGE_KEY)).andReturn(messages.asJSON());

            Capture<Message> aliceMessage = newCapture();
            expect(this.messages.store(capture(aliceMessage))).andReturn(SAVED_MESSAGE);

            Capture<Message> bobMessage = newCapture();
            expect(this.messages.store(capture(bobMessage))).andReturn(SAVED_MESSAGE);

            expect(aes.encryptToUnsignedBytes(anyObject(), eq(ALICE_EXCHANGE_KEY)))
                    .andReturn(ALICE_MESSAGE_ID_AES_PAYLOAD);

            replayAll();
            int[] messageIdAesPayload = messagesProcess.store(ALICE_PSEUDO, MESSAGES_AES_PAYLOAD);
            verifyAll();

            assertThat(messageIdAesPayload).isNotNull().isNotEmpty().isEqualTo(ALICE_MESSAGE_ID_AES_PAYLOAD);

            assertThat(aliceMessage.getValue())
                    .hasNoNullFieldsOrProperties()
                    .hasFieldOrPropertyWithValue("boxAddress", ALICE_BOX_ADDRESS)
                    .hasFieldOrPropertyWithValue("conversationId", ALICE_CONVERSATION_ID)
                    .hasFieldOrPropertyWithValue("notified", true)
                    .hasFieldOrPropertyWithValue("payload", ALICE_PAYLOAD);

            assertThat(bobMessage.getValue())
                    .hasNoNullFieldsOrProperties()
                    .hasFieldOrPropertyWithValue("boxAddress", ALICE_WRITES_TO_BOB_BOX_ADDRESS)
                    .hasFieldOrPropertyWithValue("conversationId", BOB_CONVERSATION_ID)
                    .hasFieldOrPropertyWithValue("notified", false)
                    .hasFieldOrPropertyWithValue("payload", BOB_PAYLOAD);

            assertThat(aliceMessage.getValue().getTimeline()).isNotEqualTo(bobMessage.getValue().getTimeline());
        }

        @Test
        public void shouldAssignDifferentTimelinesToStoredMessagesToAvoidLinkingByTimelineValue()
                throws JsonProcessingException {

            Boolean[] timelinesAreEqual = new Boolean[10];
            for (int i = 0; i < 10; i++) {

                resetAll();

                expect(dates.zonedDateTimeUtc())
                        .andReturn(ZonedDateTime.of(2019, 12, 28, 9, 33, 25, 0, ZoneId.of("UTC")))
                        .anyTimes();

                MessagesOut messages = new MessagesOut(listOf(
                        new MessageOut(ALICE_BOX_ADDRESS, ALICE_CONVERSATION_ID, HMAC_HASH, ALICE_PAYLOAD),
                        new MessageOut(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, HMAC_HASH, BOB_PAYLOAD)
                ));

                expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
                expect(aes.decrypt(MESSAGES_AES_PAYLOAD, ALICE_EXCHANGE_KEY)).andReturn(messages.asJSON());

                Capture<Message> aliceMessage = newCapture();
                expect(this.messages.store(capture(aliceMessage))).andReturn(SAVED_MESSAGE);

                Capture<Message> bobMessage = newCapture();
                expect(this.messages.store(capture(bobMessage))).andReturn(SAVED_MESSAGE);

                expect(aes.encryptToUnsignedBytes(anyObject(), eq(ALICE_EXCHANGE_KEY)))
                        .andReturn(ALICE_MESSAGE_ID_AES_PAYLOAD);

                replayAll();
                messagesProcess.store(ALICE_PSEUDO, MESSAGES_AES_PAYLOAD);
                verifyAll();

                timelinesAreEqual[i] = aliceMessage.getValue().getTimeline() == bobMessage.getValue().getTimeline();
            }

            long differentTimelinesCount = Arrays.stream(timelinesAreEqual).filter(e -> !e).count();
            assertThat(differentTimelinesCount).isGreaterThan(9);
        }
    }

    public static class Other extends Common {

        @Test
        public void shouldFetchTheLatestNewMessageTimeline() {
            Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1, BOX_ADDRESS_2);

            expect(messages.fetchLatestUnnotifiedTimelineByBoxAddresses(boxAddresses))
                    .andReturn(Optional.of(LATEST_NEW_MESSAGE_TIMELINE));

            replayAll();
            Optional<Long> timeline = messagesProcess.fetchLatestNewMessageTimeline(boxAddresses);
            verifyAll();

            assertThat(timeline).isPresent().get().isEqualTo(LATEST_NEW_MESSAGE_TIMELINE);
        }

        @Test
        public void notifyReceiversOfANewMessage() throws JsonProcessingException {

            MessagesOut messages = new MessagesOut(listOf(
                    new MessageOut(ALICE_BOX_ADDRESS, ALICE_CONVERSATION_ID, ALICE_PAYLOAD),
                    new MessageOut(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, BOB_PAYLOAD)
            ));

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decrypt(MESSAGES_AES_PAYLOAD, ALICE_EXCHANGE_KEY)).andReturn(messages.asJSON());

            expect(this.messages.store(anyObject(Message.class))).andReturn(SAVED_MESSAGE);

            expect(this.messages.store(anyObject(Message.class))).andReturn(SAVED_MESSAGE);

            notificationsDispatcher.notifyOfNewMessage(ALICE_WRITES_TO_BOB_BOX_ADDRESS);

            expect(aes.encryptToUnsignedBytes(anyObject(), eq(ALICE_EXCHANGE_KEY)))
                    .andReturn(ALICE_MESSAGE_ID_AES_PAYLOAD);

            replayAll();
            messagesProcess.store(ALICE_PSEUDO, MESSAGES_AES_PAYLOAD);
            verifyAll();
        }

        @Test(expected = MessagesProcess.MessagesProcessException.class)
        public void raiseAnExceptionIfNoSenderMessageIsPresentInTheAesPayload() throws JsonProcessingException {

            MessagesOut messages = new MessagesOut(listOf(
                    new MessageOut(CHUCK_BOX_ADDRESS, ALICE_CONVERSATION_ID, ALICE_PAYLOAD),
                    new MessageOut(ALICE_WRITES_TO_BOB_BOX_ADDRESS, BOB_CONVERSATION_ID, BOB_PAYLOAD)
            ));

            expect(users.fetchUnencrypted(ALICE_PSEUDO)).andReturn(alice.toUser());
            expect(aes.decrypt(MESSAGES_AES_PAYLOAD, ALICE_EXCHANGE_KEY)).andReturn(messages.asJSON());

            replayAll();
            messagesProcess.store(ALICE_PSEUDO, MESSAGES_AES_PAYLOAD);
            verifyAll();
        }
    }
}