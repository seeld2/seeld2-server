package be.seeld.conversations.messages;

import be.seeld.common.web.AESRequest;
import be.seeld.common.web.AESResponse;
import be.seeld.conversations.messages.MessagesWebApi.NewMessagesRequest;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;
import test.Unit;
import test.spring.PrincipalMock;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.Entry;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.messages.MessagesWebApi.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_PSEUDO;
import static test.junit.RestAssertions.assertThatCreateResponseIs;

@RunWith(Enclosed.class)
public class MessagesWebApiUTest {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("fe6e8650-fd7e-4117-a112-ae76b8c811de");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("6dde80ca-46a7-4170-abb3-249f969e7cbd");
    private static final UUID CONVERSATION_ID = UUID.fromString("d63a8512-73ae-480c-96a7-1a69dee56172");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("52d603cf-7e7b-4cb0-b0c2-eed89cb37950");
    private static final String HMAC_HASH_1 = "hmacHash1";
    private static final String HMAC_HASH_2 = "hmacHash2";
    private static final UUID MESSAGE_ID = UUID.fromString("3e9f9517-4332-4187-93dd-0b01389a125b");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("93cae808-2150-4ea3-88f6-9a6bfef17a5d");
    private static final int[] MESSAGES_OUT_PAYLOAD = {12, 34, 56};
    private static final String MESSAGE_PAYLOAD = "message payload";
    private static final String MESSAGE_PAYLOAD_2 = "message payload 2";

    public static class WhenFetchingLatestNewMessageTimeline extends Common {

        private static final Long LATEST_NEW_MESSAGE_TIMELINE = 123456789L;

        Set<UUID> boxAddresses = setOf(BOX_ADDRESS_1, BOX_ADDRESS_2);

        @Test
        public void shouldReturnATimelineResponseWithTheLatestNewMessageIn() {

            expect(messagesProcess.fetchLatestNewMessageTimeline(boxAddresses))
                    .andReturn(Optional.of(LATEST_NEW_MESSAGE_TIMELINE));

            replayAll();
            ResponseEntity<?> response = messagesWebApi
                    .fetchLatestNewMessageTimeline(new BoxAddressesRequest(boxAddresses));
            verifyAll();

            TimelineResponse timelineResponse = assertThatCreateResponseIs(response, TimelineResponse.class);
            assertThat(timelineResponse.getTimeline()).isEqualTo(LATEST_NEW_MESSAGE_TIMELINE);
        }

        @Test
        public void shouldReturnATimelineResponseWithNullWhenNoNewMessagesAreAvailable() {

            expect(messagesProcess.fetchLatestNewMessageTimeline(boxAddresses)).andReturn(Optional.empty());

            replayAll();
            ResponseEntity<?> response = messagesWebApi
                    .fetchLatestNewMessageTimeline(new BoxAddressesRequest(boxAddresses));
            verifyAll();

            TimelineResponse timelineResponse = assertThatCreateResponseIs(response, TimelineResponse.class);
            assertThat(timelineResponse.getTimeline()).isNull();
        }
    }

    public static class WhenFetchingNewMessages extends Common {

        @Test
        public void fetchAllNewMessagesForAListOfBoxAddresses() {

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            List<Message> messages = listOf(
                    MessageFixture.message(BOX_ADDRESS_1, CONVERSATION_ID, HMAC_HASH_1, MESSAGE_ID, MESSAGE_PAYLOAD),
                    MessageFixture.message(BOX_ADDRESS_2, CONVERSATION_ID_2, HMAC_HASH_2, MESSAGE_ID_2, MESSAGE_PAYLOAD_2)
            );

            expect(messagesProcess.fetchNew(hmacHashesByBoxAddress)).andReturn(messages);

            replayAll();
            ResponseEntity<?> response = messagesWebApi.fetchNewMessages(new NewMessagesRequest(hmacHashesByBoxAddress));
            verifyAll();

            MessagesInResponse messagesIn = assertThatCreateResponseIs(response, MessagesInResponse.class);
            assertThat(messagesIn).isNotNull().hasNoNullFieldsOrProperties();
            assertThat(messagesIn.getMessages())
                    .isNotEmpty()
                    .hasSize(2)
                    .containsExactlyInAnyOrder(
                            new MessageIn(CONVERSATION_ID, MESSAGE_ID, MESSAGE_PAYLOAD, BOX_ADDRESS_1),
                            new MessageIn(CONVERSATION_ID_2, MESSAGE_ID_2, MESSAGE_PAYLOAD_2, BOX_ADDRESS_2)
                    );
        }
    }

    public static class WhenStoringMessages extends Common {

        private static final int[] ALICE_MESSAGE_ID_AES_PAYLOAD = {98, 76, 54};

        @Test
        public void storeMessages() {

            expect(messagesProcess.store(ALICE_PSEUDO, MESSAGES_OUT_PAYLOAD))
                    .andReturn(ALICE_MESSAGE_ID_AES_PAYLOAD);

            replayAll();
            ResponseEntity<?> response = messagesWebApi
                    .storeMessages(new AESRequest(MESSAGES_OUT_PAYLOAD), PrincipalMock.of(ALICE_PSEUDO));
            verifyAll();

            AESResponse aesResponse = assertThatCreateResponseIs(response, AESResponse.class);
            assertThat(aesResponse).isNotNull().hasNoNullFieldsOrProperties();
            assertThat(aesResponse.getPayload()).isEqualTo(ALICE_MESSAGE_ID_AES_PAYLOAD);
        }
    }

    private static class Common extends Unit {

        MessagesWebApi messagesWebApi;
        @Mock
        MessagesProcess messagesProcess;

        @Before
        public void setUp() {
            messagesWebApi = new MessagesWebApi(messagesProcess);
        }
    }
}
