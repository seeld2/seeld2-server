package be.seeld.conversations.messages;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import test.Integration;
import test.TestUsers;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.conversations.messages.MessageFixture.message;
import static be.seeld.conversations.messages.MessageTestBuilder.aMessage;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class MessageRepositoryITest {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("23491110-24ce-4592-93a3-ee2e53122dd6");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("986f6333-7b7e-48a9-9b8c-cbd32ba45207");
    private static final UUID BOX_ADDRESS_3 = UUID.fromString("a1b65091-c90e-468f-9059-d1ca2aac880b");

    private static final UUID CONVERSATION_ID_1 = UUID.fromString("1196ea81-a386-41b0-851f-e593e6a20480");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("daa80723-38d8-42d4-ab63-046e26689565");
    private static final UUID CONVERSATION_ID_3 = UUID.fromString("32bff89b-4b9e-4e67-a418-79209d8b3087");

    private static final UUID MESSAGE_ID_1 = UUID.fromString("771299ec-b8ff-4970-bf36-6b95f293d735");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("1357b289-ff34-43b3-ac4e-87617422098e");
    private static final UUID MESSAGE_ID_3 = UUID.fromString("16e696e3-37a0-4168-a54d-17bc0e978ea6");
    private static final UUID MESSAGE_ID_4 = UUID.fromString("37c321af-2f66-4c85-81f3-c7e3e55ae033");
    private static final UUID MESSAGE_ID_5 = UUID.fromString("b4352ca7-a4f3-41b1-becc-bf9af1530154");

    private static final boolean NOT_NOTIFIED = false;
    private static final boolean NOTIFIED = true;

    private static class Common extends Integration {

        @Resource
        MessageRepository messageRepository;

        TestUsers.TestUser alice;
        TestUsers.TestUser bob;

        @Before
        public void setUp() {
            super.setUp();

            alice = testUsers.get(TestUsers.ALICE_PSEUDO);
            bob = testUsers.get(TestUsers.BOB_PSEUDO);
        }
    }

    public static class WhenDeletingMessages extends Common {

        @Test
        public void shouldDeleteAMessageWithSpecificBoxAddressConversationIdAndMessageId() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1));
            assertThat(messageData.find(BOX_ADDRESS_1, MESSAGE_ID_1)).isNotNull();

            messageRepository.delete(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            assertThat(messageData.find(BOX_ADDRESS_1, MESSAGE_ID_1)).isNull();
        }

        @Test
        public void shouldDeleteAllMessagesForTheSpecifiedSetOfBoxAddresses() {
            messageData.insert(message(BOX_ADDRESS_1, MESSAGE_ID_1));
            messageData.insert(message(BOX_ADDRESS_2, MESSAGE_ID_2));
            messageData.insert(message(BOX_ADDRESS_3, MESSAGE_ID_3));

            messageRepository.deleteAllForBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_3));

            assertThat(messageData.findAllWithBoxAddresses(BOX_ADDRESS_1)).isEmpty();
            assertThat(messageData.findAllWithBoxAddresses(BOX_ADDRESS_3)).isEmpty();
            assertThat(messageData.findAllWithBoxAddresses(BOX_ADDRESS_2)).isNotEmpty();
        }

        @Test
        public void shouldDeleteAllMessagesForTheSpecifiedConversationId() {
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_1).and.messageId(MESSAGE_ID_1).noBlanks());
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_1).and.messageId(MESSAGE_ID_2).noBlanks());
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_2).and.messageId(MESSAGE_ID_3).noBlanks());

            assertThat(messageData.findAll()).hasSize(3);

            messageRepository.deleteAllForConversationId(CONVERSATION_ID_1);

            assertThat(messageData.findAll())
                    .hasSize(1)
                    .extracting("messageId").containsOnly(MESSAGE_ID_3);
        }
    }

    public static class WhenFindingTheLatestUnnotifiedMessageInBoxAddresses extends Common {

        @Test
        public void shouldReturnTheLatestUnnotifiedMessageForTheSpecifiedBoxAddresses() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, NOTIFIED, 1L));
            Message latest = messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_2, MESSAGE_ID_2, NOT_NOTIFIED, 3L));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_1, MESSAGE_ID_3, NOT_NOTIFIED, 2L));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_4, NOTIFIED, 4L));

            Optional<Message> latestMessage = messageRepository
                    .findLatestByBoxAddressInAndNotifiedIsFalse(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2, BOX_ADDRESS_3));

            assertThat(latestMessage)
                    .isPresent().get()
                    .isEqualTo(latest);
        }

        @Test
        public void shouldReturnEmptyIfThereAreNoUnnotifiedMessages() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, NOTIFIED, 1L));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_2, NOTIFIED, 4L));

            Optional<Message> latestMessage = messageRepository
                    .findLatestByBoxAddressInAndNotifiedIsFalse(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2, BOX_ADDRESS_3));

            assertThat(latestMessage).isNotPresent();
        }
    }

    public static class Other extends Common {

        @Test
        public void shouldFetchAMessageWithTheSpecifiedBoxAddressConversationIdAndMessageId() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1));

            Message message = messageRepository.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            assertThat(message)
                    .hasFieldOrPropertyWithValue("boxAddress", BOX_ADDRESS_1)
                    .hasFieldOrPropertyWithValue("conversationId", CONVERSATION_ID_1)
                    .hasFieldOrPropertyWithValue("messageId", MESSAGE_ID_1);
        }

        @Test
        public void shouldFetchAllMessagesForTheSpecifiedConversationIdAndBoxAddresses() {
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_1).boxAddress(BOX_ADDRESS_1).and.messageId(MESSAGE_ID_1).noBlanks());
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_1).boxAddress(BOX_ADDRESS_2).and.messageId(MESSAGE_ID_2).noBlanks());
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_1).boxAddress(BOX_ADDRESS_3).and.messageId(MESSAGE_ID_3).noBlanks());
            messageData.insert(aMessage().with.conversationId(CONVERSATION_ID_2).boxAddress(BOX_ADDRESS_3).and.messageId(MESSAGE_ID_4).noBlanks());

            List<Message> messages = messageRepository
                    .fetchAllForConversationIdWithBoxAddresses(CONVERSATION_ID_1, setOf(BOX_ADDRESS_2, BOX_ADDRESS_3));

            assertThat(messages)
                    .hasSize(2)
                    .extracting("boxAddress").containsOnly(BOX_ADDRESS_2, BOX_ADDRESS_3);
        }

        @Test
        public void findAllMessagesForAGivenConversationId() {
            Message message1 = messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, 1L));
            Message message2 = messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_1, MESSAGE_ID_2, 2L));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_2, MESSAGE_ID_3));

            List<Message> messages = messageRepository.findByConversationIdOrderByTimelineDesc(CONVERSATION_ID_1);

            assertThat(messages)
                    .hasSize(2)
                    .containsSequence(message2, message1);
        }

        @Test
        public void findAPageOfMessagesForAGivenConversationId() {

            Message message1 = messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, 3));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_2, 2));
            Message message3 = messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_3, 4));
            Message message4 = messageData.insert(message(BOX_ADDRESS_3, CONVERSATION_ID_1, MESSAGE_ID_4, 1));
            Message message5 = messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_5, 5));

            List<Message> messages = messageRepository
                    .findByConversationIdOrderByTimelineDesc(CONVERSATION_ID_1, PageRequest.of(0, 2));
            assertThat(messages).hasSize(2).containsExactly(message5, message3);

            messages = messageRepository
                    .findByConversationIdOrderByTimelineDesc(CONVERSATION_ID_1, PageRequest.of(1, 2));
            assertThat(messages).hasSize(2).containsExactly(message1, message4);

            messages = messageRepository
                    .findByConversationIdOrderByTimelineDesc(CONVERSATION_ID_1, PageRequest.of(2, 2));
            assertThat(messages).isEmpty();
        }

        @Test
        public void findAllUnnotifiedMessagesForAGivenListOfBoxAddress() {
            messageData.insert(message(alice.getBoxAddress(), MESSAGE_ID_1, NOT_NOTIFIED));
            messageData.insert(message(alice.getBoxAddress(), MESSAGE_ID_2, NOT_NOTIFIED));
            messageData.insert(message(alice.getBoxAddress(), MESSAGE_ID_3, NOT_NOTIFIED));
            messageData.insert(message(bob.getBoxAddress(), MESSAGE_ID_4, NOT_NOTIFIED));
            messageData.insert(message(alice.getBoxAddress(), MESSAGE_ID_5, NOTIFIED));

            List<Message> unreadMessages = messageRepository
                    .findByBoxAddressInAndNotifiedIsFalse(setOf(alice.getBoxAddress()));

            assertThat(unreadMessages).hasSize(3);
            assertThat(unreadMessages).allMatch(message -> message.getBoxAddress().equals(alice.getBoxAddress()));
        }

        @Test
        public void findAllMessagesByMessagesIdsAndForAGivenConversationId() {

            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_2, MESSAGE_ID_2));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_3));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_5));

            List<Message> foundMessages = messageRepository
                    .findByMessagesIdsInAndConversationId(CONVERSATION_ID_1,
                            setOf(MESSAGE_ID_1, MESSAGE_ID_2, MESSAGE_ID_3, MESSAGE_ID_4));

            assertThat(foundMessages).hasSize(2);
            assertThat(foundMessages.stream().map(Message::getMessageId).collect(Collectors.toList()))
                    .containsOnly(MESSAGE_ID_1, MESSAGE_ID_3);
        }

        @Test
        public void findAllMessagesIdsForAGivenConversationId() {

            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_1, MESSAGE_ID_2));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_3, MESSAGE_ID_3));

            List<UUID> messagesIds = messageRepository.findIdsByConversationId(CONVERSATION_ID_1);

            assertThat(messagesIds)
                    .hasSize(2)
                    .containsOnly(MESSAGE_ID_1, MESSAGE_ID_2);
        }

        @Test
        public void findAPageOfMessagesIdsForAGivenConversationId() {

            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, 3));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_2, 2));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_3, 4));
            messageData.insert(message(BOX_ADDRESS_3, CONVERSATION_ID_1, MESSAGE_ID_4, 1));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_5, 5));

            List<UUID> ids = messageRepository.findIdsByConversationId(CONVERSATION_ID_1, PageRequest.of(0, 2));
            assertThat(ids).hasSize(2).containsExactly(MESSAGE_ID_5, MESSAGE_ID_3);

            ids = messageRepository.findIdsByConversationId(CONVERSATION_ID_1, PageRequest.of(1, 2));
            assertThat(ids).hasSize(2).containsExactly(MESSAGE_ID_1, MESSAGE_ID_4);

            ids = messageRepository.findIdsByConversationId(CONVERSATION_ID_1, PageRequest.of(2, 2));
            assertThat(ids).isEmpty();
        }

        @Test
        public void findLatestMessagesForAListOfConversationIds() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, 1));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_2, 4));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_3, MESSAGE_ID_3, 2));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_2, MESSAGE_ID_4, 3));
            messageData.insert(message(BOX_ADDRESS_3, CONVERSATION_ID_3, MESSAGE_ID_5, 5));

            Page<Message> page = messageRepository.findLatestByConversationIdsInOrderByTimelineDesc(
                    setOf(CONVERSATION_ID_3, CONVERSATION_ID_1), PageRequest.of(0, 10));

            assertThat(page).isNotNull();
            assertThat(page.getContent()).extracting("messageId").containsExactly(MESSAGE_ID_5, MESSAGE_ID_2);
        }

        @Test
        public void findLatestMessagesForAListOfBoxAddresses() {
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, 2L));
            messageData.insert(message(BOX_ADDRESS_2, CONVERSATION_ID_1, MESSAGE_ID_2, 3L));
            messageData.insert(message(BOX_ADDRESS_1, CONVERSATION_ID_2, MESSAGE_ID_3, 1L));
            messageData.insert(message(BOX_ADDRESS_3, CONVERSATION_ID_3, MESSAGE_ID_4, 4L));

            Page<Message> page1 = messageRepository.findLatestDistinctConversationIdByBoxAddressInOrderByTimelineDesc(
                    setOf(BOX_ADDRESS_1, BOX_ADDRESS_2), PageRequest.of(0, 1));

            assertThat(page1)
                    .hasSize(1)
                    .first()
                    .hasFieldOrPropertyWithValue("messageId", MESSAGE_ID_2);

            Page<Message> page2 = messageRepository.findLatestDistinctConversationIdByBoxAddressInOrderByTimelineDesc(
                    setOf(BOX_ADDRESS_1, BOX_ADDRESS_2), PageRequest.of(1, 1));

            assertThat(page2)
                    .hasSize(1)
                    .first()
                    .hasFieldOrPropertyWithValue("messageId", MESSAGE_ID_3);
        }

        @Test
        public void findAMessageWithAGivenBoxAddressAndMessageId() {
            Message message = messageData.insert(message());

            Message foundMessage = messageRepository.findOne(MessageFixture.BOX_ADDRESS, MessageFixture.MESSAGE_ID);

            assertThat(foundMessage).usingRecursiveComparison().isEqualTo(message);
        }

        @Test
        public void storeAMessage() {

            Message savedMessage = messageRepository.save(message());

            Message foundMessage = messageData.find(MessageFixture.BOX_ADDRESS, MessageFixture.MESSAGE_ID);

            assertThat(foundMessage).usingRecursiveComparison().isEqualTo(savedMessage);
        }
    }
}