package be.seeld.conversations.messages;

import org.junit.Before;
import org.junit.Test;
import test.Unit;

import java.util.List;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static org.assertj.core.api.Assertions.assertThat;

public class MessageInUTest extends Unit {

    private static final UUID CONVERSATION_ID = UUID.fromString("543566a2-f4a6-4918-aee8-b98c82afa59e");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("99dbe5c8-7b97-4696-b3b2-3bb70d3e7127");
    private static final UUID MESSAGE_ID = UUID.fromString("c9f3ce38-6604-4f49-a27a-393f4e5db429");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("2ae292da-e2a2-40b1-b13e-5d9627271f63");
    private static final String PAYLOAD = "payload";
    private static final String PAYLOAD_2 = "payload 2";
    private static final UUID READ_FROM_BOX_ADDRESS = UUID.fromString("8f996299-568d-47ea-9a41-2cbe7b727f4b");

    private MessageIn messageIn;

    @Before
    public void setUp() {
        messageIn = new MessageIn(CONVERSATION_ID, MESSAGE_ID, PAYLOAD, READ_FROM_BOX_ADDRESS);
        assertThatValidationViolationAmountIs(0, messageIn);
    }

    @Test
    public void raiseAViolationIfConversationIdIsNull() {
        messageIn.setConversationId(null);
        assertThatValidationViolationAmountIs(1, messageIn);
    }

    @Test
    public void raiseAViolationIfMessageIdIsNull() {
        messageIn.setMessageId(null);
        assertThatValidationViolationAmountIs(1, messageIn);
    }

    @Test
    public void raiseAViolationIfPayloadIsEmpty() {
        messageIn.setPayload("");
        assertThatValidationViolationAmountIs(1, messageIn);
    }

    @Test
    public void raiseAViolationIfReadFromBoxAddressIsNull() {
        messageIn.setReadFromBoxAddress(null);
        assertThatValidationViolationAmountIs(1, messageIn);
    }

    @Test
    public void produceListOfMessagesInFromListOfMessages() {

        List<Message> messages = listOf(
                MessageFixture.message(READ_FROM_BOX_ADDRESS, CONVERSATION_ID, MESSAGE_ID, PAYLOAD),
                MessageFixture.message(READ_FROM_BOX_ADDRESS, CONVERSATION_ID_2, MESSAGE_ID_2, PAYLOAD_2)
        );

        List<MessageIn> messagesIn = MessageIn.from(messages);

        assertThat(messagesIn).isNotNull().isNotEmpty().hasSize(2);
        assertThat(messagesIn).containsExactlyInAnyOrder(
                new MessageIn(CONVERSATION_ID, MESSAGE_ID, PAYLOAD, READ_FROM_BOX_ADDRESS),
                new MessageIn(CONVERSATION_ID_2, MESSAGE_ID_2, PAYLOAD_2, READ_FROM_BOX_ADDRESS)
        );
    }
}