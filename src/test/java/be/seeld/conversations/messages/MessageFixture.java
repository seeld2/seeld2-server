package be.seeld.conversations.messages;

import java.util.UUID;

/**
 * @deprecated Use {@link MessageTestBuilder}
 */
@Deprecated
public class MessageFixture {

    public static final UUID BOX_ADDRESS = UUID.fromString("1d1580ea-9602-4d74-8e00-be1ac7172a7b");
    public static final UUID CONVERSATION_ID = UUID.fromString("e917a147-3176-4325-a517-19d72e82c88b");
    public static final String HMAC_HASH = "28pSdRrwRtrmjnuF9ELs5YuC1SchR15Kd4og-_usJwhSx-PmronKHRqQ3ka2G-oI28yZSQ7d-2PE-BpZgHCeAg==";
    public static final UUID MESSAGE_ID = UUID.fromString("6849becb-ff2f-40e9-8fd7-0464a447340c");
    public static final boolean NOTIFIED = true;
    public static final long TIMELINE = 1533722068985L;
    public static final String PAYLOAD = "message payload";

    public static Message message() {
        return message(MESSAGE_ID);
    }

    public static Message message(UUID messageId) {
        return message(BOX_ADDRESS, CONVERSATION_ID, messageId, NOTIFIED, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID messageId, String payload) {
        Message message = message(BOX_ADDRESS, CONVERSATION_ID, messageId, NOTIFIED, PAYLOAD, TIMELINE);
        message.setPayload(payload);
        return message;
    }

    public static Message message(UUID boxAddress, UUID messageId) {
        return message(boxAddress, CONVERSATION_ID, messageId, NOTIFIED, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, String hmacHash) {
        return message(boxAddress, conversationId, hmacHash, MESSAGE_ID, NOTIFIED, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId) {
        return message(boxAddress, conversationId, hmacHash, messageId, NOTIFIED, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, UUID messageId) {
        return message(boxAddress, conversationId, messageId, NOTIFIED, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID messageId, boolean notified) {
        return message(boxAddress, CONVERSATION_ID, messageId, notified, PAYLOAD, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, UUID messageId, long timeline) {
        return message(boxAddress, conversationId, messageId, NOTIFIED, PAYLOAD, timeline);
    }

    public static Message message(UUID boxAddress, UUID conversationId, UUID messageId, String payload) {
        return message(boxAddress, conversationId, messageId, NOTIFIED, payload, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, UUID messageId, boolean notified, long timeline) {
        return message(boxAddress, conversationId, messageId, notified, PAYLOAD, timeline);
    }

    public static Message message(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId, String payload) {
        return message(boxAddress, conversationId, hmacHash, messageId, NOTIFIED, payload, TIMELINE);
    }

    public static Message message(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId, long timeline) {
        return message(boxAddress, conversationId, hmacHash, messageId, NOTIFIED, PAYLOAD, timeline);
    }

    public static Message message(UUID boxAddress, UUID conversationId, UUID messageId,
                                  boolean notified, String payload, long timeline) {
        return message(boxAddress, conversationId, HMAC_HASH, messageId, notified, payload, timeline);
    }

    public static Message message(UUID boxAddress, UUID conversationId, String hmacHash, UUID messageId,
                                  boolean notified, String payload, long timeline) {
        return Message.builder()
                .boxAddress(boxAddress)
                .conversationId(conversationId)
                .hmacHash(hmacHash)
                .messageId(messageId)
                .notified(notified)
                .timeline(timeline)
                .payload(payload)
                .build();
    }
}
