package be.seeld.conversations.messages;

import be.seeld.conversations.messages.MessagesWebApi.BoxAddressesRequest;
import org.junit.Before;
import org.junit.Test;
import test.Unit;

import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;

public class BoxAddressesRequestUTest extends Unit {

    private BoxAddressesRequest boxAddressesRequest;

    @Before
    public void setUp() {
        boxAddressesRequest = new BoxAddressesRequest(setOf(
                UUID.fromString("7b317b35-12c5-4774-a2ab-5e6466ab1a5b"),
                UUID.fromString("0bef9b0f-d51f-4a7a-a802-118a7f7ae4a5")
        ));
    }

    @Test
    public void raiseAViolationIfItContainsNoBoxAddresses() {
        boxAddressesRequest.setBoxAddresses(java.util.Collections.emptySet());
        assertThatValidationViolationAmountIs(1, boxAddressesRequest);
    }
}