package be.seeld.conversations.messages;

import be.seeld.common.lang.Collections;
import org.junit.Test;
import test.Unit;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.BOB_BOX_ADDRESS;

public class MessagesOutUTest extends Unit {

    private static final UUID CONVERSATION_ID = UUID.fromString("2d6eac95-2143-43bf-a86d-0324423bd174");
    private static final String HMAC_HASH = "28pSdRrwRtrmjnuF9ELs5YuC1SchR15Kd4og-_usJwhSx-PmronKHRqQ3ka2G-oI28yZSQ7d-2PE-BpZgHCeAg==";

    @Test
    public void shouldReturnTheSenderMessage() {
        MessagesOut messagesOut = new MessagesOut(Collections.listOf(
                new MessageOut(ALICE_BOX_ADDRESS, CONVERSATION_ID, "payload"),
                new MessageOut(BOB_BOX_ADDRESS, CONVERSATION_ID, HMAC_HASH, "payload")
        ));
        MessageOut senderMessage = messagesOut.senderMessage(alice.toUser());
        assertThat(senderMessage.getBoxAddress()).isEqualTo(ALICE_BOX_ADDRESS);
    }

    @Test
    public void shouldReturnTheReceiversMessages() {
        MessagesOut messagesOut = new MessagesOut(Collections.listOf(
                new MessageOut(ALICE_BOX_ADDRESS, CONVERSATION_ID, "payload"),
                new MessageOut(BOB_BOX_ADDRESS, CONVERSATION_ID, HMAC_HASH, "payload")
        ));
        List<MessageOut> messageOuts = messagesOut.receiverMessages(alice.toUser());
        assertThat(messageOuts)
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("boxAddress", BOB_BOX_ADDRESS);
    }
}
