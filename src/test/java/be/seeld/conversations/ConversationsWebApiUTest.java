package be.seeld.conversations;

import be.seeld.common.lang.Maps;
import be.seeld.conversations.ConversationsWebApi.ConversationDeleteRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationIdsRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationIdsResponse;
import be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationMessagesIdsResponse;
import be.seeld.conversations.ConversationsWebApi.ConversationsRequest;
import be.seeld.conversations.ConversationsWebApi.ConversationsResponse;
import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.MessageFixture;
import be.seeld.conversations.messages.MessageIn;
import be.seeld.conversations.messages.MessageTestBuilder;
import be.seeld.conversations.messages.MessagesInResponse;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.Unit;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.ConversationsWebApi.ConversationMessagesRequest;
import static be.seeld.conversations.messages.MessageTestBuilder.aMessage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.junit.RestAssertions.assertThatCreateResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class ConversationsWebApiUTest extends Unit {

    private static final UUID BOX_ADDRESS = UUID.fromString("096e4c9d-e651-46c9-8849-83818624b054");
    private static final UUID CONVERSATION_ID = UUID.fromString("fa84142d-b44d-4615-aa3e-c1bbd99f59ed");
    private static final String HMAC_HASH = "hmacHash1";
    private static final UUID MESSAGE_ID_1 = UUID.fromString("3e9f9517-4332-4187-93dd-0b01389a125b");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("de492550-b388-4de5-b585-396658f0d72f");

    private static final int PAGE = 0;
    private static final int PAGE_SIZE = 2;

    private ConversationsWebApi conversationsWebApi;
    @Mock
    private ConversationsProcess conversationsProcess;

    @Before
    public void setUp() {
        conversationsWebApi = new ConversationsWebApi(conversationsProcess);
    }

    @Test
    public void shouldDeleteAGivenConversation() {

        conversationsProcess.deleteConversation(CONVERSATION_ID, mapOf(Maps.Entry.of(BOX_ADDRESS, HMAC_HASH)));

        replayAll();
        conversationsWebApi.deleteConversation(
                CONVERSATION_ID,
                new ConversationDeleteRequest(mapOf(Maps.Entry.of(BOX_ADDRESS, HMAC_HASH)))
        );
        verifyAll();
    }

    @Test
    public void shouldDeleteAGivenMessage() {

        conversationsProcess.deleteMessage(BOX_ADDRESS, CONVERSATION_ID, MESSAGE_ID_1, HMAC_HASH);

        replayAll();
        ResponseEntity<?> response = conversationsWebApi.deleteMessage(
                CONVERSATION_ID,
                MESSAGE_ID_1,
                new ConversationsWebApi.MessageDeleteRequest(BOX_ADDRESS, HMAC_HASH));
        verifyAll();

        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    @Test
    public void fetchAllConversationsFromASetOfBoxAddresses() {

        Map<UUID, String> hmacHashesByBoxAddress = mapOf(setOf(BOX_ADDRESS), HMAC_HASH);

        expect(conversationsProcess.fetchConversationsFromBoxAddresses(hmacHashesByBoxAddress, PAGE, PAGE_SIZE))
                .andReturn(listOf(MessageFixture.message()));

        ConversationsRequest request = ConversationsRequest.with(hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
        replayAll();
        ResponseEntity<?> response = conversationsWebApi.fetchConversations(request);
        verifyAll();

        assertThatTheFixtureMessageHasBeenReturned(response);
    }

    @Test
    public void fetchAPageOfConversationIdsForAGivenListOfBoxAddresses() {

        Map<UUID, String> hmacHashesByBoxAddress = mapOf(setOf(BOX_ADDRESS), HMAC_HASH);

        expect(conversationsProcess.fetchConversationsIds(hmacHashesByBoxAddress, PAGE, PAGE_SIZE))
                .andReturn(setOf(CONVERSATION_ID));

        ConversationIdsRequest request = new ConversationIdsRequest(hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
        replayAll();
        ResponseEntity<?> response = conversationsWebApi.fetchConversationIds(request);
        verifyAll();

        ConversationIdsResponse conversationIdsResponse = assertThatCreateResponseIs(response, ConversationIdsResponse.class);
        assertThat(conversationIdsResponse.getConversationIds())
                .hasSize(1)
                .containsExactly(CONVERSATION_ID);
    }

    @Test
    public void fetchAllMessagesForAGivenConversationIdAndListOfMessagesIds() {

        Message message1 = MessageFixture.message(BOX_ADDRESS, CONVERSATION_ID, MESSAGE_ID_1);
        Message message2 = MessageFixture.message(BOX_ADDRESS, CONVERSATION_ID, MESSAGE_ID_2);
        List<Message> messages = listOf(message1, message2);

        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Maps.Entry.of(BOX_ADDRESS, HMAC_HASH));

        expect(conversationsProcess.fetchConversationMessages(CONVERSATION_ID, setOf(MESSAGE_ID_1, MESSAGE_ID_2), hmacHashesByBoxAddress))
                .andReturn(messages);

        ConversationMessagesRequest request = new ConversationMessagesRequest(hmacHashesByBoxAddress,
                setOf(MESSAGE_ID_1, MESSAGE_ID_2), null, null);
        replayAll();
        ResponseEntity<?> response = conversationsWebApi.fetchConversationMessages(CONVERSATION_ID, request);
        verifyAll();

        MessagesInResponse messagesInResponse = assertThatCreateResponseIs(response, MessagesInResponse.class);
        assertThat(messagesInResponse.getMessages())
                .hasSize(2)
                .containsAll(MessageIn.from(messages));
    }

    @Test
    public void fetchAPageOfMessagesForAGivenConversationId() {
        Message message1 = aMessage().with.boxAddress(BOX_ADDRESS).conversationId(CONVERSATION_ID)
                .and.messageId(MESSAGE_ID_1).noBlanks();
        Message message2 = aMessage().with.boxAddress(BOX_ADDRESS).conversationId(CONVERSATION_ID)
                .and.messageId(MESSAGE_ID_1).noBlanks();
        List<Message> messages = listOf(message1, message2);

        Map<UUID, String> hmacHashesByBoxAddress = mapOf(Maps.Entry.of(BOX_ADDRESS, HMAC_HASH));

        expect(conversationsProcess.fetchConversationMessages(CONVERSATION_ID, hmacHashesByBoxAddress, PAGE, PAGE_SIZE))
                .andReturn(messages);

        ConversationMessagesRequest request = new ConversationMessagesRequest(hmacHashesByBoxAddress, null, PAGE, PAGE_SIZE);
        replayAll();
        ResponseEntity<?> response = conversationsWebApi.fetchConversationMessages(CONVERSATION_ID, request);
        verifyAll();

        MessagesInResponse messagesInResponse = assertThatCreateResponseIs(response, MessagesInResponse.class);
        assertThat(messagesInResponse.getMessages())
                .hasSize(2)
                .containsAll(MessageIn.from(messages));
    }

    @Test
    public void fetchAPageOfMessagesIdsLinkedToAConversation() {

        Map<UUID, String> hmacHashesByBoxAddress = mapOf(setOf(BOX_ADDRESS), HMAC_HASH);

        expect(conversationsProcess
                .fetchConversationMessagesIds(CONVERSATION_ID, hmacHashesByBoxAddress, PAGE, PAGE_SIZE))
                .andReturn(setOf(MESSAGE_ID_1, MESSAGE_ID_2));

        ConversationMessagesIdsRequest request = new ConversationMessagesIdsRequest(hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
        replayAll();
        ResponseEntity<?> response = conversationsWebApi.fetchConversationMessagesIds(CONVERSATION_ID, request);
        verifyAll();

        ConversationMessagesIdsResponse conversationMessagesIdsResponse =
                assertThatCreateResponseIs(response, ConversationMessagesIdsResponse.class);
        assertThat(conversationMessagesIdsResponse.getMessagesIds())
                .hasSize(2)
                .contains(MESSAGE_ID_1, MESSAGE_ID_2);
    }

    private void assertThatTheFixtureMessageHasBeenReturned(ResponseEntity<?> response) {
        ConversationsResponse conversationsResponse = assertThatCreateResponseIs(response, ConversationsResponse.class);
        assertThat(conversationsResponse.getConversations())
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("conversationId", MessageFixture.CONVERSATION_ID)
                .hasFieldOrPropertyWithValue("payload", MessageFixture.PAYLOAD)
                .hasFieldOrPropertyWithValue("readFromBoxAddress", MessageFixture.BOX_ADDRESS);
    }
}