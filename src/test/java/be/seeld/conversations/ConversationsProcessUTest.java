package be.seeld.conversations;

import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.Messages;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.Entry;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.conversations.messages.MessageFixture.message;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;

@RunWith(Enclosed.class)
public class ConversationsProcessUTest {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("fe6e8650-fd7e-4117-a112-ae76b8c811de");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("6dde80ca-46a7-4170-abb3-249f969e7cbd");

    private static final UUID CONVERSATION_ID_1 = UUID.fromString("824ea1c7-6db8-4acf-98bc-fca076610d88");
    private static final UUID CONVERSATION_ID_2 = UUID.fromString("5724e7ad-fb06-46e3-8d60-cf4ae489fd68");

    private static final String HMAC_HASH_1 = "hmacHash1";
    private static final String HMAC_HASH_2 = "hmacHash2";
    private static final String HMAC_HASH_3 = "hmacHash3";

    private static final UUID MESSAGE_ID_1 = UUID.fromString("0e604a28-00a1-4db0-9fbe-0919bf3b4f25");
    private static final UUID MESSAGE_ID_2 = UUID.fromString("7274ed83-f70a-4799-bb6e-58c36a33bbf4");

    private static final int PAGE = 0;
    private static final int PAGE_SIZE = 2;

    private static abstract class Common extends Unit {

        protected ConversationsProcess conversationsProcess;
        @Mock
        protected Messages messages;

        @Before
        public void setUp() {
            conversationsProcess = new ConversationsProcess(messages);
        }
    }

    public static class WhenDeletingAMessage extends Common {

        @Test
        public void shouldDeleteTheMessageAndItsMessageLifeEntry() {
            expect(messages.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1)).andReturn(
                    Optional.of(message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1)));
            messages.delete(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            deleteMessage(HMAC_HASH_1);
        }

        @Test
        public void shouldHandleRequestsToDeleteNonExistingMessages() {
            expect(messages.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1))
                    .andReturn(Optional.empty());

            deleteMessage(HMAC_HASH_1);
        }

        @Test
        public void shouldHandleDeletingMessagesStoredWithoutHmacHashes() {
            expect(messages.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1)).andReturn(
                    Optional.of(message(BOX_ADDRESS_1, CONVERSATION_ID_1, null, MESSAGE_ID_1)));
            messages.delete(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            deleteMessage(null);
        }

        @Test
        public void shouldPreventAttemptsToDeleteMessagesWhenNoHmacHashIsProvided() {
            expect(messages.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1)).andReturn(
                    Optional.of(message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1)));
            messages.delete(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            deleteMessage(null);
        }

        @Test
        public void shouldPreventAttemptsToDeleteMessagesWhenTheWrongHmacHashIsProvided() {
            expect(messages.fetch(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1)).andReturn(
                    Optional.of(message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1)));
            messages.delete(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1);

            deleteMessage(HMAC_HASH_2);
        }

        private void deleteMessage(String hmacHash) {
            replayAll();
            conversationsProcess.deleteMessage(BOX_ADDRESS_1, CONVERSATION_ID_1, MESSAGE_ID_1, hmacHash);
            verifyAll();
        }
    }

    public static class WhenFetchingConversationsFromBoxAddresses extends Common {

        @Test
        public void shouldFetchConversations() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, HMAC_HASH_2);

            expect(messages.fetchAllLatestByBoxAddressesUnreadFirst(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2), PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            List<Message> conversations = fetchConversationsFromBoxAddresses(mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2)));

            assertThat(conversations).containsExactly(message1, message2);
        }

        @Test
        public void shouldFetchConversationIdsOfConversationsThatDoNotHaveHmacHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, (String) null);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, (String) null);

            expect(messages.fetchAllLatestByBoxAddressesUnreadFirst(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2), PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            List<Message> conversationIds = fetchConversationsFromBoxAddresses(mapOf(
                    Entry.of(BOX_ADDRESS_1, null),
                    Entry.of(BOX_ADDRESS_2, null)));

            assertThat(conversationIds).containsExactly(message1, message2);
        }

        @Test
        public void shouldPreventFetchingConversationIdsOfConversationsHavingHmacHashesNotMatchingTheProvidedHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_2, HMAC_HASH_2);

            expect(messages.fetchAllLatestByBoxAddressesUnreadFirst(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2), PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            List<Message> conversationIds = fetchConversationsFromBoxAddresses(mapOf(
                    Entry.of(BOX_ADDRESS_1, null),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2)));

            assertThat(conversationIds).containsExactly(message2);
        }

        private List<Message> fetchConversationsFromBoxAddresses(Map<UUID, String> hmacHashesByBoxAddress) {
            replayAll();
            List<Message> conversations = conversationsProcess
                    .fetchConversationsFromBoxAddresses(hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
            verifyAll();
            return conversations;
        }
    }

    public static class WhenFetchingConversationMessagesIds extends Common {

        @Test
        public void shouldFetchConversationMessagesIds() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            Set<UUID> messagesIds = fetchConversationMessagesIds(hmacHashesByBoxAddress);

            assertThat(messagesIds).containsExactlyInAnyOrder(MESSAGE_ID_1, MESSAGE_ID_2);
        }

        @Test
        public void shouldFetchConversationMessagesIdsOfMessagesThatDoNotHaveHmacHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, null, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, null, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, null),
                    Entry.of(BOX_ADDRESS_2, null));
            Set<UUID> messagesIds = fetchConversationMessagesIds(hmacHashesByBoxAddress);

            assertThat(messagesIds).containsExactlyInAnyOrder(MESSAGE_ID_1, MESSAGE_ID_2);
        }

        @Test
        public void shouldPreventFetchingConversationMessagesIdsOfMessagesWhoseHmacHashesDoNotMatchTheProvidedHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_3));
            Set<UUID> messagesIds = fetchConversationMessagesIds(hmacHashesByBoxAddress);

            assertThat(messagesIds).containsExactlyInAnyOrder(MESSAGE_ID_1);
        }

        private Set<UUID> fetchConversationMessagesIds(Map<UUID, String> hmacHashesByBoxAddress) {
            replayAll();
            Set<UUID> messagesIds = conversationsProcess
                    .fetchConversationMessagesIds(CONVERSATION_ID_1, hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
            verifyAll();
            return messagesIds;
        }
    }

    public static class WhenFetchingConversationMessages extends Common {

        @Test
        public void shouldFetchConversationMessages() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            List<Message> messages = fetchConversationMessages(hmacHashesByBoxAddress);

            assertThat(messages).containsExactlyInAnyOrder(message1, message2);
        }

        @Test
        public void shouldFetchConversationMessagesThatDoNotHaveHmacHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, null, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, null, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, null),
                    Entry.of(BOX_ADDRESS_2, null));
            List<Message> messages = fetchConversationMessages(hmacHashesByBoxAddress);

            assertThat(messages).containsExactlyInAnyOrder(message1, message2);
        }

        @Test
        public void shouldPreventFetchingMessagesWhoseHmacHashesDoNotMatchTheProvidedHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);

            expect(messages.fetchAllForConversationId(CONVERSATION_ID_1, PAGE, PAGE_SIZE))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_3));
            List<Message> messages = fetchConversationMessages(hmacHashesByBoxAddress);

            assertThat(messages).containsExactlyInAnyOrder(message1);
        }

        private List<Message> fetchConversationMessages(Map<UUID, String> hmacHashesByBoxAddress) {
            replayAll();
            List<Message> messages = conversationsProcess
                    .fetchConversationMessages(CONVERSATION_ID_1, hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
            verifyAll();
            return messages;
        }
    }

    public static class WhenFetchingAConversationMessages extends Common {

        @Test
        public void shouldFetchAPageOfMessages() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);
            Set<UUID> messageIds = setOf(MESSAGE_ID_1, MESSAGE_ID_2);

            expect(messages.fetchAllByMessagesIdsForConversationId(CONVERSATION_ID_1, messageIds))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_1),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            replayAll();
            List<Message> messages = conversationsProcess
                    .fetchConversationMessages(CONVERSATION_ID_1, messageIds, hmacHashesByBoxAddress);
            verifyAll();

            assertThat(messages).containsExactlyInAnyOrder(message1, message2);
        }

        @Test
        public void shouldFetchMessagesThatFoNotHaveHmacHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, null, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, null, MESSAGE_ID_2);
            Set<UUID> messageIds = setOf(MESSAGE_ID_1, MESSAGE_ID_2);

            expect(messages.fetchAllByMessagesIdsForConversationId(CONVERSATION_ID_1, messageIds))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, null),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            replayAll();
            List<Message> messages = conversationsProcess
                    .fetchConversationMessages(CONVERSATION_ID_1, messageIds, hmacHashesByBoxAddress);
            verifyAll();

            assertThat(messages).containsExactlyInAnyOrder(message1, message2);
        }

        @Test
        public void shouldPreventFetchingMessagesWhoseHmacHashesDoNotMatchTheProvidedHashes() {

            Message message1 = message(BOX_ADDRESS_1, CONVERSATION_ID_1, HMAC_HASH_1, MESSAGE_ID_1);
            Message message2 = message(BOX_ADDRESS_2, CONVERSATION_ID_1, HMAC_HASH_2, MESSAGE_ID_2);
            Set<UUID> messageIds = setOf(MESSAGE_ID_1, MESSAGE_ID_2);

            expect(messages.fetchAllByMessagesIdsForConversationId(CONVERSATION_ID_1, messageIds))
                    .andReturn(listOf(message1, message2));

            Map<UUID, String> hmacHashesByBoxAddress = mapOf(
                    Entry.of(BOX_ADDRESS_1, HMAC_HASH_3),
                    Entry.of(BOX_ADDRESS_2, HMAC_HASH_2));
            replayAll();
            List<Message> messages = conversationsProcess
                    .fetchConversationMessages(CONVERSATION_ID_1, messageIds, hmacHashesByBoxAddress);
            verifyAll();

            assertThat(messages).containsExactlyInAnyOrder(message2);
        }
    }
}