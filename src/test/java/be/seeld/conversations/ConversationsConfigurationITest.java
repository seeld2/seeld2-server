package be.seeld.conversations;

import be.seeld.common.time.Dates;
import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class ConversationsConfigurationITest extends Integration {

    @Resource
    private ConversationsConfiguration.ConversationsProperties properties;
    @Resource
    private Dates dates;

    @Test
    public void shouldLoadConversationsProperties() {
        assertThat(properties).isNotNull();
        assertThat(properties.getDefaultFetchingPageSize()).isGreaterThan(0);
        assertThat(properties.getDefaultIdsFetchingPageSize()).isGreaterThan(0);
    }

    @Test
    public void shouldDeclareBeanDates() {
        assertThat(dates).isNotNull();
    }
}