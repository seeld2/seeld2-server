package be.seeld;

import be.seeld.parameters.Parameters;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import test.Unit;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.junit.RestAssertions.assertThatOkResponseIs;

public class WebApiUTest extends Unit {

    private static final String MOBILE_VERSION = "1.2.3";

    private WebApi webApi;
    @Mock
    private Parameters parameters;

    @Before
    public void setUp() {
        webApi = new WebApi(parameters);
    }

    @Test
    public void returnServerTimeInMillisWhenPinged() {
        ResponseEntity<?> response = webApi.ping();

        PongResponse pongResponse = assertThatOkResponseIs(response, PongResponse.class);
        assertThat(pongResponse).hasNoNullFieldsOrProperties();
    }

    @Test
    public void returnVersions() {
        expect(parameters.getMobileVersion()).andReturn(Optional.of(MOBILE_VERSION));

        replayAll();
        ResponseEntity<?> response = webApi.versions();
        verifyAll();

        VersionsResponse versionsResponse = assertThatOkResponseIs(response, VersionsResponse.class);
        assertThat(versionsResponse.getMobileVersion()).isEqualTo(MOBILE_VERSION);
    }
}