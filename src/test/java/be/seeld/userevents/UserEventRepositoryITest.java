package be.seeld.userevents;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static org.assertj.core.api.Assertions.assertThat;

public class UserEventRepositoryITest extends Integration {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("df66db20-1b70-4805-b322-fca3b1e5622d");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("db0a0bab-8d59-42a2-a98a-c2cd81f6ab3e");
    private static final UUID BOX_ADDRESS_3 = UUID.fromString("f8137d30-c1ed-4043-b487-1d6bd2341f1c");

    private static final UUID EVENT_ID_1 = UUID.fromString("3fab340b-2c60-4b0d-b837-044b3a585c12");
    private static final UUID EVENT_ID_2 = UUID.fromString("9f292fdb-6b2a-48f1-bbea-40323364dbb4");
    private static final UUID EVENT_ID_3 = UUID.fromString("3f8e33ee-109b-4f38-80b0-f3090567c7cf");

    private static final String PAYLOAD = "payload";

    @Resource
    private UserEventRepository userEventRepository;

    @Test
    public void shouldDeleteAUserEventWithAGivenBoxAddressAndEventId() {
        userEventData.insert(UserEventFixture.userEvent());

        assertThat(userEventData.exists(UserEventFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID)).isTrue();

        userEventRepository.delete(UserEventFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID);

        assertThat(userEventData.exists(UserEventFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID)).isFalse();
    }

    @Test
    public void shouldDeleteAllUserEventsForTheSpecifiedBoxAddresses() {
        userEventData.insert(UserEvent.builder().boxAddress(BOX_ADDRESS_1).eventId(EVENT_ID_1).payload(PAYLOAD).build());
        userEventData.insert(UserEvent.builder().boxAddress(BOX_ADDRESS_2).eventId(EVENT_ID_2).payload(PAYLOAD).build());
        userEventData.insert(UserEvent.builder().boxAddress(BOX_ADDRESS_3).eventId(EVENT_ID_3).payload(PAYLOAD).build());

        userEventRepository.deleteAllForBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_3));

        assertThat(userEventData.find(BOX_ADDRESS_1)).isEmpty();
        assertThat(userEventData.find(BOX_ADDRESS_3)).isEmpty();
        assertThat(userEventData.find(BOX_ADDRESS_2)).isNotEmpty();
    }

    @Test
    public void findAUserEventWithAGivenBoxAddressAndEventId() {
        UserEvent userEvent = userEventData.insert(UserEventFixture.userEvent());

        UserEvent foundUserEvent = userEventRepository.findOne(UserEventFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID);

        assertThat(foundUserEvent).usingRecursiveComparison().isEqualTo(userEvent);
    }

    @Test
    public void findAllUserEventsForAGivenBoxAddress() {

        UserEvent userEvent = userEventData.insert(UserEventFixture.userEvent());

        List<UserEvent> foundUserEvents = userEventRepository.findByPkBoxAddress(UserEventFixture.BOX_ADDRESS);

        assertThat(foundUserEvents)
                .hasSize(1)
                .first()
                .usingRecursiveComparison().isEqualTo(userEvent);
    }

    @Test
    public void shouldFindAllUserEventsForASetOfBoxAddresses() {

        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_1));
        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_2));
        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_3));

        List<UserEvent> userEvents = userEventRepository.findForBoxAddresses(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2));

        assertThat(userEvents).hasSize(2);
        assertThat(userEvents).extracting(UserEvent::getBoxAddress).contains(BOX_ADDRESS_1);
        assertThat(userEvents).extracting(UserEvent::getBoxAddress).contains(BOX_ADDRESS_2);
    }

    @Test
    public void shouldCreateAUserEventEntry() {

        UserEvent savedUserEvent = userEventRepository.save(UserEventFixture.userEvent());

        UserEvent foundUserEvent = userEventData.find(UserEventFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID);
        assertThat(foundUserEvent).usingRecursiveComparison().isEqualTo(savedUserEvent);
    }
}
