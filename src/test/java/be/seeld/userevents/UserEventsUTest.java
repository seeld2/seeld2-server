package be.seeld.userevents;

import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Encryptables;
import be.seeld.userevents.UserEventsConfiguration.UserEventsProperties;
import be.seeld.users.UserFixture;
import org.easymock.Capture;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import test.Unit;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.userevents.UserEventFixture.BOX_ADDRESS;
import static be.seeld.userevents.UserEventFixture.HMAC_HASH;
import static be.seeld.userevents.UserEventFixture.RETURN_BOX_ADDRESS;
import static be.seeld.userevents.UserEventFixture.VALID_UNTIL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.CAROL_BOX_ADDRESS;

@SuppressWarnings("SameParameterValue")
public class UserEventsUTest extends Unit {

    private static final UUID ANOTHER_EVENT_ID = UUID.fromString("4ad47e37-04df-4824-ac43-edb2bfcbd029");
    private static final UUID ANOTHER_SEELD_BOX_ADDRESS = UUID.fromString("ec877028-797a-4b8d-8f89-773786909e51");
    private static final UserEvent ENCRYPTED_USER_EVENT = userEvent();
    private static final UserEvent UNENCRYPTED_USER_EVENT = userEvent();

    private static final UUID EVENT_ID = UUID.fromString("703e6fe9-2635-4a18-b153-02149a41e1e9");
    private static final String PAYLOAD = "payload";
    private static final String RETURN_PAYLOAD = "return payload";
    private static final int VALID_UNTIL_LIFETIME = 7;

    private UserEvents userEvents;
    @Mock
    private Encryptables encryptables;
    @Mock
    private UserEventRepository userEventRepository;

    @Before
    public void setUp() {
        UserEventsProperties properties = new UserEventsProperties();
        properties.setValidUntilLifetime(VALID_UNTIL_LIFETIME);

        userEvents = new UserEvents(encryptables, userEventRepository, properties);
    }

    @Test
    public void shouldDeleteAUserEventWithAGivenEventIdAndBoxAddress() {
        userEventRepository.delete(BOB_BOX_ADDRESS, EVENT_ID);

        replayAll();
        userEvents.delete(BOB_BOX_ADDRESS, EVENT_ID);
        verifyAll();
    }

    @Test
    public void shouldCheckThatAnEventIdExistsForASpecifiedEventIdAndSeeldBoxAddress() {
        expect(userEventRepository.findOne(UserFixture.BOX_ADDRESS, UserEventFixture.EVENT_ID))
                .andReturn(userEvent(UserFixture.BOX_ADDRESS));
        assertThat(exists(EVENT_ID, UserFixture.BOX_ADDRESS)).isTrue();

        resetAll();

        expect(userEventRepository.findOne(ANOTHER_SEELD_BOX_ADDRESS, ANOTHER_EVENT_ID)).andReturn(null);
        assertThat(exists(ANOTHER_EVENT_ID, ANOTHER_SEELD_BOX_ADDRESS)).isFalse();
    }

    private boolean exists(UUID eventId, UUID boxAddress) {
        replayAll();
        boolean result = userEvents.exists(eventId, boxAddress);
        verifyAll();
        return result;
    }

    @Test
    public void shouldFetchAUserEventWithAGivenEventIdAndABoxAddress() {

        UserEvent userEvent = userEvent(CAROL_BOX_ADDRESS, EVENT_ID);

        expect(userEventRepository.findOne(CAROL_BOX_ADDRESS, EVENT_ID)).andReturn(userEvent);
        expect(encryptables.ofEncrypted(userEvent)).andReturn(Encryptable.of(userEvent, userEvent, cloner));

        replayAll();
        Optional<UserEvent> fetchedUserEvent = userEvents.fetchUnencrypted(CAROL_BOX_ADDRESS, EVENT_ID);
        verifyAll();

        assertThat(fetchedUserEvent)
                .isPresent().get()
                .hasFieldOrPropertyWithValue("boxAddress", CAROL_BOX_ADDRESS)
                .hasFieldOrPropertyWithValue("eventId", EVENT_ID);
    }

    @Test
    public void shouldFetchUnencryptedUserEventsForASetOfBoxAddresses() {
        UserEvent userEvent1 = userEvent("1");
        UserEvent userEvent2 = userEvent("2");
        UserEvent encryptedUserEvent1 = userEvent("encrypted 1");
        UserEvent encryptedUserEvent2 = userEvent("encrypted 2");

        expect(userEventRepository.findForBoxAddresses(setOf(BOB_BOX_ADDRESS)))
                .andReturn(listOf(encryptedUserEvent1, encryptedUserEvent2));
        expect(encryptables.ofEncrypted(encryptedUserEvent1))
                .andReturn(Encryptable.of(userEvent1, encryptedUserEvent1, cloner));
        expect(encryptables.ofEncrypted(encryptedUserEvent2))
                .andReturn(Encryptable.of(userEvent2, encryptedUserEvent2, cloner));

        replayAll();
        List<UserEvent> userEvents = this.userEvents.fetchUnencrypted(setOf(BOB_BOX_ADDRESS));
        verifyAll();

        assertThat(userEvents)
                .hasSize(2)
                .containsExactlyInAnyOrder(userEvent1, userEvent2);
    }

    @Test
    public void shouldFetchUnencryptedUserEventsByBoxAddress() {
        Encryptable<UserEvent> encryptableUserEvent1 = Encryptable.of(userEvent("1"), userEvent("encrypted 1"), cloner);
        Encryptable<UserEvent> encryptableUserEvent2 = Encryptable.of(userEvent("2"), userEvent("encrypted 2"), cloner);

        expect(userEventRepository.findByPkBoxAddress(UserFixture.BOX_ADDRESS))
                .andReturn(Arrays.asList(encryptableUserEvent1.getEncrypted(), encryptableUserEvent2.getEncrypted()));
        expect(encryptables.ofEncrypted(encryptableUserEvent1.getEncrypted())).andReturn(encryptableUserEvent1);
        expect(encryptables.ofEncrypted(encryptableUserEvent2.getEncrypted())).andReturn(encryptableUserEvent2);

        replayAll();
        List<UserEvent> results = this.userEvents.fetchUnencryptedByBoxAddress(UserFixture.BOX_ADDRESS);
        verifyAll();

        assertThat(results).isNotNull().isNotEmpty().hasSize(2)
                .contains(encryptableUserEvent1.getUnencrypted(), encryptableUserEvent2.getUnencrypted());
    }

    @Test
    public void shouldStoreAUserEvent() {
        Encryptable<UserEvent> userEventEncryptable = Encryptable.of(UNENCRYPTED_USER_EVENT, ENCRYPTED_USER_EVENT, cloner);

        Capture<UserEvent> userEventCapture = newCapture();

        expect(encryptables.ofUnencrypted(capture(userEventCapture))).andReturn(userEventEncryptable);
        expect(userEventRepository.save(ENCRYPTED_USER_EVENT)).andReturn(ENCRYPTED_USER_EVENT);

        replayAll();
        userEvents.store(BOB_BOX_ADDRESS, HMAC_HASH, PAYLOAD, ALICE_BOX_ADDRESS.toString(), RETURN_PAYLOAD);
        verifyAll();

        UserEvent generatedUserEvent = userEventCapture.getValue();
        assertThat(generatedUserEvent)
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("boxAddress", BOB_BOX_ADDRESS)
                .hasFieldOrPropertyWithValue("hmacHash", HMAC_HASH)
                .hasFieldOrPropertyWithValue("payload", PAYLOAD)
                .hasFieldOrPropertyWithValue("returnBoxAddress", ALICE_BOX_ADDRESS.toString())
                .hasFieldOrPropertyWithValue("returnPayload", RETURN_PAYLOAD);
    }

    private static UserEvent userEvent() {
        return new UserEvent(BOX_ADDRESS, EVENT_ID, HMAC_HASH, PAYLOAD,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }

    private static UserEvent userEvent(String payload) {
        return new UserEvent(BOX_ADDRESS, EVENT_ID, HMAC_HASH, payload,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }

    private static UserEvent userEvent(UUID boxAddress) {
        return new UserEvent(boxAddress, EVENT_ID, HMAC_HASH, PAYLOAD,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }

    private static UserEvent userEvent(UUID boxAddress, UUID eventId) {
        return new UserEvent(boxAddress, eventId, HMAC_HASH, PAYLOAD,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }
}
