package be.seeld.userevents;

import java.util.UUID;

public class UserEventFixture {

    public static final String HMAC_HASH = "28pSdRrwRtrmjnuF9ELs5YuC1SchR15Kd4og-_usJwhSx-PmronKHRqQ3ka2G-oI28yZSQ7d-2PE-BpZgHCeAg==";
    public static final UUID BOX_ADDRESS = UUID.fromString("3ddc996c-faee-4d16-a82a-930f055a223e");
    public static final UUID EVENT_ID = UUID.fromString("703e6fe9-2635-4a18-b153-02149a41e1e9");
    public static final String PAYLOAD = "user event payload";
    public static final UUID RETURN_BOX_ADDRESS = UUID.fromString("066968d1-e3c7-4344-a6e0-f9952fef7b75");
    public static final String RETURN_PAYLOAD = "return payload";
    public static final long VALID_UNTIL = 1_512_811_369_498L;

    public static UserEvent userEvent() {
        return userEvent(BOX_ADDRESS, PAYLOAD);
    }

    public static UserEvent userEvent(String payload) {
        return userEvent(BOX_ADDRESS, payload);
    }

    /**
     * A new user user event is an user event whose event ID is randomly generated.
     */
    public static UserEvent newUserEvent(UUID boxAddress, String payload) {
        return userEvent(boxAddress, UUID.randomUUID(), payload);
    }

    public static UserEvent userEvent(UUID boxAddress) {
        return userEvent(boxAddress, PAYLOAD);
    }

    public static UserEvent userEvent(UUID boxAddress, String payload) {
        return userEvent(boxAddress, EVENT_ID, payload);
    }

    public static UserEvent userEvent(UUID boxAddress, UUID eventId, String payload) {
        return UserEvent.builder()
                .boxAddress(boxAddress)
                .eventId(eventId)
                .returnBoxAddress(RETURN_BOX_ADDRESS.toString())
                .returnPayload(RETURN_PAYLOAD)
                .payload(payload)
                .validUntil(VALID_UNTIL)
                .hmacHash(HMAC_HASH)
                .build();
    }
}
