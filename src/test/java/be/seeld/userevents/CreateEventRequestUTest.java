package be.seeld.userevents;

import be.seeld.userevents.EventsWebApi.CreateEventRequest;
import org.junit.Before;
import org.junit.Test;
import test.Unit;

public class CreateEventRequestUTest extends Unit {

    private CreateEventRequest createEventRequest;

    @Before
    public void setUp() {
        createEventRequest = new CreateEventRequest(new int[]{1, 5, 9},
                "contact request payload", "contact request return payload");
        assertThatValidationViolationAmountIs(0, createEventRequest);
    }

    @Test
    public void raiseAViolationIfAesPayloadIsNullOrEmpty() {

        createEventRequest.setAesPayload(new int[]{});
        assertThatValidationViolationAmountIs(1, createEventRequest);

        createEventRequest.setAesPayload(null);
        assertThatValidationViolationAmountIs(1, createEventRequest);
    }

    @Test
    public void raiseAViolationIfThePayloadIsNullOrEmpty() {

        createEventRequest.setPayload("");
        assertThatValidationViolationAmountIs(1, createEventRequest);

        createEventRequest.setPayload(null);
        assertThatValidationViolationAmountIs(1, createEventRequest);
    }

    @Test
    public void raiseAViolationIfTheReturnPayloadIsNullOrEmpty() {

        createEventRequest.setReturnPayload("");
        assertThatValidationViolationAmountIs(1, createEventRequest);

        createEventRequest.setReturnPayload(null);
        assertThatValidationViolationAmountIs(1, createEventRequest);
    }
}