package be.seeld.userevents;

import be.seeld.common.encryption.aes.AES;
import be.seeld.common.lang.Maps.Entry;
import be.seeld.users.UserFixture;
import be.seeld.users.Users;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Collections.setOf;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.userevents.UserEventFixture.PAYLOAD;
import static be.seeld.userevents.UserEventFixture.RETURN_BOX_ADDRESS;
import static be.seeld.userevents.UserEventFixture.RETURN_PAYLOAD;
import static be.seeld.userevents.UserEventFixture.VALID_UNTIL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_EXCHANGE_KEY;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.TestUsers.CAROL_BOX_ADDRESS;
import static test.TestUsers.CHUCK_BOX_ADDRESS;

@RunWith(Enclosed.class)
public class EventsProcessUTest {

    private static final int[] AES_PAYLOAD = new int[]{79, 15, 23};
    private static final UUID EVENT_ID_1 = UUID.fromString("cdbf5e3a-abef-47ec-a29e-ead9e60c9d8b");
    private static final UUID EVENT_ID_2 = UUID.fromString("205afd2d-4be6-4e01-8b1a-86394c8a884e");
    private static final String HMAC_HASH_1 = "hmacHash1";
    private static final String HMAC_HASH_2 = "hmacHash2";
    private static final String WRONG_HMAC_HASH = "wrong hmac hash";

    private static abstract class Common extends Unit {

        protected EventsProcess eventsProcess;
        @Mock
        protected AES aes;
        @Mock
        protected Users users;
        @Mock
        protected UserEvents userEvents;

        @Before
        public void setUp() {
            eventsProcess = new EventsProcess(aes, users, userEvents);
        }
    }

    public static class WhenCreatingNewUserEvents extends Common {

        @Test
        public void shouldCreateANewUserEvent() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO))
                    .andReturn(UserFixture.user(ALICE_PSEUDO, ALICE_BOX_ADDRESS, ALICE_EXCHANGE_KEY));
            expect(aes.decrypt(AES_PAYLOAD, ALICE_EXCHANGE_KEY))
                    .andReturn("{\"boxAddress\":\"" + BOB_BOX_ADDRESS + "\",\"hmacHash\":\"" + HMAC_HASH_1 + "\"}");
            userEvents.store(BOB_BOX_ADDRESS, HMAC_HASH_1, PAYLOAD, ALICE_BOX_ADDRESS.toString(), RETURN_PAYLOAD);

            replayAll();
            eventsProcess.createNewUserEvent(AES_PAYLOAD, ALICE_PSEUDO, PAYLOAD, RETURN_PAYLOAD);
            verifyAll();
        }

        @Test(expected = EventsProcess.EventsProcessException.class)
        public void shouldRaiseAnExceptionIfTheAesPayloadContentIsNotInTheCorrectFormat() {

            expect(users.fetchUnencrypted(ALICE_PSEUDO))
                    .andReturn(UserFixture.user(ALICE_PSEUDO, ALICE_BOX_ADDRESS, ALICE_EXCHANGE_KEY));
            expect(aes.decrypt(AES_PAYLOAD, ALICE_EXCHANGE_KEY)).andReturn("{something not quite correct}");
            userEvents.store(BOB_BOX_ADDRESS, HMAC_HASH_1, PAYLOAD, ALICE_BOX_ADDRESS.toString(), RETURN_PAYLOAD);

            replayAll();
            eventsProcess.createNewUserEvent(AES_PAYLOAD, ALICE_PSEUDO, PAYLOAD, RETURN_PAYLOAD);
            verifyAll();
        }
    }

    public static class WhenDeletingAUserEvent extends Common {

        @Test
        public void shouldDeleteTheEvent() {
            expect(userEvents.fetchUnencrypted(ALICE_BOX_ADDRESS, EVENT_ID_1))
                    .andReturn(Optional.of(userEvent(ALICE_BOX_ADDRESS, EVENT_ID_1, HMAC_HASH_1)));
            userEvents.delete(ALICE_BOX_ADDRESS, EVENT_ID_1);

            deleteUserEvent(HMAC_HASH_1);
        }

        @Test
        public void shouldHandleRequestsToDeleteNonExistingEvents() {
            expect(userEvents.fetchUnencrypted(ALICE_BOX_ADDRESS, EVENT_ID_1)).andReturn(Optional.empty());

            deleteUserEvent(HMAC_HASH_1);
        }

        @Test
        public void shouldHandleDeletingUserEventsStoredWithoutHmacHashes() {
            expect(userEvents.fetchUnencrypted(ALICE_BOX_ADDRESS, EVENT_ID_1))
                    .andReturn(Optional.of(userEvent(ALICE_BOX_ADDRESS, EVENT_ID_1, null)));
            userEvents.delete(ALICE_BOX_ADDRESS, EVENT_ID_1);

            deleteUserEvent(null);
        }

        @Test
        public void shouldPreventAttemptsToDeleteUserEventsWhenNoHmacHashIsProvided() {
            expect(userEvents.fetchUnencrypted(ALICE_BOX_ADDRESS, EVENT_ID_1))
                    .andReturn(Optional.of(userEvent(ALICE_BOX_ADDRESS, EVENT_ID_1, HMAC_HASH_1)));

            deleteUserEvent(null);
        }

        @Test
        public void shouldPreventAttemptsToDeleteUserEventsWhenTheWrongHmacHashIsProvided() {
            expect(userEvents.fetchUnencrypted(ALICE_BOX_ADDRESS, EVENT_ID_1))
                    .andReturn(Optional.of(userEvent(ALICE_BOX_ADDRESS, EVENT_ID_1, HMAC_HASH_1)));

            deleteUserEvent(WRONG_HMAC_HASH);
        }

        private void deleteUserEvent(String hmacHash) {
            replayAll();
            eventsProcess.deleteUserEvent(ALICE_BOX_ADDRESS, EVENT_ID_1, hmacHash);
            verifyAll();
        }
    }

    public static class WhenFetchingUserEvents extends Common {

        @Test
        public void shouldFetchTheEventsFromTheSpecifiedBoxAddresses() {

            expect(userEvents.fetchUnencrypted(setOf(CAROL_BOX_ADDRESS)))
                    .andReturn(listOf(userEvent(CAROL_BOX_ADDRESS, EVENT_ID_1, HMAC_HASH_1)));

            replayAll();
            List<UserEvent> userEvents = eventsProcess.findUserEvents(mapOf(Entry.of(CAROL_BOX_ADDRESS, HMAC_HASH_1)));
            verifyAll();

            assertThat(userEvents)
                    .hasSize(1).first()
                    .hasFieldOrPropertyWithValue("boxAddress", CAROL_BOX_ADDRESS)
                    .hasFieldOrPropertyWithValue("eventId", EVENT_ID_1)
                    .hasFieldOrPropertyWithValue("hmacHash", HMAC_HASH_1);
        }

        @Test
        public void shouldFetchUserEventsThatDoNotHaveHmacHashes() {

            expect(userEvents.fetchUnencrypted(setOf(CAROL_BOX_ADDRESS)))
                    .andReturn(listOf(userEvent(CAROL_BOX_ADDRESS, EVENT_ID_1, null)));

            replayAll();
            List<UserEvent> userEvents = eventsProcess.findUserEvents(mapOf(Entry.of(CAROL_BOX_ADDRESS, null)));
            verifyAll();

            assertThat(userEvents)
                    .hasSize(1).first()
                    .hasFieldOrPropertyWithValue("boxAddress", CAROL_BOX_ADDRESS)
                    .hasFieldOrPropertyWithValue("eventId", EVENT_ID_1)
                    .hasFieldOrPropertyWithValue("hmacHash", null);
        }

        @Test
        public void shouldPreventFetchingUserEventsHavingHmacHashesNotMatchingTheProvidedHashes() {

            expect(userEvents.fetchUnencrypted(setOf(CAROL_BOX_ADDRESS, CHUCK_BOX_ADDRESS))).andReturn(listOf(
                    userEvent(CAROL_BOX_ADDRESS, EVENT_ID_1, HMAC_HASH_1),
                    userEvent(CHUCK_BOX_ADDRESS, EVENT_ID_2, HMAC_HASH_2)));

            replayAll();
            List<UserEvent> userEvents = eventsProcess.findUserEvents(mapOf(
                    Entry.of(CAROL_BOX_ADDRESS, HMAC_HASH_1),
                    Entry.of(CHUCK_BOX_ADDRESS, WRONG_HMAC_HASH)));
            verifyAll();

            assertThat(userEvents)
                    .hasSize(1).first()
                    .hasFieldOrPropertyWithValue("boxAddress", CAROL_BOX_ADDRESS)
                    .hasFieldOrPropertyWithValue("eventId", EVENT_ID_1)
                    .hasFieldOrPropertyWithValue("hmacHash", HMAC_HASH_1);
        }
    }

    private static UserEvent userEvent(UUID boxAddress, UUID eventId, String hmacHash) {
        return new UserEvent(boxAddress, eventId, hmacHash, PAYLOAD,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }
}
