package be.seeld.userevents;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class UserEventsConfigurationTest extends Integration {

    @Resource
    private UserEventsConfiguration.UserEventsProperties properties;

    @Test
    public void shouldLoadUserEventsProperties() {
        assertThat(properties).isNotNull();
        assertThat(properties.getValidUntilLifetime()).isGreaterThan(0);
    }
}