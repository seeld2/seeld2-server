package be.seeld.userevents;

import be.seeld.userevents.EventsWebApi.FetchEventsRequest;
import be.seeld.userevents.EventsWebApi.CreateEventRequest;
import be.seeld.userevents.EventsWebApi.UserEventsResponse;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.Unit;
import test.spring.PrincipalMock;

import java.util.Map;
import java.util.UUID;

import static be.seeld.common.lang.Collections.listOf;
import static be.seeld.common.lang.Maps.Entry.of;
import static be.seeld.common.lang.Maps.mapOf;
import static be.seeld.userevents.UserEventFixture.RETURN_BOX_ADDRESS;
import static be.seeld.userevents.UserEventFixture.VALID_UNTIL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.TestUsers.ALICE_BOX_ADDRESS;
import static test.TestUsers.ALICE_PSEUDO;
import static test.TestUsers.BOB_BOX_ADDRESS;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class EventsWebApiUTest extends Unit {

    private static final int[] AES_PAYLOAD = {1, 4, 7};
    private static final UUID EVENT_ID = UUID.fromString("cdbf5e3a-abef-47ec-a29e-ead9e60c9d8b");
    private static final String HMAC_HASH = "hmacHash";
    private static final String PAYLOAD = "payload";
    private static final String RETURN_PAYLOAD = "return payload";

    private EventsWebApi eventsWebApi;
    @Mock
    private EventsProcess eventsProcess;

    @Before
    public void setUp() {
        eventsWebApi = new EventsWebApi(eventsProcess);
    }

    @Test
    public void shouldDeleteAGivenUserEvent() {

        eventsProcess.deleteUserEvent(ALICE_BOX_ADDRESS, EVENT_ID, HMAC_HASH);

        replayAll();
        ResponseEntity<?> response = eventsWebApi
                .deleteEvent(EVENT_ID, new EventsWebApi.DeleteEventRequest(ALICE_BOX_ADDRESS, HMAC_HASH));
        verifyAll();

        assertThatResponseIs(response, HttpStatus.NO_CONTENT);
    }

    @Test
    public void shouldFetchUserEventsForTheSpecifiedBoxAddresses() {
        Map<UUID, String> hmacHashesByBoxAddress = mapOf(of(BOB_BOX_ADDRESS, HMAC_HASH));

        expect(eventsProcess.findUserEvents(hmacHashesByBoxAddress))
                .andReturn(listOf(userEvent(BOB_BOX_ADDRESS, HMAC_HASH)));

        replayAll();
        ResponseEntity<?> response = eventsWebApi.fetchEvents(new FetchEventsRequest(hmacHashesByBoxAddress));
        verifyAll();

        UserEventsResponse userEventsResponse = assertThatOkResponseIs(response, UserEventsResponse.class);
        assertThat(userEventsResponse.getEvents())
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("eventId", EVENT_ID)
                .hasFieldOrPropertyWithValue("payload", PAYLOAD);
    }

    @Test
    public void shouldPostAUserEvent() {

        eventsProcess.createNewUserEvent(AES_PAYLOAD, ALICE_PSEUDO, PAYLOAD, RETURN_PAYLOAD);

        CreateEventRequest createEventRequest = new CreateEventRequest(AES_PAYLOAD, PAYLOAD, RETURN_PAYLOAD);

        replayAll();
        ResponseEntity<?> response = eventsWebApi.postEvent(createEventRequest, PrincipalMock.of(ALICE_PSEUDO));
        verifyAll();

        assertThatResponseIs(response, HttpStatus.CREATED);
    }

    private static UserEvent userEvent(UUID boxAddress, String hmacHash) {
        return new UserEvent(boxAddress, EVENT_ID, hmacHash, PAYLOAD,
                RETURN_BOX_ADDRESS.toString(), RETURN_PAYLOAD, VALID_UNTIL);
    }
}
