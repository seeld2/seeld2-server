package be.seeld.userevents;

import org.assertj.core.api.Condition;
import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UserEventsITest extends Integration {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("d50465c0-e2f6-4997-a33a-b03bd4bb28c9");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("6be4ca98-0ab5-4a56-bf0d-f0d002f5060d");
    private static final UUID EVENT_ID_1 = UUID.fromString("dab97d43-401c-4453-a5e7-dd52743995e2");
    private static final UUID EVENT_ID_2 = UUID.fromString("f797ce3b-aa9c-4b23-8d4e-c394002e4009");
    private static final UUID EVENT_ID_3 = UUID.fromString("8d7701b1-69ff-4213-b486-8305d1f2394f");

    @Resource
    private UserEvents userEvents;

    @Test
    public void findUserEventsByBoxAddress() {
        userEventData.insert(encryptables.ofUnencrypted(UserEventFixture.newUserEvent(BOX_ADDRESS_1, "payload1")).getEncrypted());
        userEventData.insert(encryptables.ofUnencrypted(UserEventFixture.newUserEvent(BOX_ADDRESS_1, "payload2")).getEncrypted());
        userEventData.insert(encryptables.ofUnencrypted(UserEventFixture.newUserEvent(BOX_ADDRESS_2, "payload3")).getEncrypted());

        List<UserEvent> results = userEvents.fetchUnencryptedByBoxAddress(BOX_ADDRESS_1);

        assertThat(results)
                .hasSize(2)
                .are(new Condition<>(userEvent
                        -> userEvent.getBoxAddress().equals(BOX_ADDRESS_1), "the right box address"));
    }

    @Test
    public void removeUserEventWithAGivenIdForAGivenBoxAddress() {
        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_1, EVENT_ID_1, "payload1"));
        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_1, EVENT_ID_2, "payload2"));
        userEventData.insert(UserEventFixture.userEvent(BOX_ADDRESS_2, EVENT_ID_3, "payload3"));

        userEvents.removeWithBoxAddressAndEventId(BOX_ADDRESS_1, EVENT_ID_2);

        List<UserEvent> userEvents = userEventData.find(BOX_ADDRESS_1);

        assertThat(userEvents)
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("eventId", EVENT_ID_1)
                .hasFieldOrPropertyWithValue("payload", "payload1");
    }
}
