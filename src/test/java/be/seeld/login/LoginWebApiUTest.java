package be.seeld.login;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import be.seeld.SecurityConfiguration.AppSecurityProperties.JwtProperties;
import be.seeld.login.LoginProcess.Challenge;
import be.seeld.login.LoginProcess.Success;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.Unit;
import test.junit.RestAssertions;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static test.junit.RestAssertions.assertThatCreateResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class LoginWebApiUTest extends Unit {

    private static final String USERNAME = "username";

    private static final String A = "a";
    private static final String B = "2ca25f0db49a76ade054dfe7782e2501f15fc130d2267772131af12edcd03a2f19a36542d2453a96329dd0bbe9a9fa93c87da8958aa9cbc2964f340d43610041bdb45097cd493503b76e6b16927aab271af3160aa767b47430e9c2a5bd902e116b1b898d918c8daae5bbaea3685cc03d0b9ad7685d31fe148e8f0559e9f5cffe3b0b0a09143d6840a76a4c1431fc1030e08775ec5899cab15bb331114eba387ce36240e6343324a4f87dbd2d27acafdecfd80daa351418836136884c0be17d99cdfc6d81b45c71a212c462742cac51f9de1dc24a2c744a15a995153582be130220a490e3d445242f9949dbfd42dca111942ad3eb35021c8dac5d8a838d778378";
    private static final String JWT = "jwt";
    private static final String M1 = "m1";
    private static final String M2 = "m2";
    private static final String PAYLOAD = "payload";
    private static final int[] PROFILE_ENCRYPTED = new int[]{1, 2, 3, 4, 5, 6, 7};
    private static final String SALT = "ab6af1e0acd426dee6e28e63722f5cc27eac5cc5a74ef39c198962cf53918a1e";

    private LoginWebApi loginWebApi;
    @Mock
    private LoginProcess loginProcess;

    @Before
    public void setUp() {
        AppSecurityProperties appSecurityProperties = new AppSecurityProperties();
        JwtProperties jwtProperties = new JwtProperties();
        jwtProperties.setPrefix("Bearer");
        appSecurityProperties.setJwt(jwtProperties);

        loginWebApi = new LoginWebApi(loginProcess, appSecurityProperties);
    }

    @Test
    public void returnAChallenge() {

        expect(loginProcess.generateChallenge(USERNAME)).andReturn(new Challenge(B, SALT));

        replayAll();
        ResponseEntity response = loginWebApi.challenge("username");
        verifyAll();

        ChallengeResponse challengeResponse = RestAssertions.assertThatOkResponseIs(response, ChallengeResponse.class);
        assertThat(challengeResponse)
                .hasFieldOrPropertyWithValue("b", B)
                .hasFieldOrPropertyWithValue("salt", SALT);
    }

    @Test
    public void loginAUserSubmittingValidLoginCredentials() {
        LoginRequest loginRequest = new LoginRequest(USERNAME, A, M1);
        Success loginSuccess = new Success(PROFILE_ENCRYPTED, JWT, M2, PAYLOAD);

        expect(loginProcess.login(USERNAME, A, M1)).andReturn(Optional.of(loginSuccess));

        replayAll();
        ResponseEntity response = loginWebApi.login(loginRequest);
        verifyAll();

        LoginResponse loginResponse = assertThatCreateResponseIs(response, LoginResponse.class);
        assertThat(loginResponse)
                .hasFieldOrPropertyWithValue("profile", PROFILE_ENCRYPTED)
                .hasFieldOrPropertyWithValue("m2", M2)
                .hasFieldOrPropertyWithValue("payload", PAYLOAD);

        HttpHeaders headers = response.getHeaders();
        assertThat(headers.containsKey(HttpHeaders.AUTHORIZATION)).isTrue();
        assertThat(headers.getFirst(HttpHeaders.AUTHORIZATION))
                .isNotNull()
                .isNotEmpty()
                .isEqualTo("Bearer " + JWT);
    }

    @Test
    public void rejectAttemptToLoginWithInvalidCredentials() {
        LoginRequest loginRequest = new LoginRequest(USERNAME, A, M1);

        expect(loginProcess.login(USERNAME, A, M1)).andReturn(Optional.empty());

        replayAll();
        ResponseEntity response = loginWebApi.login(loginRequest);
        verifyAll();

        assertThatResponseIs(response, HttpStatus.UNAUTHORIZED);
    }
}