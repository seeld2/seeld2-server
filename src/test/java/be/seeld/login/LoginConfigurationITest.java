package be.seeld.login;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginConfigurationITest extends Integration {

    @Resource
    private LoginConfiguration.Srp6aProperties properties;

    @Test
    public void shouldLoadLoginProperties() {
        assertThat(properties).isNotNull();
        assertThat(properties.getG()).isNotEmpty();
        assertThat(properties.getH()).isNotEmpty();
        assertThat(properties.getK()).isNotEmpty();
        assertThat(properties.getN()).isNotEmpty();
        assertThat(properties.getSaltBytesLength()).isGreaterThan(0);
        assertThat(properties.getSessionCacheExpiration()).isGreaterThan(0L);
    }
}
