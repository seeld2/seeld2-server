package be.seeld.login;

import be.seeld.LoginPropertiesFixture;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.easymock.Capture;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.test.util.ReflectionTestUtils;
import test.Unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;

@RunWith(Enclosed.class)
public class LoginConfigurationUTest extends Unit { // TODO This test is not ideal... Maybe make it a ITest and test whether the caches work?

    private final LoginConfiguration loginConfiguration = new LoginConfiguration();
    @Mock
    private Ignite ignite;
    @Mock
    private IgniteCache<String, SRP6JavascriptServerSession> igniteCache;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(loginConfiguration, "loginProperties", LoginPropertiesFixture.loginProperties());
        ReflectionTestUtils.setField(loginConfiguration, "ignite", ignite);
    }

    @Test
    public void configureAnSrp6aServerSessionsCache() {

        Capture<CacheConfiguration<String, SRP6JavascriptServerSession>> configuration = newCapture();
        expect(ignite.getOrCreateCache(capture(configuration))).andReturn(igniteCache);

        replayAll();
        IgniteCache<String, SRP6JavascriptServerSession> returnedCache = loginConfiguration.srp6aServerSessionsCache();
        verifyAll();

        assertThat(returnedCache).isNotNull();
        assertThat(configuration.getValue())
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", LoginConfiguration.SRP6A_SERVER_SESSIONS_CACHE)
                .hasFieldOrPropertyWithValue("cacheMode", CacheMode.REPLICATED)
                .hasFieldOrPropertyWithValue("atomicityMode", CacheAtomicityMode.ATOMIC);
    }
}