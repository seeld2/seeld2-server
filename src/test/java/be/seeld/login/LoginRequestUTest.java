package be.seeld.login;

import be.seeld.common.lang.Strings;
import org.junit.Before;
import org.junit.Test;
import test.Unit;

public class LoginRequestUTest extends Unit {

    private LoginRequest loginRequest;

    @Before
    public void setUp() {
        loginRequest = new LoginRequest("username", "A", "M1");
        assertThatValidationViolationAmountIs(0, loginRequest);
    }

    @Test
    public void raiseAViolationIfUsernameLengthIsLessThan_4() {
        loginRequest.setUsername("123");
        assertThatValidationViolationAmountIs(1, loginRequest);
    }

    @Test
    public void raiseAViolationIfUsernameLengthIsMoreThan_64() {
        loginRequest.setUsername(Strings.ofLength(65, "username"));
        assertThatValidationViolationAmountIs(1, loginRequest);
    }

    @Test
    public void raiseAViolationIf_A_IsEmpty() {
        loginRequest.setA("");
        assertThatValidationViolationAmountIs(1, loginRequest);
    }

    @Test
    public void raiseAViolationIf_M1_IsEmpty() {
        loginRequest.setM1(null);
        assertThatValidationViolationAmountIs(1, loginRequest);
    }
}