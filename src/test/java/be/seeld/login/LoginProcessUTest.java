package be.seeld.login;

import be.seeld.LoginPropertiesFixture;
import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Salts;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.security.jwt.JsonWebTokens;
import be.seeld.users.User;
import be.seeld.users.UserFixture;
import be.seeld.users.Users;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavaClientSessionSHA256;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSessionSHA256;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6ClientCredentials;
import com.nimbusds.srp6.SRP6Exception;
import com.nimbusds.srp6.SRP6ServerSession;
import org.apache.ignite.IgniteCache;
import org.easymock.Capture;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.TestUsers;
import test.Unit;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static test.TestUsers.ALICE_PSEUDO;

@RunWith(Enclosed.class)
public class LoginProcessUTest {

    private static final LoginConfiguration.Srp6aProperties LOGIN_PROPERTIES = LoginPropertiesFixture.loginProperties();

    private static final TestUsers.TestUser user;
    static {
        TestUsers testUsers = new TestUsers();
        user = testUsers.get(ALICE_PSEUDO);
    }

    public static class WhenGeneratingAChallenge extends Unit {

        private final Salts salts = new Salts(secureRandom);

        private LoginProcess loginProcess;
        @Mock
        private AES aes;
        @Mock
        private JsonWebTokens jsonWebTokens;
        @Mock
        IgniteCache<String, SRP6JavascriptServerSession> srp6aServerSessionsCache;
        @Mock
        private Users users;

        @Before
        public void setUp() {
            loginProcess = new LoginProcess(aes, jsonWebTokens, salts, LOGIN_PROPERTIES, srp6aServerSessionsCache, users);
        }

        @Test
        public void generateAChallengeForAnExistingUser() {
            Encryptable<User> userEncryptable = Encryptable.of(
                    User.builder().credentials(user.getCredentials()).username(user.getUsername()).build(),
                    UserFixture.encryptedUser(user.getUsername()),
                    cloner);

            expect(users.fetch(user.getUsername())).andReturn(Optional.of(userEncryptable));
            Capture<SRP6JavascriptServerSession> serverSession = newCapture();
            srp6aServerSessionsCache.put(eq(user.getUsername()), capture(serverSession));

            replayAll();
            LoginProcess.Challenge challenge = loginProcess.generateChallenge(user.getUsername());
            verifyAll();

            assertThat(challenge).isNotNull().hasNoNullFieldsOrProperties();

            assertThat(serverSession.getValue())
                    .hasFieldOrPropertyWithValue("state", SRP6ServerSession.State.STEP_1.toString());
        }

        @Test
        public void generateAFakeChallengeForInexistentUsers() {

            expect(users.fetch(user.getUsername())).andReturn(Optional.empty());

            replayAll();
            LoginProcess.Challenge challenge = loginProcess.generateChallenge(user.getUsername());
            verifyAll();

            assertThat(challenge).isNotNull().hasNoNullFieldsOrProperties();
        }
    }

    public static class WhenLoggingIn extends Unit {

        private static final int[] PROFILE_AES_ENCRYPTED = new int[]{1, 2, 3};

        private final Salts salts = new Salts(secureRandom);

        private LoginProcess loginProcess;
        @Mock
        private AES aes;
        @Mock
        private JsonWebTokens jsonWebTokens;
        @Mock
        IgniteCache<String, SRP6JavascriptServerSession> srp6aServerSessionsCache;
        @Mock
        private Users users;

        private SRP6JavascriptServerSession serverSession;

        private String b;
        private String salt;

        @Before
        public void setUp() {
            loginProcess = new LoginProcess(aes, jsonWebTokens, salts, LOGIN_PROPERTIES, srp6aServerSessionsCache, users);
        }

        @Test
        public void returnALoginSuccessForValidCredentials() throws SRP6Exception, IOException {
            User systemUser = user.toUser();

            challenge(User.Credentials.fromJSON(user.getCredentials(), User.Credentials.class));

            SRP6ClientCredentials credentials = obtainClientCredentialsFromChallengeValues();
            String a = BigIntegerUtils.toHex(credentials.A);
            String m1 = BigIntegerUtils.toHex(credentials.M1);

            expect(srp6aServerSessionsCache.getAndRemove(user.getUsername())).andReturn(serverSession);
            expect(users.fetchUnencrypted(user.getUsername())).andReturn(systemUser);
            Capture<String> profileJson = newCapture();
            Capture<String> sessionKey = newCapture();
            expect(aes.encryptToUnsignedBytes(capture(profileJson), capture(sessionKey)))
                    .andReturn(PROFILE_AES_ENCRYPTED);
            expect(jsonWebTokens.generate(user.getUsername())).andReturn("jwt");

            Optional<LoginProcess.Success> loginSuccessOptional = invokeLogin(user.getUsername(), a, m1);

            assertThat(loginSuccessOptional).isPresent();
            assertThat(loginSuccessOptional.orElse(null))
                    .hasFieldOrPropertyWithValue("profile", PROFILE_AES_ENCRYPTED)
                    .hasFieldOrPropertyWithValue("jwt", "jwt")
                    .hasFieldOrProperty("m2")
                    .hasFieldOrPropertyWithValue("payload", user.getPayload());

            assertThat(profileJson.getValue()).isNotNull().isNotEmpty();
            LoginProfile loginProfile = LoginProfile.fromJSON(profileJson.getValue(), LoginProfile.class);
            assertThat(loginProfile)
                    .hasFieldOrPropertyWithValue("boxAddress", systemUser.getBoxAddress())
                    .hasFieldOrPropertyWithValue("credentials", systemUser.getCredentials())
                    .hasFieldOrPropertyWithValue("displayName", systemUser.getDisplayName())
                    .hasFieldOrPropertyWithValue("picture", systemUser.getPicture());

            assertThat(sessionKey.getValue()).hasSize(64);
        }

        @Test
        public void returnAnEmptyOptionalForInexistentUser() throws IOException, SRP6Exception {

            challenge(User.Credentials.fromJSON(user.getCredentials(), User.Credentials.class));

            SRP6ClientCredentials credentials = obtainClientCredentialsFromChallengeValues();
            String a = BigIntegerUtils.toHex(credentials.A);
            String m1 = BigIntegerUtils.toHex(credentials.M1);

            expect(srp6aServerSessionsCache.getAndRemove(user.getUsername())).andReturn(null);

            Optional<LoginProcess.Success> loginSuccessOptional = invokeLogin(user.getUsername(), a, m1);

            assertThat(loginSuccessOptional).isEmpty();
        }

        @Test
        public void returnAnEmptyOptionalWhenTheCredentialsCouldNotBeValidatedAtStep2() throws IOException, SRP6Exception {

            challenge(User.Credentials.fromJSON(user.getCredentials(), User.Credentials.class));

            SRP6ClientCredentials credentials = obtainClientCredentialsFromChallengeValues();
            String a = new StringBuilder(BigIntegerUtils.toHex(credentials.A)).reverse().toString();
            String m1 = BigIntegerUtils.toHex(credentials.M1);

            expect(srp6aServerSessionsCache.getAndRemove(user.getUsername())).andReturn(serverSession);

            Optional<LoginProcess.Success> loginSuccessOptional = invokeLogin(user.getUsername(), a, m1);

            assertThat(loginSuccessOptional).isEmpty();
        }

        private void challenge(User.Credentials userCredentials) {
            serverSession = new SRP6JavascriptServerSessionSHA256(LOGIN_PROPERTIES.getN(), LOGIN_PROPERTIES.getG());
            salt = userCredentials.getSrp().getSalt();
            String verifier = userCredentials.getSrp().getVerifier();
            b = serverSession.step1(user.getUsername(), salt, verifier);
        }

        private Optional<LoginProcess.Success> invokeLogin(String username, String a, String m1) {
            replayAll();
            Optional<LoginProcess.Success> loginSuccessOptional = loginProcess.login(username, a, m1);
            verifyAll();
            return loginSuccessOptional;
        }

        private SRP6ClientCredentials obtainClientCredentialsFromChallengeValues() throws SRP6Exception {
            SRP6JavaClientSession clientSession = new SRP6JavaClientSessionSHA256(
                    LOGIN_PROPERTIES.getN(),
                    LOGIN_PROPERTIES.getG());
            clientSession.step1(user.getUsername(), user.getPassphrase());
            return clientSession.step2(salt, b);
        }
    }
}