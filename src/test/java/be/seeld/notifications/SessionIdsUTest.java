package be.seeld.notifications;

import org.junit.Test;
import test.Unit;

import java.util.LinkedHashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class SessionIdsUTest extends Unit {

    private static final String SESSION_ID_1 = "sessionId1";
    private static final String SESSION_ID_2 = "sessionId2";
    private static final String SESSION_ID_3 = "sessionId3";
    private static final String SESSION_ID_4 = "sessionId4";

    @Test
    public void shouldBuildAnEmptyInstance() {
        SessionIds sessionIds = SessionIds.empty();
        assertThat(sessionIds).isNotNull();
        assertThat(sessionIds.asCsv()).isEqualTo("");
    }

    @Test
    public void shouldBuildAnInstanceFromACsv() {
        String sessionIdsCsv = SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3;

        SessionIds sessionIds = SessionIds.from(sessionIdsCsv);

        assertThat(sessionIds).isNotNull();
        assertThat(sessionIds.asCsv()).isEqualTo(sessionIdsCsv);
    }

    @Test
    public void shouldAddASessionIdInAnOrderedWay() {
        SessionIds sessionIds = SessionIds.empty();

        sessionIds.add(SESSION_ID_1);
        assertThat(sessionIds.asCsv()).isEqualTo(SESSION_ID_1);

        sessionIds.add(SESSION_ID_2);
        assertThat(sessionIds.asCsv()).isEqualTo(SESSION_ID_1 + "," + SESSION_ID_2);

        sessionIds.add(SESSION_ID_3);
        assertThat(sessionIds.asCsv()).isEqualTo(SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3);
    }

    @Test
    public void shouldKeepTheOrderOfSessionIdsWhenAddingASessionIdThatIsAlreadyStored() {
        SessionIds sessionIds = SessionIds.from(SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3);
        sessionIds.add(SESSION_ID_2);
        assertThat(sessionIds.asCsv()).isEqualTo(SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3);
    }

    @Test
    public void shouldReturnAnOrderedSetRepresentingTheSessionIds() {
        SessionIds sessionIds = SessionIds.from(SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3);
        LinkedHashSet<String> set = sessionIds.asSet();
        assertThat(set)
                .isNotNull()
                .containsExactly(SESSION_ID_1, SESSION_ID_2, SESSION_ID_3);
    }

    @Test
    public void shouldRemoveTheOldestSessionIdWhenMoreThanThreeSessionIdsAreStored() {
        SessionIds sessionIds = SessionIds.from(SESSION_ID_1 + "," + SESSION_ID_2 + "," + SESSION_ID_3);
        sessionIds.add(SESSION_ID_4);
        assertThat(sessionIds.asCsv()).isEqualTo(SESSION_ID_2 + "," + SESSION_ID_3 + "," + SESSION_ID_4);
    }
}