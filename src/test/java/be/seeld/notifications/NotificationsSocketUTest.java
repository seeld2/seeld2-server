package be.seeld.notifications;

import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import test.Unit;

import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static org.easymock.EasyMock.expect;

public class NotificationsSocketUTest extends Unit {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("c7333544-a5f1-486d-8eb9-d63881ceff27");
    private static final String SESSION_ID_1 = "af8cenz7";

    private NotificationsSocket notificationsSocket;
    @Mock
    private NotificationsDispatcher notificationsDispatcher;

    @Before
    public void setUp() {
        notificationsSocket = new NotificationsSocket(notificationsDispatcher);
    }

    @Test
    public void registerASetOfBoxAddresses() {

        StompHeaderAccessor headerAccessor = StompHeaderAccessor.create(StompCommand.MESSAGE);
        headerAccessor.setSessionId(SESSION_ID_1);

        expect(notificationsDispatcher.register(SESSION_ID_1, setOf(BOX_ADDRESS_1))).andReturn(SESSION_ID_1);

        replayAll();
        notificationsSocket.register(new BoxAddressesRequest(setOf(BOX_ADDRESS_1)), headerAccessor);
        verifyAll();
    }
}