package be.seeld.notifications;

import be.seeld.notifications.WebSocketMessageBrokerConfiguration.WebSocketProperties;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class WebSocketMessageBrokerConfigurationITest extends Integration {

    @Resource
    private WebSocketMessageBrokerConfiguration configuration;

    @Test
    public void shouldLoadAllWebSocketProperties() {
        WebSocketProperties webSocketProperties = (WebSocketProperties) ReflectionTestUtils
                .getField(configuration, "webSocketProperties");

        assertThat(webSocketProperties.getHeartbeat()).hasNoNullFieldsOrProperties();
        assertThat(webSocketProperties.getSecurity()).hasNoNullFieldsOrProperties();
    }
}