package be.seeld.notifications;

import be.seeld.common.lang.Maps;
import org.apache.ignite.IgniteCache;
import org.easymock.Capture;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import test.Unit;

import java.util.Map;
import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;

public class NotificationsDispatcherUTest extends Unit {

    private static final UUID BOX_ADDRESS_1 = UUID.fromString("c7333544-a5f1-486d-8eb9-d63881ceff27");
    private static final UUID BOX_ADDRESS_2 = UUID.fromString("154c8645-d1ea-4a66-8cb5-c2fef5681eea");

    private static final String SESSION_ID_1 = "af8cenz7";
    private static final String SESSION_ID_2 = "hd8z1jlx";

    private NotificationsDispatcher dispatcher;
    @Mock
    private IgniteCache<UUID, String> notificationsSessionIdByBoxAddressCache;
    @Mock
    private SimpMessagingTemplate messagingTemplate;

    @Before
    public void setUp() {
        dispatcher = new NotificationsDispatcher(notificationsSessionIdByBoxAddressCache, messagingTemplate);
    }

    @Test
    public void registerANewSessionIdsBoxAddresses() {

        expect(notificationsSessionIdByBoxAddressCache.get(BOX_ADDRESS_1)).andReturn(SESSION_ID_1);

        notificationsSessionIdByBoxAddressCache.put(BOX_ADDRESS_1, SESSION_ID_1);
        notificationsSessionIdByBoxAddressCache.put(BOX_ADDRESS_2, SESSION_ID_1);

        replayAll();
        String result = dispatcher.register(SESSION_ID_1, setOf(BOX_ADDRESS_1, BOX_ADDRESS_2));
        verifyAll();

        assertThat(result).contains(SESSION_ID_1);
    }

    @Test
    public void registerMultipleSessionIdsForOneBoxAddress() {

        expect(notificationsSessionIdByBoxAddressCache.get(BOX_ADDRESS_1)).andReturn(null);
        notificationsSessionIdByBoxAddressCache.put(BOX_ADDRESS_1, SESSION_ID_1);

        expect(notificationsSessionIdByBoxAddressCache.get(BOX_ADDRESS_1)).andReturn(SESSION_ID_1);
        notificationsSessionIdByBoxAddressCache.put(BOX_ADDRESS_1, SESSION_ID_1 + "," + SESSION_ID_2);

        replayAll();
        String result1 = dispatcher.register(SESSION_ID_1, setOf(BOX_ADDRESS_1));
        String result2 = dispatcher.register(SESSION_ID_2, setOf(BOX_ADDRESS_1));
        verifyAll();

        assertThat(result1).contains(SESSION_ID_1);
        assertThat(result2).contains(SESSION_ID_1, SESSION_ID_2);
    }

    @Test
    public void sendANotificationForAListOfBoxAddresses() {

        expect(notificationsSessionIdByBoxAddressCache.getAll(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2)))
                .andReturn(Maps.mapOf(
                        Maps.Entry.of(BOX_ADDRESS_1, SESSION_ID_1),
                        Maps.Entry.of(BOX_ADDRESS_2, SESSION_ID_2)));

        Capture<Map<String, Object>> sessionId1HeadersCapture = newCapture();
        Capture<Map<String, Object>> sessionId2HeadersCapture = newCapture();

        messagingTemplate.convertAndSendToUser(eq(SESSION_ID_1), eq("/notification"),
                eq(new Notification(Notification.Type.NEW_MSG)), capture(sessionId1HeadersCapture));
        messagingTemplate.convertAndSendToUser(eq(SESSION_ID_2), eq("/notification"),
                eq(new Notification(Notification.Type.NEW_MSG)), capture(sessionId2HeadersCapture));

        replayAll();
        dispatcher.notifyOfNewMessage(setOf(BOX_ADDRESS_1, BOX_ADDRESS_2));
        verifyAll();

        Map<String, Object> sessionId1Headers = sessionId1HeadersCapture.getValue();
        assertThat(sessionId1Headers.get("simpSessionId")).isInstanceOf(String.class);
        assertThat((String) sessionId1Headers.get("simpSessionId")).isEqualTo(SESSION_ID_1);
        Map<String, Object> sessionId2Headers = sessionId2HeadersCapture.getValue();
        assertThat(sessionId2Headers.get("simpSessionId")).isInstanceOf(String.class);
        assertThat((String) sessionId2Headers.get("simpSessionId")).isEqualTo(SESSION_ID_2);
    }

    @Test
    public void skipNotificationsAddressedToUnregisteredSessionIds() {

        expect(notificationsSessionIdByBoxAddressCache.getAll(setOf(BOX_ADDRESS_1)))
                .andReturn(Maps.mapOf(Maps.Entry.of(BOX_ADDRESS_1, null)));

        replayAll();
        dispatcher.notifyOfNewMessage(setOf(BOX_ADDRESS_1));
        verifyAll();
    }
}