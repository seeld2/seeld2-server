package be.seeld.registration;

import be.seeld.common.encryption.aes.AES;
import be.seeld.registration.RegistrationConfiguration.RegistrationProperties;
import be.seeld.users.User;
import be.seeld.users.UserFixture;
import be.seeld.users.Users;
import be.seeld.parameters.Parameters;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.util.Optional;

import static be.seeld.users.UserFixture.USERNAME;
import static be.seeld.users.UserFixture.newUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;

@RunWith(Enclosed.class)
public class RegistrationsProcessUTest {

    public static class WhenVerifyingUsernameAvailability extends Common {

        @Test
        public void returnThatAUsernameIsAvailableWhenNoSeeldWithThatUsernameExists() {
            expect(users.exists(USERNAME)).andReturn(false);
            assertThat(isAvailable(USERNAME)).isTrue();
        }

        @Test
        public void returnThatAUsernameIsNotAvailableWhenAUserWithThatUsernameExists() {
            expect(users.exists(USERNAME)).andReturn(true);
            assertThat(isAvailable(USERNAME)).isFalse();
        }

        @Test
        public void returnThatAUsernameIsNotAvailableWhenItIsShorterThan_4_Characters() {
            assertThat(isAvailable(TOO_SHORT_USERNAME)).isFalse();
        }

        @Test
        public void returnThatAUsernameIsNotAvailableWhenTheTrimmedVersionOfTheUsernameAlreadyExists() {
            expect(users.exists(USERNAME)).andReturn(true);
            assertThat(isAvailable(" " + USERNAME + " ")).isFalse();
        }

        @Test
        public void returnThatAUsernameIsNotAvailableWhenTheTrimmedVersionOfTheUsernameIsReserved() {
            assertThat(isAvailable("seeLD")).isFalse();
        }

        private boolean isAvailable(String username) {
            replayAll();
            boolean available = registrationsProcess.isAvailable(username);
            verifyAll();
            return available;
        }
    }

    public static class WhenVerifyingRegistrationAvailability extends Common {

        @Test
        public void indicateThatRegistrationIsAvailable() {

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            boolean availability = isRegistrationAvailable();

            assertThat(availability).isTrue();
        }

        @Test
        public void indicateThatRegistrationIsNotAvailable() {

            expect(users.count()).andReturn(1000L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            boolean availability = isRegistrationAvailable();

            assertThat(availability).isFalse();
        }

        private boolean isRegistrationAvailable() {
            replayAll();
            boolean availability = registrationsProcess.isRegistrationAvailable();
            verifyAll();
            return availability;
        }

    }

    public static class WhenRegisteringANewUser extends Common {

        @Test
        public void registerANewUserWhenRegistrationLimitHasNotBeenReached() {
            User user = newUser();

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            expect(users.exists(USERNAME)).andReturn(false);

            expect(aes.generateSecretKeyAsBase64()).andReturn(UserFixture.DEVICE_STORAGE_KEY);
            expect(aes.generateSecretKeyAsBase64()).andReturn(UserFixture.EXCHANGE_KEY);
            expect(users.store(user)).andReturn(user);

            User registeredUser = register(user);

            assertThat(registeredUser)
                    .isNotNull()
                    .hasNoNullFieldsOrPropertiesExcept("subscriptionValidUntil");
            assertThat(user.getDeviceStorageKey()).isNotEqualTo(user.getExchangeKey());
        }

        @Test
        public void registerANewUserWhenNoRegistrationLimitIsPresent() {
            User user = newUser();

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.empty());

            expect(users.exists(USERNAME)).andReturn(false);

            expect(aes.generateSecretKeyAsBase64()).andReturn(UserFixture.DEVICE_STORAGE_KEY);
            expect(aes.generateSecretKeyAsBase64()).andReturn(UserFixture.EXCHANGE_KEY);
            expect(users.store(user)).andReturn(user);

            User registeredUser = register(user);

            assertThat(registeredUser)
                    .isNotNull()
                    .hasNoNullFieldsOrPropertiesExcept("subscriptionValidUntil");
            assertThat(user.getDeviceStorageKey()).isNotEqualTo(user.getExchangeKey());
        }

        @Test(expected = RegistrationsProcess.RegistrationsProcessException.class)
        public void rejectRegistrationForUsernameShorterThan4Characters() {

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            register(newUser(TOO_SHORT_USERNAME));
        }

        @Test(expected = RegistrationsProcess.RegistrationsProcessException.class)
        public void rejectRegistrationForUsernameThatAlreadyExist() {

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            expect(users.exists(USERNAME)).andReturn(true);

            register(newUser());
        }

        @Test(expected = RegistrationsProcess.RegistrationsProcessException.class)
        public void rejectRegistrationForReservedUsername() {

            expect(users.count()).andReturn(123L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            register(newUser("seELd"));
        }

        @Test(expected = RegistrationsProcess.RegistrationsProcessException.class)
        public void rejectRegistrationWhenRegistrationLimitHasBeenReached() {

            expect(users.count()).andReturn(1000L);
            expect(parameters.getRegistrationLimit()).andReturn(Optional.of(1000L));

            register(newUser());
        }

        private User register(User user) {
            replayAll();
            User result = registrationsProcess.register(user);
            verifyAll();
            return result;
        }
    }

    private static class Common extends Unit{

        static final RegistrationProperties REGISTRATION_PROPERTIES = new RegistrationProperties();
        static final String TOO_SHORT_USERNAME = "ABC";

        static {
            REGISTRATION_PROPERTIES.setReservedUsernames(new String[]{"seeld"});
        }

        RegistrationsProcess registrationsProcess;
        @Mock
        AES aes;
        @Mock
        Parameters parameters;
        @Mock
        Users users;

        @Before
        public void setUp() {
            registrationsProcess = new RegistrationsProcess(aes, parameters, users, REGISTRATION_PROPERTIES);
        }
    }
}
