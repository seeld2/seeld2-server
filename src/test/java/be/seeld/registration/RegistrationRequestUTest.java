package be.seeld.registration;

import be.seeld.users.User;
import be.seeld.common.lang.Strings;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import test.Unit;

import java.io.IOException;

import static be.seeld.registration.RegistrationRequestFixture.DISPLAY_NAME;
import static be.seeld.registration.RegistrationRequestFixture.PAYLOAD;
import static be.seeld.registration.RegistrationRequestFixture.PICTURE;
import static be.seeld.registration.RegistrationRequestFixture.PSEUDO;
import static be.seeld.registration.RegistrationRequestFixture.registrationRequest;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class RegistrationRequestUTest {

    public static class AtValidation extends Unit {

        private RegistrationRequest registrationRequest;

        @Before
        public void setUp() {
            registrationRequest = registrationRequest();
            assertThatValidationViolationAmountIs(0, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfDisplayNameIsLargerThan64Characters() {
            registrationRequest.setDisplayName(Strings.ofLength(65, "DisplayName"));
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfKdfIsMissing() {
            registrationRequest.setKdf(null);
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfPayloadIsEmpty() {
            registrationRequest.setPayload("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSrpIsNull() {
            registrationRequest.setSrp(null);
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfUsernameIsEmpty() {
            registrationRequest.setUsername(null);
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfUsernameIsShorterThan4Characters() {
            registrationRequest.setUsername("123");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfUsernameIsLargerThan64Characters() {
            registrationRequest.setUsername(Strings.ofLength(65, "username"));
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfKdfSaltIsEmpty() {
            registrationRequest.getKdf().setSalt("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfKdfTypeIsEmpty() {
            registrationRequest.getKdf().setType("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSrpSaltIsEmpty() {
            registrationRequest.getSrp().setSalt("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSrpTypeIsEmpty() {
            registrationRequest.getSrp().setType("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSrpVerifierIsEmpty() {
            registrationRequest.getSrp().setVerifier("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSystemKeysPrivateKeyIsEmpty() {
            registrationRequest.getSystemKeys().setPrivateKeys("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSystemKeysPublicKeyIsEmpty() {
            registrationRequest.getSystemKeys().setPublicKeys("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void raiseAnIssueIfSystemKeysTypeIsEmpty() {
            registrationRequest.getSystemKeys().setType("");
            assertThatValidationViolationAmountIs(1, registrationRequest);
        }

        @Test
        public void mapARegistrationRequestToAUserObject() throws IOException {
            RegistrationRequest registrationRequest = registrationRequest();
            registrationRequest.getKdf().setType("scrypt");
            registrationRequest.getSrp().setType("srp6a");

            User user = registrationRequest.toUser();

            assertThat(user)
                    .hasFieldOrProperty("credentials")
                    .hasFieldOrPropertyWithValue("displayName", DISPLAY_NAME)
                    .hasFieldOrPropertyWithValue("payload", PAYLOAD)
                    .hasFieldOrPropertyWithValue("picture", PICTURE)
                    .hasFieldOrPropertyWithValue("username", PSEUDO);

            User.Credentials credentials = User.Credentials.fromJSON(user.getCredentials(), User.Credentials.class);

            assertThat(credentials.getKdf().getSalt()).isEqualTo(registrationRequest.getKdf().getSalt());
            assertThat(credentials.getSrp())
                    .hasFieldOrPropertyWithValue("salt", registrationRequest.getSrp().getSalt())
                    .hasFieldOrPropertyWithValue("verifier", registrationRequest.getSrp().getVerifier());
            assertThat(credentials.getSystemKeys())
                    .hasFieldOrPropertyWithValue("privateKeys", registrationRequest.getSystemKeys().getPrivateKeys())
                    .hasFieldOrPropertyWithValue("publicKeys", registrationRequest.getSystemKeys().getPublicKeys())
                    .hasFieldOrPropertyWithValue("type", registrationRequest.getSystemKeys().getType());
        }
    }
}