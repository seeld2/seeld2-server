package be.seeld.registration;

import be.seeld.users.User;
import be.seeld.users.UserFixture;
import org.easymock.Capture;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.Unit;

import static be.seeld.users.UserFixture.user;
import static be.seeld.registration.RegistrationRequestFixture.registrationRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.springframework.http.HttpStatus.CREATED;
import static test.junit.RestAssertions.assertThatOkResponseIs;
import static test.junit.RestAssertions.assertThatResponseIs;

public class RegistrationsWebApiUTest extends Unit {

    private RegistrationsWebApi registrationsWebApi;
    @Mock
    private RegistrationsProcess registrationsProcess;

    @Before
    public void setUp() {
        registrationsWebApi = new RegistrationsWebApi(registrationsProcess);
    }

    @Test
    public void returnTheConfirmationThatAUsernameIsAvailableForRegistration() {

        expect(registrationsProcess.isAvailable(UserFixture.USERNAME)).andReturn(true);

        replayAll();
        ResponseEntity<?> response = registrationsWebApi.isAvailable(UserFixture.USERNAME);
        verifyAll();

        assertThatResponseIs(response, HttpStatus.OK, AvailabilityResponse.class);
        AvailabilityResponse availabilityResponse = (AvailabilityResponse) response.getBody();
        assertThat(availabilityResponse).isNotNull();
        assertThat(availabilityResponse.isAvailable()).isTrue();
    }

    @Test
    public void indicateThatRegistrationOfANewUserIsAvailable() {

        expect(registrationsProcess.isRegistrationAvailable()).andReturn(true);

        replayAll();
        ResponseEntity<?> response = registrationsWebApi.isRegistrationAvailable();
        verifyAll();

        AvailabilityResponse availabilityResponse = assertThatOkResponseIs(response, AvailabilityResponse.class);
        assertThat(availabilityResponse.isAvailable()).isTrue();
    }

    @Test
    public void createANewUserWhenSubmittingARegistrationRequest() {
        RegistrationRequest registrationRequest = registrationRequest();

        Capture<User> userCaptured = newCapture();
        expect(registrationsProcess.register(capture(userCaptured))).andReturn(user());

        replayAll();
        ResponseEntity<?> response = registrationsWebApi.register(registrationRequest);
        verifyAll();

        assertThatResponseIs(response, CREATED);
        assertThat(userCaptured.getValue()).hasFieldOrPropertyWithValue("username", RegistrationRequestFixture.PSEUDO);
    }
}
