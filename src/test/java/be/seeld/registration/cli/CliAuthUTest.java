package be.seeld.registration.cli;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import test.Unit;

public class CliAuthUTest extends Unit {

    private static final int PASSWORD_ENCODER_STRENGTH = 12;
    private static final String PASSWORD_TO_ENCODE = "";
    private static final String USER = "";

    @Test
    public void generateEncryptedAndSaltedPasswordForCliAuthUser() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(PASSWORD_ENCODER_STRENGTH, secureRandom);
        String encodedPassword = passwordEncoder.encode(PASSWORD_TO_ENCODE);

        System.out.println("user: " + USER);
        System.out.println("password: " + encodedPassword);
    }
}
