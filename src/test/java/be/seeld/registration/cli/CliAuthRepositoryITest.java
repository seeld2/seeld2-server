package be.seeld.registration.cli;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class CliAuthRepositoryITest extends Integration {

    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";

    @Resource
    private CliAuthRepository cliAuthRepository;

    @Test
    public void findTheCliAuthForTheSpecifiedUsername() {

        CliAuth storedCliAuth = cliAuthData.insert(CliAuth.builder().username(USERNAME).password(PASSWORD).build());

        CliAuth cliAuth = cliAuthRepository.findOne(USERNAME);

        assertThat(cliAuth).isEqualToComparingFieldByField(storedCliAuth);
    }

    @Test
    public void saveACliAuth() {

        CliAuth savedCliAuth = cliAuthRepository.save(CliAuth.builder().username(USERNAME).password(PASSWORD).build());

        assertThat(cliAuthData.findAll())
                .hasSize(1)
                .first().isEqualToComparingFieldByField(savedCliAuth);
    }
}