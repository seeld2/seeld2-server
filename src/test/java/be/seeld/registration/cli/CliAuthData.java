package be.seeld.registration.cli;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

import java.util.List;

import static be.seeld.registration.cli.CliAuth.COL_PASSWORD;
import static be.seeld.registration.cli.CliAuth.COL_USERNAME;
import static be.seeld.registration.cli.CliAuth.TABLE;
import static be.seeld.registration.cli.CliAuth.columnsAsCsv;

@SuppressWarnings("SqlDialectInspection")
public class CliAuthData {

    private final JdbcTemplate jdbcTemplate;

    public CliAuthData(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<CliAuth> findAll() {
        return jdbcTemplate.query("select * from " + TABLE, cliAuthRowMapper());
    }

    public CliAuth insert(CliAuth cliAuth) {
        jdbcTemplate.update("insert into " + TABLE + " (_key," + columnsAsCsv() + ") values (?, ?, ?)",
                cliAuth.cacheKey(),
                cliAuth.getUsername(),
                cliAuth.getPassword());
        return cliAuth;
    }

    private RowMapper<CliAuth> cliAuthRowMapper() {
        return (rs, rowNum) -> CliAuth.builder()
                .username(rs.getString(COL_USERNAME))
                .password(rs.getString(COL_PASSWORD))
                .build();
    }
}
