package be.seeld.registration;

public class RegistrationRequestFixture {

    public static final String DISPLAY_NAME = "displayName";
    public static final String KDF_SALT = "kdfSalt";
    public static final String KDF_TYPE = "kdfType";
    public static final String PAYLOAD = "payload";
    public static final String PICTURE = "picture";
    public static final String SRP_SALT = "srpSalt";
    public static final String SRP_TYPE = "srpType";
    public static final String SRP_VERIFIER = "srpVerifier";
    public static final String SYSTEM_KEYS_PRIVATE_KEY = "systemKeysPrivateKey";
    public static final String SYSTEM_KEYS_PUBLIC_KEY = "systemKeysPublicKey";
    public static final String SYSTEM_KEYS_TYPE = "systemKeysType";
    public static final String PSEUDO = "username";

    public static RegistrationRequest registrationRequest() {
        return registrationRequest(PSEUDO);
    }

    public static RegistrationRequest registrationRequest(String username) {
        return RegistrationRequest.builder()
                .displayName(DISPLAY_NAME)
                .payload(PAYLOAD)
                .picture(PICTURE)
                .kdf(RegistrationRequest.Kdf.builder()
                        .salt(KDF_SALT)
                        .type(KDF_TYPE)
                        .build())
                .srp(RegistrationRequest.Srp.builder()
                        .salt(SRP_SALT)
                        .type(SRP_TYPE)
                        .verifier(SRP_VERIFIER)
                        .build())
                .systemKeys(RegistrationRequest.SystemKeys.builder()
                        .privateKeys(SYSTEM_KEYS_PRIVATE_KEY)
                        .publicKeys(SYSTEM_KEYS_PUBLIC_KEY)
                        .type(SYSTEM_KEYS_TYPE)
                        .build())
                .username(username)
                .build();
    }
}
