package be.seeld.registration;

import org.junit.Test;
import test.Integration;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

public class RegistrationConfigurationITest extends Integration {

    @Resource
    private RegistrationConfiguration.RegistrationProperties properties;

    @Test
    public void shouldLoadRegistrationProperties() {
        assertThat(properties).isNotNull();
        assertThat(properties.getReservedUsernames()).isNotEmpty();
    }
}