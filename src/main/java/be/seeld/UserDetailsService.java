package be.seeld;

import be.seeld.users.User;
import be.seeld.users.Users;
import be.seeld.common.encryption.Encryptable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;
import java.util.Optional;

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final Users users;

    public UserDetailsService(Users users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Encryptable<User>> user = users.fetch(username);

        if (!user.isPresent()) {
            throw new UsernameNotFoundException("username not found");
        }

        User seeldUser = user.get().getUnencrypted();

        return new org.springframework.security.core.userdetails.User(
                seeldUser.getUsername(),
                "",
                Collections.singleton(new SimpleGrantedAuthority("USER")));
    }
}
