package be.seeld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication(scanBasePackages = "be.seeld")
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    static {
        LOGGER.debug("Running Application class static initializer");
    }

    @Value("${config.name}")
    private String configName;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public String configName() {
        return configName;
    }

    @PostConstruct
    public void logConfigName() {
        LOGGER.info("Using Spring Boot application configuration: " + configName);
    }
}
