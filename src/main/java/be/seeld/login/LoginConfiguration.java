package be.seeld.login;

import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import lombok.Data;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.cache.expiry.Duration;
import javax.cache.expiry.ModifiedExpiryPolicy;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties({LoginConfiguration.Srp6aProperties.class})
public class LoginConfiguration {

    public static final String SRP6A_REGION = "Srp6aRegion";

    static final String SRP6A_SERVER_SESSIONS_CACHE = "Srp6aServerSessions";

    @Resource
    private Ignite ignite;
    @Resource
    private Srp6aProperties srp6aProperties;

    @Bean
    public IgniteCache<String, SRP6JavascriptServerSession> srp6aServerSessionsCache() {

        CacheConfiguration<String, SRP6JavascriptServerSession> configuration = new CacheConfiguration<String, SRP6JavascriptServerSession>()
                .setName(SRP6A_SERVER_SESSIONS_CACHE)
                .setDataRegionName(SRP6A_REGION)
                .setAtomicityMode(CacheAtomicityMode.ATOMIC)
                .setCacheMode(CacheMode.REPLICATED)
                .setExpiryPolicyFactory(ModifiedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS,
                        srp6aProperties.getSessionCacheExpiration())));

        return ignite.getOrCreateCache(configuration);
    }

    @ConfigurationProperties(prefix = "srp6a")
    @Data
    public static final class Srp6aProperties {
        private String g;
        private String h;
        private String k;
        private String n;
        private int saltBytesLength;
        private long sessionCacheExpiration;
    }
}
