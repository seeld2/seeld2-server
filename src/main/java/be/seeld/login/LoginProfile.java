package be.seeld.login;

import be.seeld.users.User;
import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class LoginProfile extends JSONifiable {

    private String boxAddress;
    private String credentials;
    private String deviceStorageKey;
    private String displayName;
    private String exchangeKey;
    private String picture;

    public static LoginProfile from(User user) {
        return new LoginProfile(
                user.getBoxAddress(),
                user.getCredentials(),
                user.getDeviceStorageKey(),
                user.getDisplayName(),
                user.getExchangeKey(),
                user.getPicture());
    }
}
