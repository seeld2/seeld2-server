package be.seeld.login;

import be.seeld.SecurityConfiguration.AppSecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

import static be.seeld.SecurityConfiguration.AppSecurityProperties.JwtProperties;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/login")
public class LoginWebApi {

    private final JwtProperties jwtProperties;
    private final LoginProcess loginProcess;

    @Autowired
    LoginWebApi(LoginProcess loginProcess, AppSecurityProperties appSecurityProperties) {
        this.jwtProperties = appSecurityProperties.getJwt();
        this.loginProcess = loginProcess;
    }

    @RequestMapping(value = "/challenge/{username}", method = GET, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> challenge(@PathVariable String username) {
        LoginProcess.Challenge challenge = loginProcess.generateChallenge(username);
        ChallengeResponse response = ChallengeResponse.builder().b(challenge.getB()).salt(challenge.getSalt()).build();
        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest) {

        Optional<LoginProcess.Success> loginSuccessOptional = loginProcess
                .login(loginRequest.getUsername(), loginRequest.getA(), loginRequest.getM1());

        if (loginSuccessOptional.isPresent()) {
            LoginProcess.Success loginSuccess = loginSuccessOptional.get();

            String authorization = jwtProperties.getPrefix() + " " + loginSuccess.getJwt();
            return ResponseEntity.status(CREATED)
                    .header(HttpHeaders.AUTHORIZATION, authorization)
                    .body(
                            LoginResponse.builder()
                                    .profile(loginSuccess.getProfile())
                                    .m2(loginSuccess.getM2())
                                    .payload(loginSuccess.getPayload())
                                    .build());

        } else {
            return ResponseEntity.status(UNAUTHORIZED).build();
        }
    }
}
