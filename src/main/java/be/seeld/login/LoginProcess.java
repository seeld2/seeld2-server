package be.seeld.login;

import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Salts;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.security.jwt.JsonWebTokens;
import be.seeld.login.LoginConfiguration.Srp6aProperties;
import be.seeld.users.User;
import be.seeld.users.Users;
import com.bitbucket.thinbus.srp6.js.HexHashedVerifierGenerator;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSessionSHA256;
import com.nimbusds.srp6.SRP6CryptoParams;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.ignite.IgniteCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;

@Service
class LoginProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginProcess.class);

    private final AES aesHex;
    private final JsonWebTokens jsonWebTokens;
    private final Salts salts;
    private final Srp6aProperties srp6aProperties;
    private final IgniteCache<String, SRP6JavascriptServerSession> srp6aServerSessionsCache;
    private final Users users;

    LoginProcess(@Qualifier("pwaAesHex") AES aesHex,
                 JsonWebTokens jsonWebTokens,
                 Salts salts,
                 Srp6aProperties srp6aProperties,
                 @Qualifier("srp6aServerSessionsCache") IgniteCache<String, SRP6JavascriptServerSession> srp6aServerSessionsCache,
                 Users users) {

        this.aesHex = aesHex;
        this.jsonWebTokens = jsonWebTokens;
        this.salts = salts;
        this.srp6aProperties = srp6aProperties;
        this.srp6aServerSessionsCache = srp6aServerSessionsCache;
        this.users = users;
    }

    Challenge generateChallenge(String username) {

        SRP6JavascriptServerSession serverSession = new SRP6JavascriptServerSessionSHA256(
                srp6aProperties.getN(),
                srp6aProperties.getG());

        Optional<Encryptable<User>> userEncryptable = users.fetch(username);

        return userEncryptable
                .map(encryptable -> generateChallengeForExistingUser(username, encryptable.getUnencrypted(), serverSession))
                .orElseGet(() -> generateChallengeForFakeUser(username, serverSession));
    }

    private Challenge generateChallengeForExistingUser(String usernameAsEntered,
                                                       User user,
                                                       SRP6JavascriptServerSession serverSession) {
        String salt;
        String verifier;
        try {
            User.Credentials credentials = User.Credentials.fromJSON(user.getCredentials(), User.Credentials.class);
            salt = credentials.getSrp().getSalt();
            verifier = credentials.getSrp().getVerifier();
        } catch (IOException e) {
            throw new LoginProcessException("Could not deserialize user's credentials", e);
        }

        // Here we use the stored username, which was used to generated the SRP
        String b = serverSession.step1(user.getUsername(), salt, verifier);

        // But here we use the username as entered in the client
        srp6aServerSessionsCache.put(usernameAsEntered, serverSession);

        return new Challenge(b, salt);
    }

    private Challenge generateChallengeForFakeUser(String username, SRP6JavascriptServerSession serverSession) {
        String fakeSalt = salts.generateRandomSalt(srp6aProperties.getH(), srp6aProperties.getSaltBytesLength());

        SRP6CryptoParams srp6CryptoParams = new SRP6CryptoParams(
                new BigInteger(srp6aProperties.getN()),
                new BigInteger(srp6aProperties.getG()),
                srp6aProperties.getH());
        HexHashedVerifierGenerator verifierGenerator = new HexHashedVerifierGenerator(srp6CryptoParams);
        String fakeVerifier = verifierGenerator.generateVerifier(fakeSalt, username, RandomStringUtils.random(16));

        String b = serverSession.step1(username, fakeSalt, fakeVerifier);

        return new Challenge(b, fakeSalt);
    }

    Optional<Success> login(String username, String a, String m1) {

        SRP6JavascriptServerSession serverSession = srp6aServerSessionsCache.getAndRemove(username);

        if (serverSession != null) {

            try {
                String m2 = serverSession.step2(a, m1);

                User user = users.fetchUnencrypted(username);

                LoginProfile loginProfile = LoginProfile.from(user);

                String secretKey = serverSession.getSessionKey(true);
                int[] profileEncrypted = aesHex.encryptToUnsignedBytes(loginProfile.asJSON(), secretKey);

                String jwt = jsonWebTokens.generate(username);

                return Optional.of(new Success(profileEncrypted, jwt, m2, user.getPayload()));

            } catch (Exception e) {
                LOGGER.warn("Could not validate credentials at server session step 2 for username: " + username);
            }
        }

        return Optional.empty();
    }

    public static class LoginProcessException extends RuntimeException {
        LoginProcessException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @Data
    static class Challenge {
        private final String b;
        private final String salt;
    }

    @Data
    static class Success {
        private final int[] profile;
        private final String jwt;
        private final String m2;
        private final String payload;
    }
}
