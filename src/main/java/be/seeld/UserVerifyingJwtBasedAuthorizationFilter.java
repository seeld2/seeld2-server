package be.seeld;

import be.seeld.users.Users;
import be.seeld.common.security.jwt.JwtBasedAuthorizationFilter;
import be.seeld.common.security.jwt.JsonWebTokens;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.AuthenticationManager;

/**
 * This extension of a JwtBasedAuthorizationFilter verifies, when renewing tokens, whether the principal is still a
 * valid user in the system.
 */
public class UserVerifyingJwtBasedAuthorizationFilter extends JwtBasedAuthorizationFilter {

    private final Users users;

    public UserVerifyingJwtBasedAuthorizationFilter(AuthenticationManager authenticationManager,
                                                    JsonWebTokens jsonWebTokens,
                                                    Config config,
                                                    Users users) {
        super(authenticationManager, jsonWebTokens, config);
        this.users = users;
    }

    @Override
    protected boolean canRefreshTokenBeIssued(Claims claims) {
        return users.exists(claims.getSubject());
    }
}
