package be.seeld;

import lombok.Data;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.binary.BinaryBasicIdMapper;
import org.apache.ignite.binary.BinaryBasicNameMapper;
import org.apache.ignite.configuration.BinaryConfiguration;
import org.apache.ignite.configuration.ClientConnectorConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.logger.slf4j.Slf4jLogger;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.apache.ignite.ssl.SslContextFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * Watch out! There's an <strong>IgniteCoreTestConfiguration</strong> alter-ego to this class, for tests!
 */
@Configuration
@EnableConfigurationProperties(IgniteCoreConfiguration.IgniteProperties.class)
public class IgniteCoreConfiguration {

    private static final int CLIENT_CONNECTOR_PORT = 10800;

    @Resource
    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * A custom version of the binary configuration which makes ID strings case-sensitive.
     * This is to work around SRP6JavascriptServerSession with "b" and "B" properties...
     */
    public static BinaryConfiguration binaryConfiguration() {
        return new BinaryConfiguration()
                .setIdMapper(new BinaryBasicIdMapper(false))
                .setNameMapper(new BinaryBasicNameMapper());
    }

    /**
     * This bean-creation method won't run for integration tests! For integration test see {@link test.IgniteCoreTestConfiguration}
     */
    @SuppressWarnings("JavadocReference")
    @Bean
    @Profile("!integration")
    public Ignite ignite() {

        IgniteConfiguration igniteConfiguration = new IgniteConfiguration()
                .setIgniteInstanceName("local-client")
                .setGridLogger(new Slf4jLogger())
                .setMetricsLogFrequency(0L)

                .setClientMode(true)

                .setBinaryConfiguration(binaryConfiguration())

                .setClientConnectorConfiguration(new ClientConnectorConfiguration().setPort(CLIENT_CONNECTOR_PORT))
                .setCommunicationSpi(new TcpCommunicationSpi()
                        .setLocalPort(igniteProperties.getCommunicationSpiLocalPort())
                        .setLocalPortRange(igniteProperties.getCommunicationSpiLocalPortRange()))
                .setDiscoverySpi(new TcpDiscoverySpi()
                        .setLocalPort(igniteProperties.getDiscoverySpiLocalPort())
                        .setLocalPortRange(igniteProperties.getDiscoverySpiLocalPortRange())
                        .setIpFinder(new TcpDiscoveryVmIpFinder()
                                .setAddresses(Arrays.asList(igniteProperties.getDiscoverySpiAddresses()))))

                .setSslContextFactory(sslContextFactory());

        return Ignition.start(igniteConfiguration);
    }

    private SslContextFactory sslContextFactory() {
        SslContextFactory factory = new SslContextFactory();
        factory.setKeyStoreFilePath(igniteProperties.getKeyStoreFilePath());
        factory.setKeyStorePassword(igniteProperties.getKeyStorePassword().toCharArray());
        factory.setTrustStoreFilePath(igniteProperties.getTrustStoreFilePath());
        factory.setTrustStorePassword(igniteProperties.getTrustStorePassword().toCharArray());
        factory.setProtocol(igniteProperties.getProtocol());
        return factory;
    }

    @ConfigurationProperties("ignite")
    @Data
    public static class IgniteProperties {
        private int backupsAmount;
        private int communicationSpiLocalPort;
        private int communicationSpiLocalPortRange;
        private String[] discoverySpiAddresses;
        private int discoverySpiLocalPort;
        private int discoverySpiLocalPortRange;
        private String keyStoreFilePath;
        private String keyStorePassword;
        private String protocol;
        private String trustStoreFilePath;
        private String trustStorePassword;
    }
}
