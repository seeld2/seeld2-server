package be.seeld.registration;

import be.seeld.IgniteCoreConfiguration;
import be.seeld.registration.cli.CliAuth;
import be.seeld.registration.cli.CliAuthRepository;
import be.seeld.parameters.Parameter;
import be.seeld.parameters.ParameterRepository;
import org.apache.ignite.Ignite;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import static be.seeld.parameters.ParameterRepository.REGISTRATION_LIMIT;

@Configuration
public class IgniteRegistrationRepositoriesConfiguration {

    private static final String CACHE_CLI_AUTH = "CliauthCache";
    private static final String CACHE_PARAMETER = "ParameterCache";

    private static final String SQL_SCHEMA = "PUBLIC";

    @Resource
    private Ignite ignite;
    @Resource
    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * This method is where updates on the repository's caches can be defined.<br/>
     * The typical example would be the deletion of an old, unused table: this cannot be done by SQL because those
     * tables have been defined with Java, so they need to be destroyed with Java.
     */
    @PostConstruct
    void postConstruct() {

        // Ensure presence of CLIAUTH passwords
        // TODO CliAuth passwords should later be stored in the authentication server!
        cliAuthRepository().save(CliAuth.builder().username("diego").password("$2a$12$T5lwQ5KWS0t0blqW/lIXdOOURGy7bwwrFgDaW4SA.7/1JEM9Q123a").build());

        // Ensure the presence of a registration limit parameter in PARAMETER
        Parameter registrationLimit = parameterRepository().findOne(REGISTRATION_LIMIT);
        if (registrationLimit == null) {
            parameterRepository().save(Parameter.builder().key(REGISTRATION_LIMIT).value("1000").build());
        }
    }

    @Bean
    public CliAuthRepository cliAuthRepository() {
        CacheConfiguration<String, CliAuth> configuration = repositoryCacheConfiguration(
                CACHE_CLI_AUTH, CliAuth.class);
        return new CliAuthRepository(ignite.getOrCreateCache(configuration));
    }

    @Bean
    public ParameterRepository parameterRepository() {
        CacheConfiguration<String, Parameter> configuration = repositoryCacheConfiguration(
                CACHE_PARAMETER, Parameter.class);
        return new ParameterRepository(ignite.getOrCreateCache(configuration));
    }

    private <T> CacheConfiguration<String, T> repositoryCacheConfiguration(String cacheName, Class<T> type) {
        return new CacheConfiguration<String, T>()
                .setSqlSchema(SQL_SCHEMA)
                .setName(cacheName)
                .setCacheMode(CacheMode.PARTITIONED)
                .setIndexedTypes(String.class, type)
                .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
                .setBackups(igniteProperties.getBackupsAmount())
                .setAffinity(new RendezvousAffinityFunction(true));
    }
}
