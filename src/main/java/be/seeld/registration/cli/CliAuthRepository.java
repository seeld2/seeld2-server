package be.seeld.registration.cli;

import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;
import java.util.stream.Collectors;

import static be.seeld.registration.cli.CliAuth.TABLE;
import static be.seeld.registration.cli.CliAuth.columnsAsCsv;

public class CliAuthRepository {

    private final IgniteCache<String, CliAuth> cliAuthCache;

    public CliAuthRepository(IgniteCache<String, CliAuth> cliAuthCache) {
        this.cliAuthCache = cliAuthCache;
    }

    public CliAuth findOne(String username) {

        SqlFieldsQuery query = new SqlFieldsQuery("SELECT " + columnsAsCsv() + " FROM " + TABLE +
                " WHERE " + CliAuth.COL_USERNAME + " = ?")
                .setArgs(username);

        try (FieldsQueryCursor<List<?>> cursor = cliAuthCache.query(query)) {
            return DataAccessUtils.singleResult(map(cursor.getAll()));
        }
    }

    public CliAuth save(CliAuth cliAuth) {

        SqlFieldsQuery query = new SqlFieldsQuery("MERGE INTO " + TABLE +
                " (_key, " + columnsAsCsv() + ") VALUES (?, ?, ?)")
                .setArgs(cliAuth.cacheKey(), cliAuth.getUsername(), cliAuth.getPassword());

        //noinspection unused
        try (FieldsQueryCursor<List<?>> cursor = cliAuthCache.query(query)) {
            return cliAuth;
        }
    }

    private List<CliAuth> map(List<List<?>> rows) {
        return rows.stream()
                .map(row -> CliAuth.builder()
                        .username((String) row.get(0))
                        .password((String) row.get(1))
                        .build())
                .collect(Collectors.toList());
    }
}
