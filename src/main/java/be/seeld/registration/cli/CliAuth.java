package be.seeld.registration.cli;

import be.seeld.common.ignite.Persistable;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

@Builder
@Data
public class CliAuth implements Persistable {

    public static final String TABLE = "CLIAUTH";

    public static final String COL_PASSWORD = "PASSWORD";
    public static final String COL_USERNAME = "USERNAME";

    public static String columnsAsCsv() {
        return String.join(",", COL_USERNAME, COL_PASSWORD);
    }

    @QuerySqlField(name = COL_USERNAME, index = true, notNull = true)
    private String username;

    @QuerySqlField(name = COL_PASSWORD, notNull = true)
    private String password;

    @Override
    public String cacheKey() {
        return username;
    }
}
