package be.seeld.registration;

import be.seeld.users.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequest {

    @Size(max = 64)
    private String displayName;

    @NotNull
    @Valid
    private Kdf kdf;

    @NotEmpty
    private String payload;

    private String picture; // TODO establish a maximum size for this field

    @NotNull
    @Valid
    private Srp srp;

    @Valid
    private SystemKeys systemKeys;

    @NotEmpty
    @Size(max = 64, min = 4)
    private String username;

    /**
     * Maps this {@link RegistrationRequest} instance to a {@link User}
     */
    User toUser() {

        User.Credentials.CredentialsBuilder credentialsBuilder = User.Credentials.builder();

        if ("scrypt".equals(getKdf().getType())) {
            credentialsBuilder.kdf(User.Credentials.Kdf.builder()
                    .type("scrypt")
                    .salt(getKdf().getSalt())
                    .build());
        }

        if ("srp6a".equals(getSrp().getType())) {
            credentialsBuilder.srp(User.Credentials.Srp.builder()
                    .type("srp6a")
                    .salt(getSrp().getSalt())
                    .verifier(getSrp().getVerifier())
                    .build());
        }

        credentialsBuilder.systemKeys(User.Credentials.SystemKeys.builder()
                .privateKeys(getSystemKeys().getPrivateKeys())
                .publicKeys(getSystemKeys().getPublicKeys())
                .type(getSystemKeys().getType())
                .build());

        try {
            return User.builder()
                    .username(username)
                    .credentials(credentialsBuilder.build().asJSON())
                    .displayName(displayName)
                    .payload(payload)
                    .picture(picture)
                    .build();
        } catch (JsonProcessingException e) {
            throw new RegistrationRequestException("Could not map this object to instance of User", e);
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Kdf {

        @NotEmpty
        private String salt;

        @NotEmpty
        private String type;
    }

    public static class RegistrationRequestException extends RuntimeException {
        RegistrationRequestException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Srp {

        @NotEmpty
        private String salt;

        @NotEmpty
        private String type;

        @NotEmpty
        private String verifier;
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SystemKeys {

        @NotEmpty
        private String privateKeys;

        @NotEmpty
        private String publicKeys;

        @NotEmpty
        private String type;
    }
}
