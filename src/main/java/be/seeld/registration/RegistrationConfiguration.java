package be.seeld.registration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({RegistrationConfiguration.RegistrationProperties.class})
public class RegistrationConfiguration {

    @ConfigurationProperties(prefix = "registration")
    @Data
    public static final class RegistrationProperties {
        private String[] reservedUsernames;
    }
}
