package be.seeld.registration;

import be.seeld.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/registrations")
class RegistrationsWebApi {

    private final RegistrationsProcess registrationsProcess;

    @Autowired
    RegistrationsWebApi(RegistrationsProcess registrationsProcess) {
        this.registrationsProcess = registrationsProcess;
    }

    @RequestMapping(value = "/{username}/availability", method = GET, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> isAvailable(@PathVariable String username) {
        boolean available = registrationsProcess.isAvailable(username);
        return ResponseEntity.ok(AvailabilityResponse.builder().available(available).build());
    }

    @RequestMapping(value = "/availability", method = GET, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> isRegistrationAvailable() {
        boolean available = registrationsProcess.isRegistrationAvailable();
        return ResponseEntity.ok(AvailabilityResponse.builder().available(available).build());
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> register(@Valid @RequestBody RegistrationRequest registrationRequest) {
        User user = registrationRequest.toUser();
        registrationsProcess.register(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
