package be.seeld.registration;

import be.seeld.common.encryption.aes.AES;
import be.seeld.registration.RegistrationConfiguration.RegistrationProperties;
import be.seeld.parameters.Parameters;
import be.seeld.users.Subscription;
import be.seeld.users.User;
import be.seeld.users.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.UUID;

@Service
class RegistrationsProcess {

    private final AES aes;
    private final Parameters parameters;
    private final Users users;

    private final RegistrationProperties registrationProperties;

    @Autowired
    public RegistrationsProcess(@Qualifier("pwaAesBase64") AES aes,
                                Parameters parameters,
                                Users users,
                                RegistrationProperties registrationProperties) {
        this.aes = aes;
        this.parameters = parameters;
        this.users = users;
        this.registrationProperties = registrationProperties;
    }

    boolean isAvailable(String username) {
        String trimmedUsername = username.trim();

        return trimmedUsername.length() >= 4
                && !isReservedUsername(trimmedUsername)
                && !users.exists(trimmedUsername);
    }

    private boolean isReservedUsername(String username) {
        return Arrays.asList(registrationProperties.getReservedUsernames()).contains(username.toLowerCase());
    }

    public boolean isRegistrationAvailable() {
        long registeredUsersAmount = users.count();
        return parameters.getRegistrationLimit()
                .map(rl -> registeredUsersAmount < rl)
                .orElse(true);
    }

    User register(User user) {

        if (!isRegistrationAvailable()) {
            throw new RegistrationsProcessException("Registration limit has been reached");
        }

        if (!isAvailable(user.getUsername())) {
            throw new RegistrationsProcessException("Seeld user with username '" + user.getUsername()
                    + "' is not available");
        }

        user.boxAddress(UUID.randomUUID());
        user.setDeviceStorageKey(aes.generateSecretKeyAsBase64());
        user.setExchangeKey(aes.generateSecretKeyAsBase64());
        user.subscription(Subscription.FREE);

        return users.store(user);
    }

    static class RegistrationsProcessException extends RuntimeException {
        RegistrationsProcessException(String message) {
            super(message);
        }
    }
}
