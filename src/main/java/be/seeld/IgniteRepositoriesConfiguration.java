package be.seeld;

//@Configuration
public class IgniteRepositoriesConfiguration {

//    private static final String SQL_SCHEMA = "PUBLIC";

//    private static final String FILE_CACHE = "FileCache";
//    private static final String MESSAGE_CACHE = "MessageCache";
//    private static final String MESSAGE_LIFE_CACHE = "MessageLifeCache";
//    private static final String USER_CACHE = "UserCache";
//    private static final String USER_EVENT_CACHE = "UserEventCache";

//    @Resource
//    private Ignite ignite;
//    @Resource
//    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * This method is where updates on the repository's caches can be defined.<br/>
     * The typical example would be the deletion of an old, unused table: this cannot be done by SQL because those
     * tables have been defined with Java, so they need to be destroyed with Java.
     */
//    @PostConstruct
//    void postConstruct() {
        // Updates on the repository caches' tables (such as the deletion of an old table)
//        ignite.destroyCache(FILE_CACHE);

        // Adds User.SUBSCRIPTION_VALID_UNTIL column if missing
//        if (ignite.cacheNames().contains(USER_CACHE)) {
//            SqlFieldsQuery query = new SqlFieldsQuery("ALTER TABLE " + User.TABLE +
//                    " ADD COLUMN IF NOT EXISTS " + User.COL_SUBSCRIPTION_VALID_UNTIL + " varchar");
//            noinspection EmptyTryBlock
//            try (FieldsQueryCursor<List<?>> ignored = ignite.getOrCreateCache(USER_CACHE).query(query)) {}
//        }
//    }

//    @Bean
//    public MessageLifeRepository messageLifeRepository() {
//        CacheConfiguration<String, MessageLife> configuration = repositoryCacheConfiguration(
//                MESSAGE_LIFE_CACHE, MessageLife.class);
//        return new MessageLifeRepository(ignite.getOrCreateCache(configuration));
//    }
//
//    @Bean
//    public MessageRepository messageRepository() {
//        CacheConfiguration<String, Message> configuration = repositoryCacheConfiguration(
//                MESSAGE_CACHE, Message.class);
//        return new MessageRepository(ignite.getOrCreateCache(configuration));
//    }

//    @Bean
//    public UserRepository userRepository() {
//        CacheConfiguration<String, User> configuration = repositoryCacheConfiguration(
//                USER_CACHE, User.class);
//        return new UserRepository(ignite.getOrCreateCache(configuration));
//    }

//    @Bean
//    public UserEventRepository userEventRepository() {
//        CacheConfiguration<String, UserEvent> configuration = repositoryCacheConfiguration(
//                USER_EVENT_CACHE, UserEvent.class);
//        return new UserEventRepository(ignite.getOrCreateCache(configuration));
//    }

//    private <T> CacheConfiguration<String, T> repositoryCacheConfiguration(String cacheName, Class<T> type) {
//        return new CacheConfiguration<String, T>()
//                .setSqlSchema(SQL_SCHEMA)
//                .setName(cacheName)
//                .setCacheMode(CacheMode.PARTITIONED)
//                .setIndexedTypes(String.class, type)
//                .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
//                .setBackups(igniteProperties.getBackupsAmount())
//                .setAffinity(new RendezvousAffinityFunction(true));
//    }
}
