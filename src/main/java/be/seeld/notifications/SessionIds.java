package be.seeld.notifications;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class SessionIds {

    private static final int MAX_AMOUNT = 3;
    private static final String SEPARATOR = ",";

    public static SessionIds empty() {
        return new SessionIds(new LinkedHashSet<>());
    }

    public static SessionIds from(String csv) {
        LinkedHashSet<String> sessionIds = new LinkedHashSet<>();
        if (csv != null && !csv.isEmpty()) {
            sessionIds = Arrays.stream(csv.split(SEPARATOR)).collect(Collectors.toCollection(LinkedHashSet::new));
        }
        return new SessionIds(sessionIds);
    }

    private LinkedHashSet<String> sessionIds;

    private SessionIds(LinkedHashSet<String> sessionIds) {
        this.sessionIds = sessionIds;
    }

    public void add(String sessionId) {
        sessionIds.add(sessionId);
        if (sessionIds.size() > MAX_AMOUNT) {
            sessionIds = sessionIds.stream().skip(1).collect(Collectors.toCollection(LinkedHashSet::new));
        }
    }

    public String asCsv() {
        return String.join(SEPARATOR, sessionIds);
    }

    public LinkedHashSet<String> asSet() {
        return new LinkedHashSet<>(sessionIds);
    }
}
