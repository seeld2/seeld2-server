package be.seeld.notifications;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.UUID;

@Configuration
public class NotificationsConfiguration {

    public static final String NOTIFICATIONS_REGION = "NotificationsRegion";
    static final String NOTIFICATIONS_SESSION_ID_BY_BOX_ADDRESS_CACHE = "NotificationsSessionIdByBoxAddressCache";

    @Resource
    private Ignite ignite;

    @Bean
    public IgniteCache<UUID, String> notificationsSessionIdByBoxAddressCache() {

        CacheConfiguration<UUID, String> configuration = new CacheConfiguration<UUID, String>()
                .setName(NOTIFICATIONS_SESSION_ID_BY_BOX_ADDRESS_CACHE)
                .setDataRegionName(NOTIFICATIONS_REGION)
                .setAtomicityMode(CacheAtomicityMode.ATOMIC)
                .setCacheMode(CacheMode.REPLICATED);

        return ignite.getOrCreateCache(configuration);
    }
}
