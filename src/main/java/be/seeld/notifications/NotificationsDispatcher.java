package be.seeld.notifications;

import org.apache.ignite.IgniteCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import static be.seeld.common.lang.Collections.setOf;

@Service
public class NotificationsDispatcher {

    private final IgniteCache<UUID, String> notificationsSessionIdByBoxAddressCache;
    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    public NotificationsDispatcher(IgniteCache<UUID, String> notificationsSessionIdByBoxAddressCache,
                                   SimpMessagingTemplate messagingTemplate) {
        this.notificationsSessionIdByBoxAddressCache = notificationsSessionIdByBoxAddressCache;
        this.messagingTemplate = messagingTemplate;
    }

    public String register(String sessionId, Set<UUID> boxAddresses) {

        SessionIds sessionIds = boxAddresses.stream()
                .map(notificationsSessionIdByBoxAddressCache::get)
                .filter(Objects::nonNull)
                .findFirst()
                .map(SessionIds::from)
                .orElse(SessionIds.empty());

        sessionIds.add(sessionId);

        final String sessionIdsAsCsv = sessionIds.asCsv();
        boxAddresses.forEach(boxAddress -> notificationsSessionIdByBoxAddressCache.put(boxAddress, sessionIdsAsCsv));

        return sessionIdsAsCsv;
    }

    public void notifyOfNewMessage(UUID boxAddress) {
        notifyOfNewMessage(setOf(boxAddress));
    }

    public void notifyOfNewMessage(Set<UUID> boxAddresses) {

        Map<UUID, String> sessionIdsByBoxAddress = notificationsSessionIdByBoxAddressCache.getAll(boxAddresses);

        Notification notification = new Notification(Notification.Type.NEW_MSG);

        sessionIdsByBoxAddress.values().forEach(sessionIdsCsv -> {
            SessionIds sessionIds = SessionIds.from(sessionIdsCsv);
            sessionIds.asSet().forEach(sessionId -> {
                SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
                headerAccessor.setSessionId(sessionId);
                headerAccessor.setLeaveMutable(true);

                messagingTemplate.convertAndSendToUser(
                        sessionId,
                        "/notification",
                        notification,
                        headerAccessor.getMessageHeaders());
            });
        });
    }
}
