package be.seeld.notifications;

import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Set;
import java.util.UUID;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BoxAddressesRequest extends JSONifiable {

    @NotEmpty
    private Set<UUID> boxAddresses;
}
