package be.seeld.notifications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
@MessageMapping("/notifications")
public class NotificationsSocket {

    private final NotificationsDispatcher notificationsDispatcher;

    @Autowired
    public NotificationsSocket(NotificationsDispatcher notificationsDispatcher) {
        this.notificationsDispatcher = notificationsDispatcher;
    }

    // TODO This should also register the HMACs of the "read from" box addresses to listen to. When notifying of new message, the registered HMAC should be compared with the HMAC of the message whose arrival must be notified
    @MessageMapping("/register")
    void register(BoxAddressesRequest boxAddressesRequest, StompHeaderAccessor headerAccessor) {
        notificationsDispatcher.register(
                headerAccessor.getSessionId(),
                boxAddressesRequest.getBoxAddresses());
    }
}
