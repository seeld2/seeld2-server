package be.seeld.notifications;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(WebSocketMessageBrokerConfiguration.WebSocketProperties.class)
@EnableScheduling
@EnableWebSocketMessageBroker
public class WebSocketMessageBrokerConfiguration implements WebSocketMessageBrokerConfigurer {

    @Resource
    private WebSocketProperties webSocketProperties;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/notification")
                .setTaskScheduler(webSocketMessageBrokerTaskScheduler())
                .setHeartbeatValue(new long[]{
                        webSocketProperties.heartbeat.incoming,
                        webSocketProperties.heartbeat.outgoing});
        registry.setApplicationDestinationPrefixes("/wsapi");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/wsapi")
                .addInterceptors(new WebSocketHandshakeTracer())
                .setAllowedOrigins(webSocketProperties.getSecurity().getAllowedOrigins())
                .withSockJS();
    }

    @Bean
    TaskScheduler webSocketMessageBrokerTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    @ConfigurationProperties(prefix = "websocket")
    @Data
    public static class WebSocketProperties {
        private HeartbeatProperties heartbeat;
        private SecurityProperties security;

        @Data
        public static class HeartbeatProperties {
            private int incoming;
            private int outgoing;
        }

        @Data
        public static class SecurityProperties {
            private String[] allowedOrigins;
        }
    }
}
