package be.seeld.userevents;

import be.seeld.common.lang.Collections;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static be.seeld.common.lang.Strings.repeatAndJoin;
import static be.seeld.userevents.UserEvent.COL_BOX_ADDRESS;
import static be.seeld.userevents.UserEvent.COL_EVENT_ID;
import static be.seeld.userevents.UserEvent.TABLE;

public class UserEventRepository {

    private final IgniteCache<String, UserEvent> userEventCache;

    public UserEventRepository(IgniteCache<String, UserEvent> userEventCache) {
        this.userEventCache = userEventCache;
    }

    void delete(UUID boxAddress, UUID eventId) {
        //noinspection EmptyTryBlock
        try (
                FieldsQueryCursor<List<?>> ignored = userEventCache.query(new SqlFieldsQuery("" +
                        "delete from " + TABLE +
                        " where " + UserEvent.COL_BOX_ADDRESS + " = ?" +
                        " and " + UserEvent.COL_EVENT_ID + " = ?")
                        .setArgs(boxAddress, eventId))
        ) {
        }
    }

    void deleteAllForBoxAddresses(Set<UUID> boxAddresses) {

        List<Set<UUID>> subsets = Collections.partitionOf(boxAddresses, 20);

        for (Set<UUID> set : subsets) {

            SqlFieldsQuery query = new SqlFieldsQuery("" +
                    "delete from " + TABLE +
                    " where " + UserEvent.COL_BOX_ADDRESS + " IN (" + repeatAndJoin("?", set.size(), ",") + ")");
            ArrayList<Object> argsList = new ArrayList<>(set);
            query.setArgs(argsList.toArray());
            //noinspection EmptyTryBlock
            try (
                    FieldsQueryCursor<List<?>> ignored = userEventCache.query(query)
            ) {
            }
        }
    }

    UserEvent findOne(UUID boxAddress, UUID eventId) {
        SqlFieldsQuery query = new SqlFieldsQuery("SELECT" +
                " _key," + UserEvent.columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_BOX_ADDRESS + " = ? AND " + COL_EVENT_ID + " = ?")
                .setArgs(boxAddress, eventId);
        try (FieldsQueryCursor<List<?>> cursor = userEventCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            return all.size() == 0 ? null : UserEvent.fromRowObjects(all.get(0));
        }
    }

    List<UserEvent> findByPkBoxAddress(UUID boxAddress) {
        SqlFieldsQuery query = new SqlFieldsQuery("SELECT" +
                " _key," + UserEvent.columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_BOX_ADDRESS + " = ?")
                .setArgs(boxAddress);
        try (FieldsQueryCursor<List<?>> cursor = userEventCache.query(query)) {
            return StreamSupport.stream(cursor.spliterator(), false)
                    .map(UserEvent::fromRowObjects)
                    .collect(Collectors.toList());
        }
    }

    List<UserEvent> findForBoxAddresses(Set<UUID> boxAddresses) {
        List<UserEvent> result = new ArrayList<>();

        List<Set<UUID>> subsets = Collections.partitionOf(boxAddresses, 20);
        for (Set<UUID> subset : subsets) {

            SqlFieldsQuery query = new SqlFieldsQuery("" +
                    "select _key," + UserEvent.columnsAsCsv() +
                    " from " + TABLE +
                    " where " + COL_BOX_ADDRESS + " in (" + repeatAndJoin("?", subset.size(), ",") + ")");
            List<Object> in = new ArrayList<>(subset);
            query.setArgs(in.toArray());


            try (FieldsQueryCursor<List<?>> cursor = userEventCache.query(query)) {
                result.addAll(StreamSupport.stream(cursor.spliterator(), false)
                        .map(UserEvent::fromRowObjects)
                        .collect(Collectors.toList()));
            }
        }

        return result;
    }

    <S extends UserEvent> S save(S entity) {
        try (
                FieldsQueryCursor<List<?>> ignored = userEventCache.query(new SqlFieldsQuery("" +
                        "merge into " + TABLE +
                        " (_key," + UserEvent.columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?, ?, ?, ?, ?)")
                        .setArgs(
                                entity.cacheKey(),
                                entity.getBoxAddress(),
                                entity.getEventId(),
                                entity.getPayload(),
                                entity.getReturnBoxAddress(),
                                entity.getReturnPayload(),
                                entity.getValidUntil(),
                                entity.getHmacHash()))
        ) {
            return entity;
        }
    }
}
