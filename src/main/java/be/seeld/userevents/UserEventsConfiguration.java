package be.seeld.userevents;

import be.seeld.IgniteCoreConfiguration;
import lombok.Data;
import org.apache.ignite.Ignite;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

import static be.seeld.userevents.UserEvent.COL_HMAC_HASH;
import static be.seeld.userevents.UserEvent.TABLE;

@Configuration
@EnableConfigurationProperties({UserEventsConfiguration.UserEventsProperties.class})
public class UserEventsConfiguration {

    private static final String SQL_SCHEMA = "PUBLIC";

    private static final String USER_EVENT_CACHE = "UserEventCache";

    @Resource
    private Ignite ignite;
    @Resource
    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * This method is where updates on the repository's caches can be defined.<br/>
     * The typical example would be the deletion of an old, unused table: this cannot be done by SQL because those
     * tables have been defined with Java, so they need to be destroyed with Java.
     */
    @PostConstruct
    void postConstruct() {
        // Adds HMAC_HASH column if missing
        if (ignite.cacheNames().contains(USER_EVENT_CACHE)) {
            SqlFieldsQuery query = new SqlFieldsQuery("ALTER TABLE " + TABLE +
                    " ADD COLUMN IF NOT EXISTS " + COL_HMAC_HASH + " varchar");
            //noinspection EmptyTryBlock
            try (FieldsQueryCursor<List<?>> ignored = ignite.getOrCreateCache(USER_EVENT_CACHE).query(query)) {}
        }
    }

    @Bean
    public UserEventRepository userEventRepository() {
        CacheConfiguration<String, UserEvent> configuration = new CacheConfiguration<String, UserEvent>()
                .setSqlSchema(SQL_SCHEMA)
                .setName(USER_EVENT_CACHE)
                .setCacheMode(CacheMode.PARTITIONED)
                .setIndexedTypes(String.class, UserEvent.class)
                .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
                .setBackups(igniteProperties.getBackupsAmount())
                .setAffinity(new RendezvousAffinityFunction(true));
        return new UserEventRepository(ignite.getOrCreateCache(configuration));
    }

    @ConfigurationProperties(prefix = "userevents")
    @Data
    public static final class UserEventsProperties {
        private int validUntilLifetime;
    }
}
