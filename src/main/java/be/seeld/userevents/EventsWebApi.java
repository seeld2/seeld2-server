package be.seeld.userevents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/events")
public class EventsWebApi {

    private final EventsProcess eventsProcess;

    @Autowired
    public EventsWebApi(EventsProcess eventsProcess) {
        this.eventsProcess = eventsProcess;
    }

    // Unfortunately we cannot use the DELETE method here because Angular does not want us to pass a body (AESRequest) when doing a DELETE... :-(
    @RequestMapping(value = "/{eventId}/state/deleted", method = PUT)
    ResponseEntity<?> deleteEvent(@PathVariable UUID eventId,
                                  @RequestBody DeleteEventRequest deleteEventsRequest) {
        eventsProcess.deleteUserEvent(
                deleteEventsRequest.getBoxAddress(),
                eventId,
                deleteEventsRequest.getHmacHash());
        return ResponseEntity.noContent().build();
    }

    /**
     * Fetches the "public" events for the authenticated person.<br>
     * Normally, one would avoid using this API because we authenticate the person fetching the events. This is because
     * no HMAC secret key is provided (maybe because it doesn't exist yet?). Typically this happens during contact
     * requests. All other events should be fetched by box addresses (with the matching HMAC secret key).
     */
    @RequestMapping(method = PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> fetchEvents(@RequestBody FetchEventsRequest fetchEventsRequest) {
        List<UserEvent> userEvents = eventsProcess.findUserEvents(fetchEventsRequest.getHmacHashesByBoxAddress());
        return ResponseEntity.ok(UserEventsResponse.from(userEvents));
    }

    /**
     * Posts a user event.<br>
     * The person needs to be authenticated to prevent bomb-posting user requests.
     * TODO The identified person must have a limited amount of "posts" allowed...
     */
    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<?> postEvent(@Valid @RequestBody CreateEventRequest createEventRequest, Principal principal) {
        eventsProcess.createNewUserEvent(
                createEventRequest.getAesPayload(),
                principal.getName(),
                createEventRequest.getPayload(),
                createEventRequest.getReturnPayload());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class CreateEventRequest {

        @NotEmpty
        private int[] aesPayload;

        @NotEmpty
        private String payload;

        @NotEmpty
        private String returnPayload;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class DeleteEventRequest {
        private UUID boxAddress;
        private String hmacHash;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class FetchEventsRequest {
        private Map<UUID, String> hmacHashesByBoxAddress;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class UserEventResponse {
        private UUID eventId;
        private String payload;

        public static UserEventResponse from(UserEvent userEvent) {
            return new UserEventResponse(userEvent.getEventId(), userEvent.getPayload());
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class UserEventsResponse {
        private List<UserEventResponse> events;

        public static UserEventsResponse from(List<UserEvent> userEvents) {
            List<UserEventResponse> userEventResponses = userEvents.stream()
                    .map(UserEventResponse::from)
                    .collect(Collectors.toList());
            return new UserEventsResponse(userEventResponses);
        }
    }
}
