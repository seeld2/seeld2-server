package be.seeld.userevents;

import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Encryptables;
import be.seeld.userevents.UserEventsConfiguration.UserEventsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserEvents {

    private final Encryptables encryptables;
    private final UserEventRepository userEventRepository;

    private final Duration validUntilLifetime;

    @Autowired
    public UserEvents(Encryptables encryptables,
                      UserEventRepository userEventRepository,
                      UserEventsProperties properties) { // TODO Instead of injecting properties inject the duration value
        this.encryptables = encryptables;
        this.userEventRepository = userEventRepository;

        this.validUntilLifetime = Duration.ofDays(properties.getValidUntilLifetime());
    }

    public void delete(UUID boxAddress, UUID eventId) {
        userEventRepository.delete(boxAddress, eventId);
    }

    /**
     * Checks whether the user event with the specified ID and box address exists.
     */
    public boolean exists(UUID eventId, UUID boxAddress) {
        UserEvent userEvent = userEventRepository.findOne(boxAddress, eventId);
        return userEvent != null;
    }

    public Optional<UserEvent> fetchUnencrypted(UUID boxAddress, UUID eventId) {
        UserEvent userEvent = userEventRepository.findOne(boxAddress, eventId);
        if (userEvent == null) {
            return Optional.empty();
        }
        return Optional.of(encryptables.ofEncrypted(userEvent).getUnencrypted());
    }

    public List<UserEvent> fetchUnencrypted(Set<UUID> boxAddresses) {
        return userEventRepository.findForBoxAddresses(boxAddresses).stream()
                .map(encryptables::ofEncrypted)
                .map(Encryptable::getUnencrypted)
                .collect(Collectors.toList());
    }

    /**
     * Fetches and decrypts the user events for the specified box address.
     */
    public List<UserEvent> fetchUnencryptedByBoxAddress(UUID boxAddress) {
        List<UserEvent> userEvents = userEventRepository.findByPkBoxAddress(boxAddress);
        return userEvents.stream()
                .map(encryptables::ofEncrypted)
                .map(Encryptable::getUnencrypted)
                .collect(Collectors.toList());
    }

    /**
     * Removes the user events for the specified box address and event ID.
     */
    public void removeWithBoxAddressAndEventId(UUID boxAddress, UUID eventId) {
        userEventRepository.delete(boxAddress, eventId);
    }

    /**
     * Stores a user event.
     *
     * @param boxAddress The box address of the user this event is addressed to
     * @param hmacHash The hmacHash to be provided to access this user event
     * @param payload The encrypted event payload
     * @param returnBoxAddress The box address of the user that should be notified if the event is not delivered
     * @param returnPayload The payload to return if the event is not delivered
     */
    public void store(UUID boxAddress, String hmacHash, String payload, String returnBoxAddress, String returnPayload) {
        // Instant.now() effectively returns the current instant using the system UTC clock.
        long validUntil = Instant.now().plus(validUntilLifetime).toEpochMilli();
        UserEvent userEvent = UserEvent.builder()
                .boxAddress(boxAddress)
                .eventId(UUID.randomUUID())
                .hmacHash(hmacHash)
                .payload(payload)
                .returnBoxAddress(returnBoxAddress)
                .returnPayload(returnPayload)
                .validUntil(validUntil)
                .build();
        userEventRepository.save(encryptables.ofUnencrypted(userEvent).getEncrypted());
    }
}
