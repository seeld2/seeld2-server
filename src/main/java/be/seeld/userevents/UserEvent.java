package be.seeld.userevents;

import be.seeld.common.ignite.Persistable;
import be.seeld.common.encryption.Encrypted;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.List;
import java.util.UUID;

import static be.seeld.common.encryption.EncryptionType.AES;
import static be.seeld.common.encryption.EncryptionType.PGP;

@Data
@Builder
public class UserEvent implements Persistable {

    public static final String TABLE = "USEREVENT";
    public static final String COL_BOX_ADDRESS = "BOX_ADDRESS";
    public static final String COL_EVENT_ID = "EVENT_ID";
    public static final String COL_HMAC_HASH = "HMAC_HASH";
    public static final String COL_PAYLOAD = "PAYLOAD";
    public static final String COL_RETURN_BOX_ADDRESS = "RETURN_BOX_ADDRESS";
    public static final String COL_RETURN_PAYLOAD = "RETURN_PAYLOAD";
    public static final String COL_VALID_UNTIL = "VALID_UNTIL";

    public static String columnsAsCsv() {
        return String.join(",",
                COL_BOX_ADDRESS,
                COL_EVENT_ID,
                COL_PAYLOAD,
                COL_RETURN_BOX_ADDRESS,
                COL_RETURN_PAYLOAD,
                COL_VALID_UNTIL,
                COL_HMAC_HASH);
    }

    public static UserEvent fromRowObjects(List<?> rowObjects) { // TODO Test
        return UserEvent.builder()
                .boxAddress((UUID) rowObjects.get(1))
                .eventId((UUID) rowObjects.get(2))
                .payload((String) rowObjects.get(3))
                .returnBoxAddress((String) rowObjects.get(4))
                .returnPayload((String) rowObjects.get(5))
                .validUntil((Long) rowObjects.get(6))
                .hmacHash((String) rowObjects.get(7))
                .build();
    }

    @QuerySqlField(name = COL_BOX_ADDRESS, index = true, notNull = true)
    private UUID boxAddress;

    @QuerySqlField(name = COL_EVENT_ID, index = true, notNull = true)
    private UUID eventId;

    @QuerySqlField(name = COL_HMAC_HASH)
    private String hmacHash;

    @Encrypted(PGP)
    @QuerySqlField(name = COL_PAYLOAD, notNull = true)
    private String payload;

    @Encrypted(AES)
    @QuerySqlField(name = COL_RETURN_BOX_ADDRESS)
    private String returnBoxAddress;

    @Encrypted(AES)
    @QuerySqlField(name = COL_RETURN_PAYLOAD)
    private String returnPayload;

    @QuerySqlField(name = COL_VALID_UNTIL)
    private long validUntil;

    public String cacheKey() {
        return boxAddress.toString() + "_" + eventId.toString();
    }
}
