package be.seeld.userevents;

import be.seeld.common.encryption.aes.AES;
import be.seeld.common.json.JSONifiable;
import be.seeld.users.User;
import be.seeld.users.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class EventsProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventsProcess.class);

    private final AES aes;
    private final Users users;
    private final UserEvents userEvents;

    @Autowired
    public EventsProcess(@Qualifier("pwaAesBase64") AES aes, Users users, UserEvents userEvents) {
        this.aes = aes;
        this.users = users;
        this.userEvents = userEvents;
    }

    void createNewUserEvent(int[] aesPayload, String senderPseudo, String payload, String returnPayload) {
        User sender = users.fetchUnencrypted(senderPseudo);
        String exchangeKey = sender.getExchangeKey();
        String json = aes.decrypt(aesPayload, exchangeKey);

        NewUserEvent newUserEvent;
        try {
            newUserEvent = NewUserEvent.fromJSON(json, NewUserEvent.class);
        } catch (IOException e) {
            throw new EventsProcessException("Could not map JSON to object", e);
        }

        userEvents.store(
                UUID.fromString(newUserEvent.getBoxAddress()),
                newUserEvent.getHmacHash(),
                payload,
                sender.getBoxAddress(),
                returnPayload);
    }

    void deleteUserEvent(UUID boxAddress, UUID eventId, String hmacHash) {
        Optional<UserEvent> userEventOptional = userEvents.fetchUnencrypted(boxAddress, eventId);
        if (!userEventOptional.isPresent()) {
            LOGGER.warn("Attempt to delete event ID " + eventId + " failed because it was not found in DB");
            return;
        }

        String storedHmacHash = userEventOptional.get().getHmacHash();
        if (storedHmacHash == null || storedHmacHash.equals(hmacHash)) {
            userEvents.delete(boxAddress, eventId);
        }
        // TODO Otherwise log a warning
    }

    List<UserEvent> findUserEvents(Map<UUID, String> hmacHashesByBoxAddress) {
        Set<UUID> boxAddresses = hmacHashesByBoxAddress.keySet();
        return userEvents.fetchUnencrypted(boxAddresses).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .collect(Collectors.toList());
    }

    private Predicate<UserEvent> hmacAuthorized(Map<UUID, String> hmacHashesByBoxAddress) {
        return userEvent -> userEvent.getHmacHash() == null
                || userEvent.getHmacHash().equals(hmacHashesByBoxAddress.get(userEvent.getBoxAddress()));
    }

    public static final class EventsProcessException extends RuntimeException {

        EventsProcessException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class NewUserEvent extends JSONifiable {
        private String boxAddress;
        private String hmacHash;
    }
}
