package be.seeld.common.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A request whose sole purpose is to carry a payload encrypted with AES.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AESRequest {
    private int[] payload;
}
