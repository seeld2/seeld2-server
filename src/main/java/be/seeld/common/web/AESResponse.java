package be.seeld.common.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A response whose sole purpose is to carry a payload encrypted with AES.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AESResponse {
    private int[] payload;
}
