package be.seeld.common.time;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * A very basic utility class that provides a series of time-related Java objects.<br/>
 * The point of this class is really to abstract the creation of time-related objects in order to be able to easily
 * mock them during tests. Most notably, this allows to very simply mock the "now" moment.
 */
public class Dates {

    /**
     * Returns the "now" ZonedDateTime for the UTC zone.
     */
    public ZonedDateTime zonedDateTimeUtc() {
        return ZonedDateTime.now(ZoneId.of("UTC"));
    }
}
