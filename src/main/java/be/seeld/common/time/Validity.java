package be.seeld.common.time;

/**
 * Represents a Validity period: a period during which a given item is considered to be "valid" or "active".
 */
public final class Validity {

    private final Long millis;

    private Validity() {
        millis = null;
    }

    private Validity(long millis) {
        this.millis = millis;
    }

    public boolean isAlwaysValid() {
        return millis == null;
    }

    public long millis() {
        if (isAlwaysValid()) {
            throw new IllegalStateException("Object marked as always valid");
        }
        return millis;
    }

    public String millisAsString() {
        return Long.toString(millis());
    }

    public static Validity alwaysValid() {
        return new Validity();
    }

    public static Validity until(long instant) {
        return new Validity(instant);
    }
}
