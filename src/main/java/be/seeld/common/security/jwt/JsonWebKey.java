package be.seeld.common.security.jwt;

import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.IOException;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class JsonWebKey extends JSONifiable {
    private String alg;
    private boolean ext;
    private String k;
    private String[] key_ops;
    private String kty;

    public static String k(String jsonWebKey) throws IOException {
        return fromJSON(jsonWebKey, JsonWebKey.class).getK();
    }
}
