package be.seeld.common.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.UUID;

public class JsonWebTokens {

    public final Config config;

    private JsonWebTokens(Config config) {
        this.config = config;
    }

    public static JsonWebTokens of(Config config) {
        return new JsonWebTokens(config);
    }

    public String generate(String subject) {
        return generate(subject, config.secret, config.lifetime);
    }

    public void generateToResponse(String subject, HttpServletResponse response) {

        String token = generate(subject, config.secret, config.lifetime);

        response.addHeader(HttpHeaders.AUTHORIZATION, config.headerPrefix + " " + token);
    }

    public Jws<Claims> parse(String token) {
        Jws<Claims> claims = Jwts.parser().setSigningKey(config.secret).parseClaimsJws(token);
        verifyThatSignatureAlgorithmIsCorrect(claims.getHeader().getAlgorithm());
        return claims;
    }

    private String generate(String subject, String secret, long lifetime) {
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(subject)
                // TODO Test whether expiration date stands the test of different timezones!
                .setExpiration(new Date(System.currentTimeMillis() + lifetime))
                .signWith(config.signatureAlgorithm, secret)
                .compact();
    }

    private void verifyThatSignatureAlgorithmIsCorrect(String signatureAlgorithm) {
        if (!signatureAlgorithm.equals(config.signatureAlgorithm.getValue())) {
            throw new SignatureException("Incorrect signature algorithm");
        }
    }

    public static final class Config {

        private final String headerPrefix;
        private final long lifetime;
        private final String secret;
        private final SignatureAlgorithm signatureAlgorithm;

        private Config(SignatureAlgorithm signatureAlgorithm, String secret, long lifetime, String headerPrefix) {

            this.headerPrefix = headerPrefix;
            this.lifetime = lifetime;
            this.secret = secret;
            this.signatureAlgorithm = signatureAlgorithm;
        }

        public static Config create(SignatureAlgorithm signatureAlgorithm, String secret, long lifetime,
                                    String headerPrefix) {
            return new Config(signatureAlgorithm, secret, lifetime, headerPrefix);
        }
    }
}
