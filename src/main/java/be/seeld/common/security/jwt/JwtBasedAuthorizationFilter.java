package be.seeld.common.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JwtBasedAuthorizationFilter extends BasicAuthenticationFilter {

    private final JsonWebTokens jsonWebTokens;
    private final Config config;

    public JwtBasedAuthorizationFilter(AuthenticationManager authenticationManager,
                                       JsonWebTokens jsonWebTokens,
                                       Config config) {
        super(authenticationManager);
        this.config = config;
        this.jsonWebTokens = jsonWebTokens;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String token = extractTokenFromHeader(request);

        if (token != null) {
            String username;

            try {
                Jws<Claims> claims = jsonWebTokens.parse(token);
                username = claims.getBody().getSubject();

            } catch (ExpiredJwtException e) {

                Claims claims = e.getClaims();

                if (!canRefreshTokenBeIssued(claims)) {
                    SecurityContextHolder.clearContext();
                    throw new UsernameNotFoundException("username not found");
                }

                username = claims.getSubject();

                jsonWebTokens.generateToResponse(username, response);

            } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
                SecurityContextHolder.clearContext();
                throw new AccessDeniedException("Invalid or expired token");
            }

            SecurityContextHolder.getContext()
                    .setAuthentication(
                            new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList()));

        } else {
            SecurityContextHolder.clearContext();
        }

        chain.doFilter(request, response);
    }

    /**
     * Returns whether an authentication token can be refreshed.<br/>
     * This returns false by default. Override to provide specific verification logic!
     * @param claims The {@link Authentication} instance that can be used to determine if the token can be
     *                       refreshed or not
     * @return True if the token can be refreshed, false otherwise
     */
    protected boolean canRefreshTokenBeIssued(Claims claims) {
        return false;
    }

    private String extractTokenFromHeader(HttpServletRequest request) {
        String authorizationValue = request.getHeader(HttpHeaders.AUTHORIZATION);
        return (authorizationValue != null && authorizationValue.startsWith(config.headerPrefix))
                ? authorizationValue.replaceFirst(config.headerPrefix, "").trim()
                : null;
    }

    public static class Config {
        private final String headerPrefix;

        private Config(String headerPrefix) {
            this.headerPrefix = headerPrefix;
        }

        public static Config create(String headerPrefix) {
            return new Config(headerPrefix);
        }
    }
}
