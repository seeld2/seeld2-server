package be.seeld.common.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS;
import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS;
import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS;
import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;
import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS;
import static org.springframework.http.HttpHeaders.ORIGIN;

public final class CorsHeadersAssuredFilter extends OncePerRequestFilter {

    public static CorsHeadersAssuredFilter with(Configuration configuration) {
        return new CorsHeadersAssuredFilter(configuration);
    }

    private final Configuration configuration;

    private CorsHeadersAssuredFilter(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, request.getHeader(ORIGIN));
        response.setHeader(ACCESS_CONTROL_ALLOW_HEADERS, String.join(",", configuration.getAllowedHeaders()));
        response.setHeader(ACCESS_CONTROL_ALLOW_METHODS, String.join(",", configuration.getAllowedMethods()));
        response.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        response.setHeader(ACCESS_CONTROL_EXPOSE_HEADERS, String.join(",", configuration.getExposedHeaders()));
        filterChain.doFilter(request, response);
    }

    @AllArgsConstructor(staticName = "with")
    @Getter
    public static final class Configuration {
        private final String[] allowedHeaders;
        private final String[] allowedMethods;
        private final String[] allowedOrigins;
        private final boolean allowCredentials;
        private final String[] exposedHeaders;
        private final String mapping;
        private final int maxAge;
    }
}
