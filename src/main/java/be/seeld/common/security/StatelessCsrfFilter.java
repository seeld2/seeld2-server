package be.seeld.common.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class StatelessCsrfFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatelessCsrfFilter.class);

    private static final Set<String> SAFE_METHODS = new HashSet<>(Arrays.asList("GET", "HEAD", "TRACE", "OPTIONS"));

    private final String cookieName;
    private final String headerName;
    private final String[] excludedPatterns;
    private final AccessDeniedHandler accessDeniedHandler;

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    public StatelessCsrfFilter(String cookieName,
                               String headerName,
                               AccessDeniedHandler accessDeniedHandler) {
        this.cookieName = cookieName;
        this.headerName = headerName;
        this.excludedPatterns = new String[]{};
        this.accessDeniedHandler = accessDeniedHandler;
    }

    public StatelessCsrfFilter(String cookieName,
                               String headerName,
                               String[] excludedPatterns,
                               AccessDeniedHandler accessDeniedHandler) {
        this.cookieName = cookieName;
        this.headerName = headerName;
        this.excludedPatterns = excludedPatterns;
        this.accessDeniedHandler = accessDeniedHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if (csrfTokenIsRequired(request)) {
            String csrfHeaderToken = request.getHeader(headerName);

            String csrfCookieToken = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                Optional<Cookie> csrfCookie = Arrays.stream(cookies)
                        .filter(cookie -> cookie.getName().equals(cookieName))
                        .findFirst();
                if (csrfCookie.isPresent()) {
                    csrfCookieToken = csrfCookie.get().getValue();
                }
            }

            if (csrfCookieToken == null || !csrfCookieToken.equals(csrfHeaderToken)) {
                LOGGER.error("requestUrl: " + request.getRequestURL().toString());
                LOGGER.error("cookies: " + (cookies != null ? cookies.length : "no cookies") + " | csrfCookieToken: " + csrfCookieToken + " | csrfHeaderToken: " + csrfHeaderToken);
                LOGGER.error("requestUrl: " + Collections.list(request.getHeaderNames()).toString());
                accessDeniedHandler.handle(request, response, new AccessDeniedException("CSRF tokens missing or not matching"));
                return;
            }

        }

        filterChain.doFilter(request, response);
    }

    private boolean csrfTokenIsRequired(HttpServletRequest request) {

        final String servletPath = request.getServletPath();
        boolean isAnExcludedPath = Arrays.stream(excludedPatterns)
                .anyMatch((excludedPattern) -> antPathMatcher.match(excludedPattern, servletPath));

        return !isAnExcludedPath && !SAFE_METHODS.contains(request.getMethod());
    }
}
