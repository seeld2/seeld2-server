package be.seeld.common.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Provides the capacity of transforming objects to JSON strings and back.
 */
public abstract class JSONifiable {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Returns the JSON string that represents this object.
     */
    public String asJSON() throws JsonProcessingException {
        return MAPPER.writeValueAsString(this);
    }

    /**
     * Builds an instance of this class from the specifid JSON string and class type.
     */
    public static <T> T fromJSON(String json, Class<T> type) throws IOException {
        return MAPPER.readValue(json, type);
    }
}
