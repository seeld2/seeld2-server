package be.seeld.common.ignite;

import be.seeld.common.lang.Maps;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

public class DatabaseExporter {

    // CONFIGURATION

    private static final String CONNECTION_URL = "jdbc:ignite:thin://127.0.0.1";
    private static final String SAVE_PATH = "src/test/resources/db/";

    private static final String[] TABLES = new String[]{
            "CLIAUTH", "MESSAGE", "MESSAGELIFE", "PARAMETER", "USER", "USEREVENT"
    };

    // =============

    private static final Map<Class<?>, Function<Object, String>> STRING_CONVERTERS = Maps.mapOf(
            Maps.Entry.of(Boolean.class, Object::toString),
            Maps.Entry.of(Long.class, Object::toString),
            Maps.Entry.of(String.class, object -> "'" + object + "'"),
            Maps.Entry.of(UUID.class, object -> "'" + object + "'")
    );

    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

        Connection connection = connection();

        for (String table : TABLES) {

            ResultSet resultSet = connection.prepareStatement("SELECT _key, * FROM " + table).executeQuery();

            List<String> columnNames = columnNames(resultSet);

            Path path = Paths.get(SAVE_PATH + table + ".sql");
            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {

                PrintWriter printWriter = new PrintWriter(bufferedWriter);

                while (resultSet.next()) {

                    List<String> rowValues = rowValues(resultSet, columnNames);

                    String insertStatement = "INSERT INTO \"" + table + "\"" +
                            " (" + String.join(",", columnNames) + ")" +
                            " VALUES (" + String.join(",", rowValues) + ");";

                    printWriter.println(insertStatement);
                    System.out.println(insertStatement);
                }
            }
        }
    }

    private static List<String> columnNames(ResultSet resultSet) throws SQLException {
        List<String> columnNames = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnsAmount = metaData.getColumnCount();
        for (int i = 1; i <= columnsAmount; i++) {
            columnNames.add(metaData.getColumnName(i));
        }
        return columnNames;
    }

    private static Connection connection() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.ignite.IgniteJdbcThinDriver");
        return DriverManager.getConnection(CONNECTION_URL);
    }

    private static List<String> rowValues(ResultSet resultSet, List<String> columnNames)
            throws SQLException {
        List<String> rowValues = new ArrayList<>();
        for (String columnName : columnNames) {
            Object object = resultSet.getObject(columnName);

            if (object != null) {
                Function<Object, String> converter = STRING_CONVERTERS.get(object.getClass());
                if (converter == null) {
                    throw new RuntimeException("Could not find a converter for " + object.getClass());
                }
                String value = converter.apply(object);
                rowValues.add(value);

            } else {
                rowValues.add(null);
            }
        }
        return rowValues;
    }
}
