package be.seeld.common.ignite;

/**
 * Indicates that the implementor can be persisted by Ignite
 */
public interface Persistable {
    String cacheKey();
}
