package be.seeld.common.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.*;

/**
 * This validation annotation verifies whether the values of a set of fields are equal. <br/>
 * The annotation requires that the names of the fields are specified using the <em>fieldNames</em> property. <br/>
 * Example: <br/>
 * <pre><code>
 *  &#64;FieldsAreEqual(fieldsNames = {"field1", "field2"})
 *  public class TwoFieldsBean {
 *      private String field1;
 *      private String field2;
 *  }
 * </code></pre>
 */
@Constraint(validatedBy = FieldsAreEqualValidator.class)
@Documented
@Retention(RUNTIME)
@Target({ ElementType.TYPE })
public @interface FieldsAreEqual {

    String message() default "fields do not match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] fieldsNames();
}
