package be.seeld.common.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The validator implementation for the {@link FieldsAreEqual} annotation.
 */
// TODO Write a blog post about this useful little validator
public class FieldsAreEqualValidator implements ConstraintValidator<FieldsAreEqual, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FieldsAreEqualValidator.class);

	private String[] fieldsNames;

	@Override
	public void initialize(FieldsAreEqual fieldsAreEqual) {
		this.fieldsNames = fieldsAreEqual.fieldsNames();
		Arrays.sort(fieldsNames);
	}

	@Override
	public boolean isValid(Object bean, ConstraintValidatorContext context) {
		boolean isValid = false;

		Set<Object> distinctFieldValues = new HashSet<>();
		Field[] declaredFields = bean.getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			if (Arrays.binarySearch(fieldsNames, field.getName()) >= 0) {
				try {
					field.setAccessible(true);
					distinctFieldValues.add(field.get(bean));
				} catch (IllegalAccessException e) {
					LOGGER.warn("The value of the fields '" + field.getName() + "' could not be accessed. ", e);
				}
			}
		}

		if (distinctFieldValues.size() == 1) {
			isValid = true;
		}

		return isValid;
	}
}
