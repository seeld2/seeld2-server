package be.seeld.common.encryption;

import com.rits.cloning.Cloner;

/**
 * An object that can be represented in an encrypted or unencrypted form.<br/> The instance of an <em>Encryptable</em>
 * makes both representations conveniently accessible.
 *
 * @param <T> The type of the object being represented in its encrypted and unencrypted form.
 */
public final class Encryptable<T> {

    private final Cloner cloner;
    private final T encrypted;
    private final T unencrypted;

    public static <T> Encryptable<T> of(T unencrypted, T encrypted, Cloner cloner) {
        return new Encryptable<>(unencrypted, encrypted, cloner);
    }

    private Encryptable(T unencrypted, T encrypted, Cloner cloner) {
        this.cloner = cloner;
        this.encrypted = encrypted;
        this.unencrypted = unencrypted;
    }

    public T getEncrypted() {
        return cloner.deepClone(encrypted);
    }

    public T getUnencrypted() {
        return cloner.deepClone(unencrypted);
    }
}
