package be.seeld.common.encryption.openpgp;

import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.InMemoryKeyring;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;
import org.bouncycastle.util.io.Streams;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.util.Date;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class OpenPGP {

    private static final String E_IN_MEMORY_KEYRING_INSTANTIATION = "Could not create an in-memory keyring";
    private static final String E_KEYS_GENERATION = "Could not generate new keys";
    private static final String E_MESSAGE_DECRYPTION = "Could not decrypt the specified message";
    private static final String E_MESSAGE_ENCRYPTION = "Could not encrypt the specified message";
    private static final String E_SECURE_RANDOM_INSTANTIATION = "Could not obtain SecureRandom instance";

    private static final int CERTAINTY = 12;
    private static final BigInteger PUBLIC_EXPONENT = BigInteger.valueOf(0x10001);
    private static final int S2K_COUNT = 0xc0;
    private static final Charset UTF8 = StandardCharsets.UTF_8;

    private final Config config;
    private final ReadWriteLock lock = new ReentrantReadWriteLock(); // TODO This is NOT a distributed lock: IT CAN'T WORK!

    private SecureRandom secureRandom;

    private OpenPGP(Config config) {
        this.config = config;
    }

    /**
     * Generates a new set of public / secret key rings.
     */
    public void generateNewKeys() {

        Date now = new Date();

        RSAKeyPairGenerator keyPairGenerator = keyPairGenerator();

        try {

            PGPKeyPair signingKeyPair = signingKeyPair(keyPairGenerator, now);
            PGPSignatureSubpacketGenerator signingKeySignatureGenerator = signingKeySignature();

            PGPKeyPair encryptionKeyPair = encryptionKeyPair(now, keyPairGenerator);
            PGPSignatureSubpacketGenerator encryptionKeySignatureGenerator = encryptionKeySignature();

            PGPKeyRingGenerator keyRingGenerator = new PGPKeyRingGenerator(
                    PGPSignature.POSITIVE_CERTIFICATION,
                    signingKeyPair,
                    config.userId.getName() + " <" + config.userId.getEmail() + ">",
                    new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA1),
                    signingKeySignatureGenerator.generate(),
                    null,
                    new BcPGPContentSignerBuilder(signingKeyPair.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA1),
                    secretKeyEncryptor()
            );
            keyRingGenerator.addSubKey(encryptionKeyPair, encryptionKeySignatureGenerator.generate(), null);

            lock.writeLock().lock();
            try {
                config.armoredPublicKeyRing = generateArmoredPublicKeyRing(keyRingGenerator);
                config.armoredSecretKeyRing = generateArmoredSecretKeyRing(keyRingGenerator);
            } finally {
                lock.writeLock().unlock();
            }

        } catch (PGPException | IOException e) {
            throw new OpenPGPException(E_KEYS_GENERATION, e);
        }
    }

    private RSAKeyPairGenerator keyPairGenerator() {
        RSAKeyPairGenerator keyPairGenerator = new RSAKeyPairGenerator();
        keyPairGenerator.init(new RSAKeyGenerationParameters(PUBLIC_EXPONENT, secureRandom, config.keySize, CERTAINTY));
        return keyPairGenerator;
    }

    private PGPKeyPair signingKeyPair(RSAKeyPairGenerator rsaKeyPairGenerator, Date date) throws PGPException {
        return new BcPGPKeyPair(PGPPublicKey.RSA_SIGN, rsaKeyPairGenerator.generateKeyPair(), date);
    }

    private PGPSignatureSubpacketGenerator signingKeySignature() {
        PGPSignatureSubpacketGenerator signingKeySignatureGenerator = new PGPSignatureSubpacketGenerator();
        signingKeySignatureGenerator.setKeyFlags(false, KeyFlags.SIGN_DATA | KeyFlags.CERTIFY_OTHER); // GPG seems to generate keys with ENCRYPT_COMMS and ENCRYPT_STORAGE flags. However this is the signing key, so I'd avoid setting those flags. Omitting them does not seem to have an impact on the functioning of BouncyGPG...
        signingKeySignatureGenerator.setPreferredSymmetricAlgorithms(false, new int[]{SymmetricKeyAlgorithmTags.AES_256});
        signingKeySignatureGenerator.setPreferredHashAlgorithms(false, new int[]{HashAlgorithmTags.SHA512});
        signingKeySignatureGenerator.setFeature(false, Features.FEATURE_MODIFICATION_DETECTION);
        return signingKeySignatureGenerator;
    }

    private PGPKeyPair encryptionKeyPair(Date now, RSAKeyPairGenerator keyPairGenerator) throws PGPException {
        return new BcPGPKeyPair(PGPPublicKey.RSA_GENERAL, keyPairGenerator.generateKeyPair(), now);
    }

    private PGPSignatureSubpacketGenerator encryptionKeySignature() {
        PGPSignatureSubpacketGenerator encryptionKeySignatureGenerator = new PGPSignatureSubpacketGenerator();
        encryptionKeySignatureGenerator.setKeyFlags(false, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);
        return encryptionKeySignatureGenerator;
    }

    private PBESecretKeyEncryptor secretKeyEncryptor() throws PGPException {
        return new BcPBESecretKeyEncryptorBuilder(
                PGPEncryptedData.AES_256,
                new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA256),
                S2K_COUNT)
                .build(config.passphrase.toCharArray());
    }

    private String generateArmoredPublicKeyRing(PGPKeyRingGenerator keyRingGenerator) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (
                ArmoredOutputStream armoredOutputStream = new ArmoredOutputStream(outputStream);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(armoredOutputStream)
        ) {
            keyRingGenerator.generatePublicKeyRing().encode(bufferedOutputStream, true);
        }
        return outputStream.toString(UTF8.name());
    }

    private String generateArmoredSecretKeyRing(PGPKeyRingGenerator keyRingGenerator) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (
                ArmoredOutputStream armoredOutputStream = new ArmoredOutputStream(outputStream);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(armoredOutputStream)
        ) {
            keyRingGenerator.generateSecretKeyRing().encode(bufferedOutputStream);
        }
        return outputStream.toString(UTF8.name());
    }

    public String getArmoredPublicKeyRing() {
        lock.readLock().lock();
        try {
            return config.armoredPublicKeyRing;
        } finally {
            lock.readLock().unlock();
        }
    }

    public String getArmoredSecretKeyRing() {
        lock.readLock().lock();
        try {
            return config.armoredSecretKeyRing;
        } finally {
            lock.readLock().unlock();
        }
    }

    public String getUserIdName() {
        return config.userId.getName();
    }

    /**
     * Encrypts and signs a message for a given recipient.
     *
     * @param unencryptedMessage       The message to encrypt
     * @param receiverArmoredPublicKey The receiver's armored public key or armored public key ring
     * @param receiverUserIdName       The receiver's userId name
     * @return An {@link EncryptionResult} instance that carries the encrypted payload,
     * as well as the armored public key that was used to sign that payload
     */
    public EncryptionResult encryptAndSign(String unencryptedMessage, String receiverArmoredPublicKey, String receiverUserIdName) {

        EncryptionSet encryptionSet = encryptionSet(receiverArmoredPublicKey);

        ByteArrayOutputStream encryptedOutputStream = new ByteArrayOutputStream();
        try (
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(encryptedOutputStream);
                OutputStream bouncyGPGOutputStream = BouncyGPG.encryptToStream()
                        .withConfig(encryptionSet.keyring)
                        .withStrongAlgorithms()
                        .toRecipient(PGPUserId.of(receiverUserIdName, config.userIdEmailSuffix).getEmail())
                        .andSignWith(config.userId.getEmail())
                        .armorAsciiOutput()
                        .andWriteTo(bufferedOutputStream)
        ) {
            Streams.pipeAll(new ByteArrayInputStream(unencryptedMessage.getBytes()), bouncyGPGOutputStream);

        } catch (IOException | PGPException | SignatureException | NoSuchProviderException | NoSuchAlgorithmException e) {
            throw new OpenPGPException(E_MESSAGE_ENCRYPTION, e);
        }

        try {
            return EncryptionResult.of(encryptedOutputStream.toString(UTF8.name()), encryptionSet.armoredPublicKey);
        } catch (UnsupportedEncodingException e) {
            throw new OpenPGPException(E_MESSAGE_ENCRYPTION, e);
        }
    }

    /**
     * Decrypts the specified PGP message.<br>
     * <strong>Does NOT verify the signature!</strong>
     */
    public String decrypt(String encryptedMessage) {

        InMemoryKeyring keyring = keyRing();

        ByteArrayOutputStream unencryptedOutputStream = new ByteArrayOutputStream();
        try (
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(unencryptedOutputStream);
                InputStream bouncyGPGInputStream = BouncyGPG
                        .decryptAndVerifyStream()
                        .withConfig(keyring)
                        .andIgnoreSignatures()
                        .fromEncryptedInputStream(new ByteArrayInputStream(encryptedMessage.getBytes(UTF8)))
        ) {
            Streams.pipeAll(bouncyGPGInputStream, bufferedOutputStream);
            bufferedOutputStream.flush();
            return unencryptedOutputStream.toString(UTF8.name());

        } catch (IOException | NoSuchProviderException e) {
            throw new OpenPGPException(E_MESSAGE_DECRYPTION, e);
        }
    }

    /**
     * Decrypts the specified PGP message and checks it has been signed by the specified recipient.
     *
     * @param encryptedMessage       The message to decrypt
     * @param senderArmoredPublicKey The armored public key or key ring of the recipient whose signature must be checked
     * @param senderUserIdName       The user ID of the recipient whose signature must be checked
     */
    public String decryptAndVerify(String encryptedMessage, String senderArmoredPublicKey, String senderUserIdName) {

        // TODO In a later iteration it would be nice to find a way of adding keys to a keyring, to avoid rebuilding the keyring every time
        InMemoryKeyring keyring = keyRing(senderArmoredPublicKey);

        ByteArrayOutputStream unencryptedOutputStream = new ByteArrayOutputStream();
        try (
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(unencryptedOutputStream);
                InputStream bouncyGPGInputStream = BouncyGPG
                        .decryptAndVerifyStream()
                        .withConfig(keyring)
                        .andRequireSignatureFromAllKeys(PGPUserId.of(senderUserIdName, config.userIdEmailSuffix).getEmail())
                        .fromEncryptedInputStream(new ByteArrayInputStream(encryptedMessage.getBytes(UTF8)))
        ) {
            Streams.pipeAll(bouncyGPGInputStream, bufferedOutputStream);
            bufferedOutputStream.flush();
            return unencryptedOutputStream.toString(UTF8.name());

        } catch (IOException | NoSuchProviderException | PGPException e) {
            throw new OpenPGPException(E_MESSAGE_DECRYPTION, e);
        }
    }

    private EncryptionSet encryptionSet(String... additionalArmoredPublicKeys) {
        lock.readLock().lock();
        try {
            return new EncryptionSet(config.armoredPublicKeyRing, buildInMemoryKeyring(additionalArmoredPublicKeys));
        } catch (IOException | PGPException e) {
            throw new OpenPGPException(E_IN_MEMORY_KEYRING_INSTANTIATION, e);
        } finally {
            lock.readLock().unlock();
        }
    }

    private InMemoryKeyring keyRing(String... additionalArmoredPublicKeys) {
        lock.readLock().lock();
        try {
            return buildInMemoryKeyring(additionalArmoredPublicKeys);
        } catch (IOException | PGPException e) {
            throw new OpenPGPException(E_IN_MEMORY_KEYRING_INSTANTIATION, e);
        } finally {
            lock.readLock().unlock();
        }
    }

    private InMemoryKeyring buildInMemoryKeyring(String[] additionalArmoredPublicKeys) throws IOException, PGPException {
        InMemoryKeyring keyring = KeyringConfigs.forGpgExportedKeys(KeyringConfigCallbacks.withPassword(config.passphrase));

        // TODO In a later iteration, it would be nice to somehow reimplement InMemoryKeyring to add the possibility of passing PGPPublicKeyRings instead of bytes. That probably means reimplementing KeyringConfig and ditching KeyringConfigs
        keyring.addPublicKey(config.armoredPublicKeyRing.getBytes(UTF8));
        keyring.addSecretKey(config.armoredSecretKeyRing.getBytes(UTF8));

        for (String armoredPublicKey : additionalArmoredPublicKeys) {
            if (!armoredPublicKey.equals(config.armoredPublicKeyRing)) {
                keyring.addPublicKey(armoredPublicKey.getBytes(UTF8));
            }
        }
        return keyring;
    }

    /**
     * Returns whether this instance uses the specified public and secret key rings.<br/>
     * Will always return false if either this instance's public key ring or secret key ring are <em>null</em>.
     */
    public boolean hasTheseKeyRings(String armoredPublicKeyRing, String armoredSecretKeyRing) {
        return config.armoredPublicKeyRing != null
                && config.armoredSecretKeyRing != null
                && config.armoredPublicKeyRing.equals(armoredPublicKeyRing)
                && config.armoredSecretKeyRing.equals(armoredSecretKeyRing);
    }

    /**
     * Creates an instance of this class using the specified configuration.<br/>
     * If the configuration specifies key rings, then those key rings will be used. Otherwise keys will be generated.
     *
     * @return A new instance of an OpenPGP class
     */
    public static OpenPGP create(Config config) {
        OpenPGP instance = newInstance(config);
        if (config.armoredPublicKeyRing == null || config.armoredSecretKeyRing == null) {
            instance.generateNewKeys();
        }
        return instance;
    }

    /**
     * Creates an instance of this class using the specified configuration and armored key rings.
     *
     * @return A new instance of an OpenPGP class
     */
    public static OpenPGP create(Config config, String armoredPublicKeyRing, String armoredSecretKeyRing) {
        OpenPGP instance = newInstance(config);
        instance.config.armoredPublicKeyRing = armoredPublicKeyRing;
        instance.config.armoredSecretKeyRing = armoredSecretKeyRing;
        return instance;
    }

    private static OpenPGP newInstance(Config config) {

        OpenPGP instance = new OpenPGP(config);

        try {
            instance.secureRandom = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new OpenPGPException(E_SECURE_RANDOM_INSTANTIATION, e);
        }

        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

        return instance;
    }

    /**
     * This configuration object is used to provide the necessary information to build an OpenPGP instance.
     */
    public static class Config {
        private PGPUserId userId;
        private String userIdEmailSuffix;
        private int keySize;
        private String passphrase;
        private String armoredPublicKeyRing;
        private String armoredSecretKeyRing;

        private Config() {
        }

        /**
         * Create an instance of a Config object to pass at OpenPGP instance creation.
         *
         * @param userIdName        The ID name of the user encrypting/decrypting messages with the OpenPGP instance to create
         * @param userIdEmailSuffix The ID email suffix of the user encrypting/decrypting messages with the OpenPGP
         *                          instance to create. This is added to the userIDName to form the userIdEmail property
         * @param passphrase        The passphrase to use to protect the secret keys
         * @param keySize           The size of the keys to be generated, should they need to be
         */
        public static Config create(String userIdName, String userIdEmailSuffix, String passphrase, int keySize) {
            return create(userIdName, userIdEmailSuffix, passphrase, keySize, null, null);
        }

        /**
         * Create an instance of a Config object to pass at OpenPGP instance creation.
         *
         * @param userIdName           The ID name of the user encrypting/decrypting messages with the OpenPGP instance to create
         * @param userIdEmailSuffix    The ID email suffix of the user encrypting/decrypting messages with the OpenPGP
         *                             instance to create. This is added to the userIDName to form the userIdEmail property
         * @param passphrase           The passphrase to use to protect the secret keys
         * @param keySize              The size of the keys to be generated, should they need to be
         * @param armoredPublicKeyRing The armored public key of the user the OpenPGP instance is generated for
         * @param armoredSecretKeyRing The armored secret key of the user the OpenPGP instance is generated for
         */
        public static Config create(String userIdName, String userIdEmailSuffix, String passphrase, int keySize, String armoredPublicKeyRing,
                                    String armoredSecretKeyRing) {
            Config config = new Config();
            config.keySize = keySize;
            config.userId = PGPUserId.of(userIdName, userIdEmailSuffix);
            config.userIdEmailSuffix = userIdEmailSuffix;
            config.passphrase = passphrase;
            config.armoredPublicKeyRing = armoredPublicKeyRing;
            config.armoredSecretKeyRing = armoredSecretKeyRing;
            return config;
        }
    }

    public static class EncryptionResult {
        private String armoredSigningPublicKey;
        private String payload;

        public static EncryptionResult of(String payload, String armoredSigningPublicKey) {
            EncryptionResult result = new EncryptionResult();
            result.armoredSigningPublicKey = armoredSigningPublicKey;
            result.payload = payload;
            return result;
        }

        public String getArmoredSigningPublicKey() {
            return armoredSigningPublicKey;
        }

        public String getPayload() {
            return payload;
        }
    }

    public static class OpenPGPException extends RuntimeException {

        public OpenPGPException(String message) {
            super(message);
        }

        OpenPGPException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private class EncryptionSet {
        private String armoredPublicKey;
        private InMemoryKeyring keyring;

        private EncryptionSet(String armoredPublicKey, InMemoryKeyring keyring) {
            this.armoredPublicKey = armoredPublicKey;
            this.keyring = keyring;
        }
    }
}
