package be.seeld.common.encryption.openpgp;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PGPUserId {

    String email;
    String name;

    public static PGPUserId of(String name, String emailSuffix) {
        String email = emailSuffix.startsWith("@")
                ? name + emailSuffix
                : name + "@" + emailSuffix;
        return new PGPUserId(email, name);
    }
}
