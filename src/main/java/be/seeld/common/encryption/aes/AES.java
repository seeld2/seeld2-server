package be.seeld.common.encryption.aes;

import be.seeld.common.lang.Bytes;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.*;

/**
 * Provides AES encryption operations.
 */
public class AES {

    private static final String E_CIPHER_CREATION = "Could not get an instance of Cipher using the configured algorithm, mode and padding";
    private static final String E_DECRYPTION = "Could not decrypt the data";
    private static final String E_ENCRYPTION = "Could not encrypt the data";
    private static final String E_SECRET_KEY_MISSING = "The secret key is missing";
    private static final String E_SECURE_RANDOM_CREATION = "Could not get a secure instance of SecureRandom";

    private static final String ALGORITHM = "AES";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * Provides an instance of this class, using a cryptographically strong random number generator specified by the
     * passed configuration object.
     */
    public static AES of(Config config) {

        SecureRandom secureRandom;
        try {
            secureRandom = SecureRandom.getInstance(config.secureRandomAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new AESException(E_SECURE_RANDOM_CREATION, e);
        }

        return new AES(secureRandom, config);
    }

    private final int ivSize;
    private final String mode;
    private final String padding;
    private final String secretKey;
    private final int secretKeySize;
    private final SecretKeyType secretKeyType;
    private final SecureRandom secureRandom;

    /**
     * Provides an instance of this class using the passed configuration object.
     */
    private AES(SecureRandom secureRandom, Config config) {
        this.secureRandom = secureRandom;
        this.ivSize = config.ivSize;
        this.mode = config.mode;
        this.padding = config.padding;
        this.secretKey = config.secretKey;
        this.secretKeySize = config.secretKeySize;
        this.secretKeyType = config.secretKeyType;
    }

    /**
     * Encrypts the passed array of bytes.
     *
     * @return The encrypted content as an array of bytes
     */
    public byte[] encrypt(byte[] bytes) {
        return encrypt(bytes, secretKey);
    }

    /**
     * Encrypts the passed array of bytes using the specified secret key.
     *
     * @return The encrypted content as an array of bytes
     */
    public byte[] encrypt(byte[] data, String secretKey) {
        checkSecretKey(secretKey);

        byte[] iv = iv();

        Cipher cipher = cipher();
        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec(secretKey), new IvParameterSpec(iv));
            byte[] encryptedData = cipher.doFinal(data);
            return Bytes.concat(iv, encryptedData).orElse(new byte[]{});

        } catch (Exception e) {
            throw new AESException(E_ENCRYPTION, e);
        }
    }

    /**
     * Encrypts the passed text.
     *
     * @return The encrypted content as a hex string
     */
    public String encrypt(String text) {
        return Hex.toHexString(this.encrypt(text.getBytes(UTF_8)));
    }

    /**
     * Encrypts the passed string, using the specified secret key.
     *
     * @return The encrypted content as a hex string
     */
    public String encrypt(String text, String secretKey) {
        return Hex.toHexString(this.encrypt(text.getBytes(UTF_8), secretKey));
    }

    public String encryptToBase64(String text, String secretKey) {
        return Base64.getUrlEncoder().encodeToString(this.encrypt(text.getBytes(UTF_8), secretKey));
    }

    /**
     * Encrypts the passed text, using the specified secret key, to an array of <strong>unsigned</strong> bytes.
     *
     * @return The encrypted content as an array of unsigned bytes.
     */
    public int[] encryptToUnsignedBytes(String text, String secretKey) {
        byte[] encryptedBytes = encrypt(text.getBytes(UTF_8), secretKey);

        int[] unsignedEncrypted = new int[encryptedBytes.length];
        for (int i = 0; i < encryptedBytes.length; i++) {
            unsignedEncrypted[i] = Byte.toUnsignedInt(encryptedBytes[i]);
        }
        return unsignedEncrypted;
    }

    /**
     * Decrypts the specified content, expressed as an array of bytes.
     *
     * @return The decrypted content as an array of bytes
     */
    public byte[] decrypt(byte[] data) {
        return decrypt(data, secretKey);
    }

    /**
     * Decrypts the specified content, expressed as an array of bytes, using the specified secret key.
     *
     * @return The decrypted content as an array of bytes
     */
    public byte[] decrypt(byte[] data, String secretKey) {
        checkSecretKey(secretKey);

        byte[] iv = Arrays.copyOf(data, ivSize);
        byte[] encryptedData = Arrays.copyOfRange(data, ivSize, data.length);

        Cipher cipher = cipher();
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec(secretKey), new IvParameterSpec(iv));
            return cipher.doFinal(encryptedData);

        } catch (Exception e) {
            throw new AESException(E_DECRYPTION, e);
        }
    }

    /**
     * Decrypts the specified content, expressed as string of encrypted text.
     *
     * @return The decrypted content as a string
     */
    public String decrypt(String encryptedText) {
        return new String(decrypt(Hex.decode(encryptedText.getBytes(UTF_8))));
    }

    /**
     * Decrypts the specified content, expressed as string of encrypted text, using the specified secret key.
     *
     * @return The decrypted content as a string
     */
    public String decrypt(String encryptedText, String secretKey) {
        return new String(decrypt(Hex.decode(encryptedText.getBytes(UTF_8)), secretKey));
    }

    public String decryptBase64(String encryptedText, String secretKey) {
        return new String(decrypt(Base64.getUrlDecoder().decode(encryptedText.getBytes(UTF_8)), secretKey));
    }

    /**
     * Decrypts the specified content, expressed as an array of encrypted <strong>unsigned</strong> bytes, using the
     * specified secret key.
     *
     * @return The decrypted content as a string
     */
    public String decrypt(int[] encryptedUnsignedBytes, String secretKey) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(encryptedUnsignedBytes.length);
        Arrays.stream(encryptedUnsignedBytes).forEach(i -> byteBuffer.put((byte) i));
        byte[] data = byteBuffer.array();

        return new String(decrypt(data, secretKey));
    }

    private void checkSecretKey(String secretKey) {
        if (secretKey == null) {
            throw new AESException(E_SECRET_KEY_MISSING);
        }
    }

    private Cipher cipher() {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM + "/" + mode + "/" + padding, BouncyCastleProvider.PROVIDER_NAME);
        } catch (Exception e) {
            throw new AESException(E_CIPHER_CREATION, e);
        }
        return cipher;
    }

    private byte[] iv() {
        byte[] iv = new byte[ivSize];
        secureRandom.nextBytes(iv);
        return iv;
    }

    private SecretKeySpec secretKeySpec(String secretKey) {
        if (secretKeyType == SecretKeyType.BASE64) {
            return new SecretKeySpec(Base64.getUrlDecoder().decode(secretKey), ALGORITHM);
        } else {
            return new SecretKeySpec(Hex.decode(secretKey), ALGORITHM);
        }
    }

    /**
     * Generates a secret key expressed as a Base64-encoded string.
     */
    public String generateSecretKeyAsBase64() {
        return Base64.getUrlEncoder()
                .withoutPadding()
                .encodeToString(generateSecretKey().getEncoded());
    }

    public int getSecretKeySize() {
        return secretKeySize;
    }

    /**
     * Generates a secret key.
     */
    SecretKey generateSecretKey() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
            keyGenerator.init(secretKeySize, secureRandom);
            return keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new AESException("Could not generate secret key", e);
        }
    }

    public static class AESException extends RuntimeException {

        public AESException(String message) {
            super(message);
        }

        public AESException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public enum SecretKeyType {
        BASE64,
        HEX
    }

    public static class Config {
        private final int ivSize;
        private final String secretKey;
        private final int secretKeySize;
        private final SecretKeyType secretKeyType;
        private final String mode;
        private final String padding;
        private final String secureRandomAlgorithm;

        public Config(String secureRandomAlgorithm,
                      String mode,
                      String padding,
                      String secretKey, int secretKeySize,
                      SecretKeyType secretKeyType,
                      int ivSize) {
            this.ivSize = ivSize;
            this.secretKey = secretKey;
            this.secretKeySize = secretKeySize;
            this.secretKeyType = secretKeyType;
            this.mode = mode;
            this.padding = padding;
            this.secureRandomAlgorithm = secureRandomAlgorithm;
        }
    }
}
