package be.seeld.common.encryption;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Salts {

    private final SecureRandom secureRandom;

    public Salts(SecureRandom secureRandom) {
        this.secureRandom = secureRandom;
    }

    public String generateRandomSalt(String algorithm, int bytesLength)  {
        byte[] bytes = new byte[bytesLength];
        secureRandom.nextBytes(bytes);

        try {
            String randomSalt;

            do { // Sanity check on salt's length: in some rare cases we get a 63-long salt instead of 64... So we try again!
                MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
                messageDigest.reset();
                messageDigest.update(bytes, 0, bytes.length);
                randomSalt = new BigInteger(1, messageDigest.digest()).toString(16);

            } while (randomSalt.length() < bytesLength);

            return randomSalt;

        } catch (NoSuchAlgorithmException e) {
            throw new SaltsException("Could not generate a random salt", e);
        }
    }

    public static class SaltsException extends RuntimeException {
        SaltsException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
