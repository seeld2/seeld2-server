package be.seeld.common.encryption;

/**
 * Describes types of encryption: AES, PASSWORD (hashing to be used for password / passphrase encryption in the system),
 * etc.<br/> The RSA type is merely a marker type to indicate that the field value is expected to have been encrypted on
 * the client side.
 */
public enum EncryptionType {
    /**
     * Indicates that this field is to be AES-encrypted on the server side.
     */
    AES,
    /**
     * Indicates a password, which should be hashed.
     */
    PASSWORD,
    /**
     * Indicates that this field should have been PGP-encrypted by the client before being sent over.
     */
    PGP
}
