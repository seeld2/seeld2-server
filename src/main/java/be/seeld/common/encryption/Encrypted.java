package be.seeld.common.encryption;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that indicates whether the annotated element should be stored in an encrypted form.<br/> The annotation is
 * applicable to properties or to entire classes
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Encrypted {

    /**
     * Indicates which type of encryption should be used to encrypt the annotated field or class.
     */
    EncryptionType value() default EncryptionType.AES;
}
