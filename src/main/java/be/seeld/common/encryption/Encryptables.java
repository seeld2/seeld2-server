package be.seeld.common.encryption;

import be.seeld.common.encryption.aes.AES;
import com.rits.cloning.Cloner;
import org.bouncycastle.util.encoders.Hex;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Handles conversions of AES-encryptable beans by returning {@link Encryptable} instances of the specified bean.
 */
public class Encryptables {

    private static final Cloner CLONER = Cloner.standard();

    private AES aes;

    public Encryptables(AES aes) {
        this.aes = aes;
    }

    /**
     * Provides an {@link Encryptable} that embeds both encrypted and unencrypted version of the specified bean.
     * @param encryptedBean A bean with <strong>encrypted</strong> fields
     */
    public <T> Encryptable<T> ofEncrypted(T encryptedBean) {
        checkNotNull(encryptedBean);

        T unencryptedBean = decrypt(encryptedBean);
        return Encryptable.of(unencryptedBean, encryptedBean, CLONER);
    }

    private <T> void checkNotNull(T bean) {
        if (bean == null) {
            throw new EncryptablesException("Bean parameter cannot be null!");
        }
    }

    /**
     * Provides an {@link Encryptable} that embeds both encrypted and unencrypted version of the specified bean.
     * @param unencryptedBean A bean with <strong>unencrypted</strong> fields
     */
    public <T> Encryptable<T> ofUnencrypted(T unencryptedBean) {
        checkNotNull(unencryptedBean);

        T encryptedBean = encrypt(unencryptedBean);
        return Encryptable.of(unencryptedBean, encryptedBean, CLONER);
    }

    private <T> T decrypt(T encryptedBean) {
        final T unencrypted = CLONER.deepClone(encryptedBean);

        annotatedFields(unencrypted.getClass())
                .filter(onFieldsForAesEncryption())
                .forEach(field -> decryptAes(unencrypted, field));

        return unencrypted;
    }

    private <T> void decryptAes(T entity, Field field) {
        try {
            field.setAccessible(true);

            if (Collection.class.isAssignableFrom(field.getType())) {
                field.set(entity, decryptAes((Collection) field.get(entity)));

            } else if (Map.class.isAssignableFrom(field.getType())) {
                field.set(entity, decryptAes((Map) field.get(entity)));

            } else {
                field.set(entity, decryptAes((String) field.get(entity)));
            }

        } catch (Exception e) {
            throw new RuntimeException("Exception during AES decryption of " + field.getName(), e);

        } finally {
            field.setAccessible(false);
        }
    }

    private String decryptAes(String string) {
        return string != null ? new String(aes.decrypt(Hex.decode(string))) : null;
    }

    private Collection<String> decryptAes(Collection<String> collection) throws Exception {
        Collection<String> unencryptedCollection = null;
        if (collection != null) {
            if (Set.class.isAssignableFrom(collection.getClass())) {
                unencryptedCollection = Collections.EMPTY_SET.getClass().isInstance(collection)
                        ? Collections.emptySet() : collection.getClass().newInstance();
            } else {
                unencryptedCollection = Collections.EMPTY_LIST.getClass().isInstance(collection)
                        ? Collections.emptyList() : collection.getClass().newInstance();
            }
            for (String string : collection) {
                unencryptedCollection.add(decryptAes(string));
            }
        }
        return unencryptedCollection;
    }

    private Map<String, String> decryptAes(Map<String, String> map) {
        Map<String, String> unencryptedMap = null;
        if (map != null) {
            unencryptedMap = new HashMap<>();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String unencryptedKey = decryptAes(entry.getKey());
                String unencryptedValue = decryptAes(entry.getValue());
                unencryptedMap.put(unencryptedKey, unencryptedValue);
            }
        }
        return unencryptedMap;
    }

    private <T> T encrypt(T unencryptedBean) {
        final T encrypted = CLONER.deepClone(unencryptedBean);

        Class<?> beanClass = encrypted.getClass();
        Stream<Field> annotatedFields = annotatedFields(beanClass);

        annotatedFields
                .filter(onFieldsForAesEncryption())
                .forEach(field -> encryptAes(encrypted, field));

        return encrypted;
    }

    private <T> void encryptAes(T entity, Field field) {
        try {
            field.setAccessible(true);

            if (Collection.class.isAssignableFrom(field.getType())) {
                field.set(entity, encryptAes((Collection) field.get(entity)));

            } else if (Map.class.isAssignableFrom(field.getType())) {
                field.set(entity, encryptAes((Map) field.get(entity)));

            } else {
                field.set(entity, encryptAes((String) field.get(entity)));
            }

        } catch (Exception e) {
            throw new RuntimeException("Exception during AES encryption of " + field.getName(), e);

        } finally {
            field.setAccessible(false);
        }
    }

    private String encryptAes(String string) {
        return string != null ? Hex.toHexString(aes.encrypt(string.getBytes())) : null;
    }

    private Collection<String> encryptAes(Collection<String> collection) throws Exception {
        Collection<String> encryptedCollection = null;
        if (collection != null) {
            if (Set.class.isAssignableFrom(collection.getClass())) {
                encryptedCollection = Collections.EMPTY_SET.getClass().isInstance(collection)
                        ? Collections.emptySet() : collection.getClass().newInstance();
            } else {
                encryptedCollection = Collections.EMPTY_LIST.getClass().isInstance(collection)
                        ? Collections.emptyList() : collection.getClass().newInstance();
            }
            for (String string : collection) {
                encryptedCollection.add(encryptAes(string));
            }
        }
        return encryptedCollection;
    }

    private Map<String, String> encryptAes(Map<String, String> map) {
        Map<String, String> encryptedMap = null;
        if (map != null) {
            encryptedMap = new HashMap<>();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String encryptedKey = encryptAes(entry.getKey());
                String encryptedValue = encryptAes(entry.getValue());
                encryptedMap.put(encryptedKey, encryptedValue);
            }
        }
        return encryptedMap;
    }

    private Stream<Field> annotatedFields(Class<?> beanClass) {
        return Arrays
                .stream(beanClass.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Encrypted.class))
                .filter(field -> field.getAnnotation(Encrypted.class) != null);
    }

    private Predicate<Field> onFieldsForAesEncryption() {
        return field -> EncryptionType.AES.equals(field.getAnnotation(Encrypted.class).value());
    }

    static class EncryptablesException extends RuntimeException {

        EncryptablesException(String message) {
            super(message);
        }
    }
}
