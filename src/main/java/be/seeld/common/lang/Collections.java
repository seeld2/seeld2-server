package be.seeld.common.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Collections {

    /**
     * Builds a {@link List} from an array.<br/>
     * The items of the list are not backed up by the array, as opposed to {@link Arrays#asList(T... a)}.
     */
    @SafeVarargs
    public static <T> List<T> listOf(T... items) {
        return Arrays.stream(items).collect(Collectors.toList());
    }

    public static <T> List<Set<T>> partitionOf(Set<T> set, int partitionSize) {

        int amountOfItems = set.size();

        int amountOfPartitions = amountOfItems / partitionSize;
        if (amountOfItems % partitionSize > 0) {
            amountOfPartitions++;
        }

        List<Set<T>> partitions = new ArrayList<>(amountOfPartitions);
        List<T> list = new ArrayList<>(set);
        for (int i = 0; i < amountOfPartitions; i++) {
            int fromIndex = i * partitionSize;
            int toIndex = (i + 1) * partitionSize;
            if (toIndex > amountOfItems) {
                toIndex = amountOfItems;
            }
            Set<T> partition = new HashSet<>(list.subList(fromIndex, toIndex));
            partitions.add(partition);
        }

        return partitions;
    }

    /**
     * Builds a {@link Set} from an array.
     */
    @SafeVarargs
    public static <T> Set<T> setOf(T... items) {
        return Arrays.stream(items).collect(Collectors.toSet());
    }
}
