package be.seeld.common.lang;

import java.util.Collections;

public class Strings {

    /**
     * Creates a string of the specified length using the specified text as a filler.
     */
    public static String ofLength(int length, String text) {
        StringBuilder sb = new StringBuilder(length + text.length());
        while (sb.length() < length) {
            sb.append(text);
        }
        return sb.substring(0, length);
    }

    /**
     * Creates a string of <strong>n</strong> repeats of the specified <strong>text</strong>,
     * separated by the specified <strong>separator</strong>.
     */
    public static String repeatAndJoin(String text, int n, String separator) {
        return String.join(separator, Collections.nCopies(n, text));
    }

    /**
     * Reverses the specified text.
     */
    public static String reverse(String text) {
        return new StringBuilder(text).reverse().toString();
    }
}
