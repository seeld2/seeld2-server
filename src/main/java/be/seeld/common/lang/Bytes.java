package be.seeld.common.lang;

import java.util.Arrays;
import java.util.Optional;

public class Bytes {

    public static Optional<byte[]> concat(byte[]... arrays) {
        int length = Arrays.stream(arrays)
                .map(array -> array.length)
                .reduce(Integer::sum)
                .orElse(0);

        if (length == 0) {
            return Optional.empty();

        } else {
            byte[] result = new byte[length];
            int pos = 0;
            for (byte[] array : arrays) {
                System.arraycopy(array, 0, result, pos, array.length);
                pos += array.length;
            }
            return Optional.of(result);
        }
    }
}
