package be.seeld.common.lang;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Maps {

    @SafeVarargs
    public static <K, V> Map<K, V> mapOf(Entry<K, V>... entries) {
        final Map<K, V> map = new HashMap<>();
        Arrays.stream(entries).forEach(entry -> map.put(entry.getKey(), entry.getValue()));
        return map;
    }

    public static <K, V> Map<K, V> mapOf(Collection<K> entries, V value) {
        Map<K, V> map = new HashMap<>();
        entries.forEach(entry -> map.put(entry, value));
        return map;
    }

    public static class Entry<K, V> {

        private final K key;
        private final V value;

        private Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public static <K, V> Entry<K, V> of(K key, V value) {
            return new Entry<>(key, value);
        }

        K getKey() {
            return key;
        }

        V getValue() {
            return value;
        }
    }
}
