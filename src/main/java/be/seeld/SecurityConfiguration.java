package be.seeld;

import be.seeld.common.encryption.Encryptables;
import be.seeld.common.encryption.Salts;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.encryption.openpgp.OpenPGP;
import be.seeld.common.security.CorsHeadersAssuredFilter;
import be.seeld.common.security.RESTAuthenticationEntryPoint;
import be.seeld.common.security.jwt.JsonWebTokens;
import be.seeld.common.security.jwt.JwtBasedAuthorizationFilter;
import be.seeld.users.Users;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

@Configuration
@EnableConfigurationProperties(SecurityConfiguration.AppSecurityProperties.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Resource
    private AppSecurityProperties appSecurityProperties;
    @Resource
    private Users users;

    @Bean
    public AES databaseAes() {
        AppSecurityProperties.AesProperties properties = appSecurityProperties.aes;
        return AES.of(new AES.Config(
                properties.secureRandomAlgorithm,
                properties.mode,
                properties.padding,
                properties.key,
                properties.keySize,
                AES.SecretKeyType.HEX,
                properties.ivSize
        ));
    }

    @Bean
    public AES pwaAesBase64() { // TODO Rename to clientAesBase64
        AppSecurityProperties.AesProperties properties = appSecurityProperties.aes;
        return AES.of(new AES.Config(
                properties.secureRandomAlgorithm,
                properties.mode,
                properties.padding,
                null,
                properties.keySize,
                AES.SecretKeyType.BASE64,
                properties.ivSize
        ));
    }

    @Bean
    public AES pwaAesHex() { // TODO Rename to clientAesHex
        AppSecurityProperties.AesProperties properties = appSecurityProperties.aes;
        return AES.of(new AES.Config(
                properties.secureRandomAlgorithm,
                properties.mode,
                properties.padding,
                null,
                properties.keySize,
                AES.SecretKeyType.HEX,
                properties.ivSize
        ));
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new AccessDeniedHandlerImpl();
    }

    @Bean
    public Encryptables encryptables() {
        return new Encryptables(databaseAes());
    }

    @Bean
    public WebMvcConfigurer localWebMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**")
                        .allowedOrigins(appSecurityProperties.cors.allowedOrigins)
                        .allowCredentials(appSecurityProperties.cors.allowCredentials)
                        .allowedHeaders(appSecurityProperties.cors.allowedHeaders)
                        .allowedMethods(appSecurityProperties.cors.allowedMethods)
                        .maxAge(appSecurityProperties.cors.maxAge);
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new WebCallTracer());
            }
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder() throws NoSuchAlgorithmException {
        return new BCryptPasswordEncoder(appSecurityProperties.passwordEncoder.strength, secureRandom());
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new RESTAuthenticationEntryPoint();
    }

    @Bean
    public SecureRandom secureRandom() throws NoSuchAlgorithmException {
        return SecureRandom.getInstanceStrong();
    }

    @Bean
    public UserDetailsService systemUserDetailsService() {
        return new UserDetailsService(users);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(systemUserDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        CorsHeadersAssuredFilter.Configuration corsConfiguration = CorsHeadersAssuredFilter.Configuration.with(
                appSecurityProperties.cors.getAllowedHeaders(),
                appSecurityProperties.cors.getAllowedMethods(),
                appSecurityProperties.cors.getAllowedOrigins(),
                appSecurityProperties.cors.isAllowCredentials(),
                appSecurityProperties.cors.getExposedHeaders(),
                appSecurityProperties.cors.getMapping(),
                appSecurityProperties.cors.getMaxAge());
        http.addFilterAfter(CorsHeadersAssuredFilter.with(corsConfiguration), CorsFilter.class);

        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint());

        http
                .addFilter(jwtBasedAuthorizationFilter())
                .httpBasic().disable()
                .formLogin().disable()
                .logout().disable();

        http.authorizeRequests()

                .mvcMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                .mvcMatchers(HttpMethod.PUT, "/api/conversations").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/conversations/ids").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/conversations/*/messages").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/conversations/*/messages/ids").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/conversations/*/state/deleted").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/conversations/*/messages/*/state/deleted").permitAll()

                .mvcMatchers(HttpMethod.PUT, "/api/events").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/events/*/state/deleted").permitAll()

                .mvcMatchers("/api/login/**").permitAll()

                .mvcMatchers(HttpMethod.PUT, "/api/messages/new").permitAll()
                .mvcMatchers(HttpMethod.PUT, "/api/messages/new/timeline/latest").permitAll()

                .mvcMatchers(HttpMethod.GET, "/api/profiles/*").permitAll()

                .mvcMatchers("/api/registrations/**").permitAll()

                .mvcMatchers("/wsapi/**").permitAll()

                .mvcMatchers("/**").authenticated();
    }

    @Bean
    public JsonWebTokens jwtProvider() {
        AppSecurityProperties.JwtProperties jwtProperties = appSecurityProperties.jwt;
        return JsonWebTokens.of(JsonWebTokens.Config.create(
                SignatureAlgorithm.forName(jwtProperties.signatureAlgorithm),
                jwtProperties.secret, // TODO This must become a dynamic property!
                jwtProperties.lifetime,
                jwtProperties.prefix));
    }

    @Bean
    public JwtBasedAuthorizationFilter jwtBasedAuthorizationFilter() throws Exception {
        AppSecurityProperties.JwtProperties jwtProperties = appSecurityProperties.jwt;
        return new UserVerifyingJwtBasedAuthorizationFilter(
                authenticationManager(),
                jwtProvider(),
                JwtBasedAuthorizationFilter.Config.create(jwtProperties.prefix),
                users);
    }

//    @Bean
//    public OpenPGP.Config openPGPConfig() {
//        AppSecurityProperties.PGPProperties pgp = appSecurityProperties.pgp;
//        return OpenPGP.Config.create(pgp.userIdName, pgp.userIdEmailSuffix, pgp.passphrase, pgp.keySize);
//    }

    @Bean
    public Salts salts() throws NoSuchAlgorithmException {
        return new Salts(secureRandom());
    }

    @ConfigurationProperties(prefix = "security")
    @Data
    public static class AppSecurityProperties {
        private AesProperties aes;
        private CorsProperties cors;
        private JwtProperties jwt;
        private PasswordEncoderProperties passwordEncoder;
        private PGPProperties pgp;

        @Data
        public static class AesProperties {
            private int ivSize;
            private String key;
            private int keySize;
            private String mode;
            private String padding;
            private String provider;
            private String secureRandomAlgorithm;
        }

        @Data
        public static class CorsProperties {
            private String[] allowedHeaders;
            private String[] allowedMethods;
            private String[] allowedOrigins;
            private boolean allowCredentials;
            private String[] exposedHeaders;
            private String mapping;
            private int maxAge;
        }

        @Data
        public static class JwtProperties {
            private long lifetime;
            private String prefix;
            private String secret;
            private String signatureAlgorithm;
        }

        @Data
        public static class PasswordEncoderProperties {
            private int strength;
        }

        @Data
        public static class PGPProperties {
            private int keyRingCacheExpirationInHours;
            private boolean keyRingCacheInvalidateAtStartup;
            private int keySize;
            private String passphrase;
            private String userIdEmailSuffix;
            private String userIdName;
        }
    }
}
