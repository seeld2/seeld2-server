package be.seeld.users;

import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import java.util.List;

import static be.seeld.users.User.COL_PSEUDO;
import static be.seeld.users.User.TABLE;

public class UserRepository {

    private final IgniteCache<String, User> userCache;

    public UserRepository(IgniteCache<String, User> userCache) {
        this.userCache = userCache;
    }

    public long count() {
        SqlFieldsQuery query = new SqlFieldsQuery("select count(" + COL_PSEUDO + ") from " + TABLE);
        try (FieldsQueryCursor<List<?>> cursor = userCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            List<?> objects = all.get(0);
            return (Long) objects.get(0);
        }
    }

    public void delete(String pseudo) {
        //noinspection EmptyTryBlock
        try (
                FieldsQueryCursor<List<?>> ignored = userCache.query(new SqlFieldsQuery("" +
                        "delete from " + User.TABLE +
                        " where " + COL_PSEUDO + " = ?")
                        .setArgs(pseudo))
        ) {
        }
    }

    /**
     * Whether the specified pseudo exists or not.
     * Pseudo's case is ignored: the pseudo is lowercase'd at the query level.
     */
    public boolean exists(String pseudo) {
        return findOne(pseudo) != null;
    }

    /**
     * Find the user with the specified pseudo.
     * Pseudo's case is ignored: the pseudo is lowercase'd at the query level.
     */
    public User findOne(String pseudo) {
        SqlFieldsQuery query = new SqlFieldsQuery("SELECT" +
                " _key," + User.columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_PSEUDO + " = ?")
                .setArgs(pseudo.toLowerCase());
        try(FieldsQueryCursor<List<?>> cursor = userCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            return all.size() == 0 ? null : User.fromRowObjects(all.get(0));
        }
    }

    public <S extends User> S save(S entity) {
        //noinspection
        try (
                FieldsQueryCursor<List<?>> ignored = userCache.query(new SqlFieldsQuery("" +
                        "merge into " + TABLE +
                        " (_key, " + User.columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                        .setArgs(
                                entity.cacheKey(),
                                entity.getUsername(),
                                entity.getBoxAddress(),
                                entity.getCredentials(),
                                entity.getDeviceStorageKey(),
                                entity.getDisplayName(),
                                entity.getExchangeKey(),
                                entity.getPayload(),
                                entity.getPicture(),
                                entity.getSubscription(),
                                entity.getSubscriptionValidUntil()))
        ) {
            return entity;
        }
    }
}
