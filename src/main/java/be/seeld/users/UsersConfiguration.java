package be.seeld.users;

import be.seeld.IgniteCoreConfiguration;
import org.apache.ignite.Ignite;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

@Configuration
public class UsersConfiguration {

    private static final String SQL_SCHEMA = "PUBLIC";

    private static final String USER_CACHE = "UserCache";

    @Resource
    private Ignite ignite;
    @Resource
    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * This method is where updates on the repository's caches can be defined.<br/>
     * The typical example would be the deletion of an old, unused table: this cannot be done by SQL because those
     * tables have been defined with Java, so they need to be destroyed with Java.
     */
    @PostConstruct
    void postConstruct() {
        // Adds User.SUBSCRIPTION_VALID_UNTIL column if missing
        if (ignite.cacheNames().contains(USER_CACHE)) {
            SqlFieldsQuery query = new SqlFieldsQuery("ALTER TABLE " + User.TABLE +
                    " ADD COLUMN IF NOT EXISTS " + User.COL_SUBSCRIPTION_VALID_UNTIL + " varchar");
            //noinspection EmptyTryBlock
            try (FieldsQueryCursor<List<?>> ignored = ignite.getOrCreateCache(USER_CACHE).query(query)) {}
        }
    }

    @Bean
    public UserRepository userRepository() {
        CacheConfiguration<String, User> configuration = new CacheConfiguration<String, User>()
                .setSqlSchema(SQL_SCHEMA)
                .setName(USER_CACHE)
                .setCacheMode(CacheMode.PARTITIONED)
                .setIndexedTypes(String.class, User.class)
                .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
                .setBackups(igniteProperties.getBackupsAmount())
                .setAffinity(new RendezvousAffinityFunction(true));
        return new UserRepository(ignite.getOrCreateCache(configuration));
    }
}
