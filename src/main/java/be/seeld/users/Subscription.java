package be.seeld.users;

public enum Subscription {
    FREE(8);

    private final int days;

    Subscription(int days) {
        this.days = days;
    }

    public int messageLifeDays() {
        return days;
    }
}
