package be.seeld.users;

import be.seeld.common.ignite.Persistable;
import be.seeld.common.encryption.Encrypted;
import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.List;
import java.util.UUID;

import static be.seeld.common.encryption.EncryptionType.AES;
import static be.seeld.common.encryption.EncryptionType.PGP;

@Builder
@Data
public class User implements Persistable {

    public static final String TABLE = "USER";
    public static final String COL_PSEUDO = "USERNAME";
    public static final String COL_BOX_ADDRESS = "BOX_ADDRESS";
    public static final String COL_CREDENTIALS = "CREDENTIALS";
    public static final String COL_DEVICE_STORAGE_KEY = "DEVICE_STORAGE_KEY";
    public static final String COL_DISPLAY_NAME = "DISPLAY_NAME";
    public static final String COL_EXCHANGE_KEY = "EXCHANGE_KEY";
    public static final String COL_PAYLOAD = "PAYLOAD";
    public static final String COL_PICTURE = "PICTURE";
    public static final String COL_SUBSCRIPTION_TYPE = "SUBSCRIPTION_TYPE";
    public static final String COL_SUBSCRIPTION_VALID_UNTIL = "SUBSCRIPTION_VALID_UNTIL";

    public static String columnsAsCsv() {
        return String.join(",", new String[]{
                COL_PSEUDO,
                COL_BOX_ADDRESS,
                COL_CREDENTIALS,
                COL_DEVICE_STORAGE_KEY,
                COL_DISPLAY_NAME,
                COL_EXCHANGE_KEY,
                COL_PAYLOAD,
                COL_PICTURE,
                COL_SUBSCRIPTION_TYPE,
                COL_SUBSCRIPTION_VALID_UNTIL
        });
    }

    public static User fromRowObjects(List<?> rowObjects) { // TODO Test
        return User.builder()
                .username((String) rowObjects.get(1))
                .boxAddress((String) rowObjects.get(2))
                .credentials((String) rowObjects.get(3))
                .deviceStorageKey((String) rowObjects.get(4))
                .displayName((String) rowObjects.get(5))
                .exchangeKey((String) rowObjects.get(6))
                .payload((String) rowObjects.get(7))
                .picture((String) rowObjects.get(8))
                .subscription((String) rowObjects.get(9))
                .subscriptionValidUntil((String) rowObjects.get(10))
                .build();
    }

    @QuerySqlField(name = COL_PSEUDO, index = true, notNull = true)
    private String username;

    @Encrypted(AES)
    @QuerySqlField(name = COL_BOX_ADDRESS)
    private String boxAddress;

    @Encrypted(AES)
    @QuerySqlField(name = COL_CREDENTIALS)
    private String credentials;

    @Encrypted(AES)
    @QuerySqlField(name = COL_DEVICE_STORAGE_KEY)
    private String deviceStorageKey;

    @Encrypted(AES)
    @QuerySqlField(name = COL_DISPLAY_NAME)
    private String displayName;

    @Encrypted(AES)
    @QuerySqlField(name = COL_EXCHANGE_KEY)
    private String exchangeKey;

    @Encrypted(PGP)
    @QuerySqlField(name = COL_PAYLOAD)
    private String payload;

    @Encrypted(AES)
    @QuerySqlField(name = COL_PICTURE)
    private String picture;

    @Encrypted(AES)
    @QuerySqlField(name = COL_SUBSCRIPTION_TYPE)
    private String subscription;

    @Encrypted(AES)
    @QuerySqlField(name = COL_SUBSCRIPTION_VALID_UNTIL)
    private String subscriptionValidUntil;

    public UUID boxAddress() {
        return boxAddress != null ? UUID.fromString(boxAddress) : null;
    }

    public void boxAddress(UUID boxAddress) {
        this.boxAddress = boxAddress != null ? boxAddress.toString() : null;
    }

    @Override
    public String cacheKey() {
        return username;
    }

    public Subscription subscription() {
        return subscription != null ? Subscription.valueOf(subscription) : null;
    }

    public void subscription(Subscription subscription) {
        this.subscription = subscription != null ? subscription.name() : null;
    }

    // The Builder is still allowed to set a username with casing. We make sure to remove it
    public String getUsername() {
        return username != null ? username.toLowerCase() : null;
    }

    public void setUsername(String username) {
        this.username = username.toLowerCase();
    }

    @Builder
    @Data
    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Credentials extends JSONifiable {

        private Kdf kdf;
        private Srp srp;
        private SystemKeys systemKeys;

        @Builder
        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Kdf {
            private String type;
            private String salt;
        }

        @Builder
        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Srp {
            private String type;
            private String salt;
            private String verifier;
        }

        @Builder
        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static class SystemKeys {
            private String type;
            private String privateKeys;
            private String publicKeys;
        }
    }
}
