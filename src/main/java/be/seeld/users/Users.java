package be.seeld.users;

import be.seeld.common.encryption.Encryptable;
import be.seeld.common.encryption.Encryptables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;
import java.util.Optional;

@Service
public class Users {

    private final Encryptables encryptables;
    private final UserRepository userRepository;

    @Autowired
    public Users(Encryptables encryptables, UserRepository userRepository) {
        this.encryptables = encryptables;
        this.userRepository = userRepository;
    }

    public long count() {
        return userRepository.count();
    }

    public void delete(String username) {
        userRepository.delete(username);
    }

    public boolean exists(String username) {
        return userRepository.exists(username);
    }

    public Optional<Encryptable<User>> fetch(String username) {
        User user = userRepository.findOne(username);
        return user != null
                ? Optional.of(encryptables.ofEncrypted(user))
                : Optional.empty();
    }

    public User fetchUnencrypted(String username) {
        User user = userRepository.findOne(username);
        if (user == null) {
            throw new UserNotFoundException(MessageFormat.format("User {0} not found", username));
        }
        return encryptables.ofEncrypted(user).getUnencrypted();
    }

    public User store(User user) {

        User storedUser = userRepository.findOne(user.getUsername());

        if (storedUser != null) {
            Encryptable<User> encryptableStoredSeeldUser = encryptables.ofEncrypted(storedUser);
            user.setBoxAddress(encryptableStoredSeeldUser.getUnencrypted().getBoxAddress());
        }

        userRepository.save(encryptables.ofUnencrypted(user).getEncrypted());

        return user;
    }

    public User updatePayload(User user, String payload) {
        user.setPayload(payload);
        userRepository.save(encryptables.ofUnencrypted(user).getEncrypted());
        return user;
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User not found")
    public static class UserNotFoundException extends RuntimeException {
        public UserNotFoundException(String message) {
            super(message);
        }
    }
}
