package be.seeld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebCallTracer implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebCallTracer.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        LOGGER.trace("Call with origin " + request.getHeader(HttpHeaders.ORIGIN));
        return true;
    }
}
