package be.seeld.conversations.messages;

import be.seeld.common.lang.Collections;
import be.seeld.common.lang.Strings;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static be.seeld.conversations.messages.Message.COL_BOX_ADDRESS;
import static be.seeld.conversations.messages.Message.COL_CONVERSATION_ID;
import static be.seeld.conversations.messages.Message.COL_MESSAGE_ID;
import static be.seeld.conversations.messages.Message.TABLE;
import static be.seeld.conversations.messages.Message.columnsAsCsv;

public class MessageRepository {

    private final IgniteCache<String, Message> messageCache;

    public MessageRepository(IgniteCache<String, Message> messageCache) {
        this.messageCache = messageCache;
    }

    public void delete(UUID boxAddress, UUID conversationId, UUID messageId) {
        //noinspection EmptyTryBlock
        try (
                FieldsQueryCursor<List<?>> ignored = messageCache.query(new SqlFieldsQuery("" +
                        "DELETE FROM " + TABLE +
                        " WHERE " + COL_BOX_ADDRESS + " = ?" +
                        " AND " + COL_CONVERSATION_ID + " = ?" +
                        " AND " + COL_MESSAGE_ID + " = ?")
                        .setArgs(boxAddress, conversationId, messageId))
        ) {
        }
    }

    void deleteAllForBoxAddresses(Set<UUID> boxAddresses) {

        List<Set<UUID>> subsets = Collections.partitionOf(boxAddresses, 20);

        for (Set<UUID> set : subsets) {

            SqlFieldsQuery query = new SqlFieldsQuery("" +
                    "DELETE FROM " + TABLE +
                    " WHERE " + COL_BOX_ADDRESS + " IN (" + Strings.repeatAndJoin("?", set.size(), ",") + ")");
            List<Object> argsList = new ArrayList<>(set);
            query.setArgs(argsList.toArray());
            //noinspection EmptyTryBlock
            try (
                    FieldsQueryCursor<List<?>> ignored = messageCache.query(query)
            ) {
            }
        }
    }

    void deleteAllForConversationId(UUID conversationId) {
        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "DELETE FROM " + TABLE +
                " WHERE " + COL_CONVERSATION_ID + " = ?")
                .setArgs(conversationId);
        //noinspection EmptyTryBlock
        try (
                FieldsQueryCursor<List<?>> ignored = messageCache.query(query)
        ) {
        }
    }

    Message fetch(UUID boxAddress, UUID conversationId, UUID messageId) {
        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT _key," + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_BOX_ADDRESS + " = ?" +
                " AND " + COL_CONVERSATION_ID + " = ?" +
                " AND " + COL_MESSAGE_ID + " = ?")
                .setArgs(boxAddress, conversationId, messageId);
        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            return all.size() == 0 ? null : Message.fromRowObjectsWithKeyToIgnore(all.get(0));
        }
    }

    List<Message> fetchAllForConversationIdWithBoxAddresses(UUID conversationId, Set<UUID> boxAddresses) {
        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_CONVERSATION_ID + " = ?" +
                " AND " + COL_BOX_ADDRESS + " IN (" + Strings.repeatAndJoin("?", boxAddresses.size(), ",") + ")");

        List<Object> argsList = new ArrayList<>();
        argsList.add(conversationId);
        argsList.addAll(boxAddresses);

        query.setArgs(argsList.toArray());

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            return all.stream().map(Message::fromRowObjects).collect(Collectors.toList());
        }
    }

    List<Message> findByConversationIdOrderByTimelineDesc(UUID conversationId) {
        SqlFieldsQuery query = new SqlFieldsQuery("SELECT " +
                " _key," + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_CONVERSATION_ID + " = ? ORDER BY TIMELINE DESC")
                .setArgs(conversationId);
        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return StreamSupport.stream(cursor.spliterator(), false)
                    .map(Message::fromRowObjectsWithKeyToIgnore)
                    .collect(Collectors.toList());
        }
    }

    List<Message> findByConversationIdOrderByTimelineDesc(UUID conversationId, Pageable pageable) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + Message.COL_CONVERSATION_ID + " = ?" +
                " ORDER BY " + Message.COL_TIMELINE + " DESC" +
                " LIMIT " + pageable.getPageSize() + " OFFSET " + pageable.getOffset());

        query.setArgs(conversationId);

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return map(cursor.getAll());
        }
    }

    List<Message> findByBoxAddressInAndNotifiedIsFalse(Set<UUID> boxAddresses) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE + " M" +
                " JOIN TABLE (BA VARCHAR = ?) I" +
                "  ON M." + Message.COL_BOX_ADDRESS + " = I.BA" +
                " WHERE " + Message.COL_NOTIFIED + " = FALSE" +
                " ORDER BY " + Message.COL_TIMELINE + " DESC");

        query.setArgs(new Object[]{boxAddresses.stream().map(UUID::toString).toArray(String[]::new)});

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return map(cursor.getAll());
        }
    }

    List<Message> findByMessagesIdsInAndConversationId(UUID conversationId, Set<UUID> messagesIds) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + Message.COL_CONVERSATION_ID + " = ?" +
                " AND " + Message.COL_MESSAGE_ID + " IN (" + Strings.repeatAndJoin("?", messagesIds.size(), ",") + ")");

        List<Object> argsList = new ArrayList<>();
        argsList.add(conversationId);
        argsList.addAll(messagesIds);

        query.setArgs(argsList.toArray());

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return map(cursor.getAll());
        }
    }

    List<UUID> findIdsByConversationId(UUID conversationId) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + Message.COL_MESSAGE_ID +
                " FROM " + TABLE +
                " WHERE " + Message.COL_CONVERSATION_ID + " = ?");

        query.setArgs(conversationId);

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return cursor.getAll().stream()
                    .map(row -> (UUID) row.get(0))
                    .collect(Collectors.toList());
        }
    }

    List<UUID> findIdsByConversationId(UUID conversationId, Pageable pageable) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + Message.COL_MESSAGE_ID + ", " + Message.COL_TIMELINE +
                " FROM " + TABLE +
                " WHERE " + Message.COL_CONVERSATION_ID + " = ?" +
                " ORDER BY " + Message.COL_TIMELINE + " DESC" +
                " LIMIT " + pageable.getPageSize() + " OFFSET " + pageable.getOffset());

        query.setArgs(conversationId);

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            return cursor.getAll().stream()
                    .map(row -> (UUID) row.get(0))
                    .collect(Collectors.toList());
        }
    }

    @SuppressWarnings("DuplicatedCode")
    public Optional<Message> findLatestByBoxAddressInAndNotifiedIsFalse(Set<UUID> boxAddresses) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE + " M" +
                " INNER JOIN (" +
                "  SELECT " +
                "   M." + Message.COL_CONVERSATION_ID + " C_ID," +
                "   MAX(M." + Message.COL_TIMELINE + ") LATEST" +
                "  FROM " + TABLE + " M" +
                "  JOIN TABLE (BOX_ADDRESS VARCHAR = ?) BA ON M." + Message.COL_BOX_ADDRESS + " = BA.BOX_ADDRESS" +
                "  WHERE M." + Message.COL_NOTIFIED + " = FALSE" +
                "  GROUP BY C_ID" +
                "  ORDER BY LATEST DESC" +
                "  LIMIT 1" +
                " ) CONVERSATIONS" +
                " ON M." + Message.COL_CONVERSATION_ID + " = CONVERSATIONS.C_ID" +
                " AND M." + Message.COL_TIMELINE + " = CONVERSATIONS.LATEST");

        query.setArgs(new Object[]{boxAddresses.stream().map(UUID::toString).toArray(String[]::new)});

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<Message> messages = map(cursor.getAll());
            return messages.size() > 0 ? Optional.of(messages.get(0)) : Optional.empty();
        }
    }

    @SuppressWarnings("Duplicates")
    Page<Message> findLatestByConversationIdsInOrderByTimelineDesc(
            Set<UUID> conversationIds, Pageable pageable) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() + "" +
                " FROM " + TABLE + " M" +
                " INNER JOIN (" +
                "  SELECT " +
                "   M." + Message.COL_CONVERSATION_ID + " C_ID," +
                "   MAX(M." + Message.COL_TIMELINE + ") LATEST" +
                "  FROM " + TABLE + " M" +
                "  JOIN TABLE (CONVERSATION_ID VARCHAR = ?) INCI ON M." + Message.COL_CONVERSATION_ID + " = INCI.CONVERSATION_ID" +
                "  GROUP BY C_ID" +
                "  ORDER BY LATEST DESC" +
                "  LIMIT " + pageable.getPageSize() + " OFFSET " + pageable.getOffset() +
                " ) CONVERSATIONS" +
                " ON M." + Message.COL_CONVERSATION_ID + " = CONVERSATIONS.C_ID" +
                " AND M." + Message.COL_TIMELINE + " = CONVERSATIONS.LATEST");

        query.setArgs(new Object[]{conversationIds.stream().map(UUID::toString).toArray(String[]::new)});

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<Message> conversations = map(cursor.getAll());
            return new PageImpl<>(conversations);
        }
    }

    /**
     * Returns the latest messages representing all the distinct conversations in the specified box addresses.
     * In other words: for each box address in the specified list, for each distinct conversation ID in that box
     * address, fetch the latest message. <br/>
     * The list of messages representing each conversation is sorted in descending order.<br/>
     *
     * @param boxAddresses The list of box addresses' UUIDs to fetch the conversations from.
     * @param pageable     A {@link Pageable} that defines the subset of results to be returned.
     * @return A {@link Page} containing the results.
     */
    @SuppressWarnings("Duplicates")
    Page<Message> findLatestDistinctConversationIdByBoxAddressInOrderByTimelineDesc(
            Set<UUID> boxAddresses, Pageable pageable) {

        SqlFieldsQuery query = new SqlFieldsQuery("" +
                "SELECT " + columnsAsCsv() +
                " FROM " + TABLE + " M" +
                " INNER JOIN (" +
                "  SELECT " +
                "   M." + Message.COL_CONVERSATION_ID + " C_ID," +
                "   MAX(M." + Message.COL_TIMELINE + ") LATEST" +
                "  FROM " + TABLE + " M" +
                "  JOIN TABLE (BOX_ADDRESS VARCHAR = ?) BA ON M." + Message.COL_BOX_ADDRESS + " = BA.BOX_ADDRESS" +
                "  GROUP BY C_ID" +
                "  ORDER BY LATEST DESC" +
                "  LIMIT " + pageable.getPageSize() + " OFFSET " + pageable.getOffset() +
                " ) CONVERSATIONS" +
                " ON M." + Message.COL_CONVERSATION_ID + " = CONVERSATIONS.C_ID" +
                " AND M." + Message.COL_TIMELINE + " = CONVERSATIONS.LATEST");

        query.setArgs(new Object[]{boxAddresses.stream().map(UUID::toString).toArray(String[]::new)});

        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<Message> conversations = map(cursor.getAll());
            return new PageImpl<>(conversations);
        }
    }

    Message findOne(UUID boxAddress, UUID messageId) {
        SqlFieldsQuery query = new SqlFieldsQuery("SELECT " +
                " _key," + columnsAsCsv() +
                " FROM " + TABLE +
                " WHERE " + COL_BOX_ADDRESS + " = ? AND " + COL_MESSAGE_ID + " = ?")
                .setArgs(boxAddress, messageId);
        try (FieldsQueryCursor<List<?>> cursor = messageCache.query(query)) {
            List<List<?>> all = cursor.getAll();
            return all.size() == 0 ? null : Message.fromRowObjectsWithKeyToIgnore(all.get(0));
        }
    }

    Message save(Message message) {
        //noinspection
        try (FieldsQueryCursor<List<?>> ignored = messageCache.query(new SqlFieldsQuery("" +
                "MERGE INTO " + TABLE +
                " (_key," + columnsAsCsv() + ")" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?)")
                .setArgs(message.cacheKey(),
                        message.getBoxAddress(),
                        message.getConversationId(),
                        message.getMessageId(),
                        message.isNotified(),
                        message.getTimeline(),
                        message.getPayload(),
                        message.getHmacHash()))) {
            return message;
        }
    }

    private List<Message> map(List<List<?>> rows) {
        return rows.stream()
                .map(row -> Message.builder()
                        .boxAddress((UUID) row.get(0))
                        .conversationId((UUID) row.get(1))
                        .messageId((UUID) row.get(2))
                        .notified((Boolean) row.get(3))
                        .timeline((Long) row.get(4))
                        .payload((String) row.get(5))
                        .hmacHash((String) row.get(6))
                        .build())
                .collect(Collectors.toList());
    }
}
