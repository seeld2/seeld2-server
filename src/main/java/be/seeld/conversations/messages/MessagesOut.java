package be.seeld.conversations.messages;

import be.seeld.common.json.JSONifiable;
import be.seeld.users.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class MessagesOut extends JSONifiable {

    private List<MessageOut> messages;

    MessageOut senderMessage(User user) {
        return messages.stream()
                .filter(message -> message.getBoxAddress().equals(user.boxAddress()))
                .findFirst()
                .orElseThrow(() -> new MessagesProcess
                        .MessagesProcessException("No sender message found in messages to send for user "
                        + user.getUsername()));
    }

    List<MessageOut> receiverMessages(User user) {
        return messages.stream()
                .filter(message -> !message.getBoxAddress().equals(user.boxAddress()))
                .collect(Collectors.toList());
    }
}


