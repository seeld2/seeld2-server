package be.seeld.conversations.messages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageIn {

    @NotNull
    private UUID conversationId;

    @NotNull
    private UUID messageId;

    @NotEmpty
    private String payload;

    @NotNull
    private UUID readFromBoxAddress;

    public static List<MessageIn> from(List<Message> messages) {
        return messages.stream()
                .map(message -> new MessageIn(
                        message.getConversationId(),
                        message.getMessageId(),
                        message.getPayload(),
                        message.getBoxAddress()))
                .collect(Collectors.toList());
    }
}
