package be.seeld.conversations.messages.life;

import be.seeld.common.ignite.Persistable;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class MessageLife implements Persistable {

    public static final String TABLE = "MESSAGELIFE";

    public static final String COL_BOX_ADDRESS = "BOX_ADDRESS";
    public static final String COL_CONVERSATION_ID = "CONVERSATION_ID";
    public static final String COL_EXPIRES_ON = "EXPIRES_ON";
    public static final String COL_MESSAGE_ID = "MESSAGE_ID";

    public static String columnsAsCsv() {
        return String.join(",", COL_EXPIRES_ON, COL_BOX_ADDRESS, COL_CONVERSATION_ID, COL_MESSAGE_ID);
    }

    public static MessageLife fromRowObjects(List<?> rowObjects) { // TODO Test
        return MessageLife.builder()
                .expiresOn((Long) rowObjects.get(1))
                .boxAddress((UUID) rowObjects.get(2))
                .conversationId((UUID) rowObjects.get(3))
                .messageId((UUID) rowObjects.get(4))
                .build();
    }

    @QuerySqlField(name = COL_EXPIRES_ON, index = true, notNull = true)
    private long expiresOn;

    @QuerySqlField(name = COL_BOX_ADDRESS, index = true, notNull = true)
    private UUID boxAddress;

    @QuerySqlField(name = COL_CONVERSATION_ID, notNull = true)
    private UUID conversationId;

    @QuerySqlField(name = COL_MESSAGE_ID, notNull = true)
    private UUID messageId;

    @Override
    public String cacheKey() {
        return boxAddress.toString() + "_" + conversationId.toString() + "_" + messageId.toString();
    }
}
