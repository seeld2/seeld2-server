package be.seeld.conversations.messages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageOut {

    MessageOut(UUID boxAddress,
               UUID conversationId,
               String payload) {
        this.boxAddress = boxAddress;
        this.conversationId = conversationId;
        this.payload = payload;
    }

    private UUID boxAddress;
    private UUID conversationId;
    private String hmacHash;
    private String payload;
}
