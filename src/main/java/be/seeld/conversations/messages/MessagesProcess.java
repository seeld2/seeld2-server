package be.seeld.conversations.messages;

import be.seeld.notifications.NotificationsDispatcher;
import be.seeld.users.User;
import be.seeld.users.Users;
import be.seeld.common.encryption.aes.AES;
import be.seeld.common.time.Dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessagesProcess {

    private static final int TIMELINE_RANDOMIZATION_BOUND = 1000;

    private final AES aes;
    private final Dates dates;
    private final Messages messages;
    private final NotificationsDispatcher notificationsDispatcher;
    private final SecureRandom secureRandom;
    private final Users users;

    @Autowired
    MessagesProcess(@Qualifier("pwaAesBase64") AES aes,
                    Dates dates,
                    Messages messages,
                    NotificationsDispatcher notificationsDispatcher,
                    SecureRandom secureRandom,
                    Users users) {

        this.aes = aes;
        this.dates = dates;
        this.messages = messages;
        this.notificationsDispatcher = notificationsDispatcher;
        this.secureRandom = secureRandom;
        this.users = users;
    }

    Optional<Long> fetchLatestNewMessageTimeline(Set<UUID> boxAddresses) {
        return messages.fetchLatestUnnotifiedTimelineByBoxAddresses(boxAddresses);
    }

    List<Message> fetchNew(Map<UUID, String> hmacHashesByBoxAddress) {
        Set<UUID> boxAddresses = hmacHashesByBoxAddress.keySet();
        return this.messages.fetchAllUnnotifiedByBoxAddresses(boxAddresses).stream()
                .filter(message -> message.getHmacHash() == null
                        || message.getHmacHash().equals(hmacHashesByBoxAddress.get(message.getBoxAddress())))
                .collect(Collectors.toList());
    }

    /**
     * Stores the specified bundle of AES-encrypted messages sent by the specified pseudo.
     *
     * @return The ID of the pseudo's stored message, AES-encrypted
     */
    int[] store(String pseudo, int[] aesPayload) {
        ZonedDateTime now = dates.zonedDateTimeUtc();
        User user = users.fetchUnencrypted(pseudo);

        MessagesOut messagesOut = decryptMessageOuts(aesPayload, user.getExchangeKey());

        UUID senderMessageId = storeSenderMessage(messagesOut.senderMessage(user), now);

        storeReceiversMessages(messagesOut.receiverMessages(user), now);

        return aes.encryptToUnsignedBytes(senderMessageId.toString(), user.getExchangeKey());
    }

    private MessagesOut decryptMessageOuts(int[] aesPayload, String exchangeKey) {
        String json = aes.decrypt(aesPayload, exchangeKey);
        try {
            return MessagesOut.fromJSON(json, MessagesOut.class);
        } catch (IOException e) {
            throw new MessagesProcessException("Could not map JSON to object", e);
        }
    }

    private UUID storeMessage(MessageOut message, boolean notified, ZonedDateTime now) {
        UUID messageId = UUID.randomUUID();
        long timeline = now.toInstant().plusMillis(secureRandom.nextInt(TIMELINE_RANDOMIZATION_BOUND)).toEpochMilli();
        this.messages.store(Message.builder()
                .boxAddress(message.getBoxAddress())
                .conversationId(message.getConversationId())
                .messageId(messageId)
                .notified(notified)
                .timeline(timeline)
                .payload(message.getPayload())
                .hmacHash(message.getHmacHash())
                .build());
        return messageId;
    }

    private void storeReceiversMessages(List<MessageOut> messages, ZonedDateTime now) {
        messages.forEach(message -> {
            storeMessage(message, false, now);
            notificationsDispatcher.notifyOfNewMessage(message.getBoxAddress());
        });
    }

    private UUID storeSenderMessage(MessageOut senderMessage, ZonedDateTime now) {
        return storeMessage(senderMessage, true, now);
    }

    static final class MessagesProcessException extends RuntimeException {

        MessagesProcessException(String message) {
            super(message);
        }

        MessagesProcessException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
