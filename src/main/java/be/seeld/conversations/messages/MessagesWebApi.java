package be.seeld.conversations.messages;

import be.seeld.common.json.JSONifiable;
import be.seeld.common.web.AESRequest;
import be.seeld.common.web.AESResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/messages")
public final class MessagesWebApi {

    private final MessagesProcess messagesProcess;

    @Autowired
    public MessagesWebApi(MessagesProcess messagesProcess) {
        this.messagesProcess = messagesProcess;
    }

    /**
     * Fetches the latest new message's timeline, if new messages exist for the specified box addresses.
     *
     * @param boxAddressesRequest A value object containing the box addresses whose new messages must be retrieved
     * @return a {@link TimelineResponse} that eventually contains the timeline of the latest new message
     */
    @RequestMapping(value = "/new/timeline/latest", method = PUT,
            consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> fetchLatestNewMessageTimeline(@Valid @RequestBody BoxAddressesRequest boxAddressesRequest) {
        Optional<Long> timeline = messagesProcess.fetchLatestNewMessageTimeline(boxAddressesRequest.getBoxAddresses());
        TimelineResponse timelineResponse = timeline.map(TimelineResponse::new).orElse(TimelineResponse.noTimeline());
        return ResponseEntity.status(CREATED).body(timelineResponse);
    }

    /**
     * Fetches all new (unread) messages belonging to the specified box addresses.
     */
    @RequestMapping(value = "/new", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> fetchNewMessages(@Valid @RequestBody NewMessagesRequest newMessagesRequest) {
        Map<UUID, String> hmacHashesByBoxAddress = newMessagesRequest.getHmacHashesByBoxAddress();
        List<Message> messages = messagesProcess.fetchNew(hmacHashesByBoxAddress);
        return ResponseEntity.status(CREATED).body(new MessagesInResponse(MessageIn.from(messages)));
    }

    /**
     * Stores the messages contained in the encrypted payload.
     *
     * @param aesRequest A value object with the encrypted payload that contains the messages to decrypt
     * @param principal  The currently-logged user
     * @return An {@link AESResponse} containing the sender' stored message's generated ID
     */
    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> storeMessages(@RequestBody AESRequest aesRequest, Principal principal) {
        int[] aesEncryptedMessageId = messagesProcess.store(principal.getName(), aesRequest.getPayload());
        return ResponseEntity.status(CREATED).body(new AESResponse(aesEncryptedMessageId));
    }

    @Builder
    @Data
    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class BoxAddressesRequest extends JSONifiable {
        @NotEmpty
        private Set<UUID> boxAddresses;
    }

    @Builder
    @Data
    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class NewMessagesRequest extends JSONifiable {
        @NotEmpty
        private Map<UUID, String> hmacHashesByBoxAddress;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class TimelineResponse {

        public static TimelineResponse noTimeline() {
            return new TimelineResponse();
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Long timeline;
    }
}
