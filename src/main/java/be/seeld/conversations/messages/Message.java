package be.seeld.conversations.messages;

import be.seeld.common.ignite.Persistable;
import be.seeld.common.encryption.Encrypted;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.List;
import java.util.UUID;

import static be.seeld.common.encryption.EncryptionType.*;

@Data
@Builder
public class Message implements Persistable {

    public static final String TABLE = "MESSAGE";
    public static final String COL_BOX_ADDRESS = "BOX_ADDRESS";
    public static final String COL_CONVERSATION_ID = "CONVERSATION_ID";
    public static final String COL_HMAC_HASH = "HMAC_HASH";
    public static final String COL_MESSAGE_ID = "MESSAGE_ID";
    public static final String COL_NOTIFIED = "NOTIFIED";
    public static final String COL_PAYLOAD = "PAYLOAD";
    public static final String COL_TIMELINE = "TIMELINE";

    public static String columnsAsCsv() {
        return String.join(",",
                COL_BOX_ADDRESS,
                COL_CONVERSATION_ID,
                COL_MESSAGE_ID,
                COL_NOTIFIED,
                COL_TIMELINE,
                COL_PAYLOAD,
                COL_HMAC_HASH);
    }

    public static Message fromRowObjects(List<?> rowObjects) { // TODO Test
        return Message.builder()
                .boxAddress((UUID) rowObjects.get(0))
                .conversationId((UUID) rowObjects.get(1))
                .messageId((UUID) rowObjects.get(2))
                .notified((Boolean) rowObjects.get(3))
                .timeline((Long) rowObjects.get(4))
                .payload((String) rowObjects.get(5))
                .hmacHash((String) rowObjects.get(6))
                .build();
    }

    public static Message fromRowObjectsWithKeyToIgnore(List<?> rowObjects) { // TODO Test
        return Message.builder()
                .boxAddress((UUID) rowObjects.get(1))
                .conversationId((UUID) rowObjects.get(2))
                .messageId((UUID) rowObjects.get(3))
                .notified((Boolean) rowObjects.get(4))
                .timeline((Long) rowObjects.get(5))
                .payload((String) rowObjects.get(6))
                .hmacHash((String) rowObjects.get(7))
                .build();
    }

    @QuerySqlField(name = COL_BOX_ADDRESS, index = true, notNull = true)
    private UUID boxAddress;

    @QuerySqlField(name = COL_CONVERSATION_ID, index = true, notNull = true)
    private UUID conversationId;

    @QuerySqlField(name = COL_HMAC_HASH)
    private String hmacHash;

    @QuerySqlField(name = COL_MESSAGE_ID, index = true, notNull = true)
    private UUID messageId;

    @QuerySqlField(name = COL_NOTIFIED, index = true, notNull = true)
    private boolean notified;

    @Encrypted(PGP)
    @QuerySqlField(name = COL_PAYLOAD, notNull = true)
    private String payload;

    @QuerySqlField(name = COL_TIMELINE, notNull = true)
    private long timeline; // Don't AES-encrypt this: the queries use it to fetch messages in specific order

    public String cacheKey() {
        return boxAddress.toString() + "_" + messageId.toString();
    }
}
