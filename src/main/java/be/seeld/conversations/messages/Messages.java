package be.seeld.conversations.messages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class Messages { // TODO Rename to MessagesService

    private final MessageRepository messageRepository;

    @Autowired
    public Messages(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void delete(UUID boxAddress, UUID conversationId, UUID messageId) {
        messageRepository.delete(boxAddress, conversationId, messageId);
    }

    public void deleteAllWithBoxAddresses(Set<UUID> boxAddresses) {
        messageRepository.deleteAllForBoxAddresses(boxAddresses);
    }

    public void deleteConversation(UUID conversationId, Map<UUID, String> hmacHashesByBoxAddress) {
        Set<UUID> boxAddresses = hmacHashesByBoxAddress.keySet();
        // TODO Ideally we should avoid fetching Messages here, because of possible mem overflow. Instead fetch smaller "MessageInfo" or similar which would contain only the necessary information
        List<Message> messages = this.messageRepository.fetchAllForConversationIdWithBoxAddresses(conversationId, boxAddresses);

        Set<UUID> boxAddressesWithoutAuthorizationToDelete = new HashSet<>();
        for (Message message : messages) {
            if (message.getHmacHash() == null) {
                continue;
            }
            String hmacHashForBoxAddress = hmacHashesByBoxAddress.get(message.getBoxAddress());
            if (hmacHashForBoxAddress == null) {
                log.warn("\"null\" hmacHash was provided for box address " + message.getBoxAddress());
                boxAddressesWithoutAuthorizationToDelete.add(message.getBoxAddress());
                continue;
            }
            if (!message.getHmacHash().equals(hmacHashForBoxAddress)) {
                boxAddressesWithoutAuthorizationToDelete.add(message.getBoxAddress());
            }
        }

        if (boxAddressesWithoutAuthorizationToDelete.size() > 0) {
            String hashes = hmacHashesByBoxAddress.keySet().stream()
                    .map(boxAddress -> "box address: " + boxAddress + " | hash: " + hmacHashesByBoxAddress.get(boxAddress))
                    .collect(Collectors.joining(","));
            String boxes = boxAddressesWithoutAuthorizationToDelete.stream()
                    .map(boxAddress -> "box address: " + boxAddress)
                    .collect(Collectors.joining(","));
            log.warn("The conversation " + conversationId
                    + "could not be deleted because the following box addresses raised issues: " + boxes
                    + " These were raised with the following hashes: " + hashes);
        } else {
            this.messageRepository.deleteAllForConversationId(conversationId);
        }
    }

    public Optional<Message> fetch(UUID boxAddress, UUID conversationId, UUID messageId) {
        return Optional.ofNullable(messageRepository.fetch(boxAddress, conversationId, messageId));
    }

    public List<Message> fetchAllByMessagesIdsForConversationId(UUID conversationId, Set<UUID> messagesIds) {
        return messageRepository.findByMessagesIdsInAndConversationId(conversationId, messagesIds);
    }

    public List<Message> fetchAllForConversationId(UUID conversationId, int page, int pageSize) {
        return messageRepository.findByConversationIdOrderByTimelineDesc(conversationId, PageRequest.of(page, pageSize));
    }

    public List<Message> fetchAllLatestByBoxAddressesUnreadFirst(Set<UUID> boxAddresses, int page, int pageSize) {
        return messageRepository
                .findLatestDistinctConversationIdByBoxAddressInOrderByTimelineDesc(boxAddresses,
                        PageRequest.of(page, pageSize))
                .getContent();
    }

    public List<Message> fetchAllLatestByConversationIdsUnreadFirst(Set<UUID> conversationIds, int page, int pageSize) {
        return messageRepository
                .findLatestByConversationIdsInOrderByTimelineDesc(conversationIds, PageRequest.of(page, pageSize))
                .getContent();
    }

    public List<Message> fetchAllUnnotifiedByBoxAddresses(Set<UUID> boxAddresses) {
        List<Message> messages = messageRepository.findByBoxAddressInAndNotifiedIsFalse(boxAddresses);
        messages.forEach(message -> {
            message.setNotified(true);
            messageRepository.save(message);
        });
        return messages;
    }

    public Optional<Long> fetchLatestUnnotifiedTimelineByBoxAddresses(Set<UUID> boxAddresses) {
        Optional<Message> latestMessage = messageRepository.findLatestByBoxAddressInAndNotifiedIsFalse(boxAddresses);
        return latestMessage.map(Message::getTimeline);
    }

    public Message store(Message message) {
        return messageRepository.save(message);
    }
}
