package be.seeld.conversations;

import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.MessageIn;
import be.seeld.conversations.messages.MessagesInResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/conversations")
public final class ConversationsWebApi {

    private final ConversationsProcess conversationsProcess;

    @Autowired
    public ConversationsWebApi(ConversationsProcess conversationsProcess) {
        this.conversationsProcess = conversationsProcess;
    }

    @RequestMapping(value = "/{conversationId}/state/deleted", method = PUT)
    ResponseEntity<?> deleteConversation(@PathVariable UUID conversationId,
                                         @RequestBody ConversationDeleteRequest request) {
        conversationsProcess.deleteConversation(conversationId, request.getHmacHashesByBoxAddress());
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{conversationId}/messages/{messageId}/state/deleted", method = PUT)
    ResponseEntity<?> deleteMessage(@PathVariable UUID conversationId,
                                    @PathVariable UUID messageId,
                                    @RequestBody MessageDeleteRequest request) {
        conversationsProcess.deleteMessage(request.getBoxAddress(), conversationId, messageId, request.getHmacHash());
        return ResponseEntity.noContent().build();
    }

    /**
     * Fetches the messages that are part of an identified conversation and that match the specified IDs.
     */
    @RequestMapping(value = "/{conversationId}/messages", method = PUT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchConversationMessages(@PathVariable UUID conversationId,
                                                       @RequestBody ConversationMessagesRequest request) {

        List<Message> messages;
        if (request.isForFetchingMessagesById()) {
            messages = conversationsProcess.fetchConversationMessages(
                    conversationId,
                    request.getMessagesIds(),
                    request.getHmacHashesByBoxAddress());
        } else {
            messages = conversationsProcess.fetchConversationMessages(
                    conversationId,
                    request.getHmacHashesByBoxAddress(),
                    request.getPage(),
                    request.getPageSize()
            );
        }

        MessagesInResponse response = new MessagesInResponse(MessageIn.from(messages));

        return ResponseEntity.status(CREATED).body(response);
    }

    /**
     * Fetches the list of message IDs that are part of the identified conversation.
     */
    @RequestMapping(value = "/{conversationId}/messages/ids", method = PUT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchConversationMessagesIds(@PathVariable UUID conversationId,
                                                          @RequestBody ConversationMessagesIdsRequest request) {

        Set<UUID> messagesIds = conversationsProcess.fetchConversationMessagesIds(
                conversationId,
                request.getHmacHashesByBoxAddress(),
                request.getPage(),
                request.getPageSize()
        );

        ConversationMessagesIdsResponse response = new ConversationMessagesIdsResponse(messagesIds);

        return ResponseEntity.status(CREATED).body(response);
    }

    /**
     * Fetches the conversations found for a list of box addresses or conversation IDs.
     */
    @RequestMapping(method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchConversations(@RequestBody ConversationsRequest conversationsRequest) {

        List<Message> conversations;
        if (conversationsRequest.isForFetchingConversationsByIds()) {
            conversations = conversationsProcess.fetchConversationsFromConversationIds(
                    conversationsRequest.getConversationIds(),
                    conversationsRequest.getHmacHashesByBoxAddress());
        } else {
            conversations = conversationsProcess.fetchConversationsFromBoxAddresses(
                    conversationsRequest.getHmacHashesByBoxAddress(),
                    conversationsRequest.getPage(),
                    conversationsRequest.getPageSize());
        }

        ConversationsResponse response = new ConversationsResponse(
                conversations.stream()
                        .map(conversation -> new ConversationsResponse.Conversation(
                                conversation.getConversationId(),
                                conversation.getMessageId(),
                                conversation.getPayload(),
                                conversation.getBoxAddress()))
                        .collect(Collectors.toList()));

        return ResponseEntity.status(CREATED).contentType(APPLICATION_JSON).body(response);
    }

    /**
     * Fetches conversation IDs from a ConversationsRequest' box addresses.
     */
    @RequestMapping(value = "/ids", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchConversationIds(@RequestBody ConversationIdsRequest conversationIdsRequest) {

        Set<UUID> conversationIds = conversationsProcess.fetchConversationsIds(
                conversationIdsRequest.getHmacHashesByBoxAddress(),
                conversationIdsRequest.getPage(),
                conversationIdsRequest.getPageSize());

        ConversationIdsResponse response = new ConversationIdsResponse(conversationIds);

        return ResponseEntity.status(CREATED).contentType(APPLICATION_JSON).body(response);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationDeleteRequest {
        private Map<UUID, String> hmacHashesByBoxAddress;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationIdsRequest {
        private Map<UUID, String> hmacHashesByBoxAddress;
        private int page;
        private int pageSize;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationIdsResponse {
        private Set<UUID> conversationIds;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationMessagesIdsRequest {
        private Map<UUID, String> hmacHashesByBoxAddress;
        private int page;
        private int pageSize;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationMessagesIdsResponse {
        private Set<UUID> messagesIds;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationMessagesRequest {

        private Map<UUID, String> hmacHashesByBoxAddress;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Set<UUID> messagesIds;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer page;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer pageSize;

        public boolean isForFetchingMessagesById() {
            return messagesIds != null && !messagesIds.isEmpty();
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationsRequest {

        public static ConversationsRequest with(Map<UUID, String> hmacHashesByBoxAddress, int page, int pageSize) {
            ConversationsRequest conversationsRequest = new ConversationsRequest();
            conversationsRequest.hmacHashesByBoxAddress = hmacHashesByBoxAddress;
            conversationsRequest.page = page;
            conversationsRequest.pageSize = pageSize;
            return conversationsRequest;
        }

        private Set<UUID> conversationIds = new HashSet<>();
        private Map<UUID, String> hmacHashesByBoxAddress = new HashMap<>();
        private int page;
        private int pageSize;

        public boolean isForFetchingConversationsByIds() {
            return conversationIds != null && !conversationIds.isEmpty();
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class ConversationsResponse {

        private List<Conversation> conversations;

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static final class Conversation {
            private UUID conversationId;
            private UUID messageId;
            private String payload;
            private UUID readFromBoxAddress;
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class MessageDeleteRequest {
        private UUID boxAddress;
        private String hmacHash;
    }
}
