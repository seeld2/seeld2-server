package be.seeld.conversations;

import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class ConversationsProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConversationsProcess.class);

    private final Messages messages;

    @Autowired
    ConversationsProcess(Messages messages) {
        this.messages = messages;
    }

    void deleteConversation(UUID conversationId, Map<UUID, String> hmacHashesByBoxAddress) {
        this.messages.deleteConversation(conversationId, hmacHashesByBoxAddress);
    }

    void deleteMessage(UUID boxAddress, UUID conversationId, UUID messageId, String hmacHash) {
        Optional<Message> message = messages.fetch(boxAddress, conversationId, messageId);
        if (!message.isPresent()) {
            LOGGER.warn("Attempt to delete message (boxaddress:" + boxAddress + ",conversationId:" + conversationId
                    + ",messageId:" + messageId + ") failed because it was not found in DB");
            return;
        }

        String storedHmacHash = message.get().getHmacHash();
        if (storedHmacHash != null && !storedHmacHash.equals(hmacHash)) {
            LOGGER.warn("Attempt to delete message (boxAddress:" + boxAddress + ",conversationId:" + conversationId
                    + ",messageId:" + messageId + ") failed because provided Hmac Hash is incorrect: " + hmacHash);
        }

        messages.delete(boxAddress, conversationId, messageId);
    }

    List<Message> fetchConversationMessages(UUID conversationId,
                                            Set<UUID> messageIds,
                                            Map<UUID, String> hmacHashesByBoxAddress) {
        return messages.fetchAllByMessagesIdsForConversationId(conversationId, messageIds).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .collect(Collectors.toList());
    }

    List<Message> fetchConversationMessages(UUID conversationId,
                                             Map<UUID, String> hmacHashesByBoxAddress,
                                             int page,
                                             int pageSize) {
        return messages.fetchAllForConversationId(conversationId, page, pageSize).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .collect(Collectors.toList());
    }

    Set<UUID> fetchConversationMessagesIds(UUID conversationId,
                                           Map<UUID, String> hmacHashesByBoxAddress,
                                           int page,
                                           int pageSize) {
        return messages.fetchAllForConversationId(conversationId, page, pageSize).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .map(Message::getMessageId)
                .collect(Collectors.toSet());
    }

    List<Message> fetchConversationsFromBoxAddresses(Map<UUID, String> hmacHashesByBoxAddress, int page, int pageSize) {
        Set<UUID> boxAddresses = hmacHashesByBoxAddress.keySet();
        return this.messages.fetchAllLatestByBoxAddressesUnreadFirst(boxAddresses, page, pageSize).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .collect(Collectors.toList());
    }

    // TODO Remove this after mobile app 0.18.3 is discontinued
    List<Message> fetchConversationsFromConversationIds(Set<UUID> conversationIds,
                                                        Map<UUID, String> hmacHashesByBoxAddress) {
        int pageSize = 100;
        return this.messages.fetchAllLatestByConversationIdsUnreadFirst(conversationIds, 0, pageSize).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .collect(Collectors.toList());
    }

    Set<UUID> fetchConversationsIds(Map<UUID, String> hmacHashesByBoxAddress, int page, int pageSize) {
        Set<UUID> boxAddresses = hmacHashesByBoxAddress.keySet();
        return this.messages.fetchAllLatestByBoxAddressesUnreadFirst(boxAddresses, page, pageSize).stream()
                .filter(hmacAuthorized(hmacHashesByBoxAddress))
                .map(Message::getConversationId)
                .collect(Collectors.toSet());
    }

    private Predicate<Message> hmacAuthorized(Map<UUID, String> hmacHashesByBoxAddress) {
        return message -> message.getHmacHash() == null
                || message.getHmacHash().equals(hmacHashesByBoxAddress.get(message.getBoxAddress()));
    }
}
