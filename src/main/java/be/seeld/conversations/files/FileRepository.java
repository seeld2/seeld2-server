package be.seeld.conversations.files;

import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.cache.query.SqlQuery;
import org.springframework.dao.support.DataAccessUtils;

import javax.cache.Cache;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static be.seeld.conversations.files.File.*;

public class FileRepository {

    private final IgniteCache<String, File> fileCache;

    public FileRepository(IgniteCache<String, File> fileCache) {
        this.fileCache = fileCache;
    }

    public void delete(UUID fileId) {
        //noinspection EmptyTryBlock,unused
        try (
                FieldsQueryCursor<List<?>> cursor = fileCache.query(new SqlFieldsQuery("" +
                        "delete from " + TABLE +
                        " where " + COL_FILE_ID + " = ?")
                        .setArgs(fileId))
        ) {
        }
    }

    public File findOne(UUID fileId) {
        SqlQuery<String, File> query = new SqlQuery<>(File.class, COL_FILE_ID + " = ?");
        query.setArgs(fileId);
        return DataAccessUtils.singleResult(select(query));
    }

    public File save(File file) {
        //noinspection unused
        try (
                FieldsQueryCursor<List<?>> cursor = fileCache.query(new SqlFieldsQuery("" +
                        "merge into " + TABLE +
                        " (_key, " + File.columnsAsCsv() + ")" +
                        " values (?, ?, ?, ?)")
                        .setArgs(file.cacheKey(),
                                file.getFileId(),
                                file.getData(),
                                file.getValidUntil()))
        ) {
            return file;
        }
    }

    private List<File> select(SqlQuery<String, File> query) {
        try (
                QueryCursor<Cache.Entry<String, File>> cursor = fileCache.query(query)
        ) {
            return cursor.getAll().stream()
                    .map(Cache.Entry::getValue)
                    .collect(Collectors.toList());
        }
    }
}
