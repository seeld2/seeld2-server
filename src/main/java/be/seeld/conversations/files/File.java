package be.seeld.conversations.files;

import be.seeld.common.time.Validity;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.UUID;

@Data
@Builder
public class File {

    public static final String TABLE = "FILE";
    public static final String COL_FILE_ID = "FILE_ID";
    public static final String COL_DATA = "DATA";
    public static final String COL_VALID_UNTIL = "VALID_UNTIL";

    @QuerySqlField(name = COL_FILE_ID, index = true, notNull = true)
    private UUID fileId;

    @QuerySqlField(name = COL_DATA, notNull = true)
    private byte[] data;

    @QuerySqlField(name = COL_VALID_UNTIL)
    private Long validUntil;

    public String cacheKey() {
        return fileId.toString();
    }

    public Validity validUntil() {
        return validUntil != null ? Validity.until(validUntil) : Validity.alwaysValid();
    }

    public static String columnsAsCsv() {
        return String.join(",",
                COL_FILE_ID,
                COL_DATA,
                COL_VALID_UNTIL);
    }
}
