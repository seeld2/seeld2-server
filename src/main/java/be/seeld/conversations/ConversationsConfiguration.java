package be.seeld.conversations;

import be.seeld.IgniteCoreConfiguration;
import be.seeld.common.time.Dates;
import be.seeld.conversations.messages.Message;
import be.seeld.conversations.messages.MessageRepository;
import be.seeld.conversations.messages.life.MessageLife;
import lombok.Data;
import org.apache.ignite.Ignite;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties({ConversationsConfiguration.ConversationsProperties.class})
public class ConversationsConfiguration {

    private static final String SQL_SCHEMA = "PUBLIC";

    private static final String FILE_CACHE = "FileCache";
    private static final String MESSAGE_CACHE = "MessageCache";
    private static final String MESSAGE_LIFE_CACHE = "MessageLifeCache";

    @Resource
    private Ignite ignite;
    @Resource
    private IgniteCoreConfiguration.IgniteProperties igniteProperties;

    /**
     * This method is where updates on the repository's caches can be defined.<br/>
     * The typical example would be the deletion of an old, unused table: this cannot be done by SQL because those
     * tables have been defined with Java, so they need to be destroyed with Java.
     */
    @PostConstruct
    void postConstruct() {
        // Keep on creating message life cache if missing. Even though we don't use message life in seeld2-server anymore, we still create it for seeld2-shredding to work
        CacheConfiguration<String, MessageLife> configuration = repositoryCacheConfiguration(
                MESSAGE_LIFE_CACHE, MessageLife.class);
        ignite.getOrCreateCache(configuration);
    }

    @Bean
    public Dates dates() {
        return new Dates();
    }

//    @Bean
//    public FileRepository fileRepository() {
//        CacheConfiguration<String, File> configuration = repositoryCacheConfiguration(FILE_CACHE, File.class);
//        return new FileRepository(ignite.getOrCreateCache(configuration));
//    }

    @Bean
    public MessageRepository messageRepository() {
        CacheConfiguration<String, Message> configuration = repositoryCacheConfiguration(
                MESSAGE_CACHE, Message.class);
        return new MessageRepository(ignite.getOrCreateCache(configuration));
    }

    private <T> CacheConfiguration<String, T> repositoryCacheConfiguration(String cacheName, Class<T> type) {
        return new CacheConfiguration<String, T>()
                .setSqlSchema(SQL_SCHEMA)
                .setName(cacheName)
                .setCacheMode(CacheMode.PARTITIONED)
                .setIndexedTypes(String.class, type)
                .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
                .setBackups(igniteProperties.getBackupsAmount())
                .setAffinity(new RendezvousAffinityFunction(true));
    }

    @ConfigurationProperties(prefix = "conversations")
    @Data
    public static final class ConversationsProperties {
        private int defaultFetchingPageSize;
        private int defaultIdsFetchingPageSize;
    }
}
