package be.seeld;

import lombok.Data;

@Data(staticConstructor = "build")
public final class PongResponse {
    private final long epoch = System.currentTimeMillis();
}
