package be.seeld;

import lombok.Data;

@Data(staticConstructor = "with")
public final class VersionsResponse {
    private final String mobileVersion;
}
