package be.seeld;

import be.seeld.parameters.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api")
public class WebApi {

    private final Parameters parameters;

    @Autowired
    public WebApi(Parameters parameters) {
        this.parameters = parameters;
    }

    @RequestMapping(method = GET)
    ResponseEntity<?> ping() {
        PongResponse response = PongResponse.build();
        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = GET, value = "/versions", produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> versions() {
        Optional<String> mobileVersion = parameters.getMobileVersion();
        VersionsResponse versionsResponse = VersionsResponse.with(mobileVersion.orElse(null));
        return ResponseEntity.ok(versionsResponse);
    }
}
