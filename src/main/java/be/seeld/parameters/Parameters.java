package be.seeld.parameters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static be.seeld.parameters.ParameterRepository.MOBILE_VERSION;
import static be.seeld.parameters.ParameterRepository.REGISTRATION_LIMIT;

@Service
public class Parameters {

    private final ParameterRepository parameterRepository;

    @Autowired
    public Parameters(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public Optional<String> getMobileVersion() {
        Parameter parameter = parameterRepository.findOne(MOBILE_VERSION);
        return parameter == null ? Optional.empty() : Optional.of(parameter.getValue());
    }

    public Optional<Long> getRegistrationLimit() {
        Parameter parameter = parameterRepository.findOne(REGISTRATION_LIMIT);
        return parameter == null ? Optional.empty() : Optional.of(Long.valueOf(parameter.getValue()));
    }
}
