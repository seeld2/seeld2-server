package be.seeld.parameters;

import be.seeld.common.ignite.Persistable;
import lombok.Builder;
import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

@Builder
@Data
public class Parameter implements Persistable {

    public static final String TABLE = "PARAMETER";
    public static final String COL_KEY = "KEY";
    public static final String COL_VALUE = "VALUE";

    public static String columnsAsCsv() {
        return String.join(",", COL_KEY, COL_VALUE);
    }

    @QuerySqlField(name = COL_KEY, index = true, notNull = true)
    private String key;

    @QuerySqlField(name = COL_VALUE, notNull = true)
    private String value;

    @Override
    public String cacheKey() {
        return key;
    }
}
