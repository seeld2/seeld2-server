package be.seeld.parameters;

import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;
import java.util.stream.Collectors;

public class ParameterRepository {

    public static final String MOBILE_VERSION = "MOBILE_VERSION";
    public static final String REGISTRATION_LIMIT = "REGISTRATION_LIMIT";

    private final IgniteCache<String, Parameter> parameterCache;

    public ParameterRepository(IgniteCache<String, Parameter> parameterCache) {
        this.parameterCache = parameterCache;
    }

    public Parameter findOne(String key) {

        SqlFieldsQuery query = new SqlFieldsQuery("SELECT " + Parameter.columnsAsCsv() + " FROM " + Parameter.TABLE +
                " WHERE " + Parameter.COL_KEY + " = ?")
                .setArgs(key);

        try (FieldsQueryCursor<List<?>> cursor = parameterCache.query(query)) {
            return DataAccessUtils.singleResult(map(cursor.getAll()));
        }
    }

    public Parameter save(Parameter parameter) {

        SqlFieldsQuery query = new SqlFieldsQuery("MERGE INTO " + Parameter.TABLE +
                " (_key, " + Parameter.columnsAsCsv() + ") VALUES (?, ?, ?)")
                .setArgs(parameter.cacheKey(), parameter.getKey(), parameter.getValue());

        //noinspection unused
        try (FieldsQueryCursor<List<?>> cursor = parameterCache.query(query)) {
            return parameter;
        }
    }

    private List<Parameter> map(List<List<?>> rows) {
        return rows.stream()
                .map(row -> Parameter.builder()
                        .key((String) row.get(0))
                        .value((String) row.get(1))
                        .build())
                .collect(Collectors.toList());
    }
}
