package be.seeld.profiles;

import be.seeld.common.encryption.aes.AES;
import be.seeld.users.User;
import be.seeld.users.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
class ProfilesProcess {

    private final AES aes;
    private final Users users;

    @Autowired
    public ProfilesProcess(@Qualifier("pwaAesBase64") AES aes, Users users) {
        this.aes = aes;
        this.users = users;
    }

    void deleteProfile(String loggedPseudo, String aesPseudo) {
        User loggedUser = users.fetchUnencrypted(loggedPseudo);
        verifyLoggedPseudoThroughAesPseudo(loggedUser, aesPseudo, "delete the profile");
        users.delete(loggedPseudo);
    }

    String fetchPayload(String loggedPseudo, String aesPseudo) {
        User user = users.fetchUnencrypted(loggedPseudo);
        verifyLoggedPseudoThroughAesPseudo(user, aesPseudo, "fetch the payload");
        return user.getPayload();
    }

    Profile fetchProfile(String pseudo) {
        User user = users.fetchUnencrypted(pseudo);
        try {
            String profiledUserPublicKeys = User.Credentials
                    .fromJSON(user.getCredentials(), User.Credentials.class)
                    .getSystemKeys()
                    .getPublicKeys();

            return new Profile(user.getBoxAddress(), profiledUserPublicKeys, pseudo);
        } catch (IOException e) {
            throw new ProfilesProcessException("Could not build JSON to be encrypted", e);
        }
    }

    void updatePayload(String loggedPseudo, String aesPseudo, String pgpEncryptedPayload) {
        User loggedUser = users.fetchUnencrypted(loggedPseudo);
        verifyLoggedPseudoThroughAesPseudo(loggedUser, aesPseudo, "update the profile");
        users.updatePayload(loggedUser, pgpEncryptedPayload);
    }

    private void verifyLoggedPseudoThroughAesPseudo(User loggedUser, String aesPseudo, String actionName) {
        ProfilesProcessException validationThroughAesPseudoException = new ProfilesProcessException("Logged "
                + loggedUser.getUsername() + " attempted to " + actionName + " of another person !");
        try {
            String unencryptedPseudo = aes.decryptBase64(aesPseudo, loggedUser.getExchangeKey());
            if (!loggedUser.getUsername().equals(unencryptedPseudo)) {
                throw validationThroughAesPseudoException;
            }
        } catch (Exception e) {
            throw validationThroughAesPseudoException;
        }
    }

    static class ProfilesProcessException extends RuntimeException {

        ProfilesProcessException(String message) {
            super(message);
        }

        ProfilesProcessException(String message, Exception e) {
            super(message, e);
        }
    }
}
