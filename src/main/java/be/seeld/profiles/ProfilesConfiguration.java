package be.seeld.profiles;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({ProfilesConfiguration.ProfilesProperties.class})
public class ProfilesConfiguration {

    @ConfigurationProperties(prefix = "profiles")
    @Data
    public static final class ProfilesProperties {
        private int detailsResponseDelay;
    }
}
