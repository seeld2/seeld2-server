package be.seeld.profiles;

import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Profile extends JSONifiable {

    private String boxAddress;
    private String systemPublicKeys;
    private String username;
}


