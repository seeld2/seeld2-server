package be.seeld.profiles;

import be.seeld.common.json.JSONifiable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Set;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BoxAddresses extends JSONifiable {

    @NotEmpty
    private Set<UUID> boxAddresses;
}
