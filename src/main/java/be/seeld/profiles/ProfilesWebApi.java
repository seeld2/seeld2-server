package be.seeld.profiles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.concurrent.TimeUnit;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/profiles")
public class ProfilesWebApi {

    private final int detailsResponseDelay;
    private final ProfilesProcess profilesProcess;

    @Autowired
    public ProfilesWebApi(ProfilesProcess profilesProcess,
                          @Value("${profiles.detailsResponseDelay}") int detailsResponseDelay) {
        this.detailsResponseDelay = detailsResponseDelay;
        this.profilesProcess = profilesProcess;
    }

    /**
     * Deletes the logged user's profile.
     *
     * @param aesPseudo The pseudo whose whose profile must be deleted, AES-encrypted using the logged user's exchange key
     */
    @RequestMapping(value = "/{aesPseudo}/state/deleted", method = DELETE)
    ResponseEntity<?> deleteProfile(@PathVariable String aesPseudo, Principal principal) {
        profilesProcess.deleteProfile(principal.getName(), aesPseudo);
        return ResponseEntity.noContent().build();
    }

    /**
     * Fetches the profile matching the specified pseudo.<br>
     * Since we only provide the pseudo, system box address and system public keys of the specified pseudo,
     * and since those are meant to be "public", we leave this endpoint unauthenticated so we (the system) don't
     * identify who's fetching the details of a given user.
     * To further elaborate: hiding the returned data behind authentication would not help. A malevolent entity could
     * create an armada of valid profiles and use something like Selenium to try all the possible combinations and
     * harvest pseudos...
     */
    @RequestMapping(value = "/{pseudo}", method = GET, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> fetchDetails(@PathVariable String pseudo) {
        try {
            TimeUnit.SECONDS.sleep(detailsResponseDelay);
        } catch (InterruptedException ie) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        Profile profile = profilesProcess.fetchProfile(pseudo);
        return ResponseEntity.ok(new DetailsResponse(
                profile.getBoxAddress(),
                profile.getSystemPublicKeys(),
                profile.getUsername()));
    }

    @RequestMapping(value = "/{aesPseudo}/payload", method = GET, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<?> fetchOwnPayload(@PathVariable String aesPseudo, Principal principal) {
        String payload = profilesProcess.fetchPayload(principal.getName(), aesPseudo);
        return ResponseEntity.ok(new PayloadResponse(payload));
    }

    /**
     * Updates the specified profile.
     *
     * @param payloadRequest The object containing PGP-encrypted payload to store
     */
    @RequestMapping(value = "/{aesPseudo}/payload", method = PUT, consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<?> updatePayload(@PathVariable String aesPseudo,
                                    @RequestBody PayloadRequest payloadRequest,
                                    Principal principal) {
        profilesProcess.updatePayload(principal.getName(), aesPseudo, payloadRequest.getPayload());
        return ResponseEntity.status(CREATED).build();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class DetailsResponse {
        private String boxAddress;
        private String systemPublicKeys;
        private String username;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class PayloadRequest {
        private String payload;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class PayloadResponse {
        private String payload;
    }
}
