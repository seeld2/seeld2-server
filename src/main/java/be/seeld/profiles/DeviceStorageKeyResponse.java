package be.seeld.profiles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class DeviceStorageKeyResponse {
    private String deviceStorageKey;
}
